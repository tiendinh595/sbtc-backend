/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost
 Source Database       : sieuthibtc

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : utf-8

 Date: 01/09/2018 09:05:20 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `failed_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `jobs`
-- ----------------------------
BEGIN;
INSERT INTO `jobs` VALUES ('4', 'send_mail_active', '{\"displayName\":\"App\\\\Jobs\\\\SendMailActive\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMailActive\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\SendMailActive\\\":6:{s:29:\\\"\\u0000App\\\\Jobs\\\\SendMailActive\\u0000user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":3:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:31;s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";s:8:\\\"database\\\";s:5:\\\"queue\\\";s:16:\\\"send_mail_active\\\";s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', '0', null, '1515402006', '1515402006'), ('5', 'send_mail_active', '{\"displayName\":\"App\\\\Jobs\\\\SendMailActive\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMailActive\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\SendMailActive\\\":6:{s:29:\\\"\\u0000App\\\\Jobs\\\\SendMailActive\\u0000user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":3:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:32;s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";s:8:\\\"database\\\";s:5:\\\"queue\\\";s:16:\\\"send_mail_active\\\";s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', '0', null, '1515402750', '1515402750'), ('6', 'send_mail_active', '{\"displayName\":\"App\\\\Jobs\\\\SendMailActive\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMailActive\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\SendMailActive\\\":6:{s:29:\\\"\\u0000App\\\\Jobs\\\\SendMailActive\\u0000user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":3:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:33;s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";s:8:\\\"database\\\";s:5:\\\"queue\\\";s:16:\\\"send_mail_active\\\";s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', '0', null, '1515403225', '1515403225');
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1'), ('3', '2017_09_22_101717_create_permission_tables', '1'), ('4', '2017_12_28_095349_create_jobs_table', '2'), ('5', '2017_12_28_095407_create_failed_jobs_table', '2');
COMMIT;

-- ----------------------------
--  Table structure for `model_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `model_has_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `model_has_permissions` VALUES ('1', '1', 'App\\User'), ('2', '1', 'App\\User'), ('3', '1', 'App\\User'), ('4', '1', 'App\\User'), ('5', '1', 'App\\User'), ('6', '1', 'App\\User'), ('7', '1', 'App\\User'), ('8', '1', 'App\\User'), ('9', '1', 'App\\User'), ('10', '1', 'App\\User');
COMMIT;

-- ----------------------------
--  Table structure for `model_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('1', 'list permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19'), ('2', 'view permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19'), ('3', 'add permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19'), ('4', 'edit permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19'), ('5', 'delete permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19'), ('6', 'list administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27'), ('7', 'view administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27'), ('8', 'add administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27'), ('9', 'edit administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27'), ('10', 'delete administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
COMMIT;

-- ----------------------------
--  Table structure for `role_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `tbl_admin`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_admin` VALUES ('1', 'Trịnh Tâm', 'contact.tamsoft@gmail.com', '$2y$10$tB3b2d1w16iWs4gKTsAtjud7rpZMpPGBnDGnPePJcuYSJTGDz/rKO', 'pxHilaYTS52iPDibwijQcN66NBVSoOev5jLsDsktXGiXhawbNdTlUzVKtdoG', '1', '2017-09-22 10:36:35', '2017-09-22 10:42:43');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_bank`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bank`;
CREATE TABLE `tbl_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bank_code` tinyint(1) DEFAULT NULL,
  `bank_number` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1: active, 2: inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_bank`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_bank` VALUES ('2', '14', '0', '092329392u4', 'vu tien dinh', '1', '2018-01-05 16:57:19', '2018-01-05 16:57:19'), ('3', '14', '2', '23928323', 'vu tien dinh', '1', '2018-01-05 17:17:48', '2018-01-05 17:17:48');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_deal`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deal`;
CREATE TABLE `tbl_deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `owner_offer` varchar(100) DEFAULT NULL,
  `offer_id` int(11) NOT NULL,
  `trade_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `rate_vnd_usd` double NOT NULL,
  `rate_usd_coin` double DEFAULT NULL,
  `total_money` double DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: sell, 2: buy',
  `status` tinyint(4) DEFAULT '1' COMMENT '1:waiting, 2:approved,3:reject',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_deal`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_deal` VALUES ('1', 'leo', 'dinhvt', '10', null, '1', '15000', '14584.99', '15000', '1', '2', '2017-12-22 10:43:40', '2018-01-02 16:03:01'), ('2', 'dinhvt', 'leo', '2', null, '0.5', '22', '14584.99', '11', '1', '3', '2017-12-22 10:46:31', '2018-01-02 16:08:10'), ('3', 'dinhvt', 'leo', '2', null, '0.5', '1', '14584.99', '1', '1', '3', '2017-12-22 10:48:24', '2018-01-02 16:09:50'), ('4', 'dinhvt', 'leo', '10', null, '1', '10000', '14584.99', '10000', '1', '1', '2017-12-28 11:38:44', '2018-01-02 14:51:50'), ('5', 'dinhvt', 'leo', '2', null, '0.5', '0', '14584.99', '11', '1', '1', '2018-01-02 11:26:48', '2018-01-02 14:56:44'), ('6', 'dinhvt', 'leo', '2', null, '0.8', '21999', '14584.99', '256684156', '1', '1', '2018-01-02 11:36:25', '2018-01-02 14:56:45'), ('7', 'dinhvt', 'leo', '2', null, '0.5', '21', '14584.99', '153142', '1', '1', '2018-01-02 15:36:02', '2018-01-02 15:36:02');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_feedback`;
CREATE TABLE `tbl_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `reporter` varchar(255) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `rating` tinyint(4) unsigned DEFAULT '0',
  `trade_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`,`trade_id`),
  UNIQUE KEY `reporter` (`reporter`,`trade_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_feedback`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_feedback` VALUES ('1', 'leo', 'dinhvt', 'ok man', '3', '33', null, null), ('2', 'dinhvt', 'supper', 'good job', '2', null, null, null), ('5', 'dinhvt', 'leo', 'giao dich nhanh', '5', '33', '2017-12-20 14:35:30', '2017-12-20 14:35:30'), ('6', 'leo', 'dinhvt', 'giao dịch nhanh', '5', '34', '2017-12-20 14:40:29', '2017-12-20 14:40:29'), ('7', 'dinhvt', 'leo', 'nhanh gọn', '5', '34', '2017-12-20 14:40:40', '2017-12-20 14:40:40'), ('8', 'leo', 'dinhvt', 'nhanh', '5', '35', '2017-12-20 14:45:02', '2017-12-20 14:45:02'), ('9', 'dinhvt', 'leo', 'nhanh', '5', '38', '2017-12-20 17:13:39', '2017-12-20 17:13:39'), ('10', 'leo', 'dinhvt', 'giao dijch nhanh', '5', '38', '2017-12-20 17:13:53', '2017-12-20 17:13:53'), ('11', 'leo', 'dinhvt', 'ok d', '5', '39', '2017-12-20 17:16:47', '2017-12-20 17:16:47'), ('12', 'dinhvt', 'leo', 'uy tin', '2', '42', '2017-12-20 18:46:31', '2017-12-20 18:46:31'), ('13', 'dinhvt', 'leo', 'giao dich nhanh', '3', '44', '2017-12-21 11:27:07', '2017-12-21 11:27:07'), ('14', 'leo', 'dinhvt', 'ok man', '4', '44', '2017-12-21 11:27:18', '2017-12-21 11:27:18'), ('15', 'dinhvt', 'leo', 'nhanh', '3', '45', '2017-12-21 17:31:22', '2017-12-21 17:31:22'), ('16', 'leo', 'dinhvt', 'oki man', '2', '45', '2017-12-21 17:31:29', '2017-12-21 17:31:29');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_history`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_history`;
CREATE TABLE `tbl_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `type_money` tinyint(1) DEFAULT NULL COMMENT '1: btc, 2: eth, 3: vnd',
  `amount` double DEFAULT NULL,
  `payment_memo` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1: desposit, 2: withdraw, 3: buy, 4: sell',
  `status` tinyint(1) DEFAULT NULL COMMENT '1: pending, 2:success, 3: reject, 4: cancel, 5: error',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_history`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_history` VALUES ('5', '13', '16', '1', '0.002', 'rut btc', '2', '1', '2018-01-05 13:50:10', '2018-01-05 13:50:10'), ('6', '13', '17', '2', '0.04', 'rut eth', '2', '1', '2018-01-05 13:56:30', '2018-01-05 13:56:30'), ('7', '13', '18', '2', '0.0001', 'rut eth', '2', '1', '2018-01-05 13:57:01', '2018-01-05 13:57:01'), ('8', '13', '18', '3', '1000000', 'rutvnd123', '2', '1', '2018-01-05 13:57:01', '2018-01-05 13:57:01'), ('9', '13', '19', '1', '0.003', 'Rút Bitcoin', '2', '1', '2018-01-05 17:29:34', '2018-01-05 17:29:34'), ('10', '13', '20', '1', '0.1', 'Rút Bitcoin', '2', '1', '2018-01-05 17:33:10', '2018-01-05 17:33:10'), ('11', '13', '21', '3', '500000', 'RUT_VND21', '2', '1', '2018-01-05 17:34:13', '2018-01-05 17:34:13'), ('12', '13', '22', '3', '1000000', 'RUT_VND22', '2', '1', '2018-01-05 17:42:11', '2018-01-05 17:42:11'), ('13', '13', '23', '3', '1000000', 'RUT_VND23', '2', '1', '2018-01-05 17:43:12', '2018-01-05 17:43:12'), ('14', '13', '24', '3', '1000000', 'RUT_VND24', '2', '1', '2018-01-05 17:44:26', '2018-01-05 17:44:26'), ('15', '13', '25', '3', '1234', 'RUT_VND25', '2', '1', '2018-01-06 09:16:50', '2018-01-06 09:16:50'), ('16', '13', '26', '3', '1', 'NAP_VND26', '1', '0', '2018-01-06 10:39:06', '2018-01-06 10:39:06'), ('17', '13', '27', '3', '1', 'NAP_VND27', '1', '0', '2018-01-06 10:39:44', '2018-01-06 10:39:44'), ('18', '13', '28', '3', '1', 'NAP_VND28', '1', '0', '2018-01-06 10:41:43', '2018-01-06 10:41:43'), ('19', '13', '29', '3', '1', 'NAP_VND29', '1', '0', '2018-01-06 10:42:07', '2018-01-06 10:42:07'), ('20', '13', '30', '3', '100000', 'NAP_VND30', '1', '0', '2018-01-06 10:42:16', '2018-01-06 10:42:16'), ('21', '13', '31', '3', '3000', 'NAP_VND31', '1', '0', '2018-01-06 10:44:37', '2018-01-06 10:44:37'), ('22', '13', '32', '3', '122222', 'NAP_VND32', '1', '0', '2018-01-06 10:45:11', '2018-01-06 10:45:11'), ('23', '13', '33', '3', '100000', 'NAP_VND33', '1', '0', '2018-01-06 10:45:41', '2018-01-06 10:45:41'), ('24', '13', '34', '3', '1', 'NAP_VND34', '1', '0', '2018-01-06 10:59:44', '2018-01-06 10:59:44'), ('25', '13', '35', '3', '1', 'NAP_VND35', '1', '0', '2018-01-06 11:00:32', '2018-01-06 11:00:32'), ('26', '13', '36', '3', '1', 'NAP_VND36', '1', '0', '2018-01-06 11:05:01', '2018-01-06 11:05:01'), ('27', '13', '37', '3', '1', 'NAP_VND37', '1', '0', '2018-01-06 11:07:55', '2018-01-06 11:07:55'), ('28', '13', '38', '3', '1', 'NAP_VND38', '1', '0', '2018-01-06 11:08:34', '2018-01-06 11:08:34'), ('29', '13', '39', '3', '1', 'NAP_VND39', '1', '0', '2018-01-06 11:57:15', '2018-01-06 11:57:15'), ('30', '13', '40', '3', '1', 'NAP_VND40', '1', '0', '2018-01-06 11:58:02', '2018-01-06 11:58:02'), ('31', '13', '41', '3', '1', 'NAP_VND41', '1', '1', '2018-01-06 12:03:48', '2018-01-06 12:03:51'), ('32', '13', '42', '3', '1', 'NAP_VND42', '1', '1', '2018-01-06 12:05:17', '2018-01-06 12:05:20'), ('33', '13', '43', '3', '1000000', 'NAP_VND43', '1', '1', '2018-01-06 12:06:19', '2018-01-06 12:06:23'), ('34', '13', '44', '3', '2000000', 'NAP_VND44', '1', '4', '2018-01-06 12:06:38', '2018-01-06 12:06:40'), ('35', '13', '45', '3', '3000000', 'NAP_VND45', '1', '1', '2018-01-06 12:07:17', '2018-01-06 12:07:18'), ('36', '13', '46', '3', '177700', 'NAP_VND46', '1', '0', '2018-01-06 12:09:34', '2018-01-06 12:09:34'), ('37', '13', '47', '3', '1', 'NAP_VND47', '1', '0', '2018-01-06 12:20:09', '2018-01-06 12:20:09'), ('38', '13', '48', '3', '1', 'NAP_VND48', '1', '0', '2018-01-06 12:22:14', '2018-01-06 12:22:14'), ('39', '13', '49', '3', '1', 'NAP_VND49', '1', '0', '2018-01-06 12:24:04', '2018-01-06 12:24:17'), ('40', '13', '50', '3', '121', 'NAP_VND50', '1', '0', '2018-01-06 12:54:40', '2018-01-06 12:54:48'), ('41', '13', '51', '3', '100000', 'NAP_VND51', '1', '1', '2018-01-06 12:56:25', '2018-01-06 12:56:41'), ('42', '13', '52', '3', '10332', 'NAP_VND52', '1', '1', '2018-01-06 12:57:10', '2018-01-06 12:57:20');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_mail`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mail`;
CREATE TABLE `tbl_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: in, 2: out',
  `status` tinyint(1) DEFAULT '0' COMMENT '0:new, 1:seen',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_mail`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_mail` VALUES ('1', '13', '1', 'thu he thong', 'this is content', '1', '1', null, '2017-12-29 11:26:32', '2017-12-29 15:01:20'), ('2', '13', '1', 'giao dich thanh cong #123', 'giao dich thanh cong #123', '1', '1', null, '2017-12-29 11:48:26', '2017-12-29 15:01:20'), ('3', null, null, 'Phản hồi từ dinhvt', 'ok main', '2', '0', null, '2017-12-29 15:47:11', '2017-12-29 15:47:11'), ('4', '13', null, 'Phản hồi từ dinhvt', 'ok main', '2', '0', null, '2017-12-29 15:48:23', '2017-12-29 15:48:23'), ('5', '13', null, 'Phản hồi từ dinhvt', 'this is content', '2', '0', null, '2017-12-29 15:49:41', '2017-12-29 15:49:41');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_offer`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_offer`;
CREATE TABLE `tbl_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `address_wallet` varchar(100) DEFAULT NULL,
  `rate_vnd_usd` double NOT NULL DEFAULT '0',
  `rate_usd_coin` double NOT NULL,
  `price_per_coin` double NOT NULL,
  `price_per_coin_before_fee` double NOT NULL,
  `type_offer` tinyint(1) NOT NULL COMMENT '1: sell, 2: buy',
  `type_money` tinyint(1) NOT NULL COMMENT '1: btc, 2: eth',
  `max_amount` double NOT NULL,
  `min_amount` double NOT NULL,
  `current_amount` double DEFAULT NULL,
  `payment_time` tinyint(4) NOT NULL,
  `payment_method` tinyint(1) NOT NULL COMMENT '1: chuyen khoan, 2 nop tien mat',
  `bank_code` tinyint(1) DEFAULT NULL,
  `bank_number` varchar(50) DEFAULT NULL,
  `bank_account_name` varchar(100) DEFAULT NULL,
  `outgoing` tinyint(1) DEFAULT '0',
  `token` text,
  `allow_not_verify` tinyint(4) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL COMMENT '1: starting, 2: done, 3: reject',
  `deal_id` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_offer`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_offer` VALUES ('1', 'leo', null, '22750', '14584.99', '328490437', '331922272', '1', '2', '2', '0.01', '0', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-20 17:19:18'), ('2', 'leo', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '0.97', '15', '1', '0', null, null, '1', null, '0', '1', null, '2017-12-07 16:33:22', '2018-01-03 13:47:53'), ('3', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '0.99', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-25 16:23:17'), ('4', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '2', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-07 16:33:25'), ('5', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '1.99', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-20 21:58:21'), ('6', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '1.99', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2018-01-03 13:35:36'), ('7', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '2', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-07 16:33:25'), ('8', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '1', '1', '2', '0.01', '2', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-07 16:33:25'), ('9', 'dinhvt', null, '22750', '14584.99', '328490437', '331922272', '2', '2', '2', '0.01', '2', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-07 16:33:22', '2017-12-07 16:33:25'), ('10', 'leo', 'xxx', '22750', '14584.99', '328490437', '331922272', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', null, '2017-12-18 16:33:22', '2017-12-25 18:02:51'), ('12', 'dinhvt', '98989323', '22', '675.43', '15130', '15130', '2', '2', '5', '0.1', '5', '15', '1', '0', null, null, '1', null, '5', '1', null, '2017-12-25 15:42:39', '2017-12-25 15:42:39'), ('13', 'dinhvt', '98989323', '22.4', '675.43', '15129.632', '15129.632', '2', '2', '5', '0.1', '5', '15', '1', '0', null, null, '1', null, '5', '1', null, '2017-12-25 15:48:39', '2017-12-25 15:48:39'), ('14', 'dinhvt', 'edada', '23.5', '15987.83', '375714.005', '375714.005', '2', '1', '3', '1', '2', '15', '1', '0', null, null, '1', null, '3', '1', null, '2017-12-25 15:50:19', '2017-12-25 16:40:39'), ('15', 'dinhvt', 'edada', '23.5', '15987.83', '375714.005', '375714.005', '2', '1', '3', '1', '3', '15', '1', '0', null, null, '1', null, '3', '1', null, '2017-12-25 15:50:29', '2017-12-25 15:50:29'), ('16', 'dinhvt', '121212', '24.56', '15987.83', '392661.105', '392661.105', '2', '1', '3', '0.01', '2.99', '30', '1', '0', null, null, '1', null, '3', '1', null, '2017-12-25 15:51:05', '2017-12-25 16:43:10'), ('17', 'dinhvt', '121212', '24.56', '15987.83', '392661.105', '392661.105', '2', '1', '3', '0.01', '3', '30', '1', '0', null, null, '1', null, '3', '1', null, '2017-12-25 15:52:20', '2017-12-25 15:52:20'), ('18', 'dinhvt', '121212', '24.56', '15987.83', '392661.105', '392661.105', '2', '1', '3', '0.01', '2.99', '30', '1', '0', null, null, '1', null, '1', '1', null, '2017-12-25 15:53:32', '2017-12-25 16:41:38'), ('19', 'dinhvt', '121212', '24.56', '15987.83', '392661.105', '392661.105', '2', '1', '3', '0.01', '3', '30', '1', '0', null, null, '1', null, '0', '1', null, '2017-12-25 15:53:47', '2017-12-25 15:53:47'), ('20', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '24.56', '15987.83', '392661.105', '392661.105', '2', '1', '3', '0.01', '3', '30', '1', '0', null, null, '0', null, '1', '1', null, '2017-12-25 15:56:19', '2017-12-25 15:56:19'), ('21', 'dinhvt', 'okmain', '23.45', '15987.83', '374914.614', '374914.614', '2', '1', '1', '0.01', '1', '15', '1', '0', null, null, '1', null, '0', '1', null, '2017-12-25 15:59:08', '2017-12-25 15:59:08'), ('22', 'dinhvt', 'dadada', '23.45', '15987.83', '374914.614', '374914.614', '2', '1', '3', '1', '3', '15', '1', '0', null, null, '1', null, '0', '1', null, '2017-12-25 16:02:16', '2017-12-25 16:02:16'), ('23', 'dinhvt', 'okokok', '23.4', '15987.83', '374115.222', '374115.222', '2', '1', '3', '1', '3', '15', '1', '0', null, null, '1', null, '0', '1', null, '2017-12-25 16:05:24', '2017-12-25 16:05:24'), ('24', 'dinhvt', null, '23000', '15987.83', '367720090', '367720090', '1', '1', '1', '0.01', '1', '15', '1', '0', '98346723263723', 'DANG HUYNH KIM LONG', '1', null, '1', '1', null, '2017-12-25 16:12:47', '2017-12-25 16:12:47'), ('25', 'dinhvt', null, '12', '15987.83', '191854', '191854', '1', '1', '1', '1', '1', '15', '1', '0', '091212', 'DANG HUYNH KIM LONG', '1', null, '0', '1', null, '2017-12-25 16:30:56', '2017-12-25 16:30:56'), ('26', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '11', '15987.83', '175866', '175866', '2', '1', '1', '0.001', '0.799', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-25 16:45:52', '2017-12-25 16:51:57'), ('27', 'leo', 'X1cdcb4t9216bcff1df9cdadeb1ccf730b66720', '12', '675.43', '8105', '8105', '2', '2', '3', '0.1', '2.9', '15', '1', '0', null, null, '0', null, '0', '1', null, '2017-12-25 16:54:16', '2017-12-25 16:55:49'), ('28', 'leo', 'xxx', '15000', '0', '328490437', '331922272', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:48:14', '2018-01-02 15:48:14'), ('29', 'leo', 'xxx', '15000', '0', '328490437', '331922272', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:49:05', '2018-01-02 15:49:05'), ('30', 'leo', 'xxx', '15000', '0', '0', '0', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:51:24', '2018-01-02 15:51:24'), ('31', 'leo', 'xxx', '15000', '14584.99', '218774850', '218774850', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:52:41', '2018-01-02 15:52:41'), ('32', 'leo', 'xxx', '15000', '14584.99', '218774850', '218774850', '2', '1', '2', '0.01', '1.98', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:56:24', '2018-01-02 15:56:24'), ('33', 'leo', 'xxx', '15000', '14584.99', '218774850', '218774850', '2', '1', '1', '1', '1', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 15:57:49', '2018-01-02 15:57:49'), ('34', 'leo', 'xxx', '15000', '14584.99', '218774850', '218774850', '2', '1', '1', '1', '1', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 16:00:40', '2018-01-02 16:00:40'), ('35', 'leo', 'xxx', '15000', '14584.99', '218774850', '218774850', '2', '1', '1', '1', '1', '15', '1', '1', null, null, '0', null, '0', '1', '1', '2018-01-02 16:03:01', '2018-01-02 16:03:01');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_profile_extra`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_profile_extra`;
CREATE TABLE `tbl_profile_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img1` varchar(255) DEFAULT NULL,
  `img2` varchar(255) DEFAULT NULL,
  `img3` varchar(255) DEFAULT NULL,
  `img4` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_profile_extra`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_profile_extra` VALUES ('1', '28', 'dddd', '2018/01/03/1514966616.png', '2018/01/03/1514967779.jpeg', '2018/01/03/1514967826.jpeg', '2018-01-03 14:50:11', '2018-01-03 15:23:46'), ('2', '13', '2018/01/04/1515068314.jpeg', '2018/01/05/1515089242.png', '2018/01/05/1515089248.jpeg', '2018/01/05/1515089253.jpeg', '2018-01-04 14:57:42', '2018-01-05 01:07:33');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_referral`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_referral`;
CREATE TABLE `tbl_referral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `ref_username` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0: unavailable, 1: available, 2: receive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_referral`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_referral` VALUES ('1', 'leo', 'dinhvt', '0', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `tbl_system`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_system`;
CREATE TABLE `tbl_system` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_system`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_system` VALUES ('price_live_buy_btc_usd', '15987.83'), ('price_live_buy_btc_vnd', '360162825'), ('price_live_buy_eth_usd', '675.43'), ('price_live_buy_eth_vnd', '360162825'), ('price_live_sell_btc_usd', '14987.98'), ('price_live_sell_btc_vnd', '360162825'), ('price_live_sell_eth_usd', '654.32'), ('price_live_sell_eth_vnd', '360162825'), ('price_max_buy_btc_usd', '17987.83'), ('price_max_buy_btc_vnd', '360162825'), ('price_max_buy_eth_usd', '675.43'), ('price_max_buy_eth_vnd', '360162825'), ('price_max_sell_btc_usd', '14987.98'), ('price_max_sell_btc_vnd', '360162825'), ('price_max_sell_eth_usd', '654.32'), ('price_max_sell_eth_vnd', '360162825'), ('price_min_buy_btc_usd', '15987.83'), ('price_min_buy_btc_vnd', '360162825'), ('price_min_buy_eth_usd', '675.43'), ('price_min_buy_eth_vnd', '360162825'), ('price_min_sell_btc_usd', '14987.98'), ('price_min_sell_btc_vnd', '360162825'), ('price_min_sell_eth_usd', '654.32'), ('price_min_sell_eth_vnd', '360162825');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_trade`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trade`;
CREATE TABLE `tbl_trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_username` varchar(100) DEFAULT NULL,
  `seller_username` varchar(100) DEFAULT NULL,
  `from_address` varchar(100) DEFAULT NULL,
  `to_address` varchar(100) DEFAULT NULL,
  `offer_id` int(11) NOT NULL,
  `type_trade` tinyint(4) DEFAULT NULL COMMENT '1: sell, 2: buy',
  `type_money` tinyint(4) DEFAULT NULL COMMENT '1: btc, 2: eth',
  `seller_sending_coin_amount` double DEFAULT NULL,
  `buyer_receiving_coin_amount` double DEFAULT NULL,
  `seller_receiving_vnd_amount` double DEFAULT NULL COMMENT 'amount btc, eth to trade',
  `buyer_sending_vnd_amount` double DEFAULT NULL,
  `fee` double DEFAULT NULL COMMENT 'total fee by ',
  `payment_method` tinyint(1) DEFAULT NULL COMMENT '1: chuyen khoan, 2 nop tien mat',
  `bank_code` tinyint(1) DEFAULT NULL,
  `bank_number` varchar(50) DEFAULT NULL,
  `bank_account_name` varchar(100) DEFAULT NULL,
  `payment_time` datetime DEFAULT NULL,
  `payment_memo` varchar(100) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `token` text,
  `action_buyer` tinyint(4) DEFAULT '0' COMMENT '1: delivered, 2:cancel',
  `action_seller` tinyint(4) DEFAULT '0' COMMENT '1: received, 2: not received',
  `status` tinyint(1) DEFAULT NULL COMMENT '1: pending, 2: done, 3: user_cancel, 4: bot_cancel',
  `time_up` tinyint(1) DEFAULT '0',
  `outgoing` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_trade`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_trade` VALUES ('6', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 09:27:46', '2017-12-18 09:27:46'), ('7', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 09:49:12', '2017-12-18 09:49:12'), ('8', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 09:49:38', '2017-12-18 09:49:38'), ('9', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:04:40', '2017-12-18 10:04:40'), ('10', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:13:14', '2017-12-18 10:13:14'), ('11', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:40:23', '2017-12-18 10:40:23'), ('12', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:47:55', '2017-12-18 10:47:55'), ('13', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.05', '0.0495', '16424522', '16424522', '0.0005', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:48:59', '2017-12-18 10:48:59'), ('14', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '1.94', '1.9206', '637271448', '637271448', '0.0194', '1', '1', '091212', '0909090909', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:50:43', '2017-12-18 10:50:43'), ('15', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '2', '1.98', '656980874', '656980874', '0.02', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:54:48', '2017-12-18 10:54:48'), ('16', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '2', '1.98', '656980874', '656980874', '0.02', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 10:59:02', '2017-12-18 10:59:02'), ('17', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 11:03:25', '2017-12-18 11:03:25'), ('18', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '1', '0.99', '328490437', '328490437', '0.01', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 11:06:20', '2017-12-18 11:06:20'), ('22', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.99', '0.9801', '325205533', '325205533', '0.0099', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 11:13:12', '2017-12-18 11:13:12'), ('23', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 13:33:20', '2017-12-18 13:33:20'), ('24', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 13:33:38', '2017-12-18 13:33:38'), ('25', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '000000', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 13:35:17', '2017-12-18 13:35:17'), ('26', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, null, null, '', '0', '0', '1', null, '0', '2017-12-18 13:38:51', '2017-12-18 13:38:51'), ('27', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-19 18:30:00', null, '1.png', '', '1', '1', '2', '1', '0', '2017-12-18 13:39:16', '2017-12-19 17:47:43'), ('28', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '000000', 'DANG HUYNH KIM LONG', null, 'STBTC 28', null, '', '0', '0', '4', '1', '0', '2017-12-19 18:31:27', '2017-12-19 18:31:27'), ('29', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', null, 'STBTC 29', null, '', '0', '0', '4', '1', '0', '2017-12-19 18:31:50', '2017-12-19 18:31:51'), ('32', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-20 09:09:15', 'STBTC 32', null, '', '2', '0', '3', '0', '0', '2017-12-20 08:54:15', '2017-12-20 08:59:37'), ('33', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-20 12:15:05', 'STBTC 33', '2017/12/20/1513742557.png', '', '0', '1', '2', '0', '0', '2017-12-20 09:00:05', '2017-12-20 11:29:11'), ('34', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '1', '0.99', '328490437', '328490437', '0.01', '1', '1', '98346723263723', 'vu tien dinh', '2017-12-20 14:52:18', 'STBTC 34', null, '', '0', '1', '2', '0', '0', '2017-12-20 14:37:18', '2017-12-20 14:40:19'), ('35', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.5', '0.495', '164245219', '164245219', '0.005', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-20 14:58:56', 'STBTC 35', null, '', '0', '1', '2', '0', '0', '2017-12-20 14:43:56', '2017-12-20 14:44:04'), ('36', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.2', '0.198', '65698087', '65698087', '0.002', '1', '1', '98346723263723', 'vu tien dinh', '2017-12-20 15:12:47', 'STBTC 36', null, '', '0', '1', '2', '0', '0', '2017-12-20 14:57:47', '2017-12-20 14:57:51'), ('37', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.21', '0.2079', '68982992', '68982992', '0.0021', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-20 15:14:07', 'STBTC 37', null, '', '0', '1', '2', '0', '0', '2017-12-20 14:59:07', '2017-12-20 14:59:11'), ('38', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'ok', '2', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-20 17:26:31', 'STBTC 38', null, '', '1', '1', '2', '0', '0', '2017-12-20 17:11:31', '2017-12-20 17:13:34'), ('39', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'adddddd', '2', null, '1', '0.99', '0.9801', '325205533', '325205533', '0.0099', '1', '0', null, null, '2017-12-20 17:29:33', 'STBTC 39', null, '', '0', '1', '2', '0', '0', '2017-12-20 17:14:33', '2017-12-20 17:14:55'), ('40', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '123', '1', null, '1', '1', '0.99', '328490437', '328490437', '0.01', '1', '0', null, null, '2017-12-20 17:33:00', 'STBTC 40', null, '', '0', '1', '2', '0', '0', '2017-12-20 17:18:00', '2017-12-20 17:18:15'), ('41', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '1234', '1', null, '1', '1', '0.99', '328490437', '328490437', '0.01', '1', '0', null, null, '2017-12-20 17:34:18', 'STBTC 41', '2017/12/20/1513765220.png', '', '0', '0', '1', '1', '1', '2017-12-20 17:19:18', '2017-12-20 17:34:19'), ('42', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'okok', '2', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-20 17:49:23', 'STBTC 42', null, '', '0', '1', '2', '0', '1', '2017-12-20 17:34:23', '2017-12-20 17:34:31'), ('43', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'ok', '5', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-20 22:13:21', 'STBTC 43', '2017/12/20/1513781916.png', '', '1', '0', '1', '0', '1', '2017-12-20 21:58:21', '2017-12-20 21:58:39'), ('44', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-21 13:04:53', 'STBTC 44', '2017/12/21/1513830393.jpeg', '', '1', '1', '2', '0', '0', '2017-12-21 10:49:53', '2017-12-21 11:26:57'), ('45', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '98346723263723', 'DANG HUYNH KIM LONG', '2017-12-21 17:44:37', 'STBTC 45', '2017/12/21/1513852265.png', '', '1', '1', '2', '1', '0', '2017-12-21 17:29:37', '2017-12-21 17:44:37'), ('46', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '12121212121', '3', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-25 16:36:08', 'STBTC 46', null, '', '2', '0', '3', '0', '1', '2017-12-25 16:21:08', '2017-12-25 16:21:37'), ('47', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '123', '3', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-25 16:37:24', 'STBTC 47', null, '', '1', '1', '2', '0', '1', '2017-12-25 16:22:24', '2017-12-25 16:22:57'), ('48', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '1244', '3', null, '1', '1', '0.99', '328490437', '328490437', '0.01', '1', '0', null, null, '2017-12-25 16:38:17', 'STBTC 48', null, '', '0', '0', '1', '0', '1', '2017-12-25 16:23:17', '2017-12-25 16:23:17'), ('49', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'edada', '14', null, '1', '1', '0.99', '375714', '375714', '0.01', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 16:55:39', 'STBTC 49', null, '', '0', '1', '2', '0', '1', '2017-12-25 16:40:39', '2017-12-25 16:40:58'), ('50', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '121212', '18', null, '1', '0.01', '0.0099', '3927', '3927', '0.0001', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:11:38', 'STBTC 50', null, '', '0', '1', '2', '0', '1', '2017-12-25 16:41:38', '2017-12-25 16:41:54'), ('51', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '121212', '16', null, '1', '0.01', '0.0099', '3927', '3927', '0.0001', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:13:10', 'STBTC 51', null, '', '0', '1', '2', '0', '1', '2017-12-25 16:43:10', '2017-12-25 16:43:12'), ('52', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '26', null, '1', '0.001', '0.00099', '176', '176', '0.00001', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:01:47', 'STBTC 52', null, '', '0', '1', '2', '0', '0', '2017-12-25 16:46:47', '2017-12-25 16:46:50'), ('53', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '26', null, '1', '0.1', '0.099', '17587', '17587', '0.001', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:04:39', 'STBTC 53', null, '', '0', '1', '2', '0', '0', '2017-12-25 16:49:39', '2017-12-25 16:49:49'), ('54', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '26', null, '1', '0.1', '0.099', '17587', '17587', '0.001', '1', '0', '98346723263723', 'DANG HUYNH KIM LONG', '2017-12-25 17:06:57', 'STBTC 54', null, '', '0', '1', '2', '1', '0', '2017-12-25 16:51:57', '2017-12-25 17:06:57'), ('55', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:08:08', 'STBTC 55', null, '', '0', '1', '2', '0', '0', '2017-12-25 16:53:08', '2017-12-25 16:53:30'), ('56', 'leo', 'dinhvt', 'X1cdcb49216bcff1df9cdadeb1ccf730b66720', 'X1cdcb4t9216bcff1df9cdadeb1ccf730b66720', '27', null, '2', '0.1', '0.099', '811', '811', '0.001', '1', '0', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 17:10:49', 'STBTC 56', null, '', '0', '1', '2', '0', '0', '2017-12-25 16:55:49', '2017-12-25 16:55:52'), ('57', 'leo', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', 'xxx', '10', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '1', '091212', 'DANG HUYNH KIM LONG', '2017-12-25 18:17:51', 'STBTC 57', null, '', '0', '1', '2', '1', '0', '2017-12-25 18:02:51', '2017-12-25 18:17:51'), ('58', 'dinhvt', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '123', '2', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2017-12-28 11:55:07', 'STBTC 58', null, '', '0', '0', '1', '0', '1', '2017-12-28 11:40:07', '2017-12-28 11:40:07'), ('59', 'tiendinhd', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '13E9Tkp72fwf7um6XML8VMKDL1onC46FNn', '2', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2018-01-03 12:09:48', 'STBTC 59', '2018/01/03/1514955301.jpeg', '', '0', '0', '4', '1', '0', '2018-01-03 11:54:48', '2018-01-03 12:09:48'), ('60', 'tiendinhd', 'dinhvt', '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '13E9Tkp72fwf7um6XML8VMKDL1onC46FNn', '6', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2018-01-03 13:50:36', 'STBTC 60', '2018/01/03/1514961366.jpeg', '', '0', '0', '1', '0', '0', '2018-01-03 13:35:36', '2018-01-03 13:36:06'), ('61', 'tiendinhd', 'leo', 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '13E9Tkp72fwf7um6XML8VMKDL1onC46FNn', '2', null, '1', '0.01', '0.0099', '3284904', '3284904', '0.0001', '1', '0', null, null, '2018-01-03 14:02:53', 'STBTC 61', '2018/01/03/1514962089.jpeg', '', '0', '0', '1', '0', '0', '2018-01-03 13:47:53', '2018-01-03 13:48:09');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_transaction`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transaction`;
CREATE TABLE `tbl_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_before_fee` double DEFAULT NULL,
  `fee` double DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `to_address` varchar(255) DEFAULT NULL,
  `type_money` varchar(255) DEFAULT NULL COMMENT '1: btc, 2: eth, 3: vnd',
  `type` tinyint(1) DEFAULT NULL COMMENT '1: deposit,2: withdraw',
  `payment_memo` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0:init,1: pending, 2:success, 3: reject, 4: cancel, 5: error',
  `status_job` tinyint(4) DEFAULT '1' COMMENT '1:pending, 2:started',
  `extra` text,
  `token` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_transaction`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_transaction` VALUES ('1', null, '0.1', '0.000099', '0.0010000000000000009', null, 'dadad', '1', '2', null, '1', '1', null, null, '2018-01-05 11:26:43', '2018-01-05 11:26:43'), ('12', null, '0.0001', '0.000099', '0.0000010000000000000108', null, '123', '1', '2', null, '1', '1', null, 'eyJpdiI6IjZuV2d0M09NUTdWOG16UHc2NW50TGc9PSIsInZhbHVlIjoiOWl6T1dZdzZnNHJORUpPYXowcFFwQjBEM1JBOU1iRkJETWJpMDhJbjM4QTlBaVN2bWpBNGFvNTgycGtVZVlPb3Q4RDlPcGtTT0NHMDFsWEgwaSszYmhEOFVPZFJCR0k1S3dsalBGZG05OVNsaVpaV3N5Tk52M3BXbzhBK0xmY0xnQmNsVE52b2FBcFhZQ25MQXhDU1M4U200b1pkc3VLN0taYWlWeU4rNGxMOE5NNSsxczN0ZU9YSFBtaGpadDArd1NmdmJPN1VnYlFqODIzRmtBRTQraExNckZKbVlVVWFZWkIwa2l5MFI5ZlRqckJRNmRtVjlNTWFvRDlWSXpMRWlBN1dMY2trZjRhRkFhdjZmTU15eUFBZndBTmJOYU5qcnFmak1OUzBlXC93emgxQWJvQ1JERWpCS0J6KzE2aFVpTkQ1aWdxalwvVXhjeG5YUzZCYkcwMTk1cjhmV1RaQnNaSDdyblR3XC9uYzF3a1A4RU9lS3lLWkREM1wvSWdFXC8rTlhIUEhrbWZ4MlQyb1BXa2MzZHZNWFZldkhnODRDczRyYmg1c0RKQnF0b1lxcVwvcGhEaVh4WmtVazUyaG5oQWd2TyIsIm1hYyI6ImU2NGM5ODNkMjBiNjI2MTVlOWQ1MzZjYmIxN2QyNTc0NTY5Y2M3MGYxNjBhNjU5M2Y5YzAxYWIwNGY2MTM2YmIifQ==', '2018-01-05 11:58:50', '2018-01-05 11:58:50'), ('13', null, '0.0001', '0.000099', '0.0000010000000000000108', null, '123', '1', '2', null, '1', '1', null, 'eyJpdiI6IlVrc0VudHQweGpWVStMK1hPd3d6anc9PSIsInZhbHVlIjoieFNkcmRcL250YkVYS1hwOHlxemhoMXpGc2taVmhsa3VZMmlZeVAyZTJNOGl1WncxRVZ3bTZlbENMeTFkSDB4R3hiUnR4XC9NT0RxSDFVSDRTdFJnU3I1SnRuS3JDSW9wcU5sdGRkU0hnUVlnbGVYWDlMRHhZWXl0N3dPejhXRDhyVWxoT3B3WXM2dnZ3OGdcL2NCRnF0UWtYWE5CYUk4eDZkUXdONG5TWDZ0Z0hXd0NnVWJkeGhoaTM2V2pPVXVIRlhkMlJOT0NDdTJcL1RuajZub2lEclhoaEdxUFFTUWRIdHUyNWlub0NkTkdMTVM3amc4N1h0OU8wbVB4Zzh3MXdxZ0VpaGtpWE1Id1pPK3JkQ3h5dVNER0FOaHFMQk5YZnV2MkU1S2kzZkZNc1k3QzVObnFSRUpBWlB1c0pJc3lEZDlrN0FCU24zczZSdklhdGgyTWdkdkVnd0FpVjJhYzNuZHZpY0R5Sm4zQmlHZXV3T2srTGVsQytSOVwvZVY1MnJEaFNFK1lTZEpUdmgrMTRLMkhRM2ZtaWZwMlhUK0hxY2kxbVhMNkNVbEZNYnNwMnJWdUU0Rlo1KzM4RllvTGZsb25XIiwibWFjIjoiZmE1OGU4YzFkMzE3MjhmMDgxODU4NWNlMDNiZGY3ZWNkOThhMWVmYjM4MDk2NzliN2VlNTQ4NTJkYjI1Y2JkYSJ9', '2018-01-05 12:02:18', '2018-01-05 12:02:18'), ('14', '13', '0.1', '0.099', '0.0010000000000000009', null, '123', '2', '2', null, '1', '1', null, 'eyJpdiI6IkJSK0Z2cWhLcURETlBhNmxXMEloVHc9PSIsInZhbHVlIjoidmpTMFUraWhiMmVHbkVrRnZtV0xMdG1FbE0xaUM4YlA4TE1qVXhNZDJDNGlNUHVLQzlabmJyUTUwbnpRZFdwaGc2MmdDTVVuTFp6b1VhZEhLaTU2RGZDNlwvUkhJM0kyWGlQSXJEczRvSzdHZEdGb2tmMVpaZmZjZkxMZkR3OE1LZnBaUGUxV1ViMHNzVTRLNm5ZclZsb252Z21cLzJFemVERzJMbHEyaDFRNWxMMFZXaU0wblN3aEhGemc1ZGo0TGc0OHlVSVlGZWJNTjNcL0dxc2tPZGNsbnRzYlZSYjdvY1BzNk9RNjJNN3pFODdjdE5HXC93NmdtMFZiNlg2N0h3XC9UTE5tVW9GMG4wdUMxYzk2TFpCdlNpaTEwMUFoNSthcHo4cEJoMW5yRTlsNE9lWDI1bjBLblhzTmQ3U0tVbWRrOVNOb1YzSXBUYUxPZW5MSFRjbk9kam1mMGJBR29RNEhCQ0x1amx0TE91U1dBQXNITzJmUStxcXdKdUJqWlB2U2VSaFczNWlHajVZcjNtMTBONjNmbVVVWisrXC9oVVJcL29RUlMzZVByQUtyY1Q3QUUrRjgySFwveUdcL3BybTdmNUtpQ1wvaEZcL3hyWVcxQzVHVlJpSDFvNUNUUT09IiwibWFjIjoiMmIyZWQ4NDZkNDcyZmFhYjQ4MzI3YzMzMTlmNjI5N2JjNmFiZWMwODExM2Q3YTAwYzc2ODg3NDQ4YWE0ZjVmYSJ9', '2018-01-05 12:03:58', '2018-01-05 12:03:58'), ('15', '13', '0.001', '0.00099', '0.000010000000000000026', null, 'adddddd', '1', '2', null, '1', '1', null, 'eyJpdiI6IjhXYXZ4cWpVbkhLTGFseDQxTmF0RWc9PSIsInZhbHVlIjoiMEVCUWwzc3lXTkJyV0RUWlRIQldOWmVuT1IzVnh3Y1dST1JURUVoR1UrYWJ6ZHVlN0t1UDg2dzZ0aFwvYSt3cGlvT1daY2dobTVpS3ZIdWtWbEhsZUxOM2lvV09NRmZRbDgwaFpsT3ArYXZkNFErSmVBSUhubG5rYXZjVDdUenVcL0l3QU5zTkhNMHhDOEZISkc2RkliSHdDOHg5ZEFhS1ZPa1pScFRjTTVORE4wS0V5V0M3Y0k1amlkeUsrY09ZcFlmMnNqdHVURHdJVjE0blFKUTJcL2dKZzBEWjd1a1FZUno1Q3FmUHN3QlNjbEpNWE96TUdkTzJYNEI3VUt4NFMzcGdvUTlGTVBCN001QWUxQUR3dWIwa2Mrelg1WFVaTWo3emZ1NEFcL2l2Z1ZVRk9SWlZlOHJ4cGF2QVdFVXRYU25wVHVLT2d1U3BnT1YwWm1MQnFNZ2xyVVpZcVd3Y3d5U0Z5eGZIUnp3dU1Salg2aUkzQlFqOW53dko4c1h1VnBtOGFSMUpkUCtleUx1UlhONEdFQU0ybGxMS2xjY2pRQ1wveWdxN3NpaDBLbkdpRWI3S25DdHh2Q0N5V2lsQWNMVDQ1ZmF2cGhRT0VPYkJFcURDait6eFBKZz09IiwibWFjIjoiYzA4MTdhYzYzMTVjOTE0MmRiNTRkNGQwYzJhMjZkNTBiNGMzMWZiODFmMTU3NGQxZDBhMzMxYWZkMGI3NWQ5MCJ9', '2018-01-05 13:49:22', '2018-01-05 13:49:22'), ('16', '13', '0.002', '0.00198', '0.000020000000000000052', null, 'adddddd', '1', '2', null, '1', '1', null, 'eyJpdiI6IlRaRTBibk02UzRNanljSm9GNWhpd0E9PSIsInZhbHVlIjoiNE1iXC9QUmtiNmpSK21OK2FVdThlZE5rUXlIWHFIVUhnT21PbGtSNit6QVRHb3lwUG1wYW15WisrOU9xSllubDdXS0RkTlNZcjVRRWN3UkpoMnZNQlBkc2JIVWJ6YURvUWk2WDM3RkpcLzE3clJqV0pVc3lHaURlZjU5UEdcL0NRUkY2RU5uM0JrcVVJckN4ZEh3TWkwSVwvbVk5YlA4XC85SVVzcnJtcEdUdjBhcGpCN1wvXC9xeTJGMmJ0Y1BcLzRYV1lJRlN4NGI3TTIzWmp2aXVsZDV1MWl5UG5mckc1WDJBSXZ0NkhjcnB0UU5XWW1Xaks0bTgxSVFzdHlycW1VRXNvYnVyTDZJK0xQbnJIbExxeHd0dnVVVUUzNmt1RlZRNFlsWlczY1wvMkptYlBmTHlmelwvbXhNZ2VhUzZmZnBHU0lTek5ZOXRHVVhJbUNcL0U5NlkrTzFodFM5TWpiVnNLRHVhNXFUS091NlBzZmNiclV0aGFiNjRxdHl0NFdhUmdjNHJLaVo4aTlSOVhCa3k2Mzhad2Nlb0JlNnF0YmYxaHRtV0M1QUlwUVRWcWt1V3d3c2JYUUNlTEo5Nis2XC9WZlo1YnZPeiIsIm1hYyI6ImZiMTRkNjA3NGM3YWI1MjJmNDVjNGQxYThlMzVjMzRiOWRjZGExOGM5MWM2N2JjOWY5YThhNzAxOWFlNWI5OGQifQ==', '2018-01-05 13:50:10', '2018-01-05 13:50:10'), ('17', '13', '0.04', '0.0396', '0.0003999999999999976', null, 'adddddd', '2', '2', null, '1', '1', null, 'eyJpdiI6IlEzVElHa3IyZWlSbGpxS2xtUWQ0cmc9PSIsInZhbHVlIjoiNTUxMXhLcU9ieEpObVEzbG1iNFwvdUNlc2FRWnRnOUxCRUVtK1M4b0dxMVBGZjUwTU04c0VvMWN1dGJzcXVBN1FwY25OQU1Qam1xK2hmQUFLMmVzVlZUakRBb2d6UjVpRm15dTYxY2pmZ1NYaXNKYmhwUjlHVlhwTUVRekpodVJ5MmdmY0I3WTVtdFwvUlZZS2xRQkhEMnp6emwxb2RDQ2N1aXFRb01QZndqeitRcWc4d3hjcUF1aHdlWlFtRFwvZUNhcDJiM2RkUkZBREVCbENxUFlQa3FFMGNQWUhYV2o1TFlRNHk3R1NaTHhWbUtNd21sK09XNHM0Vnc1V0hSSENGTG5jQmJLOEF0WERCOUM1Z0d5c2hhbUNPZkFCSFFtSE1mTXdzTjdYdlF0Vjk3VUZhVkhsRkE4SXVYRHpyMjVVTGpSQ0VlSUdaZlwvQWxoQTNXZmZjaENJd3IwVEkzMmJLU0owXC9QRTdCRHVWQWt6dm1UOW1MMVdkRjhwSGRBaG5Zb3NlM1pyYkIwZlU5Sm9ZNjV1MzZQQzRGOTNaRTUxbHRXekVaT2RyWTl1QWVXRnliOWlvZ2tuZityakE1SlRsNmU0czhmSm9jWFZoQlUyTyswUHRpdFh4M2w4czh6WUx3dUhGR25wazduSEJ1Zz0iLCJtYWMiOiIyZmNlNzk5YTYxMmFiMWRkNDBmNzIxZjQ5ZjVjNDRiNzg4NzRlNWRlOTk1YWU0OWMwMWI1NDllNDJmZTVlYmE0In0=', '2018-01-05 13:56:30', '2018-01-05 13:56:30'), ('18', '13', '0.0001', '0.000099', '0.0000010000000000000108', null, 'adddddd', '2', '2', null, '1', '1', null, 'eyJpdiI6ImtnTHk4STRxNmN5VXJQQlhGRmMxakE9PSIsInZhbHVlIjoiaFJ0S1FzNzhKQlJjTjZEZmV4Tk9LUlVuUzZ4MGRJeVBpYUpyOVFvd0VwT3gzZ3NhSGxcLzh2M1pINkhTYnN4Q3NXT0VyMG5hRFFnN2w4QmZKV1FNZ0kwanNDM0NSQ3E0Q2ozOGIyZWNFVjVyXC9Ld2hMMGpxQytSZVpcL0JEcVRUZHJ2Rk5odDRtZUk3ajZcL2NzMWJcL1wvUktORWJScnBqbFhDdWhiNEdGQnZDQzJScVVJb1pjV1wvVW8wWDNaYXdWeHdSTjVwcE5yM3ZIcVo1bW1uY044Z3RVVDVVSlA2akpSbkxhbTRwamJlaWk1OWF3MjNvU2N4Y242K3BLY1VhTUVrM1E2VUZKVHljN092RXFUVXFTSU83WGJLbnhGSXhxUk9GenE2WVlSc1BBOWdJOGpGUGs2Q1ZabFlDMlVwK0pXK3RJUkR0aCtaTjlvQTNFRmNoSDhIc2VKeUx3TFkyK0ttRFVhbHArSmxranlqTFBCSm1aYTBlR1FjaWhZRmhIZms1YTZKYVZPYzMwR2F0ZVRaenZEbzNRRUxiRmcxXC9zNjdLcWgyWEhxXC9xSmEweDFiYXlOS0U5cmdpNmFrQ0VBVFN2K2NSWks5cFhReW9cL0dPQmFvQXNuVkF3PT0iLCJtYWMiOiI3ODdlMGRiZjhiNDcwM2Y3YTdhNzI5MGUyYTA0NjY3Y2VjN2E2MzE0NTA4OGE3YjU2NGFiY2UwNjA3MGNmZjczIn0=', '2018-01-05 13:57:01', '2018-01-05 13:57:01'), ('19', '13', '0.003', '0.00297', '0.00003000000000000008', null, 'adddddd', '1', '2', 'Rút Bitcoin', '1', '1', null, 'eyJpdiI6ImhjdU9ZT0JCblNpYWpwcE5VY1dPZ0E9PSIsInZhbHVlIjoib2JpTm5aVjlmRW9cL2J1MGdHejBqVGdoVVY3akxFT05KMUxycTBwQ2JaQmZ2cjV3VWxpaVEycWJjQTBYQzdmNWVlYU1zdFhKXC9HNWgxMWJseCtiUVh5RW44WmU2eXVJeWYraXR5cVdLU1dzODZGMGs5eENwNWJGbXU4bHlPc0gxN1RCYURtVFFpSVJjcXhWQlI3WGFLWjR6NUhYVmt6VDZZdFwvNjF4Rml3Y1dPbmVuTzBqSXl3RXpkTmVydmVVMzRiTng3VlwvNGVESThDVTF6MkdPdTBTZmdCSEdIeHpRMVliT0NSWWNEcEhlTGdZaTF5c1FrWUdmb1lLMk5aemhyZSsrNVRpejNWMUNcL2xhYW92TEdteGhnWlFmT2x0b0UrSGtyVFU1NU14K1JZV1VPblZKRk13V1V3VnRld0Y0aVlTUmJmeFZVc3lZS3NraWwrandKNFk5Y1F6VmVPUHZINEkxaVpIVjFtQWVyYUZSKzJXaUpiaTFGM3lqQkN2ZEdGV1dXVXNGVldcL1JRZ0VMeHN0XC9DMHJMUW1SNTlPaXBURmd0dG1PNzBlaHl6ekx3N1NxaDB5Z1FJVEFld0lOOXpYc1hBTUJnTFp4VWkzOTNtbUxiMEY1bTFnPT0iLCJtYWMiOiI2ZjUyMjM4MTc1YTYxOTQ1ZDNhYjgxNzVlNTMyYmZhZDFhZWY4MzY5ZGUwZDBhZmE2MjdlMDgxYmVmM2I2ZGFlIn0=', '2018-01-05 17:29:34', '2018-01-05 17:29:34'), ('20', '13', '0.1', '0.099', '0.0010000000000000009', null, 'eiwnewejbmadanmsda', '1', '2', 'Rút Bitcoin', '1', '1', null, 'eyJpdiI6IkNIRUZhUlVWbDVxWHNXcEVmSXk0Q1E9PSIsInZhbHVlIjoickxIeHFlUERWSGFpS3cydGhPMjRqUE1ZRlwvc0U2c1FyUzhYSkV4N3VIa254YkIxOE5CdG5xeUdmandMN0FtV1lHRG1tcDVYTEFaa2VYMTFtZkR6WFFOSFwvUVdCczFxVmt0OUdZMGlnaUpEZTNxWFJRVk9Fb0RhbFNRd3RteHZhcU1qc0U3NWtpYzZFSCtIa0o4a3BZWG9CcTJ0K0Y1QWNIOWFCNDgybk1OQllCSWdCdFBYU1Nlc0RmWjJHVSt2b1l4M1ptR2F5aWFOQXZMUTFZRjRrNHBlejFJRnc5MzFkSmRPWVQ4WFwveUFnaXdOUlwvR1wvano3Ylc2S2x3SEhEc3RNWjljMHUrdmZDRGl0eG5LTDBrbDVPdTVnQVdleHBRamV6Q2lWaU11Z1A5ZFpNSjg2dUl5NFBGUG41Vm8rK2NNNFR3SjRlWW0xZXdSY3B4MUJEMlIySlh1eVNZYmVZaDdmVzk4YkY3RVN4bkM2NEh5QllcL0xYVnczN045ZnV0YjRRbFZoWldsVkNZa21VNitiZUxVZHl3WjhGV3NoWjZ4SExlcmpXU01JUFpJVCtkZDZ5UitjSkFLNWkwUER5RDAwUnNPTDYySTNEMWtqbWs3YWp4MGtkcVpPelwvOVpqblZSU3ZCekFqdkllTnBBPSIsIm1hYyI6IjlhMWQxNGE1NGQxNGQ4ODMwODYzNmVjMzRlZWI1ZGI1MTk5ZGIyNDQ5Njc3OGYwMGRkNGE2MWE0ODU3ZDE3YzAifQ==', '2018-01-05 17:33:10', '2018-01-05 17:33:10'), ('21', '13', '500000', '500000', '0', null, '2', '3', '2', 'RUT_VND21', '1', '1', null, 'eyJpdiI6IkJ2TktsNEZNeHpLMm1FR25WMHB2Smc9PSIsInZhbHVlIjoiR1JnQnVENzhjdUlJcWRtMmt4VTlXblpyRnpoeVJmdkpSZ1VuQk5QYlRwNXZRVHUycDc4aDl4cGZpU09BVTNpYkFrTXE1cktHR040dWdKd1dhRFpiU3VNek9WaGdPbkt1ek5jaW1tRG5sTE1aUnZ6alN0dUpOVGl0YzVuN1diNG84czdBekpQN25KKzJyTGlkTjFrWjAzMDR1bGIrQ3NoSTRXWjNrSHdENG54cGdqc3ppSXpJN1FOVUswT2swRXczUzlYXC9WZE9wd3RhVmVmQlRnSEpZSmh2NnFxK3JNUzdkQXR5eFdJdmdHRklOMXBZNDNNNUMyVVhcLzFLVVZmYnZNTVZRclN0bFNnSzVXc2pUb042RW13RG5WekNuZ3hDaFM4ZTlKbkgzUGVUcW5Qa1A3Tml5Z2h5RHBzS3FlcXh4YWF6eXlGM2JESjBFRWxkXC9cL1Z4czBFUWxrZXpRSXVcL2gxWllhR0gxSGxUSzA0Q3JTMkd5TG9aRnNwSEtramltd3VYM1o2QlJTb1duTWEyRlJlWWpTVXhBPT0iLCJtYWMiOiIzZTNhMDEyODVmZGFkYzZiZDczMTc2ODNjZGJkMjBiMjUzODc2YjdkMDQ3MGUwMTNmYjMwNDc3OWUxYzRkZmVjIn0=', '2018-01-05 17:34:13', '2018-01-05 17:34:13'), ('22', '13', '1000000', '1000000', '0', null, '2', '3', '2', 'RUT_VND22', '1', '1', null, 'eyJpdiI6InBYXC9RRUdKTWRyTm9VWnFOQ2JIU3dBPT0iLCJ2YWx1ZSI6IlwvYVloaW9PREpHUmpVSHVWVEdPV2dBVE1Ka0FHTmsrRHRidHBlU2NrMmxUZzkwcUpUT3B2ZXBneG5uNFBocXlZd05KdDhOT1NVRnd1T2I4bGVQZ3dnZzA0TjR2S3FKTDNvZTdRUFFiSzdoZ0NWb2dcLytMN1dOK0RjcjJ4SmtvaUw0TGZ1NWgwMnVERVdLQjgyWWdFbGRPS0dyZklhQWg0WFdydTFwTUx1N0VYaStlMGFVRlVPWTFPODBkVlNSTWc3Nm1nMExSMjVmOThBeUw0SW5YOERwcG9tOElYWXhPZytKYlF2V01XUlZsdjlQeTErbUxRVzRCZzlaOEErZHBnQzUwdnJMdzd0Vm1SbmZcL2wwcWZuYmJEYmExbElPeEZjQTdvcVFpSlUrZit1Z0tqZzNVMzBZb0tKclwvOUNMWGtNc0dPVXFkZTdnZXpGUldhWndVU3FleUgwb1ZIa1VocFJmNkUrdHFNUEgyRkZXb2pmUEp3ckdVcDBaWEtZckZkU3lBWktxcEdjMFZ3emRwTTNENGt6d3NRPT0iLCJtYWMiOiIxMTM5ZjkwNzI3Njc2NDZhZjM2N2Q1NzlhZmMxODIwMjE5ZjM0MzUyZGVjOWU3MTM3M2NjNzUzMWJiNGVjZTMzIn0=', '2018-01-05 17:42:11', '2018-01-05 17:42:11'), ('23', '13', '1000000', '1000000', '0', null, '2', '3', '2', 'RUT_VND23', '1', '1', null, 'eyJpdiI6ImpIbmtqZUFna3RIRGszSUFGREw1VGc9PSIsInZhbHVlIjoiTE14MjlRYkxXbjU1YWJNWCtaalZIMXRjMjRmKzNHOHdXa2tyT0R6SHBaaFR3aHVIUlp2UjE0VGxBZU9ITHFId29HU0M3R3VCRXVZdGhUd3JmakJvWVZrczVNRGlMWHJPUWdmbG95d3FcLzFDeHFZZThpMVZOWFNyY0VKdTByUG44Q2dNcmhnZUhFZjVsUlgzMmU3SlFXbzc3OUxhTlNNdEdndzhQak9ZcUM0U0hcL3Q5T0VUbTg5aGl3bHlJdzVkMGZTMUIrQml3VkpUQTVuQjNYTWlMaFp1SHY1WWprdGUzXC9sNDNKT3hlWkhLckpNeExaYzNcL1d5dm5qS0tZQm9jTUpSVGRMMTFNT1VnVk0wR2ZVZ3RCR0ppWWxNYlpQeEtOM0RIcVRtMTBmQXFnUTdSVHJ6VzF4OGJjNmt1bFBtUXRZVzJEM2MyYkU0dlB6SGl4TlBoVzZLRXJGNFJ2TlE1UlI4ZHExRkVOWUVDbVloVmtDWXh3SVE5Y3dYXC90bDdSNTFvQnpoWVBXZkZ6WDI2bVRqM0ErUVBRPT0iLCJtYWMiOiJjN2M0MjdmYTI1M2Y1NjY3OTA3NTUzYzhkNTdiNWRiOTljMDY3YWNiZGUxYzI5YzU0NjE1NDY2MDlkYjkwMjI1In0=', '2018-01-05 17:43:12', '2018-01-05 17:43:12'), ('24', '13', '1000000', '1000000', '0', null, '2', '3', '2', 'RUT_VND24', '1', '1', null, 'eyJpdiI6IlR3dGUyNlErZ0tGRHpkS0U0VkVET1E9PSIsInZhbHVlIjoianhuT1ZUUDBVdURidFpRN0xzWUQyTHdtRnI2ZHdFSEwwaXQ4UEF1SU5NU3h5UVpYaU9FOHVDUzQxOGdjaXZ0U0FGZ2Q3UGVFbEF5clwvNkN1RERkTlhRWE5OYkZjcTdibnZmUmN2cEtoR0xQbnRNWXVRQ0lIZjJobFNtbHhHWENYeUNaTzJqRmpHMVwvaHp0ek9QWitOYzZMYVh1Q3B1czNNYUc3VmEralFkUnhUTnI1VU5xVng0OWtRWEtva002OE9IYkx1RzBYVHJEMlNSZnNQbEpUUHBrdEhFWCtxNGNoczJWVW9FWlwvd3FkKzJGT1JnbklvRFZvSFlhRW5LTTZTTUpVRHlvdFFqUzRSN2FTZm1MQjVvZjNEcFdValwvMjB6cnV4eFc4WWpOdDFXNzdPdVVHdHFBUTVjaENLWmZLbHZWUmRqXC9YN1hjMjUwcTUyTUN6WnZsTm9ya1V0U01UMXZJUzk3eXBaQStEd0g1V3pZcUJwMHZhTXVmVUg4WVQzbm0xN1RaRmx3V1BzU05iWWp6bnI1M1N3PT0iLCJtYWMiOiJkN2ZjMWFmMDYyNzYzZGZhZjVlMWQxOGY5ZmRhMDJjNTRjN2I1ZmNhMTNjMTQ4MWE4N2I3OGJiMWY3MGY0YjBiIn0=', '2018-01-05 17:44:26', '2018-01-05 17:44:26'), ('25', '13', '1234', '1234', '0', null, '2', '3', '2', 'RUT_VND25', '1', '1', null, 'eyJpdiI6IlEyQUdUSkJKYzBHRXVlclV5VWY3VEE9PSIsInZhbHVlIjoiSjUweEtWMUtlUWg3M00xSFU2XC91RlRBTmViZ2QrSU1Oc1NoN0VWRnQ1cHZGeFZZc2lFblVSRTBVTGM2aitGMFwvRDhOOVNuZ3hmSVhydlE0SVhLbjJuRjV5NnZUK3Y4SzhBRDJCbWU3VFRsb0MrWG9Wd3lpWEpzRGhPWnpxd0QwbENvOEJuOGJDMjZPaVBZXC9CNlpQcEVmdHlUb2lNNHFkZFdCMForTnF1Q2lmWTJwREN3aWZzbHF1Ym0wZnVodHlsXC9BanROdVJcL0FiZ2NPR3VGc0dFdGpYK0VjcEhjWEFwOHN6U2NQaWNrME4wTGtcLzRSQWVvemFzU1dKaW5KYm51Z2NxWjVQRmpYbkVmQldRUkpicE9JYmRnUXo1aHk5V3ZuTUlPMHgwaE5PTWdZS0JSU2ZoTnFRcEpHZG94cWZPTWkwVWpaUEZSdndRZU5ZQWhyQXRzYjlqZTN6NXNDdHVxVlNCMDJwNzFnXC9lb245ZXJEc3daV0lnNXpVWGw0RkZjQ0VwbnR1akhtU3VSRUdubERBN0R1T2c9PSIsIm1hYyI6IjBiNDZhMGY5OTYwNjYwY2JkMjg1NWY3NmZmM2RhZWRhMThkMDMyNGEzZjk0MjMwYjQ0MmE4ZDVjZjE5ZDE1ZGYifQ==', '2018-01-06 09:16:50', '2018-01-06 09:16:50'), ('26', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND26', '0', '1', null, 'eyJpdiI6IklBV0lhdTdlSTFQZk92dW9CXC80elZnPT0iLCJ2YWx1ZSI6Ild1VDJTXC9CaTJqNGtDU25DcmdzblBFMGsxVGlLMlNjR1wvd2psdnJqNE9LWmZvZWtPNEFxWlNNSEhrYXpUOXRVVWhITms3RVVmRkQzZWZCS2ZWckJaNFwvS0Z6XC9YRnhvSG9Dd2xaYTVnb1l6MjZmWlF2NTBvVWNuSkhjU3hTdkMydFRMNVk0M3M0YXF1cVQrV1duQ2thczAxdlwvNlJxVDJnWEhkNEIyWWJBOHFTUkFUVlR3M0lSQWdocWtCS2hXRzVqNGVTRmdiNmhJRXJXdVh0dDdWOUNKYVNSSTN3YW9UM2RUSmNwcDRjMHFOb0VhUEVCbVhrZ3hkK3BCNThlUk9KVG9GeHdwbzF2dVo0SGpNUFVlWjU3NjdsM01CV1c5Rlk2ZFwvT2hRbUZvK29sQXRwbUZcL0wrbWdcL2x3K0ZpK09sajZSN21lK1VFb2ZyREg0bzVMTU9BcDZJU2RiOFNNbHFWUFYxc1RNRk9Kckxsckx3SHVUeENrUDNjOUMyZDJZUW9EIiwibWFjIjoiMmM3NDhiMDJkZjFkZWUyMDRhMzg1MTFmOTJlMWExMjc1OWU2ZjdhOTQ3MjkxYWIzZjhmNjMzMTZjMmJiNDQ3NiJ9', '2018-01-06 10:39:06', '2018-01-06 10:39:06'), ('27', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND27', '0', '1', null, 'eyJpdiI6ImtUbHhXaWFxdkNObEFzMXV0VmNzN0E9PSIsInZhbHVlIjoiMEZ4SEQ5eXUyS0VyRWhrV2NZNm51ZUczbTBzQXZGTVFQVmppaENFQlFFK2c0SXhnVnEwOWdtWWV5cjd0eStMWTc4KzhuYW1OV3dtMTFlTENiaFpHeEFGQ0RIXC9FRzE3MitmcUxrSkI0N20weERYdVJDanR6UGkzQytpT0cxZmdtSXlRN0ZuXC9jcEh2Y2dIcGYzTW5BMmxVMkpTKzBMM1FyWDh5RjdESldMTVwvOXhaT2VjR2I4YTNcL1BSKzk5dThpNUNRK0lDcVpxK2tXMTdSQjFhRTJsS1ZUdlE5eFwvVEp5eHVwcDA5bGE2a2JiblZKRllhSVZJZ0VhMGVXWGN6ZDExaCtrTEdNNG45Y3VtTmdQNktvSTdtTzVkZ0YxZU9aOVd0WFlSSzI2ZTd4d1RcL1ZwXC9ZN0tQcGxpNlFjTms0OU9ZOVlhT3JUZjc1a1AzNVpyU1dWbGVUd0NcL29LXC9KS1QwTFwvQmhOM2t3YUMrM1hNWjJJbEs4VU12cGFzSmxCTzJjdSIsIm1hYyI6IjIxYjQ3MjIxMTIxZWI4YWQ3MjMzMjllNTA2NTFlMDkxMTMxNWM2NGE3M2Q4OGRkNDU3MDY4YWQ5MmFkYjY5ZWIifQ==', '2018-01-06 10:39:44', '2018-01-06 10:39:44'), ('28', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND28', '0', '1', null, 'eyJpdiI6ImgzKzh5UGxNVkhDNkVsTjdaemtMSUE9PSIsInZhbHVlIjoid2RrSWJTS2h0eWI4eEEzeUJcL0JFSWZFb3Z6ekt6ckk5U1ZpVnBFZ0t1TEs2WTV6VDlZUXZsMXFhekNWSHV2ZVJualJyMEdvK2NoejErWGI4UTQ0ZUF2WlJoYmg5NlNTZm1zUmxNMFRsTjNCZGZuS0xIb0taVDdDWGN6dU1cL0IzZjVXSVZOMGNxXC9hWEhXd295UHl2Ukt4czRVUExwbVdmRXdcL08zS25McjAxcUFFcGl1RGpOZHExeVZKSWpzT0VVS2NCRDRZa2hlQlN0SkZJSEEyVFJITDhMWWJYblwvRk5BUnRVbWlqRkJzSUM5Wkx0TEMxRHFDdEFCYkxoRXp2U1MrOCt1MlFMN2JMVGFCU1lNUjlYaEhOT1dpaEp6OHVJQ1wvM0d3bituZHowUmZ0eE43RXVOSXorV3NVaGdCQ0pMVmlcLzh1OCs4UFJEMjZiU3VcL2MrOGdnT2d0Mkw4cW0yV2ZmTGZMSlB3SkZZRm1HdmNrKzdGNlJWaWt0RGQzUnYxREVCQVwvQ3V3akRMV21VcjB6TGNZbVlMQT09IiwibWFjIjoiY2FkNzJiNjNkMjhlNjY0MDc2NzUwZmIwMGZhMjUzNWYxZmEzZjEyNDE2NTkwYWMyN2I3Zjk2MjBkMGMzMzMwNyJ9', '2018-01-06 10:41:43', '2018-01-06 10:41:43'), ('29', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND29', '0', '1', null, 'eyJpdiI6IlFXQXdMaHA3a29YSjJCZ0hsRmlmNWc9PSIsInZhbHVlIjoiNTltcUx2Z1ZyTTRyeFdpcGczekF5QlgyRUVrQ1dOcTZQR2Z6d0VmUkxsNmlHRk1ZeGhyc243NnFCMmJZRGV1d0J0c0thbGdyVWZjTnBHd2s1cHFVbFFpdDk1NWg5VUpKSHBDNkFlTDRLWWU3QkV3VlN2NkorWk80NUpuVWppYWlEZ1lnQUJlTm1pWXRMcmJ2XC9nbjJIQVpTUElEYTR3a3ZkRVFzZEtxbEFkNU4xYWNkUjJNaUprVzVKdFwvUnZwOVwvdnFBNHNpVnhvSFRxWUw5MTFRenQ1M1JBNGhSM0ZmY1dNUnhLd1hDeHlFa2Myc3h2ZXFNcndYQjZoNWtic1pRMzRlUUFOaGVJXC9EalRoZnpDYTJqTXFVeXBjaTMwMjY4cU1GNlVtaElYNVc4aGNiZHo1V0JxZHJVUlY4b1FvMHBoSDBYRUs4M25WNnFab3NJR3NqNlUrXC90dFlWT1wvTVRoQU9rWWEzWGlFY2hNXC90TUhzb0o3ak9LNVRScEJibG54VFFnNGFucklGampFVzMyTDVWU2crK2c9PSIsIm1hYyI6ImNjYzA1NDdjMzJmOTRlYWJkZGNlNTEzOTdjNTI3MTAzNjljODA1Y2ZjMDgwZTE5MjY3ZDcwYmY1M2NkNTE1YmMifQ==', '2018-01-06 10:42:07', '2018-01-06 10:42:07'), ('30', '13', '100000', '100000', '0', null, null, '3', '1', 'NAP_VND30', '0', '1', null, 'eyJpdiI6IjNSd1dDYzNUMmZnaWZ1UmtKdnBENWc9PSIsInZhbHVlIjoiZzlYVFBJODU0R1N1OVBKRGZDVDN6SGpmY1VUOWZyaHk1cVo4THBMSFBxbE9MN05cL1BTaHpyUE5DSEF6R2RUamgreWwwSllLY282UmNYRVVsY3M1RDZoejBIQjJmSXZMaHhzRUhqRDZXbUhCZDlOXC8yMG1RV2JSYU9GOEZMVUdLVEpwSlRJdEMwZG0xV1wvNnVIaU9Lb2tUMEo3ZGcyRVwvMk9cL1krd0VISUVpZU1RR3ByWklPRzI1UjVzenNoUUhFN29PTXJqbVkzNUpNM21BK0RERlpqa05cL2laaTNham5pYlR3SHUzWHdDTnY4c2NjQmE4REZUSUo2RDJIVTJDSFpwbkZJdHd0WGY3MHFIVE9QQUI5b1RTYUt3WG9ORkdyeW1nZHBnQzFjcGtic3YreHQ1aWhKZm5uVFp4dGt1bjgxaVZWQjU2aWk5TU54ck1WOXYxbGtmcEE2YVo0ZU9SRGZLdGJMejFodXdSait6VnMrUWN0TjJDNStZVzVmYkRtbEE2RUsyZ0JnZ2N4N1dDZHJWcHhnNjdtQ1pTXC9CcThnOTZGbjVLYTdOY0JQdFU9IiwibWFjIjoiMDQzYzRlZDliNmZkODYwNjk2NWQwMjgwZTczYTcwOGFlYmRjMDQwYTU0MTE2YzlkNDZkNjZiMjY3OWIzMTE0YiJ9', '2018-01-06 10:42:16', '2018-01-06 10:42:16'), ('31', '13', '3000', '3000', '0', null, null, '3', '1', 'NAP_VND31', '0', '1', null, 'eyJpdiI6Ik14QVhLbGI3TFVTUklKckhUV0Yza2c9PSIsInZhbHVlIjoiWTl2R2VlMFBsTExtVFJpcGlvVGMxNnRHTTBvR2p6bk9GWm9ZOHNWM3piMFBMNEZWK2FTdzBPdEpHRHFaWGd3ckV6NmFsb0UwSWRnbCtGQmp1aVpRejJMWmJIUW11dFRiQ0xcL3cxZzcyM1BNTGN4bVBwSUJcL0V6N0ptWnVWczB0NndXcGZ2TUIwOVFFNytkdzhNVk00d3pYQ3R5VUw0blB0cENtNWZ4V2s2blRucEFtXC9OMXZqQmErUmJCaFwvblNXd2pOZ28zcWJsQ1dRRVVNUVZXbUlrSGFHelwvdXNIQXlvT1dZSWFTMjlxMlwvR2tYVTB3dkFsU2tOMkRcL1pxcEJhZXQ3cktsdzlvKzZRMnJIT1lLbW5NR082eEtkQ3BlWDVtV3pTdFBnQVgyQ2syNlRrYkpEUkpZRk5XWDVHczZ0SFl5OGlJZnV4OWlzXC90NERpVTRzazFWZmNjN3Nrc0VBWGNFQVF4eGFcL1k2dVwveW9EMDQ4b0dmTXNnZzkzaGZcL240V0JkSGZmZjliYTZ2NUNPT0JKU0lyVUN1UXZGSXhqRXkzcWp1VG55QjZDY25JPSIsIm1hYyI6IjQ1YjZmNDZhZDcyMDNlMjZjZTkyZTg2ZDFjN2MzMGFhOWY0MjNhZmUzOTJiZWMwOTBkZGZkYTFlODAzMTUzZmIifQ==', '2018-01-06 10:44:37', '2018-01-06 10:44:37'), ('32', '13', '122222', '122222', '0', null, null, '3', '1', 'NAP_VND32', '0', '1', null, 'eyJpdiI6IlUzNVkwaTc2Uyt4N0Z1dUtTOWNnNmc9PSIsInZhbHVlIjoiV2xqRVpUWnlaRXNqdzVxVStXdkpiekJOK1QxZDFobDBZVE1JcVE3QThpSCtZNVN2b1wvT2ZJdFhlXC9FZ09MU0p2cE1ma3NSZVJsNzZlZlphYnltdjdkNnVxeDFlVjhcL3NWN1dtcjJpck9rVFlrYjBGUHFEY3Iyd1ZyOVBiNWFmOGZFV2NXc1NmcG9mR1NNR0htWlI3TmtcLzRLSDBlYkhuMDdTeHkySlA3aFRoZTEzSVwvVmFwdVFIVFlTOTExRDhKd3NRY1ZCVVAzcEpUWGJhN2ROV05iY2VKc25NeENMS2Ztb3Fsb3BvWERLZDZDWTk5RnhYT1FwS1pnSEFzMmg0NlFRTmxmT1JhZFwvRXVnYjJtc2JldENEZWNMSUUxalwvem9nckNXZjU3dUFTOVF3cFFBQzN2SitLNnF0K241UWdIVXhhUklxSmorSSszK01LNFYyUVBXbDJDWFZ0cDVoM0h4QmRmMEhZbWVRcHZxY3dtRmhrbnV1YnFaTFJGTnJRdlwva0orZVpsbVpEcUtLZm5nbW5Oc1pqTkdQXC9GVVJkSFFsUlhYTjFlTnd1NW1vcz0iLCJtYWMiOiJjYTE1ZGY3OTYyZDQxNzJhZDJkMDg0ZjgzNThlZTJhZDE3YjRjYjNjMThjZWNkYmQwOTkyNjM4N2FmNzlkNDA4In0=', '2018-01-06 10:45:11', '2018-01-06 10:45:11'), ('33', '13', '100000', '100000', '0', null, null, '3', '1', 'NAP_VND33', '0', '1', null, 'eyJpdiI6IkdlNU45VHBpMVBtYnJjcVdmczBGSFE9PSIsInZhbHVlIjoiM3FlcnFBbUs5QWtUTkRqcXRMa0NSYXFwb1FlSFpMUXFva1JjRDZHSlRydXNHV0hBcU1RZno4WHpYd0VxOFlLNEtTWFRZK1cwNTRXa2NJUmVjaUp6cHEycjdEZ2hLTVQxZmRSam9UZHlEV3c0NFhNdktzcU8xREZ1YnVIMk5GVGdRTUE3ZlN1T1B6QmE4VlZETjh2RlVDTFdEUHZHekJMSW5kTkNWOTJRWTdmQ2NCM3prRGdxZ0x2Z05rRUxzbTV3ZVVqRHo5dVd3T29BYUtBZkViSWtNUlhOY0djODEyMW94eVRcL215aXhtUXk0dnEwdzFsNHRzY1M5TGRxdnk1UUl4THJSc0hOV3U2eFNKY3QwMGVlREV3VVNjRmorM0tiY3ZGR1h2c2FyXC8zMGtDdjJjTkJCMGdJVVNKUU9cL2ROb2tLXC90MlIzNkJmMys3cWg3MHJjYzRtajdLY0daTGc1djh4TTRjK2NzakRTalJXd1NGbFZnK0dZT2JQaVBUamowZGVqeVl5NmxPK2cybVJ1b05vek52ejlCaEcrMzVLdUhBTUpvczIwVm0xdGM9IiwibWFjIjoiNmZiNjY2YTYxNzRlZTNjYTRhZDcyZWIwMTcxN2U3ZDZhNDVkODEzM2M2YjhkMzY5OWNmZTczMTUxMWZjM2MzOSJ9', '2018-01-06 10:45:41', '2018-01-06 10:45:41'), ('34', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND34', '0', '1', '{\"receipt\":\"abc.png\"}', 'eyJpdiI6InZsSGFxb3J5RXl3ejJHTFwvMHJieHhnPT0iLCJ2YWx1ZSI6IlduRkpmUjNtUkw1dTI5Sm9CRWFxMG1scElsbzRRc01UMW9lS1p3VDZcL1llT1I1NXRTbUVZZlNia0RDSTd2cWJ4VWx1dUF0d3RxS1ZnTVRLdGZQQXdkODdackZyNDQzeWlROG1qV0ErZmRIZThnN0ExTDhPR2g1WGwzNnRjd1FVbGNvQ2tuR2NTUXFqaDB5dllCXC80TVhua3JxRXZSam5GeVBiY05udCtnMW1hZjFYK05BQ1p1T2pIalNjXC9kWnJuSE0xaDhHamlGdU16SnoxNkFFc0l4bDAwaTFHUkRnVzJMbVU1WCtpdXRtcE9BeVFzYTBCK0w4V1B4VVZqeTR5SHJiMGl5cHY5eGRWN2ZcL3k4d0Z4WVZ2V2ppUVB5bHU0OHVxVVNVSmNGWEorNldHVys4SUVEZkF6bmhlbXlMc0Fqd3NEZXBhR24zZW1nQVY5UjBobWErNzFCZG9JTVFIZXU0MUVpV0lOVjNtZys1OHhRN25NcjNrTmg5XC9sUDZ6WkhSNGJ1SzByWmRwQVR0emxna1ZFQ3pUKytqdk9zZDA1aHYyaFA4d1VPSnJyWGZaWk1Oc01tZ0htSWVGNlM3Z2prR0NsZFlmUGE3YTlTb1RyaDFSeE5kTGE4aFhrUDhuTWtpem05THlaaEo2VzQ9IiwibWFjIjoiNmQ5YmM2NTRmZDYwYjIxOTA5ZmUyMTBlMmEwZGZkZmU0YTgzNmM1OWNiZWI4ZjM2NzA0ODljOTQzNzhhY2Q1OSJ9', '2018-01-06 10:59:44', '2018-01-06 10:59:44'), ('35', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND35', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IklvOGl0U0FKbjY2UzdxT2pMNnhMeGc9PSIsInZhbHVlIjoiMStIMXd0VW9lK3pxRUdRM0U5aGt6TXpWVUVGMHFQd1ZVVndSMk1BUTdjUmxhRlkrYXdvUGFQZ3N2Y1BZdmMxRG5lUm9JZkxjakRKNUUxTDhIVGdGaDRlSFl1V0VpQk03WjVkWHo5TjFrMjZkbkE3NldPckxuOFNMcTNtRmIwdFQ3MzgzUzZVOHVYTVN3TFljRGEyaFc2N3lLRVFXcW9wdmtuU3ZRdEN4MUdEc2JnNmZSd2NFbzdybityRFMxMWNKUXhTSGRwcjZlMmR3KzRZaTdOSitDcW8xa2F6Uk5LdmVjTjVudzkxamIwVm5VdmZIank1U2trWDVFRGQwM3BVSXdNNGRrWVVKVUxxaDl6elhHXC9DY3UwMHo4UTlLT1NPaFg3Wk1iU3hSbGd2VEZEN2Q1d2k2dFB5K2lKdlJoZnU4RDNSWmhhXC90dzJubmljTjNPMW4rOGJjS0xhMXRyajlldUVDUGhvK2o5WTVJY3BwMjRFVFNndDErbDdcL2RJcEhaM1FhQlwvWFk4K3Z1bEZJUmpjeVl4OGpQakJqcW41dGEwbzkyNTcwc0NTWjlydkhORGtNalRNUVdsVXl5MDBBb3hYSWV4QjVZUlJrVWNwVFZTRzdndkRnPT0iLCJtYWMiOiI5YTJiY2E3ZWQ2MDkxM2ZjNGRjZjVjOGI5M2E2MTZiODRkMjYwNzU5NWM5ZjExMDk4MGU2NzhkZGY5YjQ3ZDQwIn0=', '2018-01-06 11:00:32', '2018-01-06 11:00:32'), ('36', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND36', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IklUVHlPekhwdE81TDljeUxHNkJuVVE9PSIsInZhbHVlIjoibUNWNDNsNGxWRU9sTTdaa2dwRmFqV3B3RWJXNFhLSGZBbG0zb1pNaEQ2XC9UazE2YVRvK2VUSkw3VVhPR0UwRVduYUduZHhseENNcVdiZzNcL3ZCRHpPc3p3UDhxYkRWbmFmT2VBTkxlU2pETUJ2dVlUcnVwalwvV3pVZENJN2tCRnRIQ0ZqcUJLU1R3SWNWTjd5SkxaNVRlZWVaTWtvZWw2R01CWXJYT29qK25OTkNjQXhMaGdVM3NQM3VHMXcxYlExaWJmekh1TnBMUktFdXhsKzNoSVhOTWJLVWM5NlhvQmNBYjE1RjZWV2JiYm9XNjJHMFQ4Zm4zOENEQmFQck1xSzFcL2ExUkFIazZGbFBGbjVWYXM5eEE5V0ZXKzJVeUR2SjVvUThpRGVGeFFKUUxXbUt2OHRFNlA3bnN5eWN2cG5FRlVXQkMzMjJacDdGTlwvZDhlNE5oUVFuTnlcL0poanFISVJPY3Z4a0xxMnBBbDA4a1JRcGJrN3pvdkFSUVNoMjFHcXdVNldNd0hiVTRHRW9yNHhXNGNYSStpSE9GZXVTSjJiNTc3TUdFK1BRTW9vNGN0SjVlUitvK1BnaEdocklQZlQwbVIyVVpxTDdmK2FhYjRKSmVLSFE9PSIsIm1hYyI6IjU4ZDgzYzM0YzU1MjYwYTc3NzllNGE2ZWUxZmIzZDdlNjk0ZmE0MjJjZTdjYzkzZDdmNDBkZDliNjk1ZTc0YmEifQ==', '2018-01-06 11:05:01', '2018-01-06 11:05:01'), ('37', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND37', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IlgraEJHVWVDTG04Mk00OXVucHh0Smc9PSIsInZhbHVlIjoiNU1TenA4RVNBWUhcL2wxUkFnSlZQMXRBSHFyNEg1azNvMzBNVHp1U3cxT0dBemFmaXV5RmZcL2lvVzFHQmRBbThHUldvdjB1ZGdyaEJzSFE1MEpWNHBiTEFyU2JFMkNZWGY5UEt0eWxhSjd5Rkg3MmFpR2xUeUxEdTA5ODFSSkJIWE9MSGV0YXFIelhoays1VWFtZWpPblp5T0xiQTdNamlOUzJXMklpOURyeVlyV1N2andXbmR3aThXdGNHdUs2dUFDamxOVGdnSjFqMW1nckswQmRqUlVsSU44SG1JOXlBT1RMejROXC9SS01FWWd6YXlUVHNvc29vWEJqOU5venVMR0UxMUNYZmFqcVgxRFFMOFVwTXoxRjZ5QmJTRFpjaTJ2QXhVNjc5OURNeXpvZUtMUG5DOG1LUm5SSnN0M3p1M3VqcWlYK0huUTBmY3F5dVhZTHJWQzVhNFUrQlZcL0FuRG90MWlMOHJqUGZHVWorZW5kTWxtVDRjK0JVV1dEMTJwNVI5b1NObWNcL09GOE8wU3FYV3lxdkNjY2NWSkhacWFHMjVJV2tIS0sxSEN2RVcyNEZaNkY1SFlmSGxHU3hpVkRoN0E1QTV2WkxtKzVOTnVYXC94NDBKVnc9PSIsIm1hYyI6IjY1MjlmNDkzNTEzM2Q2NDE4NDlhNTllZGMzNjMyYmE2ZWZiZTdkZThjZjNiOWYzMjk3M2E1ZWU1MWFjOTFjOGEifQ==', '2018-01-06 11:07:55', '2018-01-06 11:07:55'), ('38', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND38', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IitNa2JLQjJGM1E2SUo2a0FDNE9lVWc9PSIsInZhbHVlIjoiTHRxaVV2bkU5VkNCYTdwb1wvWmFCXC95bzZoT1ZlQlp3bFdxSVwvdFBQSURoNHJTd1FYTXJtXC9tRjJ0akJhYmVWZ0p1Y0liV1BjaGRPRSt5bGFFMmZIWlRHR0JpdEMwY0hUZWgrbjlqS2EreUNPQ3piYlNCQzJmNmFPR2tIK2J2cnhsUjRoeTBtTElGMUlNMUlpeTk5aXZPTzNmVGk3MG9OS2tnanJpTFhxY3lDOE9lWmVCS21MakVzREtYNDVCTXFzdDJTaDBHNEEyMmNER09vajhlTjhJaEpKK1dCemhMNGd0NlVWS3NXUmdEallsYXdNMFwvTEozTnUzZWpaR2tqSXRBMmhiTU1NWDQ1bmV5WkNHd1NBK1hRcnJBMVYxUFNPN3l5b0RsN05OclwvbHcrREJFVUpvMjhtV3k5K1lUSFRoV3BMbmtsbHZZRlJmMnMyRFpRdk0xTXF3bjBxakJ2MjlCZCtrbjVcL1I0T2FKZVJUdzhvM3JDMTNiQkRjclpGb21TWWMyMTRnUlo3aVZYbnBBQkNtQkVyaXAzWCs3ZTdEY2U1Z1JiUm1oK1dNamRxYlpsNyt0WW9TQjhDZW1jc1c3bEpXWEVOeFk4Yk1Qb2pIeFJTOGp2eUdRPT0iLCJtYWMiOiJjNGIzM2ZkNGY2MzI3MDA2NzcxOTRkZGFlNjA0ZWI5ZjMyNDU5MWRkMmU1Y2E0ODAwY2QzZTdlYzA2MDNhNzVhIn0=', '2018-01-06 11:08:34', '2018-01-06 11:08:34'), ('39', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND39', '0', '1', '{\"receipt\":null}', 'eyJpdiI6ImFScEFQWWxWakpqOUFPWUh4Rzdjamc9PSIsInZhbHVlIjoiOFl4RmZTc2kwVGludTU2MXJYeHZmUFRmTXJYSnZ4eFR3VTV6Rjdjdkhac1k1OHlXWEpzSmhjRFl2MWR2cXRRR3FVM1VNdERJS2JBK3lcL25oczM2d1NnSTNDVk5HWkx4SlNySFVyenR3d0hQdEpaNjNubTN4bld2YjJWTE1teTlJUkdia0pnNTZ3ZkI1aWN2VWkwQjNNRkZzZ3MySWo2eWdrU3RaRTBjYXdaZGxJdnFxUkhRSmNGM2xpc0NqSHNZRkUrMHgwUCtSNm5vbWd0clI5RU1KbGhyMzhCZVBWaXpzbzc0VjBEdSsrYmFKUXNCY3NURXM4MkFyZTZNNlpHK1lwYnl6cG9XWGtsSlg1ZzJcL3hESjJMMHJQTzdzK3dvNENFcEhlYzdKYUtFSGdsVmtSS1hocE9kTnlLZlk0RmdlVXV6VzA2Q3d5dEdhS3VnUjNScFZVYUFaUldTbjVCU0o4NFprd2ltQUJsMnUyWVlXMzUzK04wcU5Gd1wvald3M25FbHVsWjdqdlwvN2NudFcwSENNQzFPT0tLcm5ITGUyR05kM0xEN3hDUVpwRU9YVGwySHlCTUNDQnA1cEZjNHdLV3BhM05jVW1LWmpKdG1hbGtPekUydWh3PT0iLCJtYWMiOiIxZDlmYjBjZDcwMzgwYTA5NjI0OTM0MjRhMWFlZjk1OGI5OWVkNWNiOGMxMTdhMzU3ZGNkNzRkYzEzMTM4OWFiIn0=', '2018-01-06 11:57:15', '2018-01-06 11:57:15'), ('40', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND40', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IkY4TlVhcEltSkRqTG53T3JtcUxFVlE9PSIsInZhbHVlIjoielprWFZZV2I2djhvSm1ReENudmFUY0p1S2V3ZEIrcWJma01sK3Q2akh4d3ZzUVhxTk5pTThrNEE0NXVudVpQaXBMV1AwS1BcL0o2XC8wZldsS0l0VFlpSUxwdHp6WFpNM205MmJvQVwvaTA0d1Q0aWRWMVV5RHVYSmZ3QVQ2M3lNRStUMXpwOVpwUWFHaWMzMzM0UCt5eTZcL3JjZmd3dnZ1XC9WbVA0OGdneTBXQ0ltRXdtV1lxMVBaUXJ4ektGbmFXRXRFZ1wveFNyRlpsS2pPTjd4SE5XYU91aTlrT25Cek00NW9JbHZwcGI5WlgrTUl3UnJBQ1o3dUEyemg1NVVrUnhkWFwvQ0pTd2FVQkcwUDdNVDEwQytlOU1EZ1VTZkR6Rk82YVl4dmkzK3h0dnhQR295d1ZLQ0ttUlRhN1phMk5EbXUwMXBrT1lwZkJCQWQ0cThNSHlRSDIxd1BXT09YQXRjNEVVMEk1QnVmcVM0YTNQdXNHbU5Nc0lRNVl5b0NBRnM3NzhtRzdLU2JCNUJvUWowWGo2RktvTml0RjhBRE1WWVh5WmJ4bm9nc2FkeG1VYkdRZ3hTY0hZTkl6ZlFXWGNTR1RcL0NhZ0xzSG5Oa0pqQnZKMjljTThGQT09IiwibWFjIjoiNzE5NzVhNTU0NWRjMjViMTlkOWMyNDYyYjg0MDE2NWMwMzM5N2YwNjMyNTk4NWUyYTdmMzBlYmJiZmVmYjZhZCJ9', '2018-01-06 11:58:02', '2018-01-06 11:58:02'), ('41', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND41', '1', '1', '{\"receipt\":null}', 'eyJpdiI6IjNkYk41THlxaTB5eUVEQXVic2dRY2c9PSIsInZhbHVlIjoiM09NZHNJU3FaOVNoWkVQbElrQXhSV1NzQld6U1JuYVNcL2hQTFhwZW5YQVBaSURBdEM5WGEydFJXY3g2Zk50aUFyY1doKzhBTlwvdzVuQUZLanErOWJFYXltTDFpMDkwcDgxTzZSMDBSc2tiVWp4YzVyVk9mc2ZNcHhwaEVxenFmbHpBZVhRSmpITGRxdUc5eDI3cnZROXV2QVc5dUI0c3RZQUF1Rk0rS3lMVDVZNmhhM2c3bVFnNFVtc0ZEXC9KRW92R1E2eWFoc2JTeGdJbzBOR0pqM2ppZlh5bWtzaTMwMHQ5RmhhMURSTTdZN1wvZFM2cVBJbFpoWUdPakY3TWREQU1VbWMySEU5K1JsTlZGU3Z2d01TdjJxTHd3VTZWS21XMXR1c0x5TkVTaVhSQkZlMFlSUTRpXC80Sm5vd0xyQVE0bDg2TmJTY29WamR4MXc1R2JyV2FHcGNPeTh1OTJPWFwvOWtVbmNDZ0g4bzBSaE9mR0NEcGtxdHdUV3B5Zk1GMkpBMUFCMGxub2UyN05KMzZ4WklMVEFkSkIyNDM0MjdvcG1OUWlVandOYVhcLzVnU2U1S0xiRll0aXJPNTZmUDJsMFdxU0VSdWEwOU12TllhUU94THp5c0xnPT0iLCJtYWMiOiI5Njk5M2JiZTJhYjc1ZDA1OTYwYzNiMDZhZGVmYmViMjk0ZTBhNTIwM2MyMjQ3MTc5MDczMjhmMDA0ZjZiMWQ0In0=', '2018-01-06 12:03:48', '2018-01-06 12:03:51'), ('42', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND42', '1', '1', '{\"receipt\":null}', 'eyJpdiI6IkpcL25LM2dmQThRUFdTMUE1Tk9NbTRRPT0iLCJ2YWx1ZSI6Ink1cWZRQlVobU5DOFdwWkx2bEIreDczSlhrVjJSZ1dkZzJ0bGxEUmFURXFWclFMM0lJdW9QcXhQaG5DSk84OEVEdSsycUw3WExwWVhKZ3JLWFMzZ0twdGN5NzloekFGdHJZcjVtMFZjMHI3XC9kSElJNGRYUUN2T2RxUW95SW1YQjFmQlBEMGtRcVRQM3JpVEUyZlBIeW92elwvYTNRaUttWGxWYnVoQkRlZkVYR1wvVnJRZkk2MFE0YlhSNlZvU1lCK3BXM2liR3ZTaEVqTGlZT1NFNXFNWjdtN0tFNFRGaWo0UU5oZFlxOExsK2dUTHpaZk84OHRPQnJpdFJzYldOcG5PZERIK2RYV0lkZldsQzhWa292NUp5V1ZNQ3ZaVVQxektoNnMzSjRKcDdTOHA5VVB2eXZnekF2Q0g1c3M5MEhsZWZ5M3FzaitUWTFzNXhsUUhCWVdJS3NSNE1lUkpJV0dSV0NuaDEyTElLSktvV0c0bWtHenNkT0JJRXozNUc3NE44WDZ6ZWo0aFJTcW1BenRRTXcrZWNqRDB6ZE5pSlVuVTJvdTdCK3ZcL2hFdm91NzNSaVBCaFFHeit1OU5BSklWZFgwY08yOEpjYUVESks5eXVwTGxaZz09IiwibWFjIjoiMGM0Zjk5OGViZjA1MWExN2VhZmYyOGYzYjVhZWUwMmM5ZTA3NmI0Njk3OTYxY2I2NGE1OTMwNGMzZDhjNDAxYyJ9', '2018-01-06 12:05:17', '2018-01-06 12:05:20'), ('43', '13', '1000000', '1000000', '0', null, null, '3', '1', 'NAP_VND43', '1', '1', '{\"receipt\":null}', 'eyJpdiI6IjJwMzJ1UEh1bUp5NE5DMzBHU1pPZkE9PSIsInZhbHVlIjoic0NTVkNiS1ZMWXBOTkdXM3RZN3N2T3FcLzNMXC9ienJDUGJaZmRsbm05TEQxRmVmdU5vYTRnVmhVazVzMGkxWktQdndJN1BKWHc4OHRXR1U3XC9SSENKWEdiWFFMZmN6ekVEV2NqaHdlUWd3cUcycnUzbHo4TG94OThYOERCY3RtN1g3YmRTXC9FZG5zS2xMQm5LVjVkMEc5SldFaG56S3VVWHU1eWZXVkR1c0FJMHdnTmJ6V21cL25KMFVldnhRNHFaU253UUdQdHM3WUdIQUc3RFFFNnlIbTdFUEd3MUVTSkRqd3RaekRId0cxNXlcL3VmOUZmMmErNjhkRkwybENrNU1ESUhLTTBMczJvRTNodnRMVHZHVnl5ZEJjWk5WSlMyZHRDNHJmdXRWWVZZaXg3UjNvSVo1a0dIb29aNE50T05VNDU4ZVlrVU9wa3lCc1o1NTEyeFZCVTAyU2NUeG1JNGhmajJ5SnQrRE15QjRqK2p2Y0wyNDdZTjN2YzFKQSs2MlNzVkY5UzVmTFRyT1Q4UUpQNlAzZWphcTJINm1TXC9hNzF6MXhTdlRWTWtaXC8xZFpXNVc4eG9MdmcrN3MrbFZaTXpmK0NTbmVjNXZDVWVuSVQ1Q1lWMDhzZWJ2REhPUlpuRlJTK3BiSnJDV3JPSkZIVU1DUEliYk15enozd0tZd3loMyIsIm1hYyI6ImU3MDg1NGEyNTU3YWQyMTVmY2MxMTNhNjU3OGE4MTBhYjNlZmMzMzUwMzZjMTIzNGQ1OWEzMDM0ZjFhMmMxYWIifQ==', '2018-01-06 12:06:19', '2018-01-06 12:06:23'), ('44', '13', '2000000', '2000000', '0', null, null, '3', '1', 'NAP_VND44', '4', '1', '{\"receipt\":null}', 'eyJpdiI6InBTelM4cENPdVhJWm9YSXBqNUl1eEE9PSIsInZhbHVlIjoibXpGeXBJdXIxNXJQQkVYSWZ1QjdYWW1LSWxtazBiSWZ1MWRUNGpsOVhhUWk1elppXC9PMFNDcXFhUXFoYVp2N0pBQXpKZnhaNUNGYlJjSStlRjR4c1IweGgycW1tV0dRMmMwMTlEUzhReUtneFF6K0RURXIxMnZVZE5xY3E1TmZqUUdSKzVYRFhEMG1Xb3RXNVpqMldHYktzTjdTTmgrbGtOZ2RcL0taR0MyMW9mdmE5b2VRWHIwTlRvdER3UUdLY3Q5WStiV3J0bjRLSXNqdmtPQ0lkNnhJNjJWRkgrSXN5UUxsZXNRekI4SzlxOEhhZWFzYzJ5SkVWREY2T1BZMzFjSk5Xakp3XC9HcTV4NVpSb2NQdldrQzVkekdQZFlBQ3VYUFZOMExFMkUxb3d5bHJKalFuVUU0ZlwvVWtIUnk3emlYVWh6RWI2TGpLbTVvQ0xialNkdnRadHZ5ZUwxZmhRbCtiQys2VUtPRjNkQUE0ZXVYWkNocUFjYTZaSXhUMmVlTzMzcmVORFdLbk56aTRDQ3lZN2ZqYlwvZzRaZ2txWlRZT0RcLzdiR09yeTBNK1h4SlN1ekpyS1ZYQ1IwV0ZGd0lrODJqUzhZSlZaRndcL0VaYVhQYitNUDdmdklNTTBzUm1HelFNZkFwSzc5QXE4UzRhUGFXXC9kOHc3VlRWRWZMSW5UWCIsIm1hYyI6IjY1Y2I5NDkyMzZiYWU4OWJiNmNhZWUxNjVmNjYxYzYyYmI2OTNkYzRiMjJmMzVhOWFmMGVhNmNmOGU4OGZiOTcifQ==', '2018-01-06 12:06:38', '2018-01-06 12:06:40'), ('45', '13', '3000000', '3000000', '0', null, null, '3', '1', 'NAP_VND45', '1', '1', '{\"receipt\":null}', 'eyJpdiI6Ik5tQ2xHNWcyNng1TmZIQmpYNlFiSUE9PSIsInZhbHVlIjoiWFBHeHE1dmQ3b1NIb042b3BnQTk1aExEaFRVQXZhTktJakFRVTVcL2x2RFRyQm12YWNjaDVzZ3J5Ynp5OWduZlA1TEFnQm91aGNKK2RMbnV1RWlvWmQ0dTl5WmpkMzFnU0F3XC9odkpzaUM3a1BHTVpkU1l6Vng1eXZqZnRNek1FbkJQS2wxUkFtbzRuSjVwU0FUOWlsNXVjNnNcLzcrbG51VE9TcjRWRTV1dkN2VTRLdjYwb1FxNzVoSnFhVlJSVHNkdGEwV0JhYVV2M1VOSE9aVkRYWElKVGMrcDNIQnNqOW1sZlh4V0pzUGM3aVpQa2oyRkNOYVgrMlFoTkZrSFM4bEdCa3FzeFwvK1ZyM1plTEUrbUt3ZzRNWVcrWWlDV3BkaStSZlpRTk1HcGFUNjhZSVk4M00rSXlWWEdlSU9oT3d0U3hYZnN5aG41SmtTNlJwNCsxaWhhVE95cnVPWjVHNzlhV3o5aVhoRWFcL1JQSVVwZ3doWXl4Umk0OUQrMWhHUHFpQmNpRVdWRUdxcmdzTklUdmo3UnMwMllPekpGNlVBRkV3XC83d0VQSzJOeUpYeWpTcGJRRGZmaGFnVTdKdkx4UnA4aWNOVW1BcUZtNXBlaW56WVVwYVhiVjZLYk1yWmlvXC9MYm12RkVpNHRvYUZLSmNBbHhWXC9hcVlQUENVcGFYeSIsIm1hYyI6ImRjYTE4OTc2YWMyZDJmNmUyZTYxZjQ0MzBlNWVjNmIyNmY5NTYwNWQwOWU1ZTFkZGEzMGYzMDIzMDY1NzNiODcifQ==', '2018-01-06 12:07:17', '2018-01-06 12:07:18'), ('46', '13', '177700', '177700', '0', null, null, '3', '1', 'NAP_VND46', '0', '1', '{\"receipt\":null}', 'eyJpdiI6Ijg3UkNUREw1YnVJMGtoTkFROTlQT1E9PSIsInZhbHVlIjoiN203bjI1TlNkMkRKdklaaGd0c3JuSVdRYzdUTFNTM0VcL2Jna1JqWGEwYnRmY3VcL082OGcrN2VZMDJ3ZlZDVjBUMUJ0QnQ4KzZiWWo2ZzFueFJJUlRiU3FBVnVwQjFsN3IxcDhmSFd5UGMwRkYwXC9OdGk2SitpRzNsdnhQM1Z6NE9idEhXdmJkZktxdlA5WXZ3R2RUd0lYU1BERkFURWNyM1g4NjRaZjIzQUMycEhWTm1kTHppa3NUTFJHdW0rXC91bzV5TFZhTWg0WFp5VDB6MzNyXC9IRFZoY0lWaEZ5azl5ZkE2TnBEanpqNkRRdEVRUkM3cnlJWU5qRVUycE1iaHp1WGFxb1g5dThObmJcL0xTc0Jjd1JlTHFvTWlEbk4yOENHd0M0NGJKdFdYSFVqbzJlbnhtWTVLRFBmSFIycTRwNVFLbzNtY0g5ekhKOVRcL1hIZU9FSm96Tk9HZll1UCtoMWI3alwvZzRvUlFjRncyU2NYd2JJMkFkQ1dMZjFxdkhnc0N2WWRVUDBLNW8zaWVONW1oNmdUWjYxN1RuTG5tVzQ5clhJd3A2MXFhUzhVZnM2XC9pVnNjYlBiY29HcmFCRWRLbmc1UzNxd0tGbW50QUoxSklnbFwvNm5oM0Z4cCtNV3RXZmtva0pzdjYxXC9lQT0iLCJtYWMiOiI1NDRmZmJjMDk1YjQ0ZmQ2NzJiMGNjMjMyNzJjYmE2MjRiYjE2ZjI4MmFjMmQ1Y2VlZmNhODM2NDhhMDM1YTJmIn0=', '2018-01-06 12:09:34', '2018-01-06 12:09:34'), ('47', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND47', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IlZBNnpKTnREQ0drV2ZxUUNYa0FMUlE9PSIsInZhbHVlIjoic0RrYzdJYmhEWUVaY1NWbmxNUTVvaERkVnhRNVVsSGpjM0FkZ01HaDhndUh0cDFVRmFscTFlNUJzUVE0N3pBRHM2WmNcL3IrZEl4T1J1YnJwTVNKcDBZVmpidFUrWFJVcktjQWFlem51K0FVSWN0Vk81Mk53UDBkNzczNzQxN2lxTlFRNFB6ZnZrK05sU3lETG1xbWlYMmxRWWpXXC9JalVWRnRKNFdRWkxZK1UrNW1wVGo1Qlh5QjRGa3lMTFlFVkY5NDczQnRDbnFSUWFTT2Y0YXY0R1V1cWZ5R2ZLbFF4Mkkwbm56WnhRMk0zK1JESTNNUmE0Z2grOEtINVZjVTBcLzlcLzBEcUlcL2k0YmFQV3Yzd0F2bzZ5S2Y4TExmTEhHYURyTHNFM0lYZFBQbFNtVFowRGV4azhVYzk4bWg2dlNkd2tvXC9PUmxyTGdQdnUwUER5aDdqMFVPdDVtTEg1SURrUDJPeCsxdXZkbFwvTlV6NWU5TTNOMW1PTDVNTnlwMUtLVFVNRk81T3NUYUkrSnA5MXIyNm9Sb1IydjZcL0ZPWFlKbTZoVEZMa3lZTjR6NzNpTjZcL0k5ZkdcL0pGRXg5V2U3ZFZnZjNkTnZLTHZpUnlwZUFHVnc0XC8xUT09IiwibWFjIjoiYWFmN2FhZDQ2NzhhZDU0NDZhOTc0NDU4NWRhMDIyNjQ1NjI5OTYyZDUwZmZmOGFjMWE2ZDc5MmJjN2RlYWM0ZiJ9', '2018-01-06 12:20:09', '2018-01-06 12:20:09'), ('48', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND48', '0', '1', '{\"receipt\":null}', 'eyJpdiI6IjZIeVJNVFhqclYxWkJhd3RaTUNRYnc9PSIsInZhbHVlIjoiVFdSSmVCckU2cHUyQW9oSlVLSTlTVHdwVDUxQ1ZMZlQ5VDBnRkQ3Nk5vSHpFU2ZuU1lCMmpTbk00d29ka25iWklxXC9YXC83RnJSXC9PcjRGaTJSNUNSZkROSkxsYkdqSnlqaTFEbjZiR25pODZVSzFoc1B4eFpBRWhxbzdxbU9qdmt1dmVoWG1zY3ltZzd3UGFcL1NhQzBQQlBjQUpmSkp5azV5UkpNbW9YV1ZXbTJxQXRwenE0bk5Vb1NEOTVzZGJzXC82emt4WW9cL01CR0R6aDVUZjVjOW1QYXU5VDR3bE9uMEkycnpiQWVwRGxxWTViMzZvc0lVa0dwbmRHc091ZGdTXC95dnFHWENtbjRWa09lZjFvMmdLSjQ5TVkrNG42emwzb2RxZHN2YU9UOFZPMVdNXC9SWk15dW5EV2xnYzJmZ3lUbnV6aG5nckJBY01yYW5BUkoyZ3RCc29zVERXYjZGbGNyaXRHOUxMTGxqUWdJNDl1dnRIXC9qdzFmMm56NGFSM1pVdTJmeTduMXNyUWxwQ0xWaVwvTVV3YXRyVFwvalVjckg5d2JvXC9TM1wvMVJYNlBoNlZzM2ZtXC8wbzlhXC9ZcUh3YUFSaHJIaU1DQWV5V1AxZElHelRScytVdnc9PSIsIm1hYyI6IjY5N2YxYzkyNWVlOTQwYzc0N2NlNDg2NjNjNjRhOWE4Mjk1NTM0ZDUzNzljZWU1NzRhNmQyMjQ2ZmE0MDkxZjgifQ==', '2018-01-06 12:22:14', '2018-01-06 12:22:14'), ('49', '13', '1', '1', '0', null, null, '3', '1', 'NAP_VND49', '0', '1', '{\"receipt\":\"2018\\/01\\/06\\/1515216257.png\"}', 'eyJpdiI6ImRwaWM3SGNleWJBY0ZyT0JkeDB0cUE9PSIsInZhbHVlIjoiVGpDcHpTMld6Y0lRNExSSXVZYXlPdlZPaFhaTHBzT0tKS1I0R1pXUFlKSzZXR2Q3c2R4dWFnRExNRUI4eFRSQURTVVZyMWNzZUNHSEJvZ0NtZ1o0VVAxRHE3cytLR0llbUVRTVpMdklxeUYxZDlQMHg4MnY5RERlWWhZWVwvRmhoK0xJS1VJTHJFb0RyWUwra0tGdVVQdVp2Z1pQanJPRGhWZEZ2QUJcL3JvcVlwbGVEVjRvZG1maVVBenhSRTRldjdlR3JHaE50XC9SV3ZwSTExdUpKZzZpN09cL1BrQXdrcVJnWk5obk9BR2xFUzJuQ2pJQTUxaGhLcTRvTmpJeFArS3dKdUdwNjRVcG9zdWdSMnRmcDNVTGNFYXhCRndQUVwvdHllM1dxOFF5UXl1dnVVaDRka2FmbXV4clwvYmJ1d0NpdjI3bCs1Mys2bm9NUFlJMWFXcWE4SUh4WmJTQmkrUVNTdkJsb3FDMWgwQ2ttWXFDSXRMTUxPN29pb29lTEdOWElKNlk3YW1uaGlRaXZkZnU4aVBmVEpIbFV2dElBZmhjeXhxSW9LMStCTE9kTHV6bEZwZzhzbXVvYVhHbGRmYlY2TGl3Nmtnc2pTQzFDd2dRaTl4WEJHNUE9PSIsIm1hYyI6IjkwN2QzODgxNzM1MWNlMDFlM2E0YTU0YzU5YmMzOTAwN2FjNTJkYjE3MDYyZWQ3MzMwMzNjYzA1MDY5YzMxZTcifQ==', '2018-01-06 12:24:04', '2018-01-06 12:24:17'), ('50', '13', '121', '121', '0', null, null, '3', '1', 'NAP_VND50', '0', '1', '{\"receipt\":\"2018\\/01\\/06\\/1515218088.jpeg\"}', 'eyJpdiI6IkN5ZWl0K1VMK2pVXC9DNUpQcFVFYnF3PT0iLCJ2YWx1ZSI6IjYzb1pQWTdxRnhHbE9cL0V1MEVNR1hMdVwvTG1kSWx1OXY2ak9SM29kamp2V29kdTlYSjU2YTRxNkxLWHBFTDVNUDZNeVluVGlzMEJabzFIUnR4Y0xUS2Nud3JYV3VhbkNQTjBNempwU2VWTVQzQk9kT0Rsak1MZ0pzSXdSeWJvWVlzemZXWmZqVDhpa0Y1aDROTWxkdVREWlVxUWJrNjlyam92a2ZNd09sRXZJWlRSYVkwSEFsZURcL3hOY3N5N2ZkeXlRdVQzNlZlSTBCQ00raU9uRXBCcFJHOVRrYTBhZzBFa1pDM2NUVUFnQVFhYTYwcVJidURuUkM2ZUJuekw0aWEzMWpxSlplaEtwVEZUM1Z3bnM0QWNmZDg0NjRhS1I5aTc2TGJvYnlFajZZUkgzazM4S3cxRzFkTTZnT09uWFIxZTA3SXhFV1M2K0R5U3R0SEhnZUdMZ256UXZVc1QwOXYwNGxjTlwvWWVVRXhFcVhuakR3UVdFazgxZFdaYTI5UTdHS0FRYnhGSHVtRmNqa25yR0lCeEFGQ1RFWnR0Rk9vZ3REYXNTcHFcL3JTMitNOHJ6N0crdm1SOTVLWkQ4dXRjTVFqSW1pdlIwT05ZcDh3azJBVWp0emc9PSIsIm1hYyI6ImU2MGJjNDQwMjQxM2IwYzNmODYzZTVhN2JmOTRjMTI4NGZjZDY4YWJhMGYxZTEyODM2YTc4N2JlNTM1ZDJkODIifQ==', '2018-01-06 12:54:40', '2018-01-06 12:54:48'), ('51', '13', '100000', '100000', '0', null, null, '3', '1', 'NAP_VND51', '1', '1', '{\"receipt\":\"2018\\/01\\/06\\/1515218199.jpeg\"}', 'eyJpdiI6ImVHM3FTaWRWV0NcL1F0dFU3eUhmdHFBPT0iLCJ2YWx1ZSI6ImZhVHFmMTBMV0lLYnh6QTNSVnVIUE56a1lOZXFFU1wvYVwva0EzNWZWMWFJaDdIR3BLOW9wUlNBXC8ydHlhRlpya3FoNXBBXC83aWRRS0hBcmJWSUdROFdvek83VTN2d3YzOXNYWm5tcVo3c3NjeUJ4NktDbm9BQzMwSVp4alRWRFI3M1BTUVhUMjFkNkZna083eFB3T0NXXC83WjRNRWZUVXFmTWtnM3NqYWJabFhQVHVyZ0NiV1J2TllZZFl3UUl2YzVYQURiNTVJdTA0MXJhT0dXUk1XMWdEdlJrc2JJcnB5Z1dkTkFwVnpBaDhlKzBkK3JrNFpoUU0wdjQrVEJQRjlRTENYVDhQSzBMTVZ6RW1LeFE3bzZcL0RldStwWWQ5QVwvNmZNdThzK3B6Rm9mR2xXQmpLVDRWK2VEM3N1azlZU2ozYUJ0cng2OWh5KytqcFJiK2pobk1haDZkQXhqNncyYWxaSk1DMVE5NGZZbE1CaUg0QmRWeW8zOFBZTDMrVjFpd1RLakFpWVF5NHBrekxUYWpQWEhYczduR1NzZlZzbkZjcURZSHg1Sk8xU3ZkTlBqdUJoNm5PV050d0hPVVpOTTRmeTRxNVhGWVQ0TzAyXC9LRkk3RFVhZGR3YVd3d0ZsdjZ2dlBwMVFMV0dUOWM9IiwibWFjIjoiMDc1N2VlNjQwNDBhNTBhOGFlYjk5MjI5ZDM1NmY2MjRmNWVmNWQ2ZWYwYmI5MWZlY2ZmZjAwYzYzNzA1OWZlZCJ9', '2018-01-06 12:56:25', '2018-01-06 12:56:41'), ('52', '13', '10332', '10332', '0', null, null, '3', '1', 'NAP_VND52', '1', '1', '{\"receipt\":\"2018\\/01\\/06\\/1515218237.png\"}', 'eyJpdiI6Imt2RmpRZnpxRUo5UHA5cm85QkxYQlE9PSIsInZhbHVlIjoiZmVOREcwcExGbGlOaW5iNFVjeTFsZ0JibTVlc1hnc1BjdmNDSDZEMGM0UTJPWU16Q1NENXRBcVArVVwvRVRiMjhYenduWHdEbWpcLzN1TDZLalY0Y3FpXC9BbjY5eEQwazQ1SXhna1hwMVZzNWhxbm1kWE92bDB3N0xJVXA0SGEzb3JoRHpFWHByVldTU0I0UUU3dW16K0pHTmdDVTUzU1huOEhoek9cLzlNSDZmWW5MTXZRWVRGUGtkanBEVEJOd0JKWWgrbHlCMFlvTTZmMUFIdmhSQUpWTUNxOVJTSG9NWGJlZVluUlwvT3NPRG10YUM1aFo0QVRyVkpHcWNvV1FkOWtGQWVqSnJFdUYzQnBTM2d1aGRMUDNERXluT2krQldISXdjSW93ZUhSWVBYanZ3THZHRlJGZmhIQTFGNmxiK2xjekpUajFpNHFKYzFEbWEzenMrYUlsRHZvT1wvMVRpcmF6d2J1RXhvTmlGS2dwNElsaVJ5MDhMaWFFWVwvMWdBaU9DNkVuS004N1wvbGZ0SUdDUVRCTEJzdlhZOWp3NXd2aHVmek5HV3RCc2NmWUE5TjlySFNYUjZsYURpVmxDcklDOTVKcU9PWkdMSzJBVTFPa1hUaFwvVURGWnc9PSIsIm1hYyI6IjZhMjMyNWY0YjhkNTRmNWQyNTIyZWVjYjI0ZWMwNTQ5NjMyZmQzNjViMjI0ZmI3NzExYzQwYWEwNjRkZmU2MzQifQ==', '2018-01-06 12:57:10', '2018-01-06 12:57:20');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `presenter` varchar(100) DEFAULT NULL,
  `enabled_2fa` tinyint(1) DEFAULT '0',
  `secret_2fa` varchar(50) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `token_login` varchar(50) DEFAULT NULL,
  `token_login_time` datetime DEFAULT NULL,
  `access_token` text,
  `remember_token` varchar(255) DEFAULT NULL,
  `latest_login_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: inactive, 2:active, 3: verified',
  `doc_status` tinyint(1) DEFAULT '0' COMMENT '0: unsent, 1: pending, 2: verified, 3: reject',
  `rate` double DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_user`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES ('13', 'dinh', 'dinhvt', 'tiendinh595@gmail.com', null, '$2y$10$fTPm8/gcmkcBT/hVfGGgAO3UF/ON6nYk1QsxXkNpoNDZht0nJT3M6', null, '1', 'AT5TQ46TQH2IH5VJ', null, null, null, null, '', null, '2018-01-08 11:14:20', '2', '2', '0', '2017-11-30 11:30:24', '2018-01-08 15:57:16'), ('14', 'leo', 'leo', 'leo@flychicken.net', null, '$2y$10$fTPm8/gcmkcBT/hVfGGgAO3UF/ON6nYk1QsxXkNpoNDZht0nJT3M6', null, null, null, null, null, null, null, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE0LCJpc3MiOiJodHRwOi8vYXBpLnNpZXV0aGlidGMubG9jYWwvbG9naW4vY29uZmlybS9sZW9AZmx5Y2hpY2tlbi5uZXQvNzc2NGI4NThiN2RlOTFiNzkxZmIzZGY4NmU2NWIzZGEiLCJpYXQiOjE1MTQxOTM2NTAsImV4cCI6MTUxNDI4MDA1MCwibmJmIjoxNTE0MTkzNjUwLCJqdGkiOiJvVFp4SmxwVzg5WWtDV3k1In0.yDMiKtzDjkKQAc-Wyl79VHqiVW1su4DV7-R0slru1V0', null, '2017-12-25 16:20:50', '1', '0', '0', null, '2017-12-25 16:20:50'), ('15', 'dinh', 'dinhok', 'dinh@flychicken.netd', null, '$2y$10$vyerUQqKYOJzHO.KVNXOsuXbRZGEkx7Dnc1UjLfYjW6jQiaAp2JH2', null, '0', 'QG63PJND4SHXXUDN', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 11:46:36', '2017-12-26 11:46:36'), ('16', 'dadad', 'dadad', 'dadad@gmail.com', null, '$2y$10$3SFgtsTUUHkKAP4uLbL0uuRrWspMZGl.ba567D.JgS7aaI.yZ3ns6', null, '0', '6T5CVK6XRROCARJI', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 11:54:46', '2017-12-26 11:54:46'), ('17', 'dinh', 'dinh', 'dinh1@flychicken.net', null, '$2y$10$hF3xIfEwtLH4XswKEPdTTug57IQmz385Io3PLyX6G78f/I/uHCD9O', null, '0', 'YZPWFDUSRZILPXZD', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 12:00:54', '2017-12-26 12:00:54'), ('18', 'dinh2', 'dinh2', 'dinh2@flychicken.net', null, '$2y$10$hiqK9vKVq.vkXXsFx8eO2.2DRgaJ2aR7izeRyhFWZhixm0K3MIISi', null, '0', '4CZKEZQAD65NGLOM', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 12:05:01', '2017-12-26 12:05:01'), ('19', 'newProps', 'newProps', 'newProps@gmail.com', null, '$2y$10$B.7sFeec.NAVDu7OA88Ade7vfJsdGewDaFKBwpzJGZJNO3Ri1jveW', null, '0', 'WAEFKX3UTGRHLHPW', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 12:08:18', '2017-12-26 12:08:18'), ('20', 'okmain', 'okmain', 'okmain@gmail.com', null, '$2y$10$JTAJLqq5mOUAjUdlGUEvTO.7CBBJl2DjZ2gIoZVTlSgfRwKCGKpey', null, '0', 'J5MACUEYAMK4ICRI', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-26 12:09:14', '2017-12-26 12:09:14'), ('21', 'dinh', 'okokok', 'okokok@gmail.com', null, '$2y$10$6lIupSYa.kXv8p5AXd9yE.QQbjYmK0MyCmAgL3AzgrWiwX51s0JVC', null, '0', 'IGUEFSWPSODXZAES', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-28 09:08:59', '2017-12-28 09:08:59'), ('22', 'dinh', 'omg', 'omg@gmail.com', null, '$2y$10$e6limaEG4QedyUDz5755U.ZKlYCzK60MySZvwA.trFMPGL568pOPW', null, '0', 'TF6W22T4Q2YPOVL3', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-28 09:21:07', '2017-12-28 09:21:07'), ('23', 'omg2', 'omg2', 'omg2@gmail.com', null, '$2y$10$ZtVs3P4imFd9M4UV3E9QQOSKyHZiVzAoEY6fn/50YIywjFEcVjba6', null, '0', '4RBOWSRXFI7LQX7W', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-28 09:21:50', '2017-12-28 09:21:50'), ('24', 'omg3', 'omg3', 'omg3@gmail.com', null, '$2y$10$eBPPQHhRHjAYZXQ3mjRNCOdmgcDj0KHgg2Utu9CAVdr0wVqO48HIq', null, '0', 'DRKRBWQKZKXXRHUW', null, null, null, null, null, null, null, '1', '0', '0', '2017-12-28 09:23:06', '2017-12-28 09:23:06'), ('25', 'omg4', 'omg4', 'omg4@gmail.com', null, '$2y$10$Ve2dOtoYctxCJukfoWL.WuykGPcafS3PACPYUH7EKyi1WbJT3fUkW', null, '0', 'HEJJQ3JPWWE7UALG', null, null, 'f97de483e53b1e982610876f4106efd0', '2017-12-28 09:39:50', null, null, null, '1', '0', '0', '2017-12-28 09:24:40', '2017-12-28 09:24:50'), ('26', 'omg5', 'omg5', 'omg5@gmail.com', null, '$2y$10$608NCfWti2pe4SOeffREDOWjnTGSM3TUg62JgPigzwopyblLZXFIe', null, '0', 'QCF7FUORYC6OFX25', null, null, 'd5eb2324ef62d9baa495e30034da3fc1', '2017-12-28 09:41:06', null, null, null, '1', '0', '0', '2017-12-28 09:26:02', '2017-12-28 09:26:06'), ('28', null, 'tiendinhd', 'dinhvt@hip.vn', '0964329328', '$2y$10$AsmJL7/isw/WKDzwFhy8GOlL0vwrFzY7McFJ8maLyWudr1bIy3OQ2', null, '0', 'LX6B4T7U3WHXDMJL', null, null, '9c60ae9e72afccadb335eb1c8b375661', '2018-01-03 10:42:40', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI4LCJpc3MiOiJodHRwOi8vYXBpLnNpZXV0aGlidGMubG9jYWwvbG9naW4vYWN0aXZlL2Rpbmh2dEBoaXAudm4vMTc5MGU2ZDQ0MmE4YWYyN2Q4OGQzNjg4MDZhNDU3YTUiLCJpYXQiOjE1MTQ5NDk1MzcsImV4cCI6MTUxNTAzNTkzNywibmJmIjoxNTE0OTQ5NTM3LCJqdGkiOiJNMHp0bUlXdndQT2NEd0J0In0._bHANPvOoGyRYp8KInvdXi7Hb43hrzVdy8MZ9UGLsaw', null, '2018-01-03 10:18:57', '2', '1', '0', '2018-01-03 09:49:44', '2018-01-03 15:55:07'), ('33', null, 'dinhokok', 'dinh@flychicken.net', null, '$2y$10$pHMbeVFF9t5ON08/NWKV7er5/mEozVAMlTi7oeOZ99qnW9vBjCI.a', null, '0', 'FKCD5656K4GJ5MCL', null, null, 'ed2a7baf43ead88a51a83670843f9710', null, null, null, null, '1', '0', '0', '2018-01-08 16:20:23', '2018-01-08 16:20:23');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_wallet`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_wallet`;
CREATE TABLE `tbl_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `id_address_btc` varchar(255) DEFAULT NULL,
  `address_btc` varchar(100) NOT NULL,
  `total_btc` double DEFAULT '0',
  `balance_btc` double DEFAULT '0',
  `pending_btc` double DEFAULT '0',
  `freeze_btc` double DEFAULT '0',
  `traded_btc` double DEFAULT '0',
  `id_address_eth` varchar(255) DEFAULT NULL,
  `address_eth` varchar(100) NOT NULL,
  `total_eth` double DEFAULT '0',
  `balance_eth` double DEFAULT '0',
  `pending_eth` double DEFAULT '0',
  `freeze_eth` double DEFAULT '0',
  `traded_eth` double DEFAULT '0',
  `address_vnd` varchar(100) NOT NULL,
  `total_vnd` double DEFAULT '0',
  `balance_vnd` double DEFAULT '0',
  `pending_vnd` double DEFAULT '0',
  `freeze_vnd` double DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_wallet`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_wallet` VALUES ('2', '13', 'f801a658c6a4a0caa741d2f4e1cd720494208', null, '1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '5.16899', '2.71279', '-0.99', '8.1252', '0', null, 'X1cdcb49216bcff1df9cdadeb1ccf730b66720', '10', '9.7599', '0', '0.2401', '0', 'X1cdcb49216bcff1df9cdadeb1ccf730b66720', '100000000', '96498766', '0', '3501234', null, '2018-01-06 09:16:50'), ('3', '14', 'f801a658c6a4a0caa741d2f4e1cd720494208t', null, 't1LPWVdFK1TdNifoqSyzNK7ZSoA6j8sRrA7', '2.0196', '0.8386', '-0.0495', '-0.869', '0', null, 'X1cdcb4t9216bcff1df9cdadeb1ccf730b66720', '0.099', '0.099', '0', '-0.1', '0', 'tX1cdcb49216bcff1df9cdadeb1ccf730b66720', '0', '0', '0', '0', null, '2017-12-25 18:02:56'), ('4', '15', '19c5ac6ff7db54bafcd60b7c0d32019b54729', null, '17ejXUe6ZsaKMcyZQ9CCKTphPyToXEqWpY', '0', '0', '0', '0', '0', null, 'X44fe1ae3d5c4bbd03b3f824ebf44870714831', '0', '0', '0', '0', '0', 'X44fe1ae3d5c4bbd03b3f824ebf44870714831', '0', '0', '0', '0', '2017-12-26 11:46:37', '2017-12-26 11:46:37'), ('5', '16', '9a54f56ecaae8f860111fabf1d0ebe9616365', null, '1GmAJcYA43KZJr2aaeguoAsHwpoxSZBmby', '0', '0', '0', '0', '0', null, 'Xa57cf13aef6133992b716af52096ab8853949', '0', '0', '0', '0', '0', 'Xa57cf13aef6133992b716af52096ab8853949', '0', '0', '0', '0', '2017-12-26 11:54:47', '2017-12-26 11:54:47'), ('6', '17', 'f2405c5d191a8eddcc4b1b100dd1598b23088', null, '1PjQLtoiaGbtZ4Wg8Qhc2Z63jTqoqosnyX', '0', '0', '0', '0', '0', null, 'X6c7ac70191b034619e94a2f88741d74772180', '0', '0', '0', '0', '0', 'X6c7ac70191b034619e94a2f88741d74772180', '0', '0', '0', '0', '2017-12-26 12:00:55', '2017-12-26 12:00:55'), ('7', '18', '0fe103d8a0f92906a3b9dab1abe2dc4522160', null, '13dttHmBKFjUj96MGCcgqSdwU7JjHXwj7M', '0', '0', '0', '0', '0', null, 'X4c0cffaa844c608ebe1f4814f6cccf2c1070', '0', '0', '0', '0', '0', 'X4c0cffaa844c608ebe1f4814f6cccf2c1070', '0', '0', '0', '0', '2017-12-26 12:05:03', '2017-12-26 12:05:03'), ('8', '19', '5347e5a58825ceb11f55c99af0a528f047253', null, '12s9JUHvUmjSRRedcsSVcWRhJx3yvw3tUD', '0', '0', '0', '0', '0', null, 'X5e48a75d01f71fca1f7c55261d96298a50601', '0', '0', '0', '0', '0', 'X5e48a75d01f71fca1f7c55261d96298a50601', '0', '0', '0', '0', '2017-12-26 12:08:18', '2017-12-26 12:08:18'), ('9', '20', 'dffcd2f1376390beaf8bd596bfb279bc85456', null, '1DCvwXF1gdWb7YemCXC46qwKcdrCyEE1LV', '0', '0', '0', '0', '0', null, 'X9e31be694eba27c5ad97f8df1d7d6fc037532', '0', '0', '0', '0', '0', 'X9e31be694eba27c5ad97f8df1d7d6fc037532', '0', '0', '0', '0', '2017-12-26 12:09:15', '2017-12-26 12:09:15'), ('10', '21', '84eb5668d08dd100427f2f21ffe9645f19930', null, '1FjB5qGH7TwQBQETLYc7e9DKJqPrGgUKEr', '0', '0', '0', '0', '0', null, 'X9677f7c94474bfd1fd37b086eb62005d9023', '0', '0', '0', '0', '0', 'X9677f7c94474bfd1fd37b086eb62005d9023', '0', '0', '0', '0', '2017-12-28 09:08:59', '2017-12-28 09:08:59'), ('11', '22', 'd4d233a5216816cc108f93e138d25d3982685', null, '17FLtEnHCmRUoF2QyMnAYubsvgoNzDj56i', '0', '0', '0', '0', '0', null, 'Xcb670aa26e4c54fb83afd5e2ac45a7d872103', '0', '0', '0', '0', '0', 'Xcb670aa26e4c54fb83afd5e2ac45a7d872103', '0', '0', '0', '0', '2017-12-28 09:21:07', '2017-12-28 09:21:07'), ('12', '23', 'f75ff380ced81297e063937c931fe8cc7714', null, '1K1uB7noweFtocts6VNDgqH16kMys9CjUj', '0', '0', '0', '0', '0', null, 'Xde2b848a4c7fe5ea236e1ea8975a009b23318', '0', '0', '0', '0', '0', 'Xde2b848a4c7fe5ea236e1ea8975a009b23318', '0', '0', '0', '0', '2017-12-28 09:21:51', '2017-12-28 09:21:51'), ('13', '24', '5c9d6707e0b07ca1c413e255459cfe0529846', null, '12qkTzGuXzidE2mX4KXwaki7qBp8CJXyDw', '0', '0', '0', '0', '0', null, 'Xe9631816a2aecd89377c19ce74a1bbdd41883', '0', '0', '0', '0', '0', 'Xe9631816a2aecd89377c19ce74a1bbdd41883', '0', '0', '0', '0', '2017-12-28 09:23:07', '2017-12-28 09:23:07'), ('14', '25', 'a47af93d5b6a9c6a05af0d78c2f0634497601', null, '17UYqSBZDJ6M53oxbaHQLsWgXCi9U4UFpv', '0', '0', '0', '0', '0', null, 'Xe237b1431cd2a81e143a35faa76d197763055', '0', '0', '0', '0', '0', 'Xe237b1431cd2a81e143a35faa76d197763055', '0', '0', '0', '0', '2017-12-28 09:24:40', '2017-12-28 09:24:40'), ('15', '26', '9e487727a2ceb3cef70be5acfa1ec60f62581', null, '1GeH9QbW63bVboBLEjT8QihrmkXrB3PkEK', '0', '0', '0', '0', '0', null, 'X5ff63f5b640e92a5cc8c982a584622f857862', '0', '0', '0', '0', '0', 'X5ff63f5b640e92a5cc8c982a584622f857862', '0', '0', '0', '0', '2017-12-28 09:26:02', '2017-12-28 09:26:02'), ('16', '27', 'dae443d5ce9a3cd85093ff96e576a6f775509', null, '147YHbmYUxNaCMz4CeLdFjDSj7cCX4WW2Y', '0', '0', '0', '0', '0', null, 'Xeec10b849e4cd4b29b1358201053760776915', '0', '0', '0', '0', '0', 'Xeec10b849e4cd4b29b1358201053760776915', '0', '0', '0', '0', '2018-01-03 09:46:45', '2018-01-03 09:46:45'), ('17', '28', '442d414661e16e1a2f8d75c245e92db470480', null, '13E9Tkp72fwf7um6XML8VMKDL1onC46FNn', '0', '0', '0.0297', '0', '0', null, 'X060d9ea9aa5ad47239456c907032847671733', '0', '0', '0', '0', '0', 'X060d9ea9aa5ad47239456c907032847671733', '0', '0', '0', '0', '2018-01-03 09:49:44', '2018-01-03 13:47:53'), ('18', '31', '3df599aa1cfd04e9228a3ea8dad4cf7853568', null, '0x41f62bFe81753fD99801C971543c3d87ccf0599c', '0', '0', '0', '0', '0', null, '0x41f62bFe81753fD99801C971543c3d87ccf0599c', '0', '0', '0', '0', '0', 'X6ad751f682f3ddebe00a3d266cfa3e8127265', '0', '0', '0', '0', '2018-01-08 16:00:06', '2018-01-08 16:00:06'), ('19', '32', 'ecb7558b76be6dc5abf075c7f196c03f54725', null, '1PCTFspDykHZukonvSy4wmS1oN7uS6ENHT', '0', '0', '0', '0', '0', null, '0xae7e29070FBB5c2fA07A4F5675933044EeEc9e10', '0', '0', '0', '0', '0', 'X851474e741b5dc9aa59d6189b4863ad314185', '0', '0', '0', '0', '2018-01-08 16:12:30', '2018-01-08 16:12:30'), ('20', '33', 'b117bba1226671a2edbb243c67459a5a24391', 'b45e68da-6977-598c-b691-2d11f9b94d82', '13YCeudM1gisWQ9S3b51P5GipCv1pWy2sN', '0', '0', '0', '0', '0', '6c99746f-5f80-509c-bac2-82c7cf029e75', '0xd11411a5cd3762b82A2fDfC265c6a8A1436F1312', '0', '0', '0', '0', '0', 'Xe02bc4f8c3e0374cb9660ff0d9a515fc2498', '0', '0', '0', '0', '2018-01-08 16:20:25', '2018-01-08 16:20:25');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
