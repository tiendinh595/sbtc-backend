<?php

Route::get('/t', function () {
    throw new \Exception('new bug');
});

Route::group(['domain' => config('app.domain_frontend')], function () {
    Route::post('/private', 'PrivateController@handleDeposit');
    Route::any('/callback/{user_id}/{currency}', function (\Illuminate\Http\Request $request, $user_id, $currency) {
        Log::info('-------callback-----');
        Log::info($user_id . ' - ' . $currency);
        Log::info($request->all());
        Log::info($request->getQueryString());
        Log::info('-------end callback-----');

        return 'done';
    })->name('callback');
    Route::get('/callback-OAuth', function (\Illuminate\Http\Request $request) {
        Log::info('-------callback OAuth-----');
        Log::info($request->all());
        Log::info($request->getQueryString());
        Log::info('-------end callback OAuth-----');
    });
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('/{path?}', 'Frontend\HomeController@getIndex')->name('index')->where(['path' => '.*']);

});

Route::group(['domain' => config('app.domain_api')], function () {
    Route::get('/ref/{username}');

    Route::group(['prefix' => 'profile'], function () {
        Route::put('/UpdateAuthenticator', 'Api\UserController@postUpdateAuthenticator');
        Route::get('/history-login', 'Api\UserController@getHistoryLogin');
        Route::get('/{username?}', 'Api\UserController@getSummary');
        Route::get('/{username}/feedback', 'Api\UserController@getFeedback');
        Route::post('/verify-phone', 'Api\UserController@postVerifyPhone');
        Route::post('/add-phone', 'Api\UserController@postAddPhone');
        Route::post('/send-code-verify-phone', 'Api\UserController@postSendCodeVerifyPhone');
    });

    Route::any('/logout', 'Api\UserController@getLogout');
    Route::any('/token', 'Api\UserController@getValidateToken');
    Route::post('/login', 'Api\UserController@postLoginEmail');
    Route::get('/profile-extra', 'Api\UserController@getProfileExtra');
    Route::post('/verify/upload', 'Api\UserController@postVerify');
    Route::post('/verify/confirm', 'Api\UserController@postConfirmVerify');
    Route::post('/login/confirm/{email}/{token}', 'Api\UserController@postConfirmLogin');
    Route::post('/login/active/{email}/{token}', 'Api\UserController@postActive');
    Route::post('/register', 'Api\UserController@postRegister');
    Route::post('/callback/{secret}', 'PrivateController@getCallback')->name('callback');
    Route::get('/referrals', 'Api\UserController@getReferrals');
    Route::put('/referrals/{id}', 'Api\UserController@getRewardRef');

    Route::group(['prefix' => 'offer'], function () {
        Route::get('/my/{type?}', 'Api\OfferController@getAllMyOffers');
        Route::post('/', 'Api\OfferController@postCreate');
        Route::get('/sell/{type}/{username?}', 'Api\OfferController@getSellOffer');
        Route::get('/buy/{type}/{username?}', 'Api\OfferController@getBuyOffer');
        Route::get('/{id}', 'Api\OfferController@getDetail');
        Route::get('/{id}/edit', 'Api\OfferController@getEdit');
        Route::put('/{id}', 'Api\OfferController@putUpdateOffer');
    });

    Route::group(['prefix' => 'trade'], function () {
        Route::get('/my-list/{status}', 'Api\TradeController@getListTrade');
        Route::post('create', 'Api\TradeController@postCreate');
        Route::get('{id}', 'Api\TradeController@getDetail');
        Route::put('/{id}/update-state', 'Api\TradeController@putUpdateState');
        Route::put('/{id}/time-up', 'Api\TradeController@putUpdateTimeUp');
        Route::post('/{id}/upload-receipt', 'Api\TradeController@postUploadReceipt');
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::post('/', 'Api\FeedbackController@postFeedback');
    });

    Route::group(['prefix' => 'deal'], function () {
        Route::post('/', 'Api\DealController@postDeal');
        Route::post('/cancel/{id}', 'Api\DealController@postReject');
        Route::post('/approve/{id}', 'Api\DealController@postApprove');
        Route::get('/{status}', 'Api\DealController@getAllDeal');
    });
    Route::group(['prefix' => 'system'], function () {
        Route::get('/', 'Api\SystemController@getDataGlobal');
    });
    Route::group(['prefix' => 'mail'], function () {
        Route::get('/mark-read/{id}', 'Api\MailController@getMarkRead');
        Route::post('/delete', 'Api\MailController@postDeleteMultiple');
        Route::post('/send', 'Api\MailController@postSendMail');
        Route::get('/{type}', 'Api\MailController@getAll');
    });
    Route::group(['prefix' => 'wallet'], function () {
        Route::get('/transaction/{id}/detail', 'Api\WalletController@getDetailTransaction');
        Route::post('/transaction/{id}/status', 'Api\WalletController@postUpdateStatusTransaction');
        Route::post('/transaction/{id}/receipt', 'Api\WalletController@postUploadReceiptTransaction');
        Route::get('/transaction/{coin}', 'Api\WalletController@getTransactions');
        Route::post('/withdraw', 'Api\WalletController@postWithdraw');
        Route::post('/init-deposit-fiat', 'Api\WalletController@postInitDepositFiat');
    });
    Route::group(['prefix' => 'bank'], function () {
        Route::get('/', 'Api\BankController@getAllBanks');
        Route::post('/', 'Api\BankController@postAddNew');
        Route::delete('/{id}', 'Api\BankController@deleteBank');
    });
});