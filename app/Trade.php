<?php

namespace App;

use App\Utils\Helper;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Trade
 *
 * @property int $id
 * @property string|null $buyer_username
 * @property string|null $seller_username
 * @property string|null $from_address
 * @property string|null $to_address
 * @property int $offer_id
 * @property int|null $type_trade 1: sell, 2: buy
 * @property float|null $seller_sending_coin_amount
 * @property float|null $buyer_receiving_coin_amount
 * @property float|null $seller_receiving_vnd_amount amount btc, eth to trade
 * @property float|null $buyer_sending_vnd_amount
 * @property float|null $fee total fee by
 * @property string|null $payment_method
 * @property int|null $bank_code
 * @property string|null $bank_number
 * @property string|null $bank_account_name
 * @property string|null $token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $bank_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade tradeBuy()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade tradeSell()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBankNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBuyerReceivingCoinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBuyerSendingVndAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereBuyerUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereFromAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereSellerReceivingVndAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereSellerSendingCoinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereSellerUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereToAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereTypeMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereTypeTrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $payment_time
 * @property string|null $payment_memo
 * @property string|null $receipt
 * @property int|null $action_buyer 1: delivered, 2:cancel
 * @property int|null $action_seller 1: received, 2: not received
 * @property bool $time_up
 * @property bool $outgoing
 * @property-read \App\User|null $buyer
 * @property-read mixed $payment_method_name
 * @property-read \App\Offer $offer
 * @property-read \App\User|null $seller
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereActionBuyer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereActionSeller($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereOutgoing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade wherePaymentMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade wherePaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereReceipt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Trade whereTimeUp($value)
 */
class Trade extends Model
{
    protected $table = 'tbl_trade';

    static $type = [
        'sell' => 1,
        'buy' => 2
    ];

    static $status = [
        'waiting' => 1,
        'done' => 2,
        'user_cancel' => 3,
        'bot_cancel' => 4,
        'dispute' => 5
    ];

    static public $status_transfer = [
        'init' => 0,
        'pending' => 1,
        'success' => 2,
        'reject' => 3,
        'cancel' => 4,
        'error' => 5,
    ];

    static $type_money = [
        'btc' => 1,
        'eth' => 2
    ];

    static $action_seller = [
        'received' => 1,
        'not-received' => 2
    ];

    static $action_buyer = [
        'delivered' => 1,
        'cancel' => 2,
        'dispute' => 3
    ];

    static $status_approve = [
        'pending' => 0,
        'approved' => 1
    ];

    static public $status_job = [
        'pending' => 1,
        'started' => 2
    ];

    protected $fillable = [
        'id', 'buyer_username', 'seller_username', 'from_address', 'to_address', 'offer_id', 'type_money', 'price_per_coin', 'price_per_coin_before_fee', 'seller_sending_coin_amount', 'buyer_receiving_coin_amount', 'seller_receiving_vnd_amount', 'buyer_sending_vnd_amount', 'fee', 'payment_method', 'payment_time', 'bank_code', 'bank_number', 'bank_account_name', 'payment_memo', 'receipt', 'time_up','outgoing', 'action_buyer', 'action_seller', 'token', 'status',
        'status_transfer_coin', 'status_job', 'is_approved', 'confirmations', 'extra', 'hash', 'message'
    ];

    protected $casts = [
        'time_up' => 'boolean',
        'outgoing' => 'boolean',
    ];

    protected $appends = ['bank_name', 'payment_method_name', 'status_name'];

    function offer() {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    function scopeTradeSell($query)
    {
        return $query->where('type_offer', self::$type['sell']);
    }

    function scopeTradeBuy($query)
    {
        return $query->where('type_offer', self::$type['buy']);
    }

    function setSellerSendingCoinAmountAttribute($val) {
        $this->attributes['seller_sending_coin_amount'] = Helper::formatCoin($val);
    }

    function setBuyerReceivingCoinAmountAttribute($val) {
        $this->attributes['buyer_receiving_coin_amount'] = Helper::formatCoin($val);
    }

    function setFeeAttribute($val) {
        $this->attributes['fee'] = Helper::formatCoin($val);
    }

    function getBankNameAttribute($val)
    {
        $banks = config('system.bank');
        return isset($banks[$this->bank_code]) ? $banks[$this->bank_code] : 'Ví VND';
    }

    function getPaymentMethodNameAttribute($value)
    {
        return $this->attributes['payment_method'] == 1 ? 'chuyển khoản ngân hàng' : 'nộp tiền mặt vào tài khoản ngân hàng';
    }

    function seller()
    {
        return $this->belongsTo(User::class, 'seller_username', 'username')->select(['id', 'username', 'email'])->with('wallet');
    }

    function buyer()
    {
        return $this->belongsTo(User::class, 'buyer_username', 'username')->select(['id', 'username', 'email'])->with('wallet');
    }

    function getReceiptAttribute($val) {
        return $val != '' ? url('/storage/upload/'.$val) : null;
    }

    function feedback() {
        return $this->hasMany(Feedback::class, 'trade_id');
    }

    function getStatusNameAttribute($value) {
        $status_name = ['đang giao dịch', 'đã hoàn tất', 'đã huỷ', 'đã huỷ', 'tranh chấp'];
        return $status_name[$this->status-1];
    }

    function validateToken()
    {
        try {
            $payload = \Crypt::decrypt($this->token);
            return (
                $payload['buyer_username'] == $this->buyer_username
                && $payload['seller_username'] == $this->seller_username
                && $payload['from_address'] == $this->from_address
                && $payload['to_address'] == $this->to_address
                && $payload['offer_id'] == $this->offer_id
                && $payload['type_money'] == $this->type_money
                && $payload['price_per_coin'] == $this->price_per_coin
            );
        } catch (\Exception $exception){
            return false;
        }
    }
}
