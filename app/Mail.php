<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_mail';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 'user_id', 'title', 'content', 'status', 'type'
    ];

    public $appends = [
        'date',
        'time'
    ];

    static public $type = [
        'in' => 1,
        'out' => 2
    ];

    function getDateAttribute($value) {
        return date('Y/m/d', strtotime($this->created_at));
    }

    function getTimeAttribute($value) {
        return date('H:i', strtotime($this->created_at));
    }

}
