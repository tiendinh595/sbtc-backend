<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryLogin extends Model
{
    protected $table = 'tbl_history_login';

    protected $fillable = ['id', 'user_id', 'country', 'city', 'ip'];


}
