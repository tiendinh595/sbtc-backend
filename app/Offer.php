<?php

namespace App;

use App\Utils\Helper;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Offer
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $address_wallet
 * @property float $rate_vnd_usd
 * @property float $rate_usd_coin
 * @property float $price_per_coin
 * @property float $price_per_coin_before_fee
 * @property float $max_amount
 * @property float $min_amount
 * @property float|null $current_amount
 * @property int $payment_time
 * @property int $payment_method 1: chuyen khoan, 2 nop tien mat
 * @property int|null $bank_code
 * @property string|null $bank_number
 * @property string|null $bank_account_name
 * @property string|null $token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $bank_name
 * @property-read mixed $payment_method_name
 * @property-read mixed $price_per_coin_before_fee_format
 * @property-read mixed $price_per_coin_format
 * @property-read mixed $rate_usd_coin_format
 * @property-read mixed $rate_vnd_usd_format
 * @property-read mixed $title
 * @property-read mixed $title_html
 * @property-read \App\Offer $source
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer bTC()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer eTH()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer offerBuy()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer offerSell()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereAddressWallet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereBankNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereCurrentAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereMaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereMinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer wherePaymentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer wherePricePerCoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer wherePricePerCoinBeforeFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereRateUsdCoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereRateVndUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereTypeMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereTypeOffer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereUsername($value)
 * @mixin \Eloquent
 * @property int $type_offer 1: sell, 2: buy
 * @property bool $outgoing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer done()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer open()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer reject()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutgoing($value)
 */
class Offer extends Model
{
    protected $table = 'tbl_offer';

    protected $primaryKey = 'id';

    static $type = [
        'sell' => 1,
        'buy' => 2
    ];

    static $type_money = [
        'btc' => 1,
        'eth' => 2
    ];

    static $type_money_name = [
        'btc' => 'btc',
        'eth' => 'eth'
    ];

    static $currency_name = [
        1 => 'btc',
        2 => 'eth'
    ];

    static $status = [
        'open' => 1,
        'done' => 2,
        'reject' => 3,
        'stopped' => 4,
        'deleted' => 5
    ];

    protected $fillable = [
        'id', 'username', 'address_wallet', 'rate_vnd_usd', 'rate_usd_coin', 'price_per_coin', 'price_per_coin_before_fee', 'type_offer', 'type_money', 'max_amount', 'min_amount', 'current_amount', 'payment_time', 'payment_method', 'bank_code', 'bank_number', 'bank_account_name', 'outgoing', 'allow_not_verify', 'token', 'status', 'deal_id', 'is_special'
    ];

    protected $casts = [
        'outgoing' => 'boolean',
        'allow_not_verify' => 'boolean',
        'is_special' => 'boolean'
    ];

    protected $appends = ['title', 'title_html', 'rate_vnd_usd_format', 'rate_usd_coin_format', 'price_per_coin_format', 'price_per_coin_before_fee_format', 'payment_method_name', 'bank_name', 'status_name'];

    function user()
    {
        return $this->belongsTo(User::class, 'username', 'username')->with('wallet');
    }

    function scopeOfferSell($query)
    {
        return $query->where('type_offer', self::$type['sell']);
    }
    function scopeSpecial($query)
    {
        return $query->where('is_special', 1);
    }

    function scopeOfferBuy($query)
    {
        return $query->where('type_offer', self::$type['buy']);
    }

    function scopeOpen($query)
    {
        return $query->where('status', self::$status['open']);
    }

    function scopeOriginal($query)
    {
        return $query->where('deal_id', '=', 0);
    }

    function scopeSystem($query)
    {
        return $query->where('is_special', '=', 1);
    }

    function scopeDone($query)
    {
        return $query->where('status', self::$status['done']);
    }

    function scopeReject($query)
    {
        return $query->where('status', self::$status['reject']);
    }

    function scopeBTC($query)
    {
        return $query->where('type_money', self::$type_money['btc']);
    }


    function scopeETH($query)
    {
        return $query->where('type_money', self::$type_money['eth']);
    }

    function source()
    {
        return $this->belongsTo(Offer::class, 'offer_source_id');
    }

    function getTitleAttribute($val)
    {
        return ($this->type_offer == self::$type['sell'] ? 'mua' : 'bán') . ' ' . ($this->type_money == self::$type_money['btc'] ? 'bitcoin' : 'ethereum') . ' qua ' . $this->bank_name . ' ' . ($this->type_offer == self::$type['sell'] ? 'từ' : 'cho') . ' ' . $this->username;
    }

    function getTitleHtmlAttribute($val)
    {
        return ($this->type_offer == self::$type['sell'] ? 'mua' : 'bán') . ' ' . ($this->type_money == self::$type_money['btc'] ? 'bitcoin' : 'ethereum') . ' qua ' . $this->bank_name . ' ' . ($this->type_offer == self::$type['sell'] ? 'từ' : 'cho') . ' <b>' . $this->username . '</b>';
    }

    function getRateVndUsdFormatAttribute($val)
    {
        return Helper::formatUSD($this->rate_vnd_usd);
    }

    function getRateUsdCoinFormatAttribute($val)
    {
        return Helper::formatUSD($this->rate_usd_coin);
    }

    function getPricePerCoinFormatAttribute($val)
    {
        return Helper::formatVND($this->price_per_coin);
    }

    function getPricePerCoinBeforeFeeFormatAttribute($val)
    {
        return Helper::formatVND($this->price_per_coin_before_fee);
    }

    function setRateVndUsdAttribute($value)
    {
        return $this->attributes['rate_vnd_usd'] = Helper::formatVND($value);
    }

    function setRateUsdCoinAttribute($value)
    {
        return $this->attributes['rate_usd_coin'] = Helper::formatUSD($value);
    }

    function setPricePerCoinAttribute($value)
    {
        return $this->attributes['price_per_coin'] = Helper::formatVND($value);
    }

    function setPricePerCoinBeforeFeeAttribute($value)
    {
        return $this->attributes['price_per_coin_before_fee'] = Helper::formatVND($value);
    }

    function setMinAmountAttribute($value)
    {
        return $this->attributes['min_amount'] = Helper::formatCoin($value);
    }

    function setMaxAmountAttribute($value)
    {
        return $this->attributes['max_amount'] = Helper::formatCoin($value);
    }
    function setCurrentAmountAttribute($value)
    {
        return $this->attributes['current_amount'] = Helper::formatCoin($value);
    }
    function getPaymentMethodNameAttribute($value)
    {
        return $this->attributes['payment_method'] == 1 ? 'chuyển khoản ngân hàng' : 'nộp tiền mặt vào tài khoản ngân hàng';
    }
    function getBankNameAttribute($val)
    {
        $banks = config('system.bank');
        return isset($banks[$this->bank_code]) ? $banks[$this->bank_code] : 'Ví VND';
    }

    function getStatusNameAttribute($value)
    {
        $status_name = ['đang mở', 'đã hoàn thành', 'bị từ chối', 'đã đóng', 'đã xoá'];
        return ucfirst($status_name[$this->status - 1]);
    }
}
