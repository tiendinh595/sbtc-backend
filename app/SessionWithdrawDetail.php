<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionWithdrawDetail extends Model
{
    protected $table = 'tbl_session_withdraw_detail';

    protected $fillable = [
        'id',
        'session_withdraw_id',
        'transaction_id',
        'extra',
        'status',
        'is_sent'
    ];

    static $status = [
        'init' => 0,
        'started' => 1,
        'success' => 2,
        'reject' => 3,
        'failure' => 4,
    ];

    static $is_sent = [
        'init' => 0,
        'sent' => 1,
        'crash' => 2
    ];
}
