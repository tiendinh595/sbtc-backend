<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMailVerifyPhone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $link_home = 'http://'.config('app.domain_frontend');
        \Mail::send('mail.verify-phone', array('name' => $this->user->username, 'email' => $this->user->email, 'code' => $user->phone_status, 'link_home'=>$link_home), function ($message) use ($user) {
            $message->to($user->email, $user->username)->subject('Xác thực số điên thoại SieuThiBTC');
        });
    }
}
