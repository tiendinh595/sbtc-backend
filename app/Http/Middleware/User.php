<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    public function handle($request, Closure $next)
    {

        if(!auth()->guard('users')->check())
            return redirect()->route('login');
        return $next($request);
    }
}
