<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class Admin
{
    public function handle($request, Closure $next)
    {

        if(!auth()->guard('admin')->check())
            return redirect()->route('inside-login');
        return $next($request);
    }
}
