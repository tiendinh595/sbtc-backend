<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Support\Str;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://api.sieuthibtc.local/*',
        'https://api.sieuthibtc.com/*',
        '/private'
    ];

    protected function inExceptArray($request)
    {

        foreach ($this->except as $except){
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }

            if (Str::is($except, $request->url())) {
                return true;
            }
        }

        return false;
    }
}
