<?php

namespace App\Http\Controllers\Backend;

use App\Deposit;
use App\History;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    function getIndex(Request $request)
    {
        $list = User::orderBy('id', 'DESC');

        if ($request->get('name'))
            $list = $list->where('name', 'like', "%{$request->get('name')}%")->orWhere('username', 'like', "%{$request->get('name')}%");

        $list = $list->paginate(15);

        return view('backend.user.index', ['list' => $list]);
    }

    function getDetail(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $histories = $user->history()->orderBy('id', 'DESC')->limit(10)->get();
        $deposits = $user->deposit()->orderBy('id', 'DESC')->limit(10)->get();
        $invoices = $user->invoice()->orderBy('id', 'DESC')->limit(10)->get();
        $constants = config('constant');

        if ($request->ac) {
            switch ($request->ac) {
                case 'block':
                    $user->update(['status' => 0]);
                    break;
                case 'active':
                    $user->update(['status' => 1]);
                    break;
            }
            return redirect('/inside/user/' . $id);
        }

        return view('backend.user.detail', ['user' => $user, 'histories' => $histories, 'deposits' => $deposits, 'invoices' => $invoices, 'constants' => $constants]);
    }

    function postDetail(Request $request, $id)
    {
        Validator::make($request->all(), [
            'amount' => 'required|integer|min:1',
            'transaction_type' => 'required',
            'method' => 'required',
            'type' => 'required',
        ], [
            'required' => ':attribute không được để rỗng',
            'integer' => ':attribute phải là số',
            'min' => ':attribute phải lớn hơn :min'
        ], [
            'amount' => 'Số tiền',
            'transaction_type' => 'Loại giao dịch',
            'method' => 'Hình thức thay đổi số dư',
            'type' => 'Loại',
        ])->validate();

        $user = User::findOrFail($id);
        if ($request->transaction_type == 'type_deposit') {
            $transaction = new Deposit();
            $transaction_type = 1;
        } else if ($request->transaction_type == 'type_invoice') {
            $transaction = new Invoice();
            $transaction_type = 2;
        } else {
            $request->session()->flash('status', ['class' => 'danger', 'msg' => 'Loại giao dịch không hợp lệ']);
            return redirect()->back();
        }

        DB::transaction(function () use ($transaction, $id, $request, $transaction_type, $user) {

            $transaction->user_id = $id;
            $transaction->type = $request->input('type');
            $transaction->amount = $request->input('amount');
            $transaction->detail = $request->input('detail');
            $transaction->save();

            $history = new History();
            $history->user_id = $id;
            $history->transaction_id = $transaction->id;
            $history->transaction_type = $transaction_type;
            $history->pre_amount = $user->current_money;
            if ($request->transaction_type == 'type_deposit')
                $after_amount = $user->current_money+$request->input('amount');
            else
                $after_amount = $user->current_money-$request->input('amount');

            $history->after_amount = $after_amount;
            $history->amount = $request->input('amount');
            $history->save();

            $user->current_money = $after_amount;
            $user->save();

            $request->session()->flash('status', ['class' => 'success', 'msg' => 'Thay đổi số dư thành công']);

        });
        return redirect()->back();
    }

    function getDelete(Request $request) {
        if(User::where('id', $request->get('id'))->delete()) {
            return [
                'status' => 'success',
                'msg' => 'Xoá Thành viên thành công',
                'title' => 'Thành công',
            ];
        }
        return [
            'status' => 'error',
            'msg' => 'Không thể xoá vui lòng thử lại',
            'title' => 'Lỗi',
        ];
    }
}
