<?php

namespace App\Http\Controllers\Backend;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    function getIndex(){
        return view('backend.user.login');
    }

    function postIndex(Request $request){
        $this->validate($request, [
           'email' => 'required|email',
           'password' => 'required'
        ]);
        if (!Auth::guard('admin')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')], $request->get('remember', 0)==1)) {
            $request->session()->flash('alert', ['class' => 'danger', 'msg' => 'Sai tên đăng nhập hoặc mật khẩu']);
            return redirect()->back()->withInput();
        }
        return redirect()->route('dashboard');
    }
}
