<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    function getIndex(){
        $list = Permission::all()->groupBy('group');
        return view('backend.permission.index', [
            'list' => $list
        ]);
    }

    function getAdd(){
        return view('backend.permission.form-add');
    }

    function postAdd(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'performs' => 'required'
        ]);
        $name = strtolower($request->get('name'));
        try {
            DB::beginTransaction();
            foreach ($request->get('performs') as $perform) {
                Permission::create([
                    'name' => $perform . ' ' . $name,
                    'group' => $name,
                    'guard_name' => 'admin',
                ]);
            }
            DB::commit();
            return redirect()->back()->with('alert', ['class' => 'success', 'msg' => 'Thêm quyền thành công']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['messagge' => $e->getMessage()]);
        }
    }

    function getEdit(Request $request, $id){
        $permision = Permission::where('group', $id)->get();
        $performs = array();
        foreach ($permision as $item){
            $performs[] = explode(' ', $item->name)[0];
        }
        return view('backend.permission.form-edit', [
            'id' => $id,
            'performs' => $performs,
        ]);
    }

    function postEdit(Request $request, $id){
        $this->validate($request, [
            'performs' => 'required'
        ]);
        try {
            $permision = Permission::where('group', $id)->get();
            $performs = array();
            foreach ($permision as $item){
                $performs[] = explode(' ', $item->name)[0];
            }
            DB::beginTransaction();
            foreach (Admin::$permission_performs as $perform => $name) {
                $permision = Permission::where('name', $perform . ' ' . $id)->first();
                if(!in_array($perform, $request->get('performs'))){
                    if($permision) $permision->delete();
                }else{
                    if(!$permision){
                        Permission::create([
                            'name' => $perform . ' ' . $id,
                            'group' => $id,
                            'guard_name' => 'admin',
                        ]);
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('alert', ['class' => 'success', 'msg' => 'Chỉnh sửa quyền thành công']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['messagge' => $e->getMessage()]);
        }
    }

    function getDelete(Request $request){
        if(Permission::where('group', $request->get('id'))->delete()) {
            return [
                'status' => 'success',
                'msg' => 'Xoá phân quyền thành công',
                'title' => 'Thành công',
            ];
        }
        return [
            'status' => 'error',
            'msg' => 'Không thể xoá vui lòng thử lại',
            'title' => 'Lỗi',
        ];
    }
}
