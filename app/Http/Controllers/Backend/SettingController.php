<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 10/5/17
 * Time: 11:01 PM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Utils\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function getDeposit()
    {
        $settings = Setting::readSetting();
        return view('backend.setting.deposit', ['settings' => $settings]);
    }

    public function postDeposit(Request $request)
    {
        $config = $request->deposit;
        if ($request->new_card) {
            foreach ($request->new_card['key'] as $key => $value) {
                if (trim($value) != '' && trim($request->new_card['value'][$key]) != '')
                    $config['card'][trim($value)] = trim($request->new_card['value'][$key]);
            }
        }

        if ($request->new_sms) {
            foreach ($request->new_sms['key'] as $key => $value) {
                if (trim($value) != '' && trim($request->new_sms['value'][$key]) != '')
                    $config['sms'][trim($value)] = trim($request->new_sms['value'][$key]);
            }
        }

        Setting::updateSetting('deposit', $config);
        $request->session()->flash('alert', ['class' => 'success', 'msg' => 'Cập nhật cài đặt thành công']);
        return back();
    }

    public function getDiscount()
    {
        $settings = Setting::readSetting();
        return view('backend.setting.discount', ['settings' => $settings]);
    }

    public function postDiscount(Request $request)
    {
        $config = $request->discount;
        if ($request->new_card) {
            foreach ($request->new_card['key'] as $key => $value) {
                if (trim($value) != '' && trim($request->new_card['value'][$key]) != '')
                    $config['card'][trim($value)] = trim($request->new_card['value'][$key]);
            }
        }

        if ($request->new_toup) {
            foreach ($request->new_toup['key'] as $key => $value) {
                if (trim($value) != '' && trim($request->new_toup['value'][$key]) != '')
                    $config['toup'][trim($value)] = trim($request->new_toup['value'][$key]);
            }
        }

        Setting::updateSetting('discount', $config);
        $request->session()->flash('alert', ['class' => 'success', 'msg' => 'Cập nhật cài đặt thành công']);
        return back();
    }

    public function getBank()
    {
        $settings = Setting::readSetting();
        return view('settings.bank', ['settings' => $settings]);
    }

    public function postBank(Request $request)
    {
        $config = $request->bank;
        Setting::updateSetting('bank', $config);
        $request->session()->flash('alert', ['class' => 'success', 'msg' => 'Cập nhật cài đặt thành công']);
        return back();
    }

}
