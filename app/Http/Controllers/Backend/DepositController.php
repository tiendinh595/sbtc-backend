<?php

namespace App\Http\Controllers\Backend;

use App\Deposit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepositController extends Controller
{
    function getIndex(Request $request)
    {
        $deposits = Deposit::orderBy('id', 'DESC')->with('user');

        if ($request->get('user_id'))
            $deposits = $deposits->where('user_id', $request->get('user_id'));

        $deposits = $deposits->paginate(15);

        return view('backend.history.deposit', ['list' => $deposits]);
    }
}
