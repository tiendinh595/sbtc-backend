<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class AdministratorController extends Controller
{
    function getIndex(Request $request){
        $active = $request->get('active', '');
        $key = $request->get('key', '');
        $list = Admin::orderBy('id', 'DESC');
        if($active){
            $list = $list->where('active', $active);
        }
        if($key){
            $list = $list->where(function ($q) use ($key) {
                return $q->where('name', 'LIKE', '%'.$key.'%')
                    ->orWhere('email', 'LIKE', '%'.$key.'%');
            });
        }
        $list = $list->paginate(config('defined.page_limit'));
        $_SERVER['QUERY_STRING'] = preg_replace('#&page=[0-9]*#isu', '', $_SERVER['QUERY_STRING']);
        $list->setPath('?' . $_SERVER['QUERY_STRING']);
        return view('backend.administrator.index', [
            'list' => $list
        ]);
    }

    function getAdd(){
        $permissions = Permission::all()->groupBy('group');
        return view('backend.administrator.form-add', [
            'permissions' => $permissions
        ]);
    }

    function postAdd(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $email = $request->get('email');
            $name = $request->get('name');
            $password = $request->get('password');
            $user = Admin::where('email', $email)->first();
            if ($user) {
                throw new \Exception('Quản tri viên đã tồn tại');
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('Không đúng định dạng email');
            }
            $user = new Admin();
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->active = $request->get('active', 1);
            $user->save();
            if(is_array($request->get('permissions'))) {
                foreach ($request->get('permissions') as $key => $permissions) {
                    foreach ($permissions as $mkey => $permission) {
                        $user->givePermissionTo($mkey . ' ' . $key);
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('alert', ['class' => 'success', 'msg' => 'Thêm quản trị viên thành công']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }


    function getEdit(Request $request, $id)
    {
        $data = [
            'user' => Admin::findOrFail($id),
            'permissions' => Permission::all()->groupBy('group'),
            'user_has_permissions' => []
        ];
        $user_has_permissions = $data['user']->permissions()->select('name')->get()->toArray();
        foreach ($user_has_permissions as $p) {
            $tmp = explode(' ', $p['name']);
            $data['user_has_permissions'][$tmp[1]][] = $tmp[0];
        }
        return view('backend.administrator.form-edit', $data);
    }

    function postEdit(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $password = $request->get('password', '');
            $name = $request->get('name');
            $user = Admin::findOrFail($id);
            $user->name = $name;
            if(!empty($password)) $user->password = Hash::make($password);
            $user->active = $request->get('active', 1);
            $user->save();
            foreach (Permission::all() as $p) {
                $user->revokePermissionTo($p->name);
            }
            if(is_array($request->get('permissions'))) {
                foreach ($request->get('permissions') as $key => $permissions) {
                    foreach ($permissions as $mkey => $permission) {
                        $user->givePermissionTo($mkey . ' ' . $key);
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('alert', ['class' => 'success', 'msg' => 'Chỉnh sửa quản trị viên thành công']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }

    function getDelete(Request $request){
        if(Admin::where('id', $request->get('id'))->where('id', '!=', auth()->guard('admin')->user()->id)->delete()) {
            return [
                'status' => 'success',
                'msg' => 'Xoá quản trị viên thành công',
                'title' => 'Thành công',
            ];
        }
        return [
            'status' => 'error',
            'msg' => 'Không thể xoá vui lòng thử lại',
            'title' => 'Lỗi',
        ];
    }

    function getProfile(){
        return view('backend.administrator.profile');
    }

    function postProfile(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);

        $password = $request->get('password', '');
        $cfg_password = $request->get('cfg_password', '');
        $name = $request->get('name');
        if (!empty($password) && $password!=$cfg_password){
            return redirect()->back()->withErrors(['message' => 'Mật khẩu nhập lại không khớp'])->withInput();
        }

        $user = Admin::findOrFail(auth()->guard('admin')->user()->id);
        $user->name = $name;
        if(!empty($password)) $user->password = Hash::make($password);
        $user->save();
        return redirect()->back()->with('alert', ['class' => 'success', 'msg' => 'Cập nhật trang cá nhận thành công']);
    }

    function getLogout(){
        auth()->guard('admin')->logout();
        return redirect()->route('inside-login');
    }
}
