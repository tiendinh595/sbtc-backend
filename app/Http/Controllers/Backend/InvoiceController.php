<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    function getIndex(Request $request)
    {
        $deposits = Invoice::orderBy('id', 'DESC')->with('user');

        if ($request->get('user_id'))
            $deposits = $deposits->where('user_id', $request->get('user_id'));

        $deposits = $deposits->paginate(15);

        return view('backend.history.invoice', ['list' => $deposits]);
    }
}
