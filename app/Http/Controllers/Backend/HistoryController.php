<?php

namespace App\Http\Controllers\Backend;

use App\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    function getIndex(Request $request)
    {
        $deposits = History::orderBy('id', 'DESC')->with('user');

        if ($request->get('user_id'))
            $deposits = $deposits->where('user_id', $request->get('user_id'));

        $deposits = $deposits->paginate(15);

        return view('backend.history.index', ['list' => $deposits]);
    }
}
