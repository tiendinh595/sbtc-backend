<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 11/17/17
 * Time: 9:29 PM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Http\Controllers;

//
//use App\Deposit;
//use App\Jobs\WithdrawBTC;
use App\Transaction;
use App\User;
use App\Utils\Helper;
use App\Wallet;
use Illuminate\Http\Request;

class PrivateController extends Controller
{
    function handleDeposit(Request $request)
    {
        try {
            \DB::beginTransaction();
            $type = $request->get('type');

            if ($type == 'wallet:addresses:new-payment') {
                $data = $request->all();

                $address = $data['data']['address'];
                $network = $data['data']['network'];
                $hash = $data['additional_data']['hash'];
                $amount_before_fee = $data['additional_data']['amount']['amount'];
                $fee = Helper::formatCoin((config('system.fee_deposit') * $amount_before_fee) / 100);
                $amount = Helper::formatCoin($amount_before_fee - $fee);

                $transaction = Transaction::where('hash', $hash)->first();
                if (!$transaction) {
                    if ($network == 'bitcoin') {
                        $wallet = Wallet::where('address_btc', $address)->with('user')->firstOrfail();
                        $wallet->total_btc = $wallet->total_btc + $amount;
                        $wallet->balance_btc = $wallet->balance_btc + $amount;
                        $wallet->save();

                        $type_money = Transaction::$type_money['btc'];
                    } else {
                        $wallet = Wallet::where('address_btc', $address)->with('user')->firstOrfail();
                        $wallet->total_eth = $wallet->total_eth + $amount;
                        $wallet->balance_eth = $wallet->balance_eth + $amount;
                        $wallet->save();

                        $type_money = Transaction::$type_money['eth'];
                    }
                    $user = $wallet->user;

                    $payment_memo = 'Nạp ' . ucfirst($network);
                    $transaction = Transaction::create([
                        'user_id' => $user->id,
                        'amount' => $amount,
                        'amount_before_fee' => $amount_before_fee,
                        'fee' => $fee,
                        'from_address' => '',
                        'to_address' => $address,
                        'type_money' => $type_money,
                        'type' => Transaction::$type['deposit'],
                        'payment_memo' => $payment_memo,
                        'extra' => [],
                        'status' => Transaction::$status['success'],
                        'status_job' => Transaction::$status_job['started'],
                        'hash' => $hash
                    ]);
                }
            }
            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::error($exception);
        }
    }
}