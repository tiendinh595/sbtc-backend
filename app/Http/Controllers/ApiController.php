<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 7/21/2017
 * Time: 10:11 AM
 */

namespace App\Http\Controllers;
use App\User;
use App\Utils\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiController extends Controller
{

    function authenticate($new_data = false)  {
        $user = JWTAuth::parseToken()->authenticate();
        if($user)
        {
            if($user->status == 3)
            {
                echo Response::not_permission("account is banned")->content();
                exit();
            }
        }
        $user = $new_data ? User::where('access_token', $user->access_token)->first() : $user;
        return $user;
    }

}