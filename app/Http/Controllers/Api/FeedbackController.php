<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use App\Http\Controllers\ApiController;
use App\Trade;
use App\Utils\Blockchain;
use App\Utils\GoogleAuthenticator;
use App\Utils\Response;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class FeedbackController extends ApiController
{

    function getFeedback($username)
    {
        try {
            $data = [
                'list_feedback' => Feedback::where('username', $username)->paginate(10)
            ];
            return Response::success($data);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trade_id' => 'required|exists:tbl_trade,id',
            'rating' => [
                'required',
                Rule::in(range(1, 5))
            ],
            'content' => 'required|min:3|max:255'
        ],[
            'required' => ':attribute bắt buộc phải nhập',
            'in'=>'Đánh giá không hợp lệ',
            'min'=> 'Nội dung tối thiểu :min ký tự',
            'max'=> 'Nội dung tối đa :max ký tự',
        ], [
            'content'=>'Nội dung'
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();

            if (Feedback::where(['reporter'=>$user->username, 'trade_id'=>$request->get('trade_id')])->first())
                return Response::error('Bạn đã đánh giá cho giao dịch này');

            $feedback_data = $request->only(['trade_id', 'rating', 'content']);
            $trade = Trade::where(['id' => $feedback_data['trade_id']])
                ->whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')")
                ->first();

            if ($trade == null)
                return Response::not_found('giao dịch không tìm thấy');

            if ($trade->seller_username == $user->username) {
                $feedback_data['username'] = $trade->buyer_username;
                $feedback_data['reporter'] = $trade->seller_username;
            } else {
                $feedback_data['username'] = $trade->seller_username;
                $feedback_data['reporter'] = $trade->buyer_username;
            }

            $feedback = Feedback::create($feedback_data);

            User::where('username', $feedback_data['username'])->update(['rate'=>Feedback::where('username', $feedback_data['username'])->avg('rating')]);

            return Response::success([],[], 'Đánh giá thành công');
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

}
