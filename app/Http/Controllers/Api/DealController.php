<?php

namespace App\Http\Controllers\Api;

use App\Deal;
use App\Feedback;
use App\Http\Controllers\ApiController;
use App\Offer;
use App\Trade;
use App\User;
use App\Utils\Helper;
use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DealController extends ApiController
{

    function postDeal(Request $request)
    {
        Validator::extend('greater_then_zero', function ($attribute, $value, $parameters, $validator) {
            return $value > 0;
        });

        $validator = Validator::make($request->all(), [
            'offer_id' => 'required|exists:tbl_offer,id',
            'rate_vnd_usd' => 'required|integer|min:1',
            'amount' => 'required|greater_then_zero'
        ], [
            'required' => ':attribute bắt buộc phải nhập',
            'exists' => 'Offer không tồn tại',
            'min' => 'Giá tối thiểu 1 vnd',
            'greater_then_zero' => ':attribute phải lớn hơn 0',
            'integer'=>'Tỉ giá phải là số nguyên'
        ], [
            'amount' => 'Sô lượng',
            'rate_vnd_usd' => 'Tỉ giá'
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate(true);

            $offer = Offer::findOrFail($request->get('offer_id'));

            if(!$offer->is_special)
                return Response::error('Không thể tạo thoả thuận với offer này');

            $type_deal = Offer::$type['buy'] == $offer->type_offer ? Offer::$type['sell'] : Offer::$type['buy'];

            $data_deal = $request->only(['offer_id', 'amount', 'rate_vnd_usd']);
            $data_deal['owner_offer'] = $offer->username;
            $data_deal['username'] = $user->username;
            $data_deal['amount'] = Helper::formatCoin($data_deal['amount']);
            $data_deal['rate_vnd_usd'] = Helper::formatUSD($data_deal['rate_vnd_usd']);
            $data_deal['rate_usd_coin'] = $offer->rate_usd_coin;
            $data_deal['total_money'] = Helper::formatVND($data_deal['rate_vnd_usd'] * $data_deal['amount'] * $offer->rate_usd_coin);
            $data_deal['status'] = Deal::$status['waiting'];
            $data_deal['type'] = $type_deal;

            if ($offer->status != Offer::$status['open'])
                return Response::error('Offer này đã đóng');

            if ($data_deal['amount'] > $offer->current_amount || $data_deal['amount'] < $offer->min_amount)
                return Response::error("Số lượng thoả thuận không phù hợp, vui lòng chọn trong khoảng {$offer->min_amount} - {$offer->current_amount}");

            $balance = $offer->type_money == Offer::$type_money['btc'] ? $user->wallet->balance_btc : $user->wallet->balance_eth;
            if ($offer->type_offer == Offer::$type['buy'] && $data_deal['amount'] > $balance)
                return Response::error('Số dư của bạn không đủ để tạo thoả thuận');

            Deal::create($data_deal);

            return Response::success([], [], 'Tạo thoả thuận thành công');
        } catch (\Exception $exception) {
            return Response::error();
        }
    }


    function getAllDeal($status)
    {
        try {
            $user = $this->authenticate();

            $deals = Deal::whereRaw("status={$status} and (username='{$user->username}' or owner_offer='{$user->username}')")
                ->with(['offer', 'offerRef'])
                ->orderBy('id', 'DESC')
                ->paginate(15);
            return Response::success($deals);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postReject($id)
    {
        try {
            $user = $this->authenticate();
            $deal = Deal::whereRaw("id={$id} and (username='{$user->username}' or owner_offer='{$user->username}') and status=1")->firstOrFail();
            $deal->status = Deal::$status['reject'];
            $deal->save();
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function postApprove($id)
    {
        try {
            $user = $this->authenticate();
            $deal = Deal::whereRaw("id={$id} and (owner_offer='{$user->username}') and status = 1")->with('offer')->firstOrFail();
            $offer = $deal->offer;

            //pre validate
            if ($deal->status == Deal::$status['reject'])
                return Response::error('Thoả thuận này đã đóng');

            if ($offer->status != Offer::$status['open']) {
                $deal->status = Deal::$status['reject'];
                $deal->save();
                return Response::error('Offer này đã đóng');
            }

            if ($deal->amount > $offer->current_amount)
                return Response::error("Sô lượng thoả thuận không phù hơp với offer (<= {$offer->current_amount})");

            \DB::beginTransaction();

            $address_wallet = $offer->type_money == Offer::$type_money['btc'] ? $deal->user->wallet->address_btc : $deal->user->wallet->address_eth;
            $type_offer = $deal->type;

            //check and update balance for seller (only for deal sell)
            if ($type_offer == Offer::$type['sell']) {
                $seller = $deal->user;
                $wallet = $seller->wallet;
                $balance = $offer->type_money == Offer::$type_money['btc'] ? $wallet->balance_btc : $wallet->balance_eth;
                if ($balance < $deal->amount) {
                    $deal->status = Deal::$status['reject'];
                    $deal->save();
                    \DB::commit();
                    return Response::error('Số dư của người bán không đủ để khởi tạo giao dịch');
                }

                if ($offer->type_money == Offer::$type_money['btc'])
                    $wallet->balance_btc = $wallet->balance_btc - $deal->amount;
                else
                    $wallet->balance_eth = $wallet->balance_eth - $deal->amount;
                $wallet->save();
            }

            // clone offer from offer master
            $new_offer = $offer->replicate();
            $new_offer->rate_vnd_usd = $deal->rate_vnd_usd;
            $new_offer->rate_usd_coin = $deal->rate_usd_coin;
            $new_offer->deal_id = $deal->id;
            $new_offer->min_amount = $deal->amount;
            $new_offer->max_amount = $deal->amount;
            $new_offer->current_amount = $deal->amount;
            $new_offer->price_per_coin = $deal->rate_vnd_usd * $offer->rate_usd_coin;
            $new_offer->price_per_coin_before_fee = $deal->rate_vnd_usd * $offer->rate_usd_coin;
            $new_offer->username = $deal->username;
            $new_offer->type_offer = $type_offer;
            $new_offer->address_wallet = $type_offer == Offer::$type['buy'] ? $address_wallet : '';
            $new_offer->outgoing = 0;
            $new_offer->save();

            //update status for deal
            $deal->status = Deal::$status['approved'];
            $deal->save();

            //update mount, status for offer origin
            $offer->current_amount = $offer->current_amount - $deal->amount;
            if ($offer->current_amount == 0) {
                $offer->status = Offer::$status['done'];
            }
            $offer->save();


            //trade

            $user = $this->authenticate();
            $user = User::where(['id' => $user->id])->with('wallet')->firstOrFail();
            $user_offer = $new_offer->user;

            if ($new_offer->status != Offer::$status['open'])
                return Response::error('Offer này đã đóng');

            if ($new_offer->username == $user->username)
                return Response::error('Không thể thao tác với chính offer của bạn');

            $amount_coin = round($deal->amount, 8);
            $coin_name = $new_offer->type_money == Offer::$type_money['btc'] ? 'BTC' : 'ETH';

            //for buy order
            if ($new_offer->type_offer == Offer::$type['buy']) {
                $seller = $user;
                $buyer = $user_offer;

                $outgoing = $new_offer->outgoing;

                $balance_coin = $new_offer->type_money == Offer::$type_money['btc'] ? $seller->wallet->balance_btc : $seller->wallet->balance_eth;

                // check amount coin with offer
                if ($amount_coin > $new_offer->current_amount || $amount_coin < $new_offer->min_amount)
                    return Response::error("Số lượng {$coin_name} bạn bán không phù hợp với offer này, vui lòng chọn trong khoảng {$new_offer->min_amount} - {$new_offer->current_amount}");

                // check with amount coin to trade
                if ($balance_coin < $amount_coin && $new_offer->deal_id == 0)
                    return Response::error("Bạn không đủ {$amount_coin} {$coin_name} để bán");

                $buyer_receiving_coin_amount = Helper::calCoinToBuyer($amount_coin);
                $seller_receiving_vnd_amount = Helper::formatVND($amount_coin * $new_offer->price_per_coin);
                $buyer_sending_vnd_amount = Helper::formatVND($amount_coin * $new_offer->price_per_coin);
                $fee = Helper::calFeeToBuyer($amount_coin);


//                update
                if($buyer_sending_vnd_amount > $buyer->wallet->balance_vnd)
                    return Response::error("Số dư vnd không đủ để chấp nhận thỏa thuận này");

                $using_vnd_to_pay = false;
                //using vnd to pay
                if ($buyer_sending_vnd_amount <= $buyer->wallet->balance_vnd) {
                    $using_vnd_to_pay = true;
                    $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
                    $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;
                    $buyer->wallet->balance_vnd = $buyer->wallet->balance_vnd - $buyer_sending_vnd_amount;
                }

                //btc
                if ($new_offer->type_money == Offer::$type_money['btc']) {
                    $from_address = $seller->wallet->address_btc;
                    $to_address = $buyer->wallet->address_btc;
                    //seller
                    if ($new_offer->deal_id == 0) {
                        $seller->wallet->balance_btc = $seller->wallet->balance_btc - $amount_coin;
                        if (!$using_vnd_to_pay)
                            $seller->wallet->freeze_btc = $seller->wallet->freeze_btc + $amount_coin;
                    }

                    //buyer
                    if ($new_offer->outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_btc = $buyer->wallet->pending_btc + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_btc = $buyer->wallet->total_btc + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_btc = $buyer->wallet->balance_btc + $buyer_receiving_coin_amount;
                        }
                    }
                } //eth
                else {
                    $from_address = $seller->wallet->address_eth;
                    $to_address = $buyer->wallet->address_eth;
                    //seller
                    if ($new_offer->deal_id == 0) {
                        $seller->wallet->balance_eth = $seller->wallet->balance_eth - $amount_coin;
                        if (!$using_vnd_to_pay)
                            $seller->wallet->freeze_eth = $seller->wallet->freeze_eth + $amount_coin;
                    }

                    //buyer
                    if ($new_offer->outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_eth = $buyer->wallet->pending_eth + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_eth = $buyer->wallet->total_eth + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_eth = $buyer->wallet->balance_eth + $buyer_receiving_coin_amount;
                        }
                    }

                }
                $seller->wallet->save();
                $buyer->wallet->save();

                $trade_status = $using_vnd_to_pay == true ? Trade::$status['done'] : Trade::$status['waiting'];

                $data_trade = [
                    'buyer_username' => $buyer->username,
                    'seller_username' => $seller->username,
                    'from_address' => $from_address,
                    'to_address' => $to_address,
                    'offer_id' => $new_offer->id,
                    'type_money' => $new_offer->type_money,
                    'seller_sending_coin_amount' => $amount_coin,
                    'buyer_receiving_coin_amount' => $buyer_receiving_coin_amount,
                    'seller_receiving_vnd_amount' => $seller_receiving_vnd_amount,
                    'buyer_sending_vnd_amount' => $buyer_sending_vnd_amount,
                    'price_per_coin' => $new_offer->price_per_coin,
                    'price_per_coin_before_fee' => $new_offer->price_per_coin_before_fee,
                    'fee' => $fee,
                    'payment_method' => $new_offer->payment_method,
                    'payment_time' => Carbon::now()->addMinutes($new_offer->payment_time)->format('Y-m-d H:i:s'),
                    'token' => '',
//                    'bank_code' => $request->get('bank_code'),
//                    'bank_number' => $request->get('bank_number'),
//                    'bank_account_name' => $request->get('bank_account_name'),
                    'status' => $trade_status,
                    'outgoing' => $outgoing
                ];

                $trade = Trade::create($data_trade);
                $trade->payment_memo = "STBTC {$trade->id}";
                $trade->save();

            } //for sell order
            else {
                $seller = $user_offer;
                $buyer = $user;

//                $outgoing = Wallet::checkAddressInternal($request->get('to_address'), strtolower($coin_name));
                $outgoing = false;

                // check amount coin with offer
                if ($amount_coin > $new_offer->current_amount || $amount_coin < $new_offer->min_amount)
                    return Response::error("Số lượng {$coin_name} bạn mua không phù hợp với offer này, vui lòng chọn trong khoảng {$new_offer->min_amount} - {$new_offer->current_amount}");

                $buyer_receiving_coin_amount = Helper::calCoinToBuyer($amount_coin);
                $seller_receiving_vnd_amount = Helper::formatVND($amount_coin * $new_offer->price_per_coin);
                $buyer_sending_vnd_amount = Helper::formatVND($amount_coin * $new_offer->price_per_coin);
                $fee = Helper::calFeeToBuyer($amount_coin);


                $using_vnd_to_pay = false;
                //using vnd to pay
//                if ($buyer_sending_vnd_amount <= $buyer->wallet->balance_vnd) {
//                    $using_vnd_to_pay = true;
//                    $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
//                    $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;
//                    $buyer->wallet->balance_vnd = $buyer->wallet->balance_vnd - $buyer_sending_vnd_amount;
//                }


//                update
                $using_vnd_to_pay = true;
                $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
                $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;

                //btc
                if ($new_offer->type_money == Offer::$type_money['btc']) {
                    $from_address = $seller->wallet->address_btc;
                    $to_address = $buyer->wallet->address_btc;
                    //buyer
                    if ($outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_btc = $buyer->wallet->pending_btc + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_btc = $buyer->wallet->total_btc + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_btc = $buyer->wallet->balance_btc + $buyer_receiving_coin_amount;
                        }
                    }
                } //eth
                else {
                    $from_address = $seller->wallet->address_eth;
                    $to_address = $buyer->wallet->address_eth;

                    //buyer
                    if ($outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_eth = $buyer->wallet->pending_eth + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_eth = $buyer->wallet->total_eth + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_eth = $buyer->wallet->balance_eth + $buyer_receiving_coin_amount;
                        }
                    }
                }
                $seller->wallet->save();
                $buyer->wallet->save();

                $trade_status = $using_vnd_to_pay == true ? Trade::$status['done'] : Trade::$status['waiting'];
                $data_trade = [
                    'buyer_username' => $buyer->username,
                    'seller_username' => $seller->username,
                    'from_address' => $from_address,
                    'to_address' => $to_address,
                    'offer_id' => $new_offer->id,
                    'type_money' => $new_offer->type_money,
                    'seller_sending_coin_amount' => $amount_coin,
                    'buyer_receiving_coin_amount' => $buyer_receiving_coin_amount,
                    'seller_receiving_vnd_amount' => $seller_receiving_vnd_amount,
                    'buyer_sending_vnd_amount' => $buyer_sending_vnd_amount,
                    'price_per_coin' => $new_offer->price_per_coin,
                    'price_per_coin_before_fee' => $new_offer->price_per_coin_before_fee,
                    'fee' => $fee,
                    'payment_method' => $new_offer->payment_method,
                    'payment_time' => Carbon::now()->addMinutes($new_offer->payment_time)->format('Y-m-d H:i:s'),
                    'token' => '',
                    'bank_code' => $new_offer->bank_code,
                    'bank_number' => $new_offer->bank_number,
                    'bank_account_name' => $new_offer->bank_account_name,
                    'status' => $trade_status,
                    'outgoing' => $outgoing
                ];

                $trade = Trade::create($data_trade);
                $trade->payment_memo = "STBTC {$trade->id}";
                $trade->save();
            }

            // update offer
            $new_offer->current_amount = $new_offer->current_amount - $amount_coin;
            if ($new_offer->current_amount == 0) {
                $new_offer->status = Offer::$status['done'];
            }
            $new_offer->save();

            if ($new_offer->deal_id != 0) {
                Deal::find($new_offer->deal_id)->update(['trade_id' => $trade->id]);
            }

            \DB::commit();
            return Response::success($trade);
//
//            \DB::commit();
//            return Response::success($new_offer);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error();
        }
    }
}
