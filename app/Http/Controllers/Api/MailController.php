<?php

namespace App\Http\Controllers\Api;

use App\Deal;
use App\Feedback;
use App\Http\Controllers\ApiController;
use App\Mail;
use App\Offer;
use App\Utils\Helper;
use App\Utils\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MailController extends ApiController
{

    function getAll($type)
    {
        try {
            $user = $this->authenticate();
            $mails = Mail::where('user_id', $user->id);
            if ($type != 'all')
                $mails = $mails->where('type', $type);
            $mails = $mails->orderBy('id', 'DESC')->paginate(10);

            return Response::success($mails);
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function getMarkRead($id)
    {
        try {
            $user = $this->authenticate();
            Mail::where(['user_id' => $user->id, 'status' => 0, 'id' => $id])->update(['status' => 1]);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function postDeleteMultiple(Request $request)
    {
        try {
            $user = $this->authenticate();
            Mail::where(['user_id' => $user->id])->whereIn('id', $request->get('list_id'))->delete();
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function postSendMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category' => ['required', Rule::in(array_keys(config('system.category_mail')))],
            'content' => 'required|min:3|max:500'
        ], [
            'required' => ':attribute không được để trống',
            'in' => 'mục không hợp lệ',
            'min' => 'Tối thiểu :min kí tự',
            'max' => 'Tối đa :max kí tự',
        ], [
            'category' => 'Chuyên mục',
            'content' => 'Nội dung'
        ]);
        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();
            $data = [
                'user_id'=>$user->id,
                'category_id' => $request->get('category'),
                'content' => $request->get('content'),
                'title' => "Phản hồi từ {$user->username}",
                'type'=> Mail::$type['out'],
            ];

            Mail::create($data);
            return Response::success([],[], 'Gửi phản hồi thành công');
        } catch (\Exception $exception) {
            return Response::error();
        }
    }
}
