<?php

namespace App\Http\Controllers\Api;

use App\Deal;
use App\Feedback;
use App\History;
use App\Http\Controllers\ApiController;
use App\Offer;
use App\Transaction;
use App\Utils\File;
use App\Utils\Helper;
use App\Utils\Response;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class WalletController extends ApiController
{
    function getTransactions(Request $request, $coin)
    {
        try {
            $user = $this->authenticate(false);
            $transactions = History::where(['user_id' => $user->id, 'type_money' => Transaction::$type_money[$coin]])->where('status', '<>', Transaction::$status['init'])->orderBy('id', 'DESC')->paginate(10);
            return Response::success($transactions);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postWithdraw(Request $request)
    {
        Validator::extend('greater_then_zero', function ($attribute, $value, $parameters, $validator) {
            return $value > 0;
        });

        $validator = Validator::make($request->all(), [
            'type_money' => ['required', Rule::in('btc', 'eth', 'fiat')],
            'amount' => 'required|numeric|greater_then_zero',
            'to_address' => 'required'
        ], [
            'required' => ':attribute bắt buộc phải nhập',
            'numeric' => ':attribute phải là số',
            'greater_then_zero' => ':attribute phải lớn hơn 0',
        ], [
            'amount' => 'Lượng rút ra',
            'to_address' => 'Địa chỉ nhận',
            'type_money' => 'Loại tiền'
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            \DB::beginTransaction();

            $user = $this->authenticate();
            $wallet = $user->wallet;

            $data_withdraw = $request->only(['type_money', 'amount', 'to_address']);
            $data_withdraw['type_money'] = Transaction::$type_money[$data_withdraw['type_money']];

            if ($data_withdraw['type_money'] == Transaction::$type_money['btc']) {
                $balance = $wallet->balance_btc;
                $data_withdraw['amount_before_fee'] = Helper::formatCoin($data_withdraw['amount']);
                $data_withdraw['amount'] = Transaction::calAmountWithdrawAfterFee($data_withdraw['amount_before_fee'], $data_withdraw['type_money']);
                $data_withdraw['outgoing'] = Wallet::checkAddressInternal($data_withdraw['to_address'], 'btc');

                // update wallet
                $wallet->balance_btc = $wallet->balance_btc - $data_withdraw['amount'];
                $wallet->freeze_btc = $wallet->freeze_btc + $data_withdraw['amount'];
            } else if ($data_withdraw['type_money'] == Transaction::$type_money['eth']) {
                $balance = $wallet->balance_eth;
                $data_withdraw['amount_before_fee'] = Helper::formatCoin($data_withdraw['amount']);
                $data_withdraw['amount'] = Transaction::calAmountWithdrawAfterFee($data_withdraw['amount_before_fee'], $data_withdraw['type_money']);
                $data_withdraw['outgoing'] = Wallet::checkAddressInternal($data_withdraw['to_address'], 'eth');

                // update wallet
                $wallet->balance_eth = $wallet->balance_eth - $data_withdraw['amount'];
                $wallet->freeze_eth = $wallet->freeze_eth + $data_withdraw['amount'];
            } else {
                $balance = $wallet->balance_vnd;
                $data_withdraw['amount'] = Helper::formatVND($data_withdraw['amount']);
                $data_withdraw['amount_before_fee'] = Helper::formatVND($data_withdraw['amount']);
                $data_withdraw['outgoing'] = 1;

                // update wallet
                $wallet->balance_vnd = $wallet->balance_vnd - $data_withdraw['amount'];
                $wallet->freeze_vnd = $wallet->freeze_vnd + $data_withdraw['amount'];
            }
            $data_withdraw['fee'] = Helper::formatCoin($data_withdraw['amount_before_fee'] - $data_withdraw['amount']);
            $data_withdraw['type'] = Transaction::$type['withdraw'];
            $data_withdraw['status_job'] = Transaction::$status_job['pending'];
            $data_withdraw['status'] = Transaction::$status['pending'];
            $data_withdraw['user_id'] = $user->id;

            if ($data_withdraw['amount'] > $balance)
                return Response::error("Số lương bạn rút ra vượt quá số dư khả dụng ($balance)");

            $withdraw = Transaction::create($data_withdraw);
            $wallet->save();

            \DB::commit();
            return Response::success([], [], 'Lệnh rút của bạn đã được đưa vào hàng đợi thành công.');
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error($exception->getMessage());
        }
    }

    function postInitDepositFiat(Request $request)
    {
        Validator::extend('greater_then_zero', function ($attribute, $value, $parameters, $validator) {
            return $value > 0;
        });
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric|greater_then_zero'
        ], [
            'required' => ':attribute bắt buộc phải nhập',
            'numeric' => ':attribute phải là số',
            'greater_then_zero' => ':attribute phải lớn hơn 0',
        ], [
            'amount' => 'Số tiền nạp',
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();
            \DB::beginTransaction();
            $data_deposit = [
                'user_id' => $user->id,
                'amount' => $request->get('amount'),
                'amount_before_fee' => $request->get('amount'),
                'type_money' => Transaction::$type_money['fiat'],
                'type' => Transaction::$type['deposit'],
                'status' => Transaction::$status['init'],
                'status_job' => Transaction::$status_job['pending'],
                'extra' => ['receipt' => null]
            ];
            $data_deposit['fee'] = $data_deposit['amount_before_fee'] - $data_deposit['amount'];
            $deposit = Transaction::create($data_deposit);
            $deposit = Transaction::select(['id', 'payment_memo', 'amount', 'extra'])->find($deposit->id);
            \DB::commit();
            return Response::success($deposit);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error();
        }
    }

    function postUpdateStatusTransaction(Request $request, $id)
    {
        try {
            if (!in_array($request->get('status'), [1, 4]))
                return Response::error('Trạng thái không hợp lệ');

            $user = $this->authenticate();
            $transaction = Transaction::where(['id' => $id, 'user_id' => $user->id])->firstOrFail();

            if($transaction->status == Transaction::$status['init']) {
                if ($transaction->extra['receipt'] == null && $request->get('status') == 1)
                    return Response::error('Vui lòng gửi biên lai trước khi xác nhận');

                $transaction->status = $request->get('status');
                $transaction->save();
            }
            $msg = $request->get('status') == 1 ? 'Xác nhận lệnh nạp tiền thành công' : 'Bạn đã huỷ lệnh nạp';
            return Response::success([], [], $msg);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postUploadReceiptTransaction(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'receipt' => 'required|image'
        ], [
            'required' => ':attribute không được để trống',
            'image' => ':attribute  không phải là hình'
        ], [
            'receipt' => 'Biên lai'
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();

            $transaction = Transaction::where(['id' => $id, 'user_id' => $user->id, 'status' => Transaction::$status['init']])->firstOrFail();

            $name = time() . '.' . $request->receipt->extension();
            $url = File::uploadFile($_FILES['receipt'], $name);

            $transaction->extra = ['receipt' => $url];
            $transaction->save();
            return Response::success($transaction, [],'Tải biên lên thành công');
        } catch (\Exception $exception) {
            return Response::error();
        }
    }


    function getDetailTransaction($id)
    {
        try {
            $user = $this->authenticate();
            $transaction = Transaction::where(['id' => $id, 'user_id' => $user->id])->firstOrFail();
            return Response::success($transaction);
        } catch (\Exception $exception) {
            return Response::error();
        }
    }
}
