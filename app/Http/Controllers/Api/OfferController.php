<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use App\Http\Controllers\ApiController;
use App\Offer;
use App\System;
use App\Utils\Blockchain;
use App\Utils\GoogleAuthenticator;
use App\Utils\Helper;
use App\Utils\Response;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class OfferController extends ApiController
{
    function getSellOffer(Request $request, $type = 'all', $username = null)
    {
        try {
            $user = $this->authenticate();
            $user = $user->username;
        } catch (\Exception $exception) {
            $user = null;
        }

        try {
            $offers = Offer::OfferSell()->where('username', '<>', $user)->open()->original()->with(['user' => function ($relation) {
                $relation->select(['id', 'username']);
            }]);

            if ($username !== null)
                $offers = $offers->where('username', $username);

            if ($request->get('amount', 0) != 0) {
                $amount = $request->get('amount');
                $offers = $offers->where('min_amount', '<', $amount)->where('max_amount', '>', $amount);
            }

            if ($type != 'all') {
                if ($type == Offer::$type_money_name['btc'])
                    $offers = $offers->BTC();

                if ($type == Offer::$type_money_name['eth'])
                    $offers = $offers->ETH();
            }

            $offers = $offers->orderBy('rate_usd_coin', 'ASC')->paginate(5);
            return Response::success($offers);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }


    function getBuyOffer(Request $request, $type = 'all', $username = null)
    {
        try {
            $user = $this->authenticate();
            $user = $user->username;
        } catch (\Exception $exception) {
            $user = null;
        }

        try {
            $offers = Offer::OfferBuy()->where('username', '<>', $user)->open()->original()->with(['user' => function ($relation) {
                $relation->select(['id', 'username']);
            }]);

            if ($username !== null)
                $offers = $offers->where('username', $username);

            if ($request->get('amount', 0) != 0) {
                $amount = $request->get('amount');
                $offers = $offers->where('min_amount', '<', $amount);
            }

            if ($type != 'all') {
                if ($type == Offer::$type_money_name['btc'])
                    $offers = $offers->BTC();

                if ($type == Offer::$type_money_name['eth'])
                    $offers = $offers->ETH();
            }


            $offers = $offers->orderBy('rate_usd_coin', 'DESC')->paginate(5);
            return Response::success($offers);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    function getDetail($id)
    {
        try {
            $user = $this->authenticate();
            $offer = Offer::findOrFail($id);
            return Response::success($offer);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }


    function postCreate(Request $request)
    {
        $action = $request->get('action');

        if (!in_array($action, ['buy_btc', 'sell_btc', 'buy_eth', 'sell_eth']))
            return Response::error('Hành động không hợp lệ');

        $currency = explode('_', $action);
        $currency = end($currency);
        $currency_name = $currency == 'btc' ? 'Bitcoin' : 'Ethereum';

        $action = $action == 'buy_btc' || $action == 'buy_eth' ? 'buy' : 'sell';


        Validator::extend('greater_then_zero', function ($attribute, $value, $parameters, $validator) {
            return $value > 0;
        });

        $array_validate = [
            'rate_vnd_usd' => 'required|numeric|greater_then_zero',
            'min_amount' => 'required|numeric|min:0.001',
            'max_amount' => 'required|numeric|max:100',
            'payment_time' => ['required', 'numeric', 'greater_then_zero', Rule::in(15, 30)],
            'allow_not_verify' => 'required',
            'payment_method' => ['required', 'numeric', 'greater_then_zero', Rule::in(1, 2)],
            'bank_code' => [
                'required',
                Rule::in(array_keys(config('system.bank')))
            ],
        ];

        if ($action == 'sell')
            $array_validate = array_merge($array_validate, ['bank_number' => 'required', 'bank_account_name' => 'required']);
//        else
//            $array_validate = array_merge($array_validate, ['address_wallet' => 'required']);

        $array_message = [
            'required' => ':attribute bắt buộc phải nhập',
            'numeric' => ':attribute phải là số',
            'greater_then_zero' => ':attribute phải lớn hơn 0',
            'in' => ':attribute không hợp lệ',
            'min' => ':attribute phải lớn hơn :min',
            'max' => ':attribute phải nhỏ hơn :max',
        ];

        $array_attribute = [
            'rate_vnd_usd' => 'Tỉ giá USD',
            'min_amount' => "Số {$currency} tối thiểu",
            'max_amount' => "Số {$currency} tối đa",
            'payment_time' => 'Thời gian thanh toán',
            'allow_not_verify' => 'Từ chối người mua chưa xác minh',
            'payment_method' => 'Phương thức thanh toán',
            'bank_code' => 'Tên ngân hàng',
            'bank_number' => 'Số tài khoản',
            'bank_account_name' => 'Tên tài khoản',
            'address_wallet' => 'Địa chỉ nhận'
        ];

        $validator = Validator::make($request->all(), $array_validate, $array_message, $array_attribute);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            \DB::beginTransaction();
            $user = $this->authenticate(true);
            $wallet = $user->wallet;
            $balance_coin = $currency == 'btc' ? $wallet->balance_btc : $wallet->balance_eth;
            $address_wallet = $currency == 'btc' ? $wallet->address_btc : $wallet->address_eth;

            $data_offer = $request->only(['rate_vnd_usd', 'price_per_coin', 'min_amount', 'max_amount', 'payment_time', 'allow_not_verify', 'payment_method', 'bank_code', 'bank_number', 'bank_account_name']);
            $data_offer['address_wallet'] = $address_wallet;

            $data_global = \Cache::get('data_global');
            $data_offer['rate_usd_coin'] = $currency == 'btc' ? $data_global['price_last_btc'] : $data_global['price_last_eth'];

            $data_offer['username'] = $user->username;
            $data_offer['price_per_coin'] = $data_offer['rate_vnd_usd'] * $data_offer['rate_usd_coin'];
            $data_offer['price_per_coin_before_fee'] = $data_offer['rate_vnd_usd'] * $data_offer['rate_usd_coin'];
            $data_offer['type_offer'] = Offer::$type[$action];
            $data_offer['type_money'] = Offer::$type_money[$currency];
            $data_offer['current_amount'] = $data_offer['max_amount'];
//            $data_offer['outgoing'] = Wallet::checkAddressInternal($request->get('address_wallet'), strtolower($currency));
            $data_offer['outgoing'] = false;
            $data_offer['status'] = Offer::$status['open'];

            if ($balance_coin < $data_offer['max_amount'] && $action == 'sell')
                return Response::error("Số dư {$currency_name} của ban không đủ để thực hiện giao dịch");

            if ($data_offer['max_amount'] < $data_offer['min_amount'])
                return Response::error("Số tiền tối thiểu phải nhỏ hơn số tiền tối đa");

            if ($data_offer['rate_usd_coin'] == 0 || $data_offer['price_per_coin'] == 0 || $data_offer['price_per_coin_before_fee'] == 0 || $data_offer['rate_vnd_usd'] == 0)
                return Response::error('Có lỗi xảy ra trong quá trình tạo, vui lòng thử lại');

            //sell offer
            if ($action == 'sell') {
                if ($currency == 'btc') {
                    $wallet->balance_btc = $wallet->balance_btc - $data_offer['max_amount'];
                    $wallet->freeze_btc = $wallet->freeze_btc + $data_offer['max_amount'];
                } else {
                    $wallet->balance_eth = $wallet->balance_eth - $data_offer['max_amount'];
                    $wallet->freeze_eth = $wallet->freeze_eth + $data_offer['max_amount'];
                }
                $wallet->save();
            } else {
                $amount_vnd = Helper::formatVND($data_offer['max_amount'] * $data_offer['price_per_coin']);
                $amount_vnd_format = number_format($amount_vnd, 0, ',', '.');
                if($wallet->balance_vnd < $amount_vnd) {
                    return Response::error("Bạn cần {$amount_vnd_format} vnd để tạo offer này");
                }
                else {
                    $wallet->balance_vnd = $wallet->balance_vnd - $amount_vnd;
                    $wallet->save();
                }
            }

            $offer = Offer::create($data_offer);
            \DB::commit();

            return Response::success($offer, [], 'Đăng tin thành công');
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error($exception->getMessage());
        }

    }


    function getAllMyOffers($type = null)
    {
        try {
            $user = $this->authenticate();
            $offers = Offer::where('username', $user->username)->where('status', '<>', Offer::$status['deleted']);
            if ($type != null)
                $offers->where('type_offer', $type);
            $offers = $offers->orderBy('id', 'DESC')->paginate(6);

            return Response::success($offers);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function getEdit($id)
    {
        try {
            $user = $this->authenticate();
            $offer = Offer::where(['id' => $id, 'username' => $user->username])->firstOrFail();
            return Response::success($offer);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function putUpdateOffer(Request $request, $id)
    {

        try {
            $user = $this->authenticate(true);
            $offer = Offer::findOrFail($id);
        } catch (\Exception $exception) {
            return Response::error();
        }

        $user = $this->authenticate(true);
        $wallet = $user->wallet;
        $currency = $offer->type_money == Offer::$type_money['btc'] ? 'btc' : 'eth';
        $address_wallet = $currency == 'btc' ? $wallet->address_btc : $wallet->address_eth;
        $currency_name = $offer->type_money == Offer::$type_money['btc'] ? 'Bitcoin' : 'Ethereum';

        Validator::extend('greater_then_zero', function ($attribute, $value, $parameters, $validator) {
            return $value > 0;
        });

        $array_validate = [
            'rate_vnd_usd' => 'required|numeric|greater_then_zero',
            'min_amount' => 'required|numeric|greater_then_zero',
            'max_amount' => 'required|numeric|greater_then_zero',
            'payment_time' => ['required', 'numeric', 'greater_then_zero', Rule::in(15, 30)],
            'allow_not_verify' => 'required',
            'payment_method' => ['required', 'numeric', 'greater_then_zero', Rule::in(1, 2)],
            'bank_code' => [
                'required',
                Rule::in(array_keys(config('system.bank')))
            ],
            'status' => ['required', Rule::in(array_values(Offer::$status))]
        ];

        if ($offer->type_offer == Offer::$type['sell'])
            $array_validate = array_merge($array_validate, ['bank_number' => 'required', 'bank_account_name' => 'required']);
//        else
//            $array_validate = array_merge($array_validate, ['address_wallet' => 'required']);

        $array_message = [
            'required' => ':attribute bắt buộc phải nhập',
            'numeric' => ':attribute phải là số',
            'greater_then_zero' => ':attribute phải lớn hơn 0',
            'in' => ':attribute không hợp lệ'
        ];

        $array_attribute = [
            'rate_vnd_usd' => 'Tỉ giá USD',
            'min_amount' => 'Số tiền tối thiểu',
            'max_amount' => 'Số tiền tối đa',
            'payment_time' => 'Thời gian thanh toán',
            'allow_not_verify' => 'Từ chối người mua chưa xác minh',
            'payment_method' => 'Phương thức thanh toán',
            'bank_code' => 'Tên ngân hàng',
            'bank_number' => 'Số tài khoản',
            'bank_account_name' => 'Tên tài khoản',
            'address_wallet' => 'Địa chỉ nhận'
        ];

        $validator = Validator::make($request->all(), $array_validate, $array_message, $array_attribute);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            \Cache::forget('data_global');
            \Cache::remember('data_global', 5, function () {
                return System::all()->mapWithKeys(function ($item) {
                    return [$item->key => $item->value];
                });
            });


            $data_offer = $request->only(['rate_vnd_usd', 'price_per_coin', 'min_amount', 'payment_time', 'allow_not_verify', 'payment_method', 'bank_code', 'bank_number', 'bank_account_name', 'status']);
            $data_offer['address_wallet'] = $address_wallet;

            if ($data_offer['status'] == $offer->status) {

                $data_global = \Cache::get('data_global');
                $data_offer['rate_usd_coin'] = $currency == 'btc' ? $data_global['price_last_btc'] : $data_global['price_last_eth'];

                $data_offer['price_per_coin'] = $data_offer['rate_vnd_usd'] * $data_offer['rate_usd_coin'];
                $data_offer['price_per_coin_before_fee'] = $data_offer['rate_vnd_usd'] * $data_offer['rate_usd_coin'];

                if($offer->type_offer == Offer::$type['buy']) {
                    $wallet->balance_vnd = $wallet->balance_vnd + Helper::formatVND($offer->current_amount * $offer->price_per_coin);
                    $amount_vnd = Helper::formatVND($offer->current_amount * $data_offer['price_per_coin']);
                    $amount_vnd_format = number_format($amount_vnd, 0, ',', '.');
                    $cost_amount_vnd = $amount_vnd - $wallet->balance_vnd;
                    $cost_amount_vnd_format = number_format($cost_amount_vnd, 0, ',', '.');
                    if($wallet->balance_vnd < $amount_vnd) {
                        return Response::error("Bạn cần thêm {$cost_amount_vnd_format} vnd để cập nhật offer này");
                    }
                    else {
                        $wallet->balance_vnd = $wallet->balance_vnd - $amount_vnd;
                        $wallet->save();
                    }
                }
            }

//            $data_offer['outgoing'] = Wallet::checkAddressInternal($request->get('address_wallet'), strtolower($currency));
            $data_offer['outgoing'] = false;

            if ($offer->max_amount < $data_offer['min_amount'])
                return Response::error("Số tiền tối thiểu phải nhỏ hơn số tiền tối đa");

            if ($offer->status != Offer::$status['open'] && $offer->status != Offer::$status['stopped'] && $offer->status == $data_offer['status'])
                return Response::error("Chỉ cập nhật được khi bài đăng ở trạng thái mở hoặc đóng");

            if ($offer->status != $data_offer['status'] && $data_offer['status'] == Offer::$status['deleted']) {
                $wallet = $user->wallet;
                if ($offer->type_money == Offer::$type_money['btc']) {
                    $wallet->balance_btc = $wallet->balance_btc + $offer->current_amount;
                    $wallet->freeze_btc = $wallet->freeze_btc - $offer->current_amount;
                } else {
                    $wallet->balance_eth = $wallet->balance_eth + $offer->current_amount;
                    $wallet->freeze_eth = $wallet->freeze_eth - $offer->current_amount;
                }
                $wallet->save();
            }

            $offer->update($data_offer);
            return Response::success($offer, [], 'Cập nhật thành công');
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }
}
