<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use App\HistoryLogin;
use App\Http\Controllers\ApiController;
use App\Jobs\SendMailActive;
use App\Jobs\SendMailLogin;
use App\Jobs\SendMailVerifyPhone;
use App\ProfileExtra;
use App\Referral;
use App\Trade;
use App\Utils\File;
use App\Utils\GoogleAuthenticator;
use App\Utils\Response;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends ApiController
{
    function postLoginEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email_login' => 'required|email|exists:tbl_user,email'
        ], [
            'exists' => 'email không tồn tại',
            'required' => 'email không được để trống',
            'email' => 'không đúng định dạng email'
        ]);

        if ($validator->fails())
            return Response::error($validator->errors());

        try {
            $user = User::where('email', $request->get('email_login'))->firstOrFail();
            if ($user->status == User::$status['inactive'])
                return Response::error(['email_login' => ['Tài khoản chưa kích hoạt, vui lòng kích hoạt qua mail']]);

            $user->token_login = md5($user->id . time());
            $user->token_login_time = Carbon::now()->addMinutes(15);
            $user->save();
            $this->dispatch((new SendMailLogin($user))->onQueue('send_mail_login')->onConnection('database'));
            return Response::success();
        } catch (Exception $e) {
            return Response::error();
        }
    }

    function postConfirmLogin($email, $token)
    {
        try {
            \DB::beginTransaction();
            $user = User::where(['email' => $email, 'token_login' => $token])->where('token_login_time', '>=', Carbon::now()->toDateTimeString())->firstOrFail();
            $user = $user->load('Wallet');
            $user->token_login = null;
            $user->token_login_time = null;
            $token = JWTAuth::attempt(['email' => $email, 'password' => 1234]);
            $user->access_token = $token;
            $user->latest_login_at = Carbon::now();
            $user->save();


            $ip = $_SERVER['REMOTE_ADDR'];
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
            $location = file_get_contents("https://ipapi.co/{$ip}/json/", false, stream_context_create($arrContextOptions));
            $location = json_decode($location, true);
            $history_login = [
                'user_id' => $user->id,
                'country' => isset($location['country']) ? $location['country'] : '',
                'city' => isset($location['city']) ? $location['city'] : '',
                'ip' => $ip
            ];
            HistoryLogin::create($history_login);

            \DB::commit();
            return Response::success(['token' => $token, 'profile' => $user]);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error('Liên kết đăng nhập của bạn đã hết hạn. Vui lòng đăng nhập lại.');
        }
    }

    function postActive($email, $token)
    {
        try {
            $user = User::where(['email' => $email, 'token_login' => $token])->firstOrFail();
            $user->token_login = null;
            $user->status = User::$status['active'];
            $user->token_login_time = null;
            $token = JWTAuth::attempt(['email' => $email, 'password' => 1234]);
            $user->access_token = $token;
            $user->latest_login_at = Carbon::now();
            $user->save();
            return Response::success(['token' => $token, 'profile' => $user]);
        } catch (\Exception $exception) {
            return Response::error('Liên kết đăng nhập của bạn đã hết hạn. Vui lòng đăng nhập lại.');
        }
    }

    function postRegister(Request $request)
    {
        Validator::extend('username', function ($attribute, $value, $parameters, $validator) {
            return strrpos($value, " ") === false ? true : false;
        });

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:tbl_user',
            'username' => 'required|unique:tbl_user|username',
            'confirm_email' => 'same:email',
            'presenter' => 'nullable|exists:tbl_user,username'
        ], [
            'unique' => ':attribute đã tồn tại',
            'required' => ':attribute không được để trống',
            'email' => 'không đúng định dạng email',
            'same' => '2 email không giống nhau',
            'max' => 'tối đa 100 ký tự',
            'exists' => 'người giới thiệu không tồn tại',
            'username' => 'Tên định danh không được có khoảng trắng'
        ], [
            'username' => 'định danh',
        ]);

        if ($validator->fails())
            return Response::error($validator->errors());

        try {
            \DB::beginTransaction();
            $data = $request->only(['email', 'full_name', 'presenter', 'username']);
            $data['password'] = Hash::make(1234);
            $data['token_login'] = md5($data['email']);

            $ga = new GoogleAuthenticator();
            $secret = $ga->createSecret();
            $data['secret_2fa'] = $secret;

            $user = User::create($data);

            $address_vnd = 'X' . md5(time() . '-' . uniqid()) . rand(0, 99999);
            $secret = md5(time() . '-' . uniqid()) . rand(0, 99999);

            $address_btc = new Address([
                'name' => $user->username
            ]);
            $address_btc->setCallbackUrl("https://sieuthibtc.com/callback/{$user->id}/btc");

            $address_eth = new Address([
                'name' => $user->username
            ]);
            $address_eth->setCallbackUrl("https://sieuthibtc.com/callback/{$user->id}/eth");

            $configuration = Configuration::apiKey(config('system.coinbase.key'), config('system.coinbase.secret'));
            $client = Client::create($configuration);
            $btc_account = $client->getAccount(config('system.coinbase.main_btc_account'));
            $eth_account = $client->getAccount(config('system.coinbase.main_eth_account'));

            $btc_wallet = $client->createAccountAddress($btc_account, $address_btc);
            $eth_wallet = $client->createAccountAddress($eth_account, $address_eth);

            $data_wallet = [
                'id_address_btc' => $btc_wallet->getId(),
                'address_btc' => $btc_wallet->getAddress(),
                'id_address_eth' => $eth_wallet->getId(),
                'address_eth' => $eth_wallet->getAddress(),
                'address_vnd' => $address_vnd,
                'user_id' => $user->id,
                'secret' => $secret
            ];

            Wallet::create($data_wallet);

            $ref = $request->get('presenter', null);
            if ($ref != null) {
                $data_ref = ['username' => $data['username'], 'ref_username' => $ref, 'status' => Referral::$status['unavailable']];
                Referral::create($data_ref);
            }

            \DB::commit();

            $this->dispatch((new SendMailActive($user))->onQueue('send_mail_active')->onConnection('database'));

            return Response::success();
        } catch (Exception $e) {
            \DB::rollBack();
            return Response::error();
        }
    }


    function getLogout()
    {
        try {
            $user = $this->authenticate();
            User::findOrFail($user->id)->update(['access_token' => '']);
            return Response::success($user);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    function getValidateToken()
    {
        try {
            $user = $this->authenticate(true);
            $user->wallet = $user->wallet;
            return Response::success($user);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    function getSummary(Request $request, $username = null)
    {

        try {
            $user = $this->authenticate();
            $username = $username == null ? $user->username : $username;

            if ($request->has('full')) {
                $profile = User::where('username', $username)
                    ->withCount(['tradeIn' => function ($query) {
                        return $query->where('status', Trade::$status['done']);
                    }])
                    ->withCount(['tradeOut' => function ($query) {
                        return $query->where('status', Trade::$status['done']);
                    }])
                    ->firstOrFail();
                $data = [
                    'wallet' => [
                        'balance_btc' => $profile->Wallet->balance_btc,
                        'balance_eth' => $profile->Wallet->balance_eth,
                        'balance_vnd' => $profile->Wallet->balance_vnd,
                        'traded_btc' => $profile->Wallet->traded_btc,
                        'traded_eth' => $profile->Wallet->traded_eth,
                    ],
                    'info' => $profile,
                ];
            } else {
                $profile = User::where('username', $username)
                    ->select(['id', 'username', 'full_name', 'email', 'phone', 'latest_login_at', 'created_at', 'rate', 'doc_status', 'phone_status'])
                    ->withCount(['tradeIn' => function ($query) {
                        return $query->where('status', Trade::$status['done']);
                    }])
                    ->withCount(['tradeOut' => function ($query) {
                        return $query->where('status', Trade::$status['done']);
                    }])
                    ->firstOrFail();
                $data = [
                    'wallet' => [
                        'balance_btc' => $profile->Wallet->balance_btc,
                        'balance_eth' => $profile->Wallet->balance_eth,
                        'balance_vnd' => $profile->Wallet->balance_vnd,
                        'traded_btc' => $profile->Wallet->traded_btc,
                        'traded_eth' => $profile->Wallet->traded_eth,
                    ],
                    'info' => $profile,
                ];
            }

            return Response::success($data);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function getFeedback($username)
    {
        try {
            $data = [
                'list_feedback' => Feedback::where('username', $username)->paginate(10)
            ];
            return Response::success($data);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postUpdateAuthenticator(Request $request)
    {
        try {
            $user = $this->authenticate();
            Validator::extend('2fa', function ($attribute, $value, $parameters, $validator) use ($user) {
                $ga = new GoogleAuthenticator();
                return $ga->verifyCode($user->secret_2fa, $value, 2);
            });

            $validator = Validator::make($request->all(), [
                '2fa' => 'required|2fa'
            ], [
                'required' => 'mã xác thực không được dể trống',
                '2fa' => 'mã không hợp lệ',
            ]);

            if ($validator->fails())
                return Response::error(array_values($validator->errors()->toArray()));

            if ($user->enabled_2fa == 0) {
                $user->enabled_2fa = 1;
                $msg = ['Bật xác thực thành công'];
            } else {
                $ga = new GoogleAuthenticator();
                $user->secret_2fa = $ga->createSecret();
                $user->enabled_2fa = 0;
                $msg = ['Tắt xác thực thành công thành công'];
            }
            $user->save();

            return Response::success($msg);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function getProfileExtra()
    {
        try {
            $user = $this->authenticate();
            $profile = ProfileExtra::firstOrCreate(['user_id' => $user->id]);

            return Response::success($profile);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|image',
            'name' => ['required', Rule::in(['img1', 'img2', 'img3', 'img4'])]
        ], [
            'required' => ':attribute không được để trống',
            'image' => 'Tài liệu phải là hình ảnh',
            'in' => 'Không hợp lệ'
        ], [
            'file' => 'Tập tin',
            'name' => 'Tên'
        ]);

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();
            if ($user->doc_status != User::$doc_status['unsent'])
                return Response::error();

            $name = time() . '.' . $request->file->extension();
            $url = File::uploadFile($_FILES['file'], $name);

            $name = $request->get('name');

            $profile = ProfileExtra::firstOrCreate(['user_id' => $user->id]);
            $profile->$name = $url;
            $profile->save();
            return Response::success($profile);

        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function postConfirmVerify(Request $request)
    {
        try {
            $user = $this->authenticate();
            if ($user->doc_status != User::$doc_status['unsent'])
                return Response::error('Tài khoản của bạn đã gửi tài liệu xác minh');

            $profile = ProfileExtra::where('user_id', $user->id)->where('img1', '<>', null)->where('img2', '<>', null)->where('img3', '<>', null)->where('img4', '<>', null)->firstOrFail();
            $user->doc_status = User::$doc_status['pending'];
            $user->save();
            return Response::success([], [], 'Gửi tài liệu thành công');
        } catch (\Exception $exception) {
            return Response::error('Vui lòng gửi đầy đủ tài liệu trước khi xác nhận gửi');
        }
    }

    function getReferrals()
    {
        try {
            $user = $this->authenticate();
            $refs = Referral::where('ref_username', $user->username)->paginate(10);
            return Response::success($refs);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function getRewardRef($id)
    {
        try {
            \DB::beginTransaction();
            $user = $this->authenticate(true);
            $ref = Referral::where(['ref_username' => $user->username, 'id' => $id])->firstOrFail();
            if ($ref->status != Referral::$status['available'])
                return Response::error('Không khả dụng');
            $ref->status = Referral::$status['receive'];
            $ref->save();

            $wallet = $user->wallet;
            $wallet->total_vnd = $wallet->total_vnd + config('system.bonus_ref');
            $wallet->balance_vnd = $wallet->balance_vnd + config('system.bonus_ref');
            $wallet->save();

            \DB::commit();
            return Response::success($ref, [], 'Đã nhận thưởng thành công');
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error($exception->getMessage());
        }
    }

    function getHistoryLogin()
    {
        try {
            $user = $this->authenticate();
            $data = HistoryLogin::where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(10);
            return Response::success($data);
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function postVerifyPhone(Request $request)
    {
        try {
            $user = $this->authenticate(true);
            if ($user->phone_status == $request->get('code') && !in_array($request->get('code'), [0, 2])) {
                $user->phone_status = 2;
                $user->save();
                return Response::success([], [], 'Xác thực số điện thoại thành công');
            }
            return Response::error('Mã không hợp lệ');
        } catch (\Exception $exception) {
            return Response::error('Có lỗi xảy ra');
        }
    }

    function postAddPhone(Request $request)
    {
        try {
            $user = $this->authenticate();
            if ($user->phone_status == 2)
                return Response::error('Số điện thoại của bạn đã được xác minh');
            if ($user->phone != null)
                return Response::error('Bạn đã nhập số điện thoại');

            if (preg_match("/^(\d{8,11})$/", $request->get('phone')) == 0)
                return Response::error('Số điện thoại không hợp lệ');

            if(User::wherePhone($request->get('phone'))->first())
                return Response::error('Số điện thoại đã tồn tại');

            $user->phone = $request->get('phone');
            $user->phone_status = rand(1111, 9999);
            $user->save();
            $this->dispatch((new SendMailVerifyPhone($user))->onQueue('send_mail_verify_phone')->onConnection('database'));
            return Response::success([], [], 'Thêm số điện thoại thành công');
        } catch (\Exception $exception) {
            return Response::error('Có lỗi xảy ra');
        }
    }

    function postSendCodeVerifyPhone(Request $request)
    {
        try {
            $user = $this->authenticate(true);
            if ($user->phone_status == 2)
                return Response::error('Số điện thoại của bạn đã được xác minh');
            if ($user->phone == null)
                return Response::error('Bạn chưa nhập số điện thoại');

            $user->phone_status = rand(1111, 9999);
            $user->save();

            $this->dispatch((new SendMailVerifyPhone($user))->onQueue('send_mail_verify_phone')->onConnection('database'));
            return Response::success([], [], 'Mã xác thực đã được gửi về mail đăng kí');
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }
}
