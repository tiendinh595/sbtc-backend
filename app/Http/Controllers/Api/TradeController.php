<?php

namespace App\Http\Controllers\Api;

use App\Deal;
use App\Feedback;
use App\Http\Controllers\ApiController;
use App\Offer;
use App\Trade;
use App\Utils\Blockchain;
use App\Utils\File;
use App\Utils\GoogleAuthenticator;
use App\Utils\Helper;
use App\Utils\Response;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class TradeController extends ApiController
{
    function postCreate(Request $request)
    {

        try {
            $offer = Offer::where(['id' => $request->get('offer_id')])->with('user')->firstOrFail();
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }

        Validator::extend('bank_code', function ($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, config('system.bank'));
        });

        if ($offer->type_offer == Offer::$type['buy']) {
            $validator = Validator::make($request->all(), [
                'amount_coin' => 'required|numeric',
                'offer_id' => 'required|exists:tbl_offer,id',
                'bank_code' => 'required|bank_code',
                'bank_number' => 'required',
                'bank_account_name' => 'required',
            ], [
                'exists' => ':attribute không tồn tại',
                'required' => ':attribute bắt được phải nhập',
                'bank_code' => 'Ngân hàng không hơp lê'
            ], [
                'offer_id' => 'offer',
                'bank_code' => 'Tên ngân hàng',
                'bank_number' => 'Số tài khoản',
                'bank_account_name' => 'Tên chủ tài khoản',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'amount_coin' => 'required|numeric',
                'offer_id' => 'required|exists:tbl_offer,id',
//                'to_address' => 'required'
            ], [
                'exists' => ':attribute không tồn tại',
                'required' => ':attribute bắt được phải nhập',
            ], [
                'offer_id' => 'offer',
                'to_address' => 'Địa chỉ nhận'
            ]);
        }

        if ($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate();
            $user = User::where(['id' => $user->id])->with('wallet')->firstOrFail();
            $user_offer = $offer->user;

            if ($offer->status != Offer::$status['open'])
                return Response::error('Offer này đã đóng');

            if ($offer->username == $user->username)
                return Response::error('Không thể thao tác với chính offer của bạn');

            \DB::beginTransaction();

            $amount_coin = round($request->get('amount_coin'), 8);
            $coin_name = $offer->type_money == Offer::$type_money['btc'] ? 'BTC' : 'ETH';

            //for buy order
            if ($offer->type_offer == Offer::$type['buy']) {
                $seller = $user;
                $buyer = $user_offer;

                $outgoing = $offer->outgoing;

                $balance_coin = $offer->type_money == Offer::$type_money['btc'] ? $seller->wallet->balance_btc : $seller->wallet->balance_eth;

                // check amount coin with offer
                if ($amount_coin > $offer->current_amount || $amount_coin < $offer->min_amount)
                    return Response::error("Số lượng {$coin_name} bạn bán không phù hợp với offer này, vui lòng chọn trong khoảng {$offer->min_amount} - {$offer->current_amount}");

                // check with amount coin to trade
                if ($balance_coin < $amount_coin && $offer->deal_id == 0)
                    return Response::error("Bạn không đủ {$amount_coin} {$coin_name} để bán");

                $buyer_receiving_coin_amount = Helper::calCoinToBuyer($amount_coin);
                $seller_receiving_vnd_amount = Helper::formatVND($amount_coin * $offer->price_per_coin);
                $buyer_sending_vnd_amount = Helper::formatVND($amount_coin * $offer->price_per_coin);
                $fee = Helper::calFeeToBuyer($amount_coin);

                $using_vnd_to_pay = false;
                //using vnd to pay
//                if ($buyer_sending_vnd_amount <= $buyer->wallet->balance_vnd) {
//                    $using_vnd_to_pay = true;
//                    $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
//                    $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;
//                    $buyer->wallet->balance_vnd = $buyer->wallet->balance_vnd - $buyer_sending_vnd_amount;
//                }
                //update
                $using_vnd_to_pay = true;
                $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
                $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;

                //btc
                if ($offer->type_money == Offer::$type_money['btc']) {
                    $from_address = $seller->wallet->address_btc;
                    $to_address = $buyer->wallet->address_btc;
                    //seller
                    if ($offer->deal_id == 0) {
                        $seller->wallet->balance_btc = $seller->wallet->balance_btc - $amount_coin;
                        if (!$using_vnd_to_pay)
                            $seller->wallet->freeze_btc = $seller->wallet->freeze_btc + $amount_coin;
                    }

                    //buyer
                    if ($offer->outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_btc = $buyer->wallet->pending_btc + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_btc = $buyer->wallet->total_btc + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_btc = $buyer->wallet->balance_btc + $buyer_receiving_coin_amount;
                        }
                    }
                } //eth
                else {
                    $from_address = $seller->wallet->address_eth;
                    $to_address = $buyer->wallet->address_eth;
                    //seller
                    if ($offer->deal_id == 0) {
                        $seller->wallet->balance_eth = $seller->wallet->balance_eth - $amount_coin;
                        if (!$using_vnd_to_pay)
                            $seller->wallet->freeze_eth = $seller->wallet->freeze_eth + $amount_coin;
                    }

                    //buyer
                    if ($offer->outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_eth = $buyer->wallet->pending_eth + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_eth = $buyer->wallet->total_eth + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_eth = $buyer->wallet->balance_eth + $buyer_receiving_coin_amount;
                        }
                    }

                }
                $seller->wallet->save();
                $buyer->wallet->save();

                $trade_status = $using_vnd_to_pay == true ? Trade::$status['done'] : Trade::$status['waiting'];

                $data_trade = [
                    'buyer_username' => $buyer->username,
                    'seller_username' => $seller->username,
                    'from_address' => $from_address,
                    'to_address' => $to_address,
                    'offer_id' => $offer->id,
                    'type_money' => $offer->type_money,
                    'seller_sending_coin_amount' => $amount_coin,
                    'buyer_receiving_coin_amount' => $buyer_receiving_coin_amount,
                    'seller_receiving_vnd_amount' => $seller_receiving_vnd_amount,
                    'buyer_sending_vnd_amount' => $buyer_sending_vnd_amount,
                    'price_per_coin' => $offer->price_per_coin,
                    'price_per_coin_before_fee' => $offer->price_per_coin_before_fee,
                    'fee' => $fee,
                    'payment_method' => $offer->payment_method,
                    'payment_time' => Carbon::now()->addMinutes($offer->payment_time)->format('Y-m-d H:i:s'),
                    'token' => '',
                    'bank_code' => $request->get('bank_code'),
                    'bank_number' => $request->get('bank_number'),
                    'bank_account_name' => $request->get('bank_account_name'),
                    'status' => $trade_status,
                    'outgoing' => $outgoing
                ];

                $trade = Trade::create($data_trade);
                $trade->payment_memo = "STBTC {$trade->id}";
                $trade->save();

            } //for sell order
            else {
                $seller = $user_offer;
                $buyer = $user;

//                $outgoing = Wallet::checkAddressInternal($request->get('to_address'), strtolower($coin_name));
                $outgoing = false;

                // check amount coin with offer
                if ($amount_coin > $offer->current_amount || $amount_coin < $offer->min_amount)
                    return Response::error("Số lượng {$coin_name} bạn mua không phù hợp với offer này, vui lòng chọn trong khoảng {$offer->min_amount} - {$offer->current_amount}");

                $buyer_receiving_coin_amount = Helper::calCoinToBuyer($amount_coin);
                $seller_receiving_vnd_amount = Helper::formatVND($amount_coin * $offer->price_per_coin);
                $buyer_sending_vnd_amount = Helper::formatVND($amount_coin * $offer->price_per_coin);
                $fee = Helper::calFeeToBuyer($amount_coin);

                $using_vnd_to_pay = false;
                //using vnd to pay
                if ($buyer_sending_vnd_amount <= $buyer->wallet->balance_vnd) {
                    $using_vnd_to_pay = true;
                    $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
                    $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;
                    $buyer->wallet->balance_vnd = $buyer->wallet->balance_vnd - $buyer_sending_vnd_amount;
                } else {
                    return Response::error('Bạn không đủ vnd để thực hiện mua');
                }

//                update
//                $using_vnd_to_pay = true;
//                $seller->wallet->total_vnd = $seller->wallet->total_vnd + $seller_receiving_vnd_amount;
//                $seller->wallet->balance_vnd = $seller->wallet->balance_vnd + $seller_receiving_vnd_amount;

                //btc
                if ($offer->type_money == Offer::$type_money['btc']) {
                    $from_address = $seller->wallet->address_btc;
                    $to_address = $buyer->wallet->address_btc;
                    //buyer
                    if ($outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_btc = $buyer->wallet->pending_btc + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_btc = $buyer->wallet->total_btc + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_btc = $buyer->wallet->balance_btc + $buyer_receiving_coin_amount;
                        }
                    }
                } //eth
                else {
                    $from_address = $seller->wallet->address_eth;
                    $to_address = $buyer->wallet->address_eth;

                    //buyer
                    if ($outgoing == false) {
                        if (!$using_vnd_to_pay)
                            $buyer->wallet->pending_eth = $buyer->wallet->pending_eth + $buyer_receiving_coin_amount;
                        else {
                            $buyer->wallet->total_eth = $buyer->wallet->total_eth + $buyer_receiving_coin_amount;
                            $buyer->wallet->balance_eth = $buyer->wallet->balance_eth + $buyer_receiving_coin_amount;
                        }
                    }
                }


                $trade_status = $using_vnd_to_pay == true ? Trade::$status['done'] : Trade::$status['waiting'];
                $data_trade = [
                    'buyer_username' => $buyer->username,
                    'seller_username' => $seller->username,
                    'from_address' => $from_address,
                    'to_address' => $to_address,
                    'offer_id' => $offer->id,
                    'type_money' => $offer->type_money,
                    'seller_sending_coin_amount' => $amount_coin,
                    'buyer_receiving_coin_amount' => $buyer_receiving_coin_amount,
                    'seller_receiving_vnd_amount' => $seller_receiving_vnd_amount,
                    'buyer_sending_vnd_amount' => $buyer_sending_vnd_amount,
                    'price_per_coin' => $offer->price_per_coin,
                    'price_per_coin_before_fee' => $offer->price_per_coin_before_fee,
                    'fee' => $fee,
                    'payment_method' => $offer->payment_method,
                    'payment_time' => Carbon::now()->addMinutes($offer->payment_time)->format('Y-m-d H:i:s'),
                    'token' => '',
                    'bank_code' => $offer->bank_code,
                    'bank_number' => $offer->bank_number,
                    'bank_account_name' => $offer->bank_account_name,
                    'status' => $trade_status,
                    'outgoing' => $outgoing
                ];

                $trade = Trade::create($data_trade);
                $trade->payment_memo = "STBTC {$trade->id}";
                $trade->save();
            }

            // update offer
            $offer->current_amount = $offer->current_amount - $amount_coin;
            if ($offer->current_amount == 0) {
                $offer->status = Offer::$status['done'];
            }
            $offer->save();

            if ($offer->deal_id != 0) {
                Deal::find($offer->deal_id)->update(['trade_id' => $trade->id]);
            }
            $seller->wallet->save();
            $buyer->wallet->save();

            \DB::commit();
            return Response::success($trade);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error($exception->getMessage());
        }
    }

    function getDetail($id)
    {
        try {
            $user = $this->authenticate();
            $trade = Trade::where(['id' => $id])->whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')")->first();
            if ($trade == null)
                return Response::not_found('giao dịch không tìm thấy');

            $trade->role = $trade->buyer_username == $user->username ? 'buyer' : 'seller';
            $trade->feedback = Feedback::where(['trade_id' => $id, 'reporter' => $user->username])->first();
            return Response::success($trade);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    function putUpdateState(Request $request, $id)
    {
        try {
            $user = $this->authenticate();
            $trade = Trade::where(['id' => $id])
                ->whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')")
                ->with(['buyer', 'seller', 'offer'])
                ->first();

            if ($trade == null)
                return Response::not_found('giao dịch không tìm thấy');

            if ($trade->status != Trade::$status['waiting'])
                return Response::error('giao dịch đã đóng', 500, $trade);

            $role = $trade->buyer_username == $user->username ? 'buyer' : 'seller';

            $seller = $trade->seller;
            $buyer = $trade->buyer;

            \DB::beginTransaction();
            switch ($request->get('status')) {
                case 'received':
                    if ($role == 'buyer')
                        return Response::not_permission();

                    $trade->status = Trade::$status['done'];
                    $trade->action_seller = Trade::$action_seller['received'];
                    $trade->save();

                    // update balance
                    if ($trade->type_money == Offer::$type_money['btc']) {
                        $seller->wallet->freeze_btc = $seller->wallet->freeze_btc - $trade->seller_sending_coin_amount;
                        if ($trade->outgoing == false) {
                            $buyer->wallet->pending_btc = $buyer->wallet->pending_btc - $trade->buyer_receiving_coin_amount;
                            $buyer->wallet->balance_btc = $buyer->wallet->balance_btc + $trade->buyer_receiving_coin_amount;
                            $buyer->wallet->total_btc = $buyer->wallet->total_btc + $trade->buyer_receiving_coin_amount;
                        }
                    } else {
                        $seller->wallet->freeze_eth = $seller->wallet->freeze_eth - $trade->seller_sending_coin_amount;
                        if ($trade->outgoing == false) {
                            $buyer->wallet->pending_eth = $buyer->wallet->pending_eth - $trade->buyer_receiving_coin_amount;
                            $buyer->wallet->balance_eth = $buyer->wallet->balance_eth + $trade->buyer_receiving_coin_amount;
                            $buyer->wallet->total_eth = $buyer->wallet->total_eth + $trade->buyer_receiving_coin_amount;
                        }
                    }
                    $seller->wallet->save();
                    $buyer->wallet->save();

                    break;

                case 'not-received':
                    if ($role == 'buyer')
                        return Response::not_permission();

                    if (strtotime($trade->payment_time) - time() > 180)
                        return Response::not_permission('Không thể thực hiện trong thời gian giao dịch');

//                    // update balance
//                    if ($trade->type_money == Offer::$type_money['btc']) {
//                        $seller->wallet->freeze_btc = $seller->wallet->freeze_btc - $trade->seller_sending_coin_amount;
//                        $seller->wallet->balance_btc = $seller->wallet->balance_btc + $trade->seller_sending_coin_amount;
//                    } else {
//                        $seller->wallet->freeze_eth = $seller->wallet->freeze_eth - $trade->seller_sending_coin_amount;
//                        $seller->wallet->balance_eth = $seller->wallet->balance_eth + $trade->seller_sending_coin_amount;
//                    }
//                    $seller->wallet->save();
//                    $buyer->wallet->save();

                    $trade->action_seller = Trade::$action_seller['not-received'];
                    $trade->save();

                    break;

                case 'delivered':
                    if ($role == 'seller')
                        return Response::not_permission();

                    $trade->action_buyer = Trade::$action_buyer['delivered'];
                    $trade->save();
                    break;

                case 'cancel':
                    if ($role == 'seller')
                        return Response::not_permission();

                    $offer = $trade->offer;

                    $offer->current_amount = $offer->current_amount + $trade->seller_sending_coin_amount;
                    if ($offer->current_amount > 0)
                        $offer->status = Offer::$status['open'];
                    $offer->save();

                    $trade->action_buyer = Trade::$action_buyer['cancel'];
                    $trade->status = Trade::$status['user_cancel'];
                    $trade->save();
                    break;

                case 'dispute':
                    if ($role == 'seller')
                        return Response::not_permission();
                    if ($trade->action_buyer == Trade::$action_buyer['dispute'])
                        return Response::not_permission('Giao dịch đã đưa vào trạng thái tranh chấp');

                    $trade->action_buyer = Trade::$action_buyer['dispute'];
                    $trade->save();

                    break;

                default:
                    return Response::error('Trạng thái không hợp lệ');
            }
            \DB::commit();

            $trade->role = $role;
            return Response::success($trade);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error('Có lỗi xảy ra');
        }

    }

    function postUploadReceipt(Request $request, $id)
    {
        try {
            $user = $this->authenticate();

            $validator = Validator::make($request->all(), [
                'image' => 'required|image'
            ], [
                'required' => 'Vui lòng chọn file',
                'image' => 'Tệp tin không hợp lệ'
            ]);

            if ($validator->fails())
                return Response::error(array_values($validator->errors()->toArray()));

            $trade = Trade::where(['id' => $id])
                ->whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')")
                ->with(['buyer', 'seller'])
                ->first();

            if ($trade == null)
                return Response::not_found('giao dịch không tìm thấy');

            if ($trade->status != Trade::$status['waiting'])
                return Response::error('giao dịch đã đóng', 500, $trade);

            $role = $trade->buyer_username == $user->username ? 'buyer' : 'seller';
            if ($role == 'seller')
                return Response::not_permission();

            $name = time() . '.' . $request->image->extension();
            $url = File::uploadFile($_FILES['image'], $name);
            $trade->receipt = $url;
            $trade->save();

            \DB::commit();
            return Response::success($trade);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return Response::error('Có lỗi xảy ra');
        }
    }

    function putUpdateTimeUp(Request $request, $id)
    {
        try {
            $user = $this->authenticate();
            $trade = Trade::where(['id' => $id])
                ->whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')")
                ->with(['buyer', 'seller', 'offer'])
                ->first();

            if (strtotime($trade->payment_time) > time())
                return Response::not_permission('Không thể thực hiện trong thời gian giao dịch');

            $trade->time_up = 1;

            if ($trade->status == Trade::$status['waiting']) {
                if (in_array($trade->action_buyer, [0, 2]) && in_array($trade->action_seller, [0, 2])) {

                    $trade->status = Trade::$status['bot_cancel'];

                    $offer = $trade->offer;
                    $offer->current_amount = $offer->current_amount + $trade->seller_sending_coin_amount;
                    $offer->save();
                } else {
                    $trade->status = Trade::$status['dispute'];
                }
            }

            $trade->save();

            return Response::success($trade);
        } catch (\Exception $exception) {
            \DB::rollBack();
//            return Response::error($exception->getMessage());
            return Response::error('Có lỗi xảy ra');
        }
    }


    function getListTrade($status)
    {
        try {
            $user = $this->authenticate();
            if (!in_array($status, ['active', 'closed']))
                return Response::error();
            $trades = Trade::whereRaw("(seller_username = '$user->username' or buyer_username = '$user->username')");
            $trades = $status == 'active' ? $trades->where('status', Trade::$status['waiting']) : $trades->where('status', '<>', Trade::$status['waiting']);
            $trades = $trades->orderBy('id', 'DESC')->paginate(6);
            return Response::success($trades);
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }
}
