<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\System;
use App\Utils\Response;

class SystemController extends ApiController
{
    function getDataGlobal()
    {
        $data = \Cache::remember('data_global', 5, function () {
            return System::all()->mapWithKeys(function ($item) {
                return [$item->key => $item->value];
            });
        });
        return Response::success($data);
    }

}
