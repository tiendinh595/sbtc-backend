<?php

namespace App\Http\Controllers\Api;

use App\Bank;
use App\Deal;
use App\Feedback;
use App\History;
use App\Http\Controllers\ApiController;
use App\Offer;
use App\Transaction;
use App\Utils\Helper;
use App\Utils\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BankController extends ApiController
{
    function getAllBanks(Request $request)
    {
        try {
            $user = $this->authenticate(false);
            $banks = Bank::where('user_id', $user->id)->active()->get();
            return Response::success($banks);
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function postAddNew(Request $request)
    {
        Validator::extend('bank_code', function ($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, config('system.bank'));
        });
        $validator = Validator::make($request->all(), [
            'bank_code' => 'required|bank_code',
            'bank_number' => 'required',
            'bank_account_name' => 'required'
        ],[
            'required' => ':attribute không được để trống',
            'bank_code'=>'Ngân hàng không hợp lệ'
        ],[
            'bank_code'=>'Ngân hàng',
            'bank_number'=>'Số tài khoản',
            'bank_account_name' => 'Chủ tài khoản'
        ]);

        if($validator->fails())
            return Response::error(array_values($validator->errors()->toArray()));

        try {
            $user = $this->authenticate(false);
            $data_bank = $request->only(['bank_code','bank_number', 'bank_account_name']);
            $data_bank['user_id'] = $user->id;
            $data_bank['status'] = Bank::$status['active'];
            Bank::create($data_bank);
            $banks = Bank::where('user_id', $user->id)->get();

            return Response::success($banks, [], 'Thêm ngân hàng thành công');
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    function deleteBank($id) {
        try {
            $user = $this->authenticate(false);
            $bank = Bank::where(['user_id'=> $user->id, 'id'=>$id])->firstOrFail();
            $bank->status = Bank::$status['inactive'];
            $bank->save();
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }
}
