<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/20/17
 * Time: 9:29 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\ApiController;
use App\Utils\File;
use App\Utils\Response;
use Illuminate\Http\Request;

class FileController extends ApiController
{
    function postUpload(Request $request)
    {
        try {
            $user = $this->authenticate();
            $this->validate($request, [
                'file'=>'image|required'
            ]);

            $name = time() . '.' . $request->file->extension();
            $url = File::uploadFile($_FILES['file'], $name);

            return Response::success([
                'url'=>$url
            ]);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

}