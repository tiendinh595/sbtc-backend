<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/21/17
 * Time: 11:08 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Observers;


use App\Jobs\SendMailNotification;
use App\Mail;
use App\Offer;
use App\Trade;
use App\Utils\Helper;
use App\Utils\Pusher;

class TradeObserver
{
    public function updated(Trade $trade)
    {
        $pusher = Pusher::getInstance();
        $pusher->trigger('trade', 'update', ['trade_id' => $trade->id]);

        $currency_name = strtoupper(Offer::$currency_name[$trade->type_money]);
        if ($trade->getOriginal('status') != $trade->status) {
            switch ($trade->status) {
                case Trade::$status['done']:
                    $data_mail = [
                        'title' => "Trạng thái giao dịch $currency_name #{$trade->id}",
                        'content' => "Giao dịch $currency_name #{$trade->id} của bạn đã hoàn tất.<br/> <a href='https://sieuthibtc.com/trade/{$trade->id}'><b>Tới giao dịch</b></a>"
                    ];

                    if ($trade->type_money == Trade::$type_money['btc']) {
                        $trade->seller->wallet->traded_btc = $trade->seller->wallet->traded_btc + $trade->seller_sending_coin_amount;
                        $trade->buyer->wallet->traded_btc = $trade->buyer->wallet->traded_btc + $trade->buyer_receiving_coin_amount;
                    } else {
                        $trade->seller->wallet->traded_eth = $trade->seller->wallet->traded_eth + $trade->seller_sending_coin_amount;
                        $trade->buyer->wallet->traded_eth = $trade->buyer->wallet->traded_eth + $trade->buyer_receiving_coin_amount;
                    }
                    $trade->seller->wallet->save();
                    $trade->buyer->wallet->save();

                    break;

                case Trade::$status['user_cancel']:
                case Trade::$status['bot_cancel']:
                    $data_mail = [
                        'title' => "Trạng thái giao dịch $currency_name #{$trade->id}",
                        'content' => "Giao dịch $currency_name #{$trade->id} của bạn đã bị huỷ.<br/> <a href='https://sieuthibtc.com/trade/{$trade->id}'><b>Tới giao dịch</b></a>"
                    ];
                    break;
                case Trade::$status['dispute']:
                    $data_mail = [
                        'title' => "Trạng thái giao dịch $currency_name #{$trade->id}",
                        'content' => "Giao dịch $currency_name #{$trade->id} của bạn đã đưa vào trạng thái tranh chấp.<br/> <a href='https://sieuthibtc.com/trade/{$trade->id}'><b>Tới giao dịch</b></a>"
                    ];
                    break;
            }

            if (isset($data_mail)) {
                $data_mail_internal_seller = array_merge($data_mail, ['user_id' => $trade->seller->id, 'type' => Mail::$type['in']]);
                $data_mail_external_seller = array_merge($data_mail, ['name' => $trade->seller->username, 'email' => $trade->seller->email]);
                Mail::create($data_mail_internal_seller);
                dispatch((new SendMailNotification($data_mail_external_seller))->onQueue('send_mail_notification')->onConnection('database'));

                $data_mail_internal_buyer = array_merge($data_mail, ['user_id' => $trade->buyer->id, 'type' => Mail::$type['in']]);
                $data_mail_external_buyer = array_merge($data_mail, ['name' => $trade->buyer->username, 'email' => $trade->buyer->email]);
                Mail::create($data_mail_internal_buyer);
                dispatch((new SendMailNotification($data_mail_external_buyer))->onQueue('send_mail_notification')->onConnection('database'));
            }

        }
    }

    function created(Trade $trade)
    {
        $trade_arr = $trade->toArray();
        unset($trade_arr['token']);
        $trade->update(['token' => \Crypt::encrypt($trade_arr)]);
        
        if ($trade->status == Trade::$status['done']) {
            if ($trade->type_money == Trade::$type_money['btc']) {
                $trade->seller->wallet->traded_btc = $trade->seller->wallet->traded_btc + $trade->seller_sending_coin_amount;
                $trade->buyer->wallet->traded_btc = $trade->buyer->wallet->traded_btc + $trade->buyer_receiving_coin_amount;
            } else {
                $trade->seller->wallet->traded_eth = $trade->seller->wallet->traded_eth + $trade->seller_sending_coin_amount;
                $trade->buyer->wallet->traded_eth = $trade->buyer->wallet->traded_eth + $trade->buyer_receiving_coin_amount;
            }
            $trade->seller->wallet->save();
            $trade->buyer->wallet->save();
        }

        $offer = Offer::where('id', $trade->offer_id)->select(['id', 'username'])->with('user')->first();
        $user = $offer->user;
        if ($user->username == $trade->seller_username) {
            $trade_name = 'mua';
            $username = $trade->buyer_username;
            $amount_vnd = $trade->seller_receiving_vnd_amount;
            $amount_coin = $trade->seller_sending_coin_amount;
        } else {
            $trade_name = 'bán';
            $username = $trade->seller_username;
            $amount_vnd = $trade->buyer_sending_vnd_amount;
            $amount_coin = $trade->buyer_receiving_coin_amount;
        }
        $currency_name = strtoupper(Offer::$currency_name[$trade->type_money]);
        $amount_vnd_format = number_format($amount_vnd);
        $data_mail = [
            'title' => "Bạn có giao dịch {$trade_name} {$currency_name} mới",
            'content' => "$username muốn {$trade_name} {$amount_coin}{$currency_name} - {$amount_vnd_format}VND với bạn lúc {$trade->created_at}.<br/> <a href='https://sieuthibtc.com/trade/{$trade->id}'><b>Tới giao dịch</b></a>"
        ];
        $data_mail_internal = array_merge($data_mail, ['user_id' => $user->id, 'type' => Mail::$type['in']]);
        $data_mail_external = array_merge($data_mail, ['name' => $user->username, 'email' => $user->email]);

        Mail::create($data_mail_internal);
        dispatch((new SendMailNotification($data_mail_external))->onQueue('send_mail_notification')->onConnection('database'));
    }
}