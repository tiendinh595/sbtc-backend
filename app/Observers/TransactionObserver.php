<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/21/17
 * Time: 11:08 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Observers;


use App\Bank;
use App\History;
use App\Jobs\SendMailNotification;
use App\Mail;
use App\Transaction;
use App\User;
use App\Utils\Helper;

class TransactionObserver
{
    public function updated(Transaction $transaction)
    {
        History::where(['ref_id' => $transaction->id, 'type' => $transaction->type])->update(['status' => $transaction->status]);
        if($transaction->getOriginal('status') != $transaction->status)
            $this->sendMail($transaction);
    }

    public function created(Transaction $transaction)
    {
        if ($transaction->type_money == 3) {
            $uid = uniqid();
            $payment_memo = $transaction->type == 1 ? "{$uid}{$transaction->id}" : "{$uid}{$transaction->id}";
        } else {
            $coin_name = ['Bitcoin', 'Ethereum'];
            $payment_memo = $transaction->type == 1 ? "Nạp {$coin_name[$transaction->type_money - 1]}" : "Rút {$coin_name[$transaction->type_money - 1]}";
        }

        $transaction_arr = $transaction->toArray();
        unset($transaction['token']);
        $transaction->update(['token' => \Crypt::encrypt($transaction_arr), 'payment_memo' => $payment_memo]);

        History::create([
            'user_id' => $transaction->user_id,
            'ref_id' => $transaction->id,
            'type_money' => $transaction->type_money,
            'amount' => $transaction->amount,
            'type' => $transaction->type,
            'status' => $transaction->status,
            'payment_memo' => $transaction->payment_memo
        ]);

        $this->sendMail($transaction);
    }

    /**
     * @param Transaction $transaction
     */
    public function sendMail(Transaction $transaction)
    {
        $currencies = ['BTC', 'ETH', 'VND'];
        $currency_name = $currencies[$transaction->type_money - 1];
        $amount = $transaction->type_money == $transaction->amount_format;

        $data_mail = [];
        if ($transaction->type == Transaction::$type['deposit']) {
            switch ($transaction->status) {
                case Transaction::$status['pending']:
                    $data_mail = [
                        'title' => 'Lệnh nạp ' . $currency_name . ' mới được khởi tạo',
                        'content' => "Chúng tôi nhận được lệnh nạp <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at}. Lệnh nạp của bạn đang được xử lý."
                    ];
                    break;
                case Transaction::$status['success']:
                    $data_mail = [
                        'title' => 'Tài khoản của bạn vừa được nạp ' . $currency_name,
                        'content' => "Tài khoản của bạn vừa được nạp <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at}"
                    ];
                    break;
                case Transaction::$status['reject']:
                    $data_mail = [
                        'title' => "Lệnh nạp $currency_name bị từ chối",
                        'content' => "Lệnh nạp #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn đã bị từ chối"
                    ];
                    break;
                case Transaction::$status['cancel']:
                    $data_mail = [
                        'title' => "Lệnh nạp $currency_name bị hủy",
                        'content' => "Lệnh nạp #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn đã bị bủy"
                    ];
                    break;
                case Transaction::$status['error']:
                    $data_mail = [
                        'title' => "Lệnh nạp $currency_name có lỗi",
                        'content' => "Lệnh nạp #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn có lỗi xảy ra"
                    ];
                    break;
            }

        } else {
            $to_address = $transaction->type_money == Transaction::$type_money['fiat'] ? Bank::find($transaction->to_address)->bank_name : $transaction->to_address;
            switch ($transaction->status) {
                case Transaction::$status['pending']:
                    $data_mail = [
                        'title' => 'Lệnh rút ' . $currency_name . ' mới được khởi tạo',
                        'content' => "Chúng tôi nhận được lệnh rút <b>{$amount}</b> {$currency_name} tới địa chỉ <b>{$to_address}</b> vào lúc {$transaction->created_at}. Lệnh rút của bạn đang được xử lý."
                    ];
                    break;
                case Transaction::$status['success']:
                    $data_mail = [
                        'title' => "Rút $currency_name thành công",
                        'content' => "Lệnh rút <b>{$amount}</b> {$currency_name} tới địa chỉ <b>{$to_address}</b> vào lúc {$transaction->created_at} đã được xử lý"
                    ];
                    break;
                case Transaction::$status['reject']:
                    $data_mail = [
                        'title' => "Lệnh rút $currency_name bị từ chối",
                        'content' => "Lệnh rút #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn đã bị từ chối"
                    ];
                    break;
                case Transaction::$status['cancel']:
                    $data_mail = [
                        'title' => "Lệnh rút $currency_name bị hủy",
                        'content' => "Lệnh rút #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn đã bị bủy"
                    ];
                    break;
                case Transaction::$status['error']:
                    $data_mail = [
                        'title' => "Lệnh rút $currency_name có lỗi",
                        'content' => "Lệnh rút #{$transaction->id} <b>{$amount}</b> {$currency_name} vào lúc {$transaction->created_at} của bạn có lỗi xảy ra"
                    ];
                    break;
            }
        }

        if (count($data_mail)) {
            $user = User::find($transaction->user_id);
            $data_mail_internal = array_merge($data_mail, ['user_id'=>$transaction->user_id, 'type'=>Mail::$type['in']]);
            $data_mail_external = array_merge($data_mail, ['name'=>$user->username, 'email'=>$user->email]);

            Mail::create($data_mail_internal);
            dispatch((new SendMailNotification($data_mail_external))->onQueue('send_mail_notification')->onConnection('database'));
        }
    }
}