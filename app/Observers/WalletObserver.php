<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/21/17
 * Time: 11:08 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Observers;


use App\Utils\Pusher;
use App\Wallet;

class WalletObserver
{
    public function updated(Wallet $wallet)
    {
        $pusher = Pusher::getInstance();
        $pusher->trigger('user', 'update', ['user_id' => $wallet->user_id]);
    }
}