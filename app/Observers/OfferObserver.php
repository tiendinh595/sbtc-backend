<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/18/17
 * Time: 9:57 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Observers;

use App\Offer;
use App\Utils\Response;

class OfferObserver
{
    function updated(Offer $offer)
    {
//        try {
        if ($offer->getOriginal('status') == $offer->status) {
            if ($offer->current_amount == 0) {
                $offer->status = Offer::$status['done'];
                $offer->save();
            }
        }

//        } catch (\Exception $exception) {
//            dd($exception);
//        }
    }
}