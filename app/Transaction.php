<?php

namespace App;
use App\Utils\Helper;
use Illuminate\Database\Eloquent\Model;
class Transaction extends Model
{
    protected $table = 'tbl_transaction';

    protected $fillable = [
        'id', 'user_id', 'amount', 'amount_before_fee', 'fee', 'from_address', 'to_address', 'type_money', 'type', 'payment_memo', 'extra', 'status', 'status_job', 'token', 'message', 'hash', 'confirmations', 'outgoing', 'is_approved'
    ];

    protected $appends = ['amount_format'];

    protected $casts = [
        'outgoing' => 'boolean',
    ];

    static public $type_money = [
        'btc' => 1,
        'eth' => 2,
        'fiat' => 3
    ];

    static public $status = [
        'init' => 0,
        'pending' => 1,
        'success' => 2,
        'reject' => 3,
        'cancel' => 4,
        'error' => 5,
    ];

    static $status_approve = [
        'pending' => 0,
        'approved' => 1,
        'reject'=>2
    ];

    static public $status_job = [
        'pending' => 1,
        'started' => 2
    ];

    static public $type = [
        'deposit' => 1,
        'withdraw' => 2
    ];

    function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    static function calAmountWithdrawAfterFee($amount, $type_money)
    {
        $fee = $type_money == Transaction::$type_money['btc'] ? config('system.fee_withdraw_btc') : config('system.fee_withdraw_eth');
        return Helper::formatCoin($amount - $fee);
    }

    function getAmountFormatAttribute($value)
    {
        if ($this->type_money == 3)
            return number_format($this->amount);
        return $this->amount;
    }

    function setExtraAttribute($value)
    {
        try {
            $value = json_encode($value);
        } catch (\Exception $exception) {
            $value = json_encode([]);
        }
        $this->attributes['extra'] = $value;
    }

    function getExtraAttribute($value)
    {
        try {
            return json_decode($value, true);
        } catch (\Exception $exception) {
            return [];
        }
    }

    function validateToken()
    {
        try {
            $payload = \Crypt::decrypt($this->token);
            return (
                $payload['user_id'] == $this->user_id
                && $payload['amount'] == $this->amount
                && $payload['amount_before_fee'] == $this->amount_before_fee
                && $payload['type_money'] == $this->type_money
                && $payload['type'] == $this->type
            );
        } catch (\Exception $exception) {
            return false;
        }
    }
}
