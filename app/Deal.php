<?php

namespace App;

use App\Utils\Helper;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Deal
 *
 * @property int $id
 * @property string|null $username
 * @property int $offer_id
 * @property float $amount
 * @property float $rate_vnd_usd
 * @property float|null $total_money
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Offer $offer
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whererate_vnd_usd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereTotalMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deal whereUsername($value)
 * @mixin \Eloquent
 */
class Deal extends Model
{
    protected $table = 'tbl_deal';

    protected $fillable = [
        'id', 'username', 'owner_offer', 'offer_id', 'trade_id', 'amount', 'rate_vnd_usd', 'rate_usd_coin', 'total_money', 'type', 'status'
    ];

    static $status = [
        'waiting' => 1,
        'approved' => 2,
        'reject' => 3
    ];

    public $casts = [
        'trade_id'=>'integer'
    ];

    static $type = [
        'sell' => 1,
        'buy' => 2
    ];

    function user()
    {
        return $this->belongsTo(User::class, 'username', 'username')->with('wallet');
    }

    function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id', 'id');
    }

    function offerRef() {
        return $this->hasOne(Offer::class, 'deal_id');
    }

    function setAmountAttribute($value)
    {
        return $this->attributes['amount'] = Helper::formatCoin($value);
    }
//
//    function setRateVndUsdAttribute($value)
//    {
//        return $this->attributes['rate_vnd_usd'] = Helper ($value);
//    }

    function setTotalMoneyAttribute($value)
    {
        return $this->attributes['total_money'] = Helper::formatVND($value);
    }
}
