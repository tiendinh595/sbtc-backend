<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionWithdraw extends Model
{
    protected $table = 'tbl_session_withdraw';

    protected $fillable = [
        'id',
        'amount',
        'type_money',
        'total_transaction',
        'transaction_success',
        'transaction_failure',
        'status',
        'extra'
    ];

    static public $type_money = [
        'btc' => 1,
        'eth' => 2,
        'fiat' => 3
    ];

    static $status = [
        'init' => 0,
        'starting' => 1,
        'completed' => 2,
        'error' => 3
    ];
}
