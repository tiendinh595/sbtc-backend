<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'tbl_referral';

    protected $fillable = ['id', 'username', 'ref_username', 'status'];

    static public $status = [
        'unavailable' => 0,
        'available' => 1,
        'receive' => 2
    ];
}
