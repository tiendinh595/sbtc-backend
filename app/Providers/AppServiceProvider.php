<?php

namespace App\Providers;

use App\Observers\OfferObserver;
use App\Observers\TradeObserver;
use App\Observers\TransactionObserver;
use App\Observers\WalletObserver;
use App\Offer;
use App\Trade;
use App\Transaction;
use App\Wallet;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
//        Offer::observe(OfferObserver::class);
        Trade::observe(TradeObserver::class);
        Wallet::observe(WalletObserver::class);
        Transaction::observe(TransactionObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
