<?php

namespace App;

use App\Utils\GoogleAuthenticator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\User
 *
 * @property int $id
 * @property string $full_name
 * @property string $username
 * @property string $email
 * @property string|null $password
 * @property string|null $presenter
 * @property int|null $enabled_2fa
 * @property string|null $secret_2fa
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $token_login
 * @property string|null $token_login_time
 * @property string|null $access_token
 * @property string|null $remember_token
 * @property string|null $latest_login_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $ref
 * @property-read mixed $url_q_r_code
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read \App\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEnabled2fa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLatestLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePresenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSecret2fa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTokenLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTokenLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'tbl_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'username', 'full_name', 'email', 'password', 'presenter', 'facebook', 'twitter', 'token_login', 'token_login_time', 'secret_2fa', 'enabled_2fa', 'access_token', 'status', 'doc_status', 'phone_status', 'is_special', 'rate'
    ];

    protected $appends = ['url_qr_code', 'doc_status_name', 'phone_status_name'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token'
    ];

    public static $status = [
        'inactive' => 1,
        'active' => 2,
        'verified' => 3
    ];

    public static $doc_status = [
        'unsent' => 0,
        'pending' => 1,
        'verified' => 2,
        'reject' => 3
    ];

    public $casts = [
        'status' => 'int',
        'doc_status' => 'int',
        'phone_status' => 'int',
    ];

    function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id');
    }

    function tradeIn()
    {
        return $this->hasMany(Trade::class, 'buyer_username', 'username');
    }

    function tradeOut()
    {
        return $this->hasMany(Trade::class, 'seller_username', 'username');
    }

    function referrals()
    {
        return $this->hasMany(Referral::class, 'ref_username', 'username');
    }

    function historyLogin()
    {
        return $this->hasMany(HistoryLogin::class, 'user_id');
    }

    function getTradeInCountAttribute($value)
    {
        return (int)$value;
    }

    function getTradeOutCountAttribute($value)
    {
        return (int)$value;
    }

    function getRefAttribute($value)
    {
        return url($this->username);
    }

    function getUrlQRCodeAttribute($val)
    {
        $ga = new GoogleAuthenticator();
        return $ga->getQRCodeGoogleUrl($this->email, $this->secret_2fa, config('app.name'));
    }

    function getDocStatusNameAttribute($val)
    {
        try {
            return ['chưa gửi', 'đang duyệt', 'đã duyệt', 'từ trối'][$this->doc_status];
        } catch (\Exception $exception) {
            return 'chưa gửi';
        }
    }

    function getPhoneStatusNameAttribute($val)
    {
        try {
            return ['chưa gửi', 'đã duyệt'][$this->phone_status];
        } catch (\Exception $exception) {
            return 'đang chờ xác nhận';
        }
    }


}
