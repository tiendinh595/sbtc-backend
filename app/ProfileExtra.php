<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileExtra extends Model
{
    protected $table = 'tbl_profile_extra';

    protected $fillable = [
        'id', 'user_id', 'img1', 'img2', 'img3', 'img4'
    ];
}
