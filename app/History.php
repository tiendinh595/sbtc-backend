<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'tbl_history';

    protected $fillable = [
        'id', 'user_id', 'ref_id', 'type_money', 'amount', 'type', 'status', 'payment_memo'
    ];

    protected $appends = ['status_name', 'amount_format'];

    static public $type = [
        'deposit' => 1,
        'withdraw' => 2,
        'buy' => 3,
        'sell' => 4
    ];

    static public $status = [
        'init' => 0,
        'pending' => 1,
        'success' => 2,
        'reject' => 3,
        'cancel' => 4,
        'error' => 5,
    ];

    function getStatusNameAttribute($value) {
        $array_status = ['đang xử lý', 'thành công', 'từ chối', 'huỷ bỏ', 'có lỗi'];
        return $array_status[$this->status - 1];
    }

    function getAmountFormatAttribute($value) {
        if($this->type_money == 3)
            return number_format($this->amount);
        return $this->amount;
    }
}
