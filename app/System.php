<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $primaryKey = 'key';

    public $incrementing = false;

    protected $table = 'tbl_system';

    protected $fillable = ['key', 'value'];

    public $timestamps = false;
}
