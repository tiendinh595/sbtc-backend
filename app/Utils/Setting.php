<?php
/**
 * Created by PhpStorm.
 * User: trinhtam
 * Date: 10/13/17
 * Time: 11:20
 */

namespace App\Utils;


class Setting
{
    public static $file_config = 'sys.ini';
    public static $file_invoice = 'sys_invoice.ini';
    public static $file_deposit = 'sys_deposit.ini';

    public static function updateSetting($key, $data)
    {
        $settings = self::readSetting();
        $settings[$key] = $data;
        file_put_contents(config_path(self::$file_config), json_encode($settings));
    }

    public static function readSetting($key = null)
    {
        $settings = json_decode(file_get_contents(config_path(self::$file_config)), true);
        if ($key == null)
            return $settings;
        if (in_array($key, $settings))
            return $settings[$key];
        return [];
    }

    public static function invoice($settings){
        $details = json_decode(file_get_contents(config_path(self::$file_invoice)), true);
        $new_settings = [];
        if(is_array($settings)){
            foreach ($settings as $key => $setting){
                $new_settings[$key] = [
                    'value' => $setting,
                    'detail' => $details[$key],
                ];
            }
            return $new_settings;
        }else if (isset($details[$settings]))
            return $details[$settings];
        return $settings;
    }

    public static function deposit($settings){
        $details = json_decode(file_get_contents(config_path(self::$file_deposit)), true);
        $new_settings = [];
        if(is_array($settings)){
            foreach ($settings as $key => $setting){
                $new_settings[$key] = [
                    'value' => $setting,
                    'detail' => $details[$key],
                ];
            }
            return $new_settings;
        }else if (isset($details[$settings]))
            return $details[$settings];
        return $settings;
    }
}