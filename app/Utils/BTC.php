<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 11/15/17
 * Time: 9:43 PM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Utils;


use App\System;

class BTC
{
    static function checkAddress($address)
    {
        $origbase58 = $address;
        $dec = "0";

        for ($i = 0; $i < strlen($address); $i++)
        {
            $dec = bcadd(bcmul($dec,"58",0),strpos("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",substr($address,$i,1)),0);
        }

        $address = "";

        while (bccomp($dec,0) == 1)
        {
            $dv = bcdiv($dec,"16",0);
            $rem = (integer)bcmod($dec,"16");
            $dec = $dv;
            $address = $address.substr("0123456789ABCDEF",$rem,1);
        }

        $address = strrev($address);

        for ($i = 0; $i < strlen($origbase58) && substr($origbase58,$i,1) == "1"; $i++)
        {
            $address = "00".$address;
        }

        if (strlen($address)%2 != 0)
        {
            $address = "0".$address;
        }

        if (strlen($address) != 50)
        {
            return false;
        }

        if (hexdec(substr($address,0,2)) > 0)
        {
            return false;
        }

        return substr(strtoupper(hash("sha256",hash("sha256",pack("H*",substr($address,0,strlen($address)-8)),true))),0,8) == substr($address,strlen($address)-8);
    }

    static function validateTransaction($transaction)
    {
        $token = \Crypt::decrypt($transaction->token);
        if($token->$transaction->from_address != $transaction->from_address || $token->$transaction->to_address != $transaction->to_address || $token->$transaction->type != $transaction->type || $token->$transaction->amount != $transaction->amount || $token->$transaction->transaction_hash != $transaction->transaction_hash)
            return false;
        return true;
    }

    static function getConfirmations($hash) {
        $blockchain = System::getInstanceBlockchain();
        $latest = $blockchain->Explorer->getLatestBlock();
        $tx = $blockchain->Explorer->getTransaction($hash);
        if($tx->block_height)
            return ($latest->height - $tx->block_height + 1);
        return $latest;
    }
}