<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 11/30/17
 * Time: 11:00 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Utils;


class Blockchain
{

    static $instance_blockchain = null;

    static function getInstanceBlockchain() {
        if (self::$instance_blockchain == null) {
            self::$instance_blockchain = new \Blockchain\Blockchain(config('app.api_code'));
            self::$instance_blockchain->setServiceUrl(config('app.url_api_wallet'));
            self::$instance_blockchain->Wallet->credentials(config('system.wallet_withdraw.guid_main'), config('system.wallet_withdraw.pwd_main'));

        }
        return self::$instance_blockchain;
    }
}