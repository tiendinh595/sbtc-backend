<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 1/12/18
 * Time: 9:35 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Utils;


class Pusher
{
    static private $_instance = null;

    static function getInstance() {
//        $app_cluster = 'mt1';
//        $host = config('app.slanger.host');
//        $port = config('app.slanger.port');
//        $pusher = new \Pusher\Pusher(config('app.slanger.key'), config('app.slanger.secret'), 1, array('cluster' => $app_cluster), $host, $port);
        if(self::$_instance == null) {
            self::$_instance = new \Pusher\Pusher(config('app.slanger.key'), config('app.slanger.secret'), config('app.slanger.app_id'), array(
                'cluster' => 'ap1',
                'encrypted' => true
            ));
        }
        return self::$_instance;
    }
}