<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/16/17
 * Time: 11:15 AM
 * Contact: tiendinh595@gmail.com
 */

namespace App\Utils;


use App\System;

class Helper
{
    static function calCoinToBuyer($coin)
    {
        $fee = System::where('key', 'fee_trade')->first()->value * $coin / 100;
        $amount = self::formatCoin($coin - $fee);
        return $amount;
    }

    static function calFeeToBuyer($coin) {
        $fee = System::where('key', 'fee_trade')->first()->value * $coin / 100;
        $fee = self::formatCoin($fee);
        return $fee;
    }

    static function formatVND($amount) {
        return round($amount);
    }

    static function formatUSD($amount) {
        return round($amount, 2);
    }

    static function formatCoin($amount) {
        return round($amount, 8);
    }

    static function formatETH($amount) {
        return round($amount, 8);
    }

    static function isAddressEth($address) {
        if (!preg_match('/^(0x)?[0-9a-f]{40}$/i',$address)) {
            // check if it has the basic requirements of an address
            return false;
        } elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/',$address) || preg_match('/^(0x)?[0-9A-F]{40}$/',$address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return self::isChecksumAddressEth($address);
        }
    }

    static function isChecksumAddressEth($address) {
        // Check each case
        $address = str_replace('0x','',$address);
        $addressHash = hash('sha3',strtolower($address));
        $addressArray=str_split($address);
        $addressHashArray=str_split($addressHash);

        for($i = 0; $i < 40; $i++ ) {
            // the nth letter should be uppercase if the nth digit of casemap is 1
            if ((intval($addressHashArray[$i], 16) > 7 && strtoupper($addressArray[$i]) !== $addressArray[$i]) || (intval($addressHashArray[$i], 16) <= 7 && strtolower($addressArray[$i]) !== $addressArray[$i])) {
                return false;
            }
        }
        return true;
    }

    static function isAddressBtc($address){
        try {
            $decoded = self::decodeBase58($address);

            $d1 = hash("sha256", substr($decoded,0,21), true);
            $d2 = hash("sha256", $d1, true);

            if(substr_compare($decoded, $d2, 21, 4)){
                return false;
//                throw new \Exception("bad digest");
            }
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    static function decodeBase58($input) {
        $alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

        $out = array_fill(0, 25, 0);
        for($i=0;$i<strlen($input);$i++){
            if(($p=strpos($alphabet, $input[$i]))===false){
                throw new \Exception("invalid character found");
            }
            $c = $p;
            for ($j = 25; $j--; ) {
                $c += (int)(58 * $out[$j]);
                $out[$j] = (int)($c % 256);
                $c /= 256;
                $c = (int)$c;
            }
            if($c != 0){
                throw new \Exception("address too long");
            }
        }

        $result = "";
        foreach($out as $val){
            $result .= chr($val);
        }

        return $result;
    }

}