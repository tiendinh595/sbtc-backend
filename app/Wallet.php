<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Wallet
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $secret
 * @property string $address_btc
 * @property float|null $total_btc
 * @property float|null $balance_btc
 * @property float|null $pending_btc
 * @property float|null $freeze_btc
 * @property float|null $traded_btc
 * @property string $address_eth
 * @property float|null $total_eth
 * @property float|null $balance_eth
 * @property float|null $pending_eth
 * @property float|null $freeze_eth
 * @property float|null $traded_eth
 * @property string $address_vnd
 * @property float|null $total_vnd
 * @property float|null $balance_vnd
 * @property float|null $pending_vnd
 * @property float|null $freeze_vnd
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereAddressBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereAddressEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereAddressVnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereBalanceBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereBalanceEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereBalanceVnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereFreezeBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereFreezeEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereFreezeVnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet wherePendingBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet wherePendingEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet wherePendingVnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereTotalBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereTotalEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereTotalVnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereTradedBtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereTradedEth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wallet whereUserId($value)
 * @mixin \Eloquent
 */
class Wallet extends Model
{
    protected $table = 'tbl_wallet';
    protected $fillable = ['id', 'user_id', 'secret', 'id_address_btc', 'address_btc', 'total_btc', 'balance_btc', 'pending_btc', 'freeze_btc', 'traded_btc', 'id_address_eth', 'address_eth', 'total_eth', 'balance_eth', 'pending_eth', 'freeze_eth', 'traded_eth', 'address_vnd', 'total_vnd', 'balance_vnd', 'pending_vnd', 'freeze_vnd'];

    function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    function setBalanceBtcAttribute($val) {
        $this->attributes['balance_btc'] = round($val, 8);
    }

    function setTotalBtcAttribute($val) {
        $this->attributes['total_btc'] = round($val, 8);
    }

    function setPendingBtcAttribute($val) {
        $this->attributes['pending_btc'] = round($val, 8);
    }

    function setFreezeBtcAttribute($val) {
        $this->attributes['freeze_btc'] = round($val, 8);
    }

    function setBalanceEthAttribute($val) {
        $this->attributes['balance_eth'] = round($val, 8);
    }

    function setTotalEthAttribute($val) {
        $this->attributes['total_eth'] = round($val, 8);
    }

    function setPendingEthAttribute($val) {
        $this->attributes['pending_eth'] = round($val, 8);
    }

    function setFreezeEthAttribute($val) {
        $this->attributes['freeze_eth'] = round($val, 8);
    }

    static function checkAddressInternal($address, $type) {
        return Wallet::where(['address_'.$type => $address])->first() == null;
    }
}
