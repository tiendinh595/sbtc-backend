<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Feedback
 *
 * @property int $id
 * @property string $username
 * @property string $feedbacker_username
 * @property string|null $content
 * @property int|null $rating
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereFeedbackerUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereUsername($value)
 * @mixin \Eloquent
 * @property string $reporter
 * @property int|null $trade_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereReporter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feedback whereTradeId($value)
 */
class Feedback extends Model
{
    protected $table = 'tbl_feedback';
    protected $fillable = ['id', 'username', 'reporter', 'rating', 'content', 'trade_id'];

}
