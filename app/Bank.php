<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'tbl_bank';

    protected $fillable = ['id', 'user_id', 'bank_code', 'bank_number', 'bank_account_name', 'status'];

    static public $status = [
        'active'=>1,
        'inactive'=>2
    ];

    protected $appends = ['bank_name'];

    function scopeActive($query) {
        return $query->where('status', Bank::$status['active']);
    }

    function scopeInactive($query) {
        return $query->where('status', Bank::$status['inactive']);
    }

    function getBankNameAttribute($value) {
        $banks = config('system.bank');
        return isset($banks[$this->bank_code]) ? $banks[$this->bank_code] : '';
    }
}
