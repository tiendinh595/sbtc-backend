<?php

namespace App\Console;

use App\Console\Commands\ScanTimeUpTrade;
use App\Console\Commands\TransferTradeBTC;
use App\Console\Commands\TransferTradeETH;
use App\Console\Commands\UpdatePrice;
use App\Console\Commands\WithdrawBTC;
use App\Console\Commands\WithdrawETH;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ScanTimeUpTrade::class,
        WithdrawETH::class,
        WithdrawBTC::class,
        UpdatePrice::class,
        TransferTradeBTC::class,
        TransferTradeETH::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command(ScanTimeUpTrade::class)->everyMinute();
        $schedule->command(UpdatePrice::class)->everyMinute();
        $schedule->command(WithdrawETH::class)->everyTenMinutes();
        $schedule->command(WithdrawBTC::class)->everyTenMinutes();
//        $schedule->command(TransferTradeETH::class)->everyTenMinutes();
//        $schedule->command(TransferTradeBTC::class)->everyTenMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
