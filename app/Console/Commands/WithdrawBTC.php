<?php

namespace App\Console\Commands;

use App\SessionWithdraw;
use App\SessionWithdrawDetail;
use App\Utils\Blockchain;
use Illuminate\Console\Command;
use App\Transaction;
use App\Utils\Helper;
use App\Wallet;


class WithdrawBTC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw_btc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'withdraw_btc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions_pending = Transaction::where([
            'type_money' => Transaction::$type_money['btc'],
            'status_job' => Transaction::$status_job['pending'],
            'is_approved' => Transaction::$status_approve['approved']
        ])->get();


        //blockchain
        $block_chain = \App\Utils\Blockchain::getInstanceBlockchain();
        $balance_wallet_withdraw = $block_chain->Wallet->getBalance();
        if($balance_wallet_withdraw < $transactions_pending->sum('amount'))
            return;

        $recipients = [];

        $ids_internal = [];
        $ids_external = [];


        $hash_internal = md5(time() . uniqid());

        $transaction_update = [];
        $data_update_session_detail = $session_withdraws_detail = [];
        $data_update_session = [
            'status' => SessionWithdraw::$status['init'],
            'extra' => ''
        ];

        $session_withdraw = null;
        if ($transactions_pending->count() > 0) {
            $session_withdraw = SessionWithdraw::create([
                'amount' => $transactions_pending->sum('amount'),
                'type_money' => SessionWithdraw::$type_money['btc'],
                'total_transaction' => $transactions_pending->count(),
                'transaction_success' => 0,
                'transaction_failure' => 0,
                'status' => SessionWithdraw::$status['init']
            ]);

            $session_withdraws_detail = [];
            foreach ($transactions_pending as $key => $transaction) {
                $session_withdraws_detail[$transaction->id] = SessionWithdrawDetail::create([
                    'session_withdraw_id' => $session_withdraw->id,
                    'transaction_id' => $transaction->id,
                    'status' => SessionWithdrawDetail::$status['init'],
                ]);
                $data_update_session_detail[$transaction->id] = [
                    'status' => SessionWithdrawDetail::$status['started'],
                    'extra' => ''
                ];
            }
        }

        try {

            if ($session_withdraw != null) {
                \DB::beginTransaction();

                $data_update_session['status'] = SessionWithdraw::$status['starting'];

                foreach ($transactions_pending as $key => $transaction) {
                    $transaction->status_job = Transaction::$status_job['started'];
                    $data_update_session_detail[$transaction->id]['status'] = SessionWithdrawDetail::$status['started'];
                    try {
                        if ($transaction->validateToken()) {
                            try {
                                if (!$transaction->outgoing) {
                                    $wallet_internal = Wallet::where('address_btc', $transaction->to_address)->first();
                                    \Log::info('internal ' . $transaction->id);
                                    $ids_internal[] = $transaction->id;
                                    $this->transferCoinToInternalWallet($transaction, $wallet_internal, $hash_internal);
                                    $data_update_session_detail[$transaction->id]['is_sent'] = SessionWithdrawDetail::$is_sent['sent'];
                                } else {
                                    \Log::info('external ' . $transaction->id);

                                    if (!Helper::isAddressBtc($transaction->to_address))
                                        throw new \Exception('invalid address');
                                    $ids_external[] = $transaction->id;
                                    $recipients[$transaction->to_address] = $transaction->amount;
                                }
                                $data_update_session_detail[$transaction->id]['status'] = SessionWithdrawDetail::$status['success'];
                                $transaction_update[$transaction->id] = [
                                    'status' => Transaction::$status['success'],
                                    'message' => ''
                                ];
//                                $transaction->status = Transaction::$status['success'];
                            } catch (\Exception $exception) {
                                \Log::error('withdraw error ' . $transaction->id);
                                \Log::error($exception);
                                $transaction_update[$transaction->id] = [
                                    'status' => Transaction::$status['error'],
                                    'message' => $exception->getMessage()
                                ];
//                                $transaction->status = Transaction::$status['error'];
//                                $transaction->message = $exception->getMessage();

                                $data_update_session_detail[$transaction->id] = [
                                    'status' => SessionWithdrawDetail::$status['failure'],
                                    'extra' => $exception->getMessage()
                                ];
                            }
                        } else {
                            \Log::error('withdraw reject ' . $transaction->id . ' : invalid token');
//                            $transaction->status = Transaction::$status['reject'];
//                            $transaction->message = 'invalid token';

                            $transaction_update[$transaction->id] = [
                                'status' => Transaction::$status['reject'],
                                'message' => 'invalid token'
                            ];

                            $data_update_session_detail[$transaction->id] = [
                                'status' => SessionWithdrawDetail::$status['reject'],
                                'extra' => 'invalid token'
                            ];
                        }

                    } catch (\Exception $ex) {
                        $transaction_update[$transaction->id] = [
                            'status' => Transaction::$status['error'],
                            'message' => $ex->getMessage()
                        ];
                        $data_update_session_detail[$transaction->id] = [
                            'status' => SessionWithdrawDetail::$status['failure'],
                            'extra' => $ex->getMessage()
                        ];
                    }

                    $transaction->save();

                }


                $data_update_session['status'] = SessionWithdraw::$status['completed'];

                try {
                    if (count($recipients)) {
                        $block_chain = Blockchain::getInstanceBlockchain();
                        $response = $block_chain->Wallet->sendMany($recipients, config('system.wallet_withdraw.address_main'));
                        Transaction::whereIn('id', $ids_external)->update(['hash' => $response->tx_hash]);
                        foreach ($ids_external as $id_transaction) {
                            $data_update_session_detail[$id_transaction]['is_sent'] = SessionWithdrawDetail::$is_sent['sent'];
                        }
                    }
                } catch (\Exception $exception) {
                    $data_update_session = [
                        'status' => SessionWithdraw::$status['error'],
                        'extra' => $exception->getMessage() . "\n" . $exception->getTraceAsString()
                    ];

                    foreach ($ids_external as $id_transaction) {
                        if ($transaction_update[$id_transaction]['status'] == Transaction::$status['success']) {
                            $transaction_update[$id_transaction]['status'] = Transaction::$status['error'];
                            $data_update_session_detail[$id_transaction]['is_sent'] = SessionWithdrawDetail::$is_sent['crash'];
                        }
                    }
                }


                if (count($ids_internal)) {
                    Transaction::whereIn('id', $ids_internal)->update(['hash' => $hash_internal]);
                }

                \DB::commit();
            }
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::error('WithdrawBTC');
            \Log::error($exception->getMessage());
            $data_update_session = ['status' => SessionWithdraw::$status['error'],
                'extra' => $exception->getMessage() . "\n" . $exception->getTraceAsString()];
        }

        if ($session_withdraw != null)
            $session_withdraw->update($data_update_session);

        foreach ($transactions_pending as $key => $transaction) {
            $session_withdraws_detail[$transaction->id]->update($data_update_session_detail[$transaction->id]);
            $transaction->update($transaction_update[$transaction->id]);
        }
    }

    private
    function transferCoinToInternalWallet($transaction, $wallet, $hash_internal)
    {
        $amount_before_fee = $transaction->amount;
        $fee = Helper::formatCoin((config('system.fee_deposit') * $amount_before_fee) / 100);
        $amount = Helper::formatCoin($amount_before_fee - $fee);

        $wallet->total_btc = $wallet->total_btc + $amount;
        $wallet->balance_btc = $wallet->balance_btc + $amount;
        $wallet->save();

        $type_money = Transaction::$type_money['btc'];

        $user = $wallet->user;

        Transaction::create([
            'user_id' => $user->id,
            'amount' => $amount,
            'amount_before_fee' => $amount_before_fee,
            'fee' => $fee,
            'from_address' => $transaction->from_address,
            'to_address' => $transaction->to_address,
            'type_money' => $type_money,
            'type' => Transaction::$type['deposit'],
            'extra' => [],
            'status' => Transaction::$status['success'],
            'status_job' => Transaction::$status_job['started'],
            'hash' => $hash_internal
        ]);
    }
}
