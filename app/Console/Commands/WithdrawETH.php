<?php

namespace App\Console\Commands;

use App\SessionWithdraw;
use App\SessionWithdrawDetail;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\EthereumAddress;
use Coinbase\Wallet\Value\Money;
use Illuminate\Console\Command;
use App\Transaction;
use App\Utils\Helper;
use App\Wallet;

class WithdrawETH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw_eth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'withdraw_eth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions_pending = Transaction::where([
            'type_money' => Transaction::$type_money['eth'],
            'status_job' => Transaction::$status_job['pending'],
            'is_approved' => Transaction::$status_approve['approved']
        ])->get();

        $configuration = Configuration::apiKey(config('system.coinbase.key'), config('system.coinbase.secret'));
        $client = Client::create($configuration);
        $eth_account = $client->getAccount(config('system.coinbase.main_eth_account'));
        $balance_wallet_withdraw = $eth_account->getBalance()->getAmount();
        if($balance_wallet_withdraw < $transactions_pending->sum('amount'))
            return;

        $ids_internal = [];

        $transaction_update = [];
        $data_update_session_detail = $session_withdraws_detail = [];
        $data_update_session = [
            'status' => SessionWithdraw::$status['init'],
            'extra' => ''
        ];

        $session_withdraw = null;
        if ($transactions_pending->count() > 0) {
            $session_withdraw = SessionWithdraw::create([
                'amount' => $transactions_pending->sum('amount'),
                'type_money' => SessionWithdraw::$type_money['btc'],
                'total_transaction' => $transactions_pending->count(),
                'transaction_success' => 0,
                'transaction_failure' => 0,
                'status' => SessionWithdraw::$status['init']
            ]);

            $session_withdraws_detail = [];
            foreach ($transactions_pending as $key => $transaction) {
                $session_withdraws_detail[$transaction->id] = SessionWithdrawDetail::create([
                    'session_withdraw_id' => $session_withdraw->id,
                    'transaction_id' => $transaction->id,
                    'status' => SessionWithdrawDetail::$status['init'],
                ]);
                $data_update_session_detail[$transaction->id] = [
                    'status' => SessionWithdrawDetail::$status['started'],
                    'extra' => ''
                ];
            }
        }


        try {

            $data_update_session['status'] = SessionWithdraw::$status['starting'];

            $hash_internal = md5(time() . uniqid());
            foreach ($transactions_pending as $transaction) {
                $transaction->status_job = Transaction::$status_job['started'];
                if ($transaction->validateToken()) {
                    try {
                        \DB::beginTransaction();

                        if (!$transaction->outgoing) {
                            $wallet_internal = Wallet::where('address_eth', $transaction->to_address)->first();
                            \Log::info('internal ' . $transaction->id);
                            $ids_internal[] = $transaction->id;
                            $this->transferCoinToInternalWallet($transaction, $wallet_internal, $hash_internal);
                        } else {
                            \Log::info('external ' . $transaction->id);

                            if (!Helper::isAddressEth($transaction->to_address))
                                throw new \Exception('invalid address');

                            $transaction_eth = \Coinbase\Wallet\Resource\Transaction::send();

                            $transaction_eth->setAmount(new \Coinbase\Wallet\Value\Money($transaction->amount, 'ETH'));
                            $transaction_eth->setToEthereumAddress($transaction->to_address);
                            $transaction_response = $client->createAccountTransaction($eth_account, $transaction_eth);
                            $client->enableActiveRecord();
                            $transaction_response = $eth_account->getTransaction($transaction_response->getId());
                            $transaction_response = $transaction_response->getRawData();
                            \Log::info($transaction_response);
//                        $transaction->hash = $transaction_response['network']['hash'];
                            $transaction->extra = ['id_transaction' => $transaction_response['id']];
                        }

                        $data_update_session_detail[$transaction->id]['is_sent'] = SessionWithdrawDetail::$is_sent['sent'];
                        $data_update_session_detail[$transaction->id]['status'] = SessionWithdrawDetail::$status['success'];

                        $transaction->status = Transaction::$status['success'];
                        $transaction->save();


                        \DB::commit();
                    } catch (\Exception $exception) {
                        $data_update_session_detail[$transaction->id] = [
                            'status' => SessionWithdrawDetail::$status['failure'],
                            'extra' => $exception->getMessage()
                        ];

                        \DB::rollBack();
                        \DB::beginTransaction();
                        \Log::error('withdraw error ' . $transaction->id);
                        \Log::error($exception);
                        $transaction->status = Transaction::$status['error'];
                        $transaction->message = $exception->getMessage();
                        $transaction->save();
                        \DB::commit();
                    }
                } else {
                    $data_update_session_detail[$transaction->id] = [
                        'status' => SessionWithdrawDetail::$status['reject'],
                        'extra' => 'invalid token'
                    ];

                    \DB::beginTransaction();
                    \Log::error('withdraw reject ' . $transaction->id . ' : invalid token');
                    $transaction->status = Transaction::$status['reject'];
                    $transaction->message = 'invalid token';
                    $transaction->save();
                    \DB::commit();
                }

                $session_withdraws_detail[$transaction->id]->update($data_update_session_detail[$transaction->id]);
            }

            if (count($ids_internal)) {
                Transaction::whereIn('id', $ids_internal)->update(['hash' => $hash_internal]);
            }
            $data_update_session['status'] = SessionWithdraw::$status['completed'];

        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::error('WithdrawETH');
            \Log::error($exception->getMessage());
            $data_update_session = ['status' => SessionWithdraw::$status['error'],
                'extra' => $exception->getMessage() . "\n" . $exception->getTraceAsString()];
        }

        if ($session_withdraw != null)
            $session_withdraw->update($data_update_session);

    }

    private function transferCoinToInternalWallet($transaction, $wallet, $hash_internal)
    {
        try {
            \DB::beginTransaction();
            $amount_before_fee = $transaction->amount;
            $fee = Helper::formatCoin((config('system.fee_deposit') * $amount_before_fee) / 100);
            $amount = Helper::formatCoin($amount_before_fee - $fee);

            $wallet->total_eth = $wallet->total_eth + $amount_before_fee;
            $wallet->balance_eth = $wallet->balance_eth + $amount_before_fee;
            $wallet->save();

            $type_money = Transaction::$type_money['eth'];

            $user = $wallet->user;

            Transaction::create([
                'user_id' => $user->id,
                'amount' => $amount,
                'amount_before_fee' => $amount_before_fee,
                'fee' => $fee,
                'from_address' => $transaction->from_address,
                'to_address' => $transaction->to_address,
                'type_money' => $type_money,
                'type' => Transaction::$type['deposit'],
                'extra' => [],
                'status' => Transaction::$status['success'],
                'status_job' => Transaction::$status_job['started'],
                'hash' => $hash_internal
            ]);
            \DB::commit();
        } catch (\Exception $exception) {
            \Log::error('transferCoinToInternalWallet');
            \Log::error($exception->getMessage());
            \DB::rollBack();
        }

    }
}
