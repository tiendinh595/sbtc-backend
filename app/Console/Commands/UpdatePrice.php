<?php

namespace App\Console\Commands;

use App\System;
use App\Utils\Pusher;
use Illuminate\Console\Command;

class UpdatePrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );

            $price_btc = file_get_contents('https://www.bitstamp.net/api/v2/ticker/btcusd/', false, stream_context_create($arrContextOptions));
            $price_btc = json_decode($price_btc, true);

            $price_eth = file_get_contents('https://www.bitstamp.net/api/v2/ticker/ethusd/', false, stream_context_create($arrContextOptions));
            $price_eth = json_decode($price_eth, true);

            $data_price = [
                'price_high_btc' => $price_btc['high'],
                'price_low_btc' => $price_btc['low'],
                'price_bid_btc' => $price_btc['bid'],
                'price_ask_btc' => $price_btc['ask'],
                'price_last_btc' => $price_btc['last'],
                'price_high_eth' => $price_eth['high'],
                'price_low_eth' => $price_eth['low'],
                'price_bid_eth' => $price_eth['bid'],
                'price_ask_eth' => $price_eth['ask'],
                'price_last_eth' => $price_eth['last'],
            ];

            foreach ($data_price as $key => $value) {
                System::whereKey($key)->update(['value' => $value]);
            }

            \Cache::forget('data_global');
            \Cache::remember('data_global', 5, function () {
                return System::all()->mapWithKeys(function ($item) {
                    return [$item->key => $item->value];
                });
            });

            $pusher = Pusher::getInstance();
            $pusher->trigger('system', 'update', \Cache::get('data_global'));
        } catch (\Exception $exception) {

        }

    }
}
