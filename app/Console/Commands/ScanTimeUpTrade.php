<?php

namespace App\Console\Commands;

use App\Trade;
use Illuminate\Console\Command;

class ScanTimeUpTrade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scan_timeup_trade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'scan order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $trades = Trade::whereRaw("UNIX_TIMESTAMP(now()) > UNIX_TIMESTAMP(payment_time) and time_up = 0")->with('offer')->get();
        foreach ($trades as $trade) {
            $trade->time_up = 1;

            if ($trade->status == Trade::$status['waiting']) {
                if (in_array($trade->action_buyer, [0, 2]) && in_array($trade->action_seller, [0, 2])) {

                    $trade->status = Trade::$status['bot_cancel'];

                    $offer = $trade->offer;
                    $offer->current_amount = $offer->current_amount + $trade->seller_sending_coin_amount;
                    $offer->save();
                } else {
                    $trade->status = Trade::$status['dispute'];
                }
            }

            $trade->save();
        }
    }
}
