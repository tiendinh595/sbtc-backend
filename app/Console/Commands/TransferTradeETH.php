<?php

namespace App\Console\Commands;

use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Illuminate\Console\Command;
use App\Trade;
use App\Utils\Helper;
use App\Wallet;

class TransferTradeETH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer_eth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'transfer_eth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $trades_pending = Trade::where([
            'type_money' => Trade::$type_money['eth'],
            'status_job' => Trade::$status_job['pending'],
            'is_approved' => Trade::$status_approve['approved'],
            'outgoing' => 1
        ])->get();


        $configuration = Configuration::apiKey(config('system.coinbase.key'), config('system.coinbase.secret'));
        $client = Client::create($configuration);
        $eth_account = $client->getAccount(config('system.coinbase.main_eth_account'));

        foreach ($trades_pending as $trade) {
            $trade->status_job = Trade::$status_job['started'];
            if ($trade->validateToken()) {
                try {
                    \DB::beginTransaction();

                    \Log::info('transfer_eth ' . $trade->id);

                    $trade_eth = \Coinbase\Wallet\Resource\Transaction::send();

                    $trade_eth->setAmount(new \Coinbase\Wallet\Value\Money($trade->buyer_receiving_coin_amount, 'ETH'));
                    $trade_eth->setToEthereumAddress($trade->to_address);
                    $trade_response = $client->createAccountTransaction($eth_account, $trade_eth);
                    $client->enableActiveRecord();
                    $trade_response = $eth_account->getTransaction($trade_response->getId());
                    $trade_response = $trade_response->getRawData();
                    \Log::info($trade_response);
//                        $trade->hash = $trade_response['network']['hash'];
                    $trade->extra = ['id_transaction' => $trade_response['id']];
                    $trade->status_transfer_coin = Trade::$status_transfer['success'];
                    $trade->save();
                    \DB::commit();
                } catch (\Exception $exception) {
                    \DB::rollBack();
                    \DB::beginTransaction();
                    \Log::error('transfer error ' . $trade->id);
                    \Log::error($exception);
                    $trade->status_transfer_coin = Trade::$status_transfer['error'];
                    $trade->message = $exception->getMessage();
                    $trade->save();
                    \DB::commit();
                }
            } else {
                \DB::beginTransaction();
                $trade->status_transfer_coin = Trade::$status_transfer['reject'];
                \Log::error('transfer reject ' . $trade->id . ' : invalid token');
                $trade->message = 'invalid token';
                $trade->save();
                \DB::commit();
            }

        }
        

    }
}
