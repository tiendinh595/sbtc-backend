<?php

namespace App\Console\Commands;

use App\Trade;
use App\Utils\Blockchain;
use Illuminate\Console\Command;
use App\Utils\Helper;
use App\Wallet;


class TransferTradeBTC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer_btc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'transfer_btc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $trades_pending = Trade::where([
            'type_money' => Trade::$type_money['btc'],
            'status_job' => Trade::$status_job['pending'],
            'is_approved' => Trade::$status_approve['approved'],
            'outgoing' => 1
        ])->get();
        $recipients = [];

        $ids_external = [];

        try {
            \DB::beginTransaction();
            foreach ($trades_pending as $trade) {
                $trade->status_job = Trade::$status_job['started'];
                if ($trade->validateToken()) {
                    try {
                        $ids_external[] = $trade->id;
                        $recipients[$trade->to_address] = $trade->buyer_receiving_coin_amount;
                        $trade->status_transfer_coin = Trade::$status_transfer['success'];
                    } catch (\Exception $exception) {
                        \Log::error('TransferTradeBTC error ' . $trade->id);
                        \Log::error($exception);
                        $trade->status_transfer_coin = Trade::$status_transfer['error'];
                        $trade->status_transfer_coin = $exception->getMessage();
                    }
                } else {
                    $trade->status_transfer_coin = Trade::$status_transfer['reject'];
                    \Log::error('TransferTradeBTC reject ' . $trade->id . ' : invalid token');
                    $trade->message = 'invalid token';
                }

                $trade->save();
            }

            if (count($recipients)) {
                $block_chain = Blockchain::getInstanceBlockchain();
                $response = $block_chain->Wallet->sendMany($recipients, config('system.wallet_withdraw.address_main'));
                Trade::whereIn('id', $ids_external)->update(['hash' => $response->tx_hash]);
            }

            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::error('TransferTradeBTC');
            \Log::error($exception->getMessage());
        }
    }

}
