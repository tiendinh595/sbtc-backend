/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost:3306
 Source Schema         : ot_thenap

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : 65001

 Date: 21/10/2017 15:12:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_09_22_101717_create_permission_tables', 1);
COMMIT;

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------
BEGIN;
INSERT INTO `model_has_permissions` VALUES (1, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (2, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (3, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (4, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (5, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (6, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (7, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (8, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (9, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (10, 1, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (1, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (2, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (3, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (4, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (5, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (6, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (7, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (8, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (9, 2, 'App\\Admin');
INSERT INTO `model_has_permissions` VALUES (10, 2, 'App\\Admin');
COMMIT;

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES (1, 'list permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19');
INSERT INTO `permissions` VALUES (2, 'view permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19');
INSERT INTO `permissions` VALUES (3, 'add permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19');
INSERT INTO `permissions` VALUES (4, 'edit permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19');
INSERT INTO `permissions` VALUES (5, 'delete permission', 'admin', 'permission', '2017-09-22 10:40:19', '2017-09-22 10:40:19');
INSERT INTO `permissions` VALUES (6, 'list administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
INSERT INTO `permissions` VALUES (7, 'view administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
INSERT INTO `permissions` VALUES (8, 'add administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
INSERT INTO `permissions` VALUES (9, 'edit administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
INSERT INTO `permissions` VALUES (10, 'delete administrator', 'admin', 'administrator', '2017-09-22 10:40:27', '2017-09-22 10:40:27');
COMMIT;

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for tbl_admin
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tbl_admin
-- ----------------------------
BEGIN;
INSERT INTO `tbl_admin` VALUES (1, 'Trịnh Tâm', 'contact.tamsoft@gmail.com', '$2y$10$tB3b2d1w16iWs4gKTsAtjud7rpZMpPGBnDGnPePJcuYSJTGDz/rKO', 'A1pZuxb66oRQSMuxtn2IiANZpbU6eKXDDYKCwjqptEo20jn5hW2LWcBNJzf1', 1, '2017-09-22 10:36:35', '2017-09-22 10:42:43');
INSERT INTO `tbl_admin` VALUES (2, 'dinh', 'tiendinh595@gmail.com', '$2y$10$ywbRT29JPTxcKx.GJxg5MeGcyHjGVHzNTnS/BSWxQxE/RVxcGz7mq', 'Ea5IwQphW2SKZvQLmQPoOw2YMUv2LPRbe2bCdiki475WOFtwV8wNlQCVH6Yu', 1, '2017-10-04 22:27:57', '2017-10-04 22:27:57');
COMMIT;

-- ----------------------------
-- Table structure for tbl_bank
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bank`;
CREATE TABLE `tbl_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `number` varchar(50) NOT NULL,
  `holder_name` varchar(50) NOT NULL,
  `branch` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_bank
-- ----------------------------
BEGIN;
INSERT INTO `tbl_bank` VALUES (1, 44, 'Vietcombank', '123456789', 'Trinh Thanh Tam', 'Ben Tre');
COMMIT;

-- ----------------------------
-- Table structure for tbl_deposit
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deposit`;
CREATE TABLE `tbl_deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(30) NOT NULL COMMENT '1: sms, 2: card, 3: bank, 4: nhan tien',
  `detail` text,
  `value` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: success, 0: error',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_deposit
-- ----------------------------
BEGIN;
INSERT INTO `tbl_deposit` VALUES (1, 1, '2', 'Mạng: viettel<br>\r\ncode:123456789<br>\r\nserial:65648796', 10000, 9000, 1, '2016-12-18 13:57:28', NULL);
INSERT INTO `tbl_deposit` VALUES (2, 1, '2', 'Mạng: viettel<br>\r\ncode:123456789<br>\r\nserial:65648796', 10000, 9000, 1, '2016-12-26 13:57:28', '0000-00-00 00:00:00');
INSERT INTO `tbl_deposit` VALUES (3, 1, '3', '', NULL, 10000, 1, '2016-12-28 08:43:54', '2016-12-20 08:43:54');
INSERT INTO `tbl_deposit` VALUES (4, 1, '3', '', NULL, 5000, 1, '2016-12-20 08:46:25', '2016-12-20 08:46:25');
INSERT INTO `tbl_deposit` VALUES (5, 1, '3', '', NULL, 2000, 1, '2016-12-20 08:47:17', '2016-12-20 08:47:17');
INSERT INTO `tbl_deposit` VALUES (7, 44, '1', NULL, NULL, 10000, 1, '2017-10-06 16:02:59', '2017-10-06 16:02:59');
INSERT INTO `tbl_deposit` VALUES (8, 44, '1', NULL, NULL, 30000, 1, '2017-10-06 16:03:24', '2017-10-06 16:03:24');
COMMIT;

-- ----------------------------
-- Table structure for tbl_history
-- ----------------------------
DROP TABLE IF EXISTS `tbl_history`;
CREATE TABLE `tbl_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL COMMENT '1: deposit, 2: invoice',
  `pre_amount` double DEFAULT NULL,
  `after_amount` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `note` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_history
-- ----------------------------
BEGIN;
INSERT INTO `tbl_history` VALUES (1, 44, 1, 1, 0, 10000, 10000, NULL, '2016-12-19 13:52:35', '2016-12-19 13:52:35');
INSERT INTO `tbl_history` VALUES (2, 44, 3, 1, 0, 10000, 10000, NULL, '2016-12-20 08:43:54', '2016-12-20 08:43:54');
INSERT INTO `tbl_history` VALUES (3, 44, 4, 1, 10000, 15000, 5000, NULL, '2016-12-20 08:46:25', '2016-12-20 08:46:25');
INSERT INTO `tbl_history` VALUES (4, 44, 5, 1, 15000, 17000, 2000, NULL, '2016-12-20 08:47:17', '2016-12-20 08:47:17');
INSERT INTO `tbl_history` VALUES (5, 44, 2, 2, 17000, 27000, 10000, NULL, '2016-12-20 08:47:53', '2016-12-20 08:47:53');
INSERT INTO `tbl_history` VALUES (6, 44, 3, 2, 27000, 26000, 1000, NULL, '2016-12-20 08:50:53', '2016-12-20 08:50:53');
INSERT INTO `tbl_history` VALUES (7, 44, 9, 2, 100000, 62800, 37200, NULL, '2017-01-03 17:17:42', '2017-01-03 17:17:42');
INSERT INTO `tbl_history` VALUES (9, 44, 7, 1, 100000, 110000, 10000, NULL, '2017-10-06 16:02:59', '2017-10-06 16:02:59');
INSERT INTO `tbl_history` VALUES (10, 44, 8, 1, 110000, 140000, 30000, NULL, '2017-10-06 16:03:24', '2017-10-06 16:03:24');
INSERT INTO `tbl_history` VALUES (11, 44, 10, 2, 1000000, 900000, 100000, NULL, '2017-10-17 12:02:05', '2017-10-17 12:02:05');
COMMIT;

-- ----------------------------
-- Table structure for tbl_invoice
-- ----------------------------
DROP TABLE IF EXISTS `tbl_invoice`;
CREATE TABLE `tbl_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT '1: nap game, 2 card, 3: toup, 4: rut tien, 5: chuyen tien',
  `detail` text,
  `amount` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: success, 0: error',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_invoice
-- ----------------------------
BEGIN;
INSERT INTO `tbl_invoice` VALUES (1, 44, '2', 'Mạng: viettel<br>\r\nMệnh giá: 20k<br>\r\nCode: 123456987<br>\r\nSerial: 154578', 20000, 1, '2017-01-03 15:23:45', NULL);
INSERT INTO `tbl_invoice` VALUES (2, 44, '1', '', 10000, 1, '2017-01-03 08:47:53', '2016-12-20 08:47:53');
INSERT INTO `tbl_invoice` VALUES (3, 44, '2', '', 1000, 1, '2017-01-03 08:50:53', '2016-12-20 08:50:53');
INSERT INTO `tbl_invoice` VALUES (8, 44, '2', NULL, 37200, 0, '2017-01-03 17:17:12', '2017-01-03 17:17:12');
INSERT INTO `tbl_invoice` VALUES (9, 44, '2', 'a:11:{s:4:\"name\";s:9:\"Vinaphone\";s:11:\"servicecode\";s:7:\"VTC0028\";s:8:\"quantity\";s:1:\"2\";s:6:\"amount\";i:20000;s:8:\"f_amount\";d:37200;s:3:\"tax\";d:0.070000000000000007;s:4:\"date\";s:14:\"20170103171742\";s:10:\"orgtransid\";s:15:\"43.9.1483438661\";s:10:\"vtctransid\";i:86713739;s:8:\"listcard\";a:2:{i:0;a:3:{s:8:\"CardCode\";i:1057190659;s:10:\"CardSerial\";i:1064992977;s:11:\"ExpriceDate\";s:10:\"30/12/2017\";}i:1;a:3:{s:8:\"CardCode\";i:1076034738;s:10:\"CardSerial\";i:1183970092;s:11:\"ExpriceDate\";s:10:\"30/12/2017\";}}s:7:\"partner\";s:7:\"partner\";}', 37200, 1, '2017-01-03 17:17:42', '2017-01-03 17:17:42');
INSERT INTO `tbl_invoice` VALUES (10, 44, '4', 'Ngân hàng: Vietcombank<br/>Số tài khoản: 123456789<br/>Tên tài khoản: Trinh Thanh Tam<br/>Chi nhánh: Ben Tre<br/><hr/>Phí rút: 11.000<br/>Tiền nhận: 89.000 (100.000 - 11.000)<br/>', 100000, 1, '2017-10-17 12:02:05', '2017-10-17 12:02:05');
COMMIT;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `password2` varchar(100) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `total_money` double DEFAULT '0',
  `current_money` double DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `otp_code` varchar(7) DEFAULT NULL,
  `getotp_at` datetime DEFAULT NULL,
  `otp_email` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES (1, 'Chelsey Effertz', 'selmer85', '123456', NULL, '402.696.8507', 553976, 26000, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', '2016-12-30 23:00:00');
INSERT INTO `tbl_user` VALUES (2, 'Coleman Reichel Sr.', 'freda.howe', '$2y$10$L8MJcr.AXP6c50e.ngy4k.RdhKzJ/btt033qHl72oFRjZhpgQ/zIG', NULL, '+1-660-740-4840', 199885, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (3, 'Adonis Kautzer Jr.', 'dina30', '$2y$10$quKnLhfRKHhoXShLp66U9.v41f.kza0oDHWIUy3nARCR9M59t8Hvm', NULL, '1-358-584-3153 x9333', 674732, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (4, 'Annabelle Wisozk', 'madonna.weber', '$2y$10$5fZiH2CFpcqcg9BVHxi91OsKTcGL5u30Ux7MrcvjLDj7KoNZozh7G', NULL, '335-362-8398 x21043', 193846, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (5, 'Dr. Samantha Fahey', 'graham.stoltenberg', '$2y$10$TTHlj1njs81Top1yxkX4au10PV8N8dFP1ALHLR7p9GfKH8zCmHLi.', NULL, '+18593346334', 523019, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (6, 'Leonard Nienow', 'brooke17', '$2y$10$Kw4nsvH1Cwhd99Zb.dJ.w.cq4qdsGBsKDH3wPHqJkPcqm.nzsBvcm', NULL, '1-232-827-0549', 473187, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (7, 'Mrs. Marielle Brown', 'tromp.clare', '$2y$10$lod1CDFKrV2A4ZyeJwkHFOOeZiDWvJDd09OlFQKgrGUKlRrzMm1IS', NULL, '589-904-0836 x2730', 231010, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (8, 'Karley Stiedemann', 'schulist.kira', '$2y$10$qPvZOYPBk0RB7rd6utsDcOU79iQ4TOKrcw6JyRERzJcfvTJKgYfqi', NULL, '+18767599593', 19825, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (9, 'Mr. Granville Nikolaus MD', 'bbins', '$2y$10$BbbtQ2VutthRpyZCiPd9bO2AuRGJpdNfULapCDvPhyseMCmHi1mmG', NULL, '(953) 654-7529 x671', 71652, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (10, 'Ms. Eloisa Cormier', 'emorar', '$2y$10$SQhE.9lI9DScQixJOTOlDesVn9rsUoXzpY708wzNayN.CYMTHpe6m', NULL, '1-690-945-6242', 692851, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (11, 'Elmo Frami', 'vance42', '$2y$10$l11jW4qD42dDXWOMkRAwYeQoWo8RvtsnKClv6ZVSjybUSBhSDwyb.', NULL, '(919) 557-0509 x08861', 962631, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (12, 'Perry Altenwerth', 'dietrich.nico', '$2y$10$S2Ey7os5.JCJ/Gn/oKwDC.wm4VpxSklc2/DQW/FfP9Z22ajyWV2YG', NULL, '+1 (784) 714-4157', 810342, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (13, 'Velva Johnson', 'dubuque.germaine', '$2y$10$gI25pH.MUeDBMnID4OB5ZuYXj9jzXXTg1ozWSWsYpeHQKztez8X/O', NULL, '1-264-366-9096 x5853', 973782, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', '2016-12-19 08:41:17');
INSERT INTO `tbl_user` VALUES (14, 'Ashtyn Marks', 'xsipes', '$2y$10$FxkyjRH/I7EuDyxlmgXY6uqRE4lgRl/GhIG4mTjUpTCPSE2j7ZaDu', NULL, '1-456-694-2655 x820', 986642, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (15, 'Maybelle McCullough', 'edmund.luettgen', '$2y$10$MZtetcC9t3RgnDYtaSyEGu2X9NUHFlOARRqWtVFKOyOofB2t2Nl3C', NULL, '1-898-716-3960', 558624, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (16, 'Kasandra Mosciski', 'cole.ernestina', '$2y$10$3bRkkNpc.dQF4kEJni55euKU7J8a8cavMz1iFtXSkN0W0MjCzGOzy', NULL, '835-981-8480 x171', 806606, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (17, 'Orval Runolfsdottir', 'rmoore', '$2y$10$9v3dLwXbYEmXK0/mZ07GEuKo/ppsIYKp2tTCHV2.Yt9I8UkdKvHLO', NULL, '1-635-633-2538 x775', 834822, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (18, 'Frederic King', 'kurtis.medhurst', '$2y$10$noPWM4kUNUL843OGoqwbYuNF5/Y9BSy8RqEIhL.WxfuopRYdQCLW6', NULL, '271.756.7507 x903', 692799, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (19, 'Arch Kris', 'shaun82', '$2y$10$fSTWX82epPzdFFCoQdlFGulzux46Y0DikqdvCS/D1qy7fyRxqyC02', NULL, '1-968-371-4761 x554', 509527, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (20, 'Rusty Cummings', 'pearlie.lindgren', '$2y$10$LcZLzAbgCMVdDUo8H3sWXuAy.47pRicMcd7wmN46Ss2OzmEwxigIe', NULL, '(391) 415-4299 x1621', 85765, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (21, 'Quinten Gleason', 'van02', '$2y$10$Kt0WvulYcnNQBSmjAFB9O.JZWQLv9taTOUqCI0VTZG6RT/ObYVybq', NULL, '428-953-3834', 987908, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (22, 'Elvis Breitenberg', 'wcormier', '$2y$10$gbPItACfLklSh6D.WQmw4OgDzZBdzUpzj3zPmAEAk6ZCCzF4bv1LK', NULL, '871-288-7143', 785860, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (23, 'Elijah Kilback', 'dawson.mccullough', '$2y$10$VjbbniETSocuNM/c7Z9Lou/cjUtTyUToBNEjz6n4wLKv4/KNm7gCO', NULL, '1-810-228-5806 x9047', 465427, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (24, 'Ursula White', 'collier.jerrold', '$2y$10$Gs6ed6BcqN7ikrSZA4szP./jO39qFH6sszdu60Yd1iGY1h9OuyNK.', NULL, '1-235-727-9350 x809', 292411, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (25, 'Allison Wiza', 'lcasper', '$2y$10$BtuWS46zfKtbwARXCTFGT..Krf0zCXHwda6ypJj6BuP4TOSfXwMm6', NULL, '(540) 404-7881', 948063, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (26, 'Juston Russel', 'wlarson', '$2y$10$jR1WGPwuv0CLA6UK71szPOsXYR.ols/pPWot1zopsnjuDSY0gXPBe', NULL, '1-265-739-7497', 50994, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (29, 'Julian Durgan', 'tressa96', '$2y$10$Qt3iyaUd926Msl8pKKE.GOhqFxDLQVDGZj.vALooF15WtQnQGfKuW', NULL, '+1 (569) 847-6608', 439400, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (30, 'Dr. Opal Weissnat', 'kstanton', '$2y$10$APo4x1EGezTvGhnZQBTZ5OICmu2gCwqH3SbPf8wtnSsZtff.59nmS', NULL, '223-394-0904', 265233, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (31, 'Newton Walsh', 'queen.bernier', '$2y$10$ENZntADJek3RboE6wZfm8e4lLjU1OBu0cJPmUT6w3sm.8X.mehr1O', NULL, '652-682-4823', 297684, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (32, 'Verda Skiles', 'nrowe', '$2y$10$M6z5YOk5ZGEjMDW8sMIkzeAnBJT/kIFta6hRZUL1GLXSB.MjPlHUG', NULL, '(473) 928-3126', 597521, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (33, 'Paige Murray', 'tomasa.rippin', '$2y$10$ypRN3FmzDI.KhlWYaMrfoeZwsqc2QVPaRf2VIoMoOhnkzxfxbc8sC', NULL, '1-767-238-1264 x1297', 98548, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (34, 'Ian Beer', 'dare.tony', '$2y$10$PUbglME1BfkUXDzWN.9Qn.girrfj1RI9rpY4jUeIma3c/WRyAHmw.', NULL, '943.792.4867 x040', 550612, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (35, 'Emelia Hills', 'sedrick36', '$2y$10$z87P6rull9iUimfZsb6xhOgtRj3D8ovUlyUFunmAtalfGgks16/2C', NULL, '1-658-353-7525', 706376, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (36, 'Jovanny Kuphal', 'emanuel83', '$2y$10$1zZ2QZj0DMyvZol7UQuuH.RbCA6gIMB3IXOEdP5P4p7LwlCS4PjOm', NULL, '+1 (519) 383-0708', 808974, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (37, 'Amy Wolf', 'alind', '$2y$10$r7jG43ezffH5KIrxJku7RuI9PqXsx2qyQjSHy/DtFfKCrGiudPpSO', NULL, '325-951-2066 x3186', 487146, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (38, 'Waylon Breitenberg', 'jeffery52', '$2y$10$NzDDAh.82fGdj/vduZZ5rO8mVCvh6DQ3Xklh7BU1sYDuxa0LQHfLO', NULL, '1-736-530-2887', 245888, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (39, 'Maegan Hamill PhD', 'trantow.vickie', '$2y$10$3hqOj1ZkZSQXGR7JifV/uubGAgqfVHH2DIMeUXwyPjMeEKGaHKtJS', NULL, '+1.452.466.5155', 851326, 0, 1, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', NULL);
INSERT INTO `tbl_user` VALUES (40, 'Mr. Emanuel Muller', 'gaylord11', '$2y$10$qYiOmmt8o/eHlb8wNNFTu.iHAl72McgzDi4//1KPZQYbn/oBDIkJC', NULL, '+17107518501', 107534, 0, 0, NULL, NULL, NULL, NULL, '2016-12-19 11:34:49', '2017-09-30 12:57:22');
INSERT INTO `tbl_user` VALUES (44, 'Trịnh Tâm', 'contact.tamsoft@gmail.com', '$2y$10$bUEn8qBo0D4HcxOFphlHreJbmo0OkJU71l/bWYhZmtd7PR4avEcIy', '$2y$10$v4Bf65ToIZ5u/Frnq8WgBOpiH8S9gCXox5CVbf8EnjucfmJ7PZosa', '0964739681', 1000000, 900000, 1, NULL, NULL, '', 'TJoWsjxWjiwP5jNYofsQNGxipKRbMu7xG100CB2vUaq0ky1bUI8fLeerLYrQ', '2017-10-12 10:25:00', '2017-10-19 23:33:27');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
