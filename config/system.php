<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 12/16/17
 * Time: 11:12 AM
 * Contact: tiendinh595@gmail.com
 */

return [
    'fee_deposit' => 0,
    'fee_withdraw_eth' => 0.005,
    'fee_withdraw_btc' => 0.0005,

    'bank' => [
        'Vietcombank',
        'Vietinbank',
        'ACB',
        'Angribank',
        'Sacombank',
        'NamABank',
        'Eximbank',
        'BIDV'
    ],
    'category_mail' => [
        0 => 'Giao dich'
    ],
    'wallet_withdraw' => [
        'guid_main' => '86235ee2-268a-4312-8033-705e0bdfc12f',
        'address_main' => '1MPEyAgNdBJfy36tjwgnTmQ1e1rMFmCjsS',
        'pwd_main' => 'Qp<"]\4b+=QR;xW[y&P#PDyY_rA]n@,Q',
    ],
    'coinbase' => [
        'key' => 'BDz8KoGAum5QEVEa',
        'secret' => 'TC9IEUQdQH17V4vbrg7Ukm7pClnVypfd',
        'main_btc_account' => 'f35d957f-7da9-5775-9b93-2fc837093ee9',
        'main_eth_account' => 'fed3cea1-bddc-59f2-b6b6-900603d582a9',
    ],
    'bonus_ref' => 1000000
];