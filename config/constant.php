<?php
return [
    'type_deposit' => [ // nạp tiền
        1 => 'sms',
        2 => 'card',
        3 => 'bank',
        4 => 'Nhận tiền'
    ],
    'type_invoice' => [ //rút tiền
        1 => 'Nạp tiền game',
        2 => 'Mua thẻ điện thoại',
        3 => 'Nạp tiền điện thoại',
        4 => 'Rút tiền',
        5 => 'Chuyển tiền'
    ],
    'history_type' => [
        1 => 'deposit',
        2 => 'invoice'
    ],
    'message' => [
        0 => 'pending',
        1 => 'success',
        2 => 'reject'
    ]
];