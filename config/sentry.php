<?php

return array(
    'dsn' => 'https://c316a3aba7c04a30a777bc13ec6eb62f:53bcd8b15ef74cbda33b147b41922e3d@sentry.io/284362',

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => true,
);
