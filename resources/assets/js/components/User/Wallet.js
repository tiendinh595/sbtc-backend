/**
 * Created by dinh on 12/14/17.
 */
import React from 'react'
import * as Helper from '../../utils/Helper'


class Wallet extends React.Component {
    constructor(props) {
        super(props)

    }

    render() {
        const {wallet} = this.props;
        const style = {
            margin: '0px 0px 3px'
        };

        if(typeof wallet != "object")
            return null;

        return (
            <div className="row">
                <p className="title-menu"><img src="/resources/assets/frontend/img/ic_login.png" alt/><span>ví của bạn</span></p>
                <div className="pn active">
                    <div className="col-12 panel-body text-uppercase" style={{display: 'block'}}>
                                    <span className="border-bottom"><img
                                        src="/resources/assets/frontend/img/ic_btc_1x.png"
                                        style={style}/> {wallet.balance_btc} btc</span>
                        <span className="border-bottom ml-5"><img
                            src="/resources/assets/frontend/img/ic_eth_small.png"
                            style={style}/> {wallet.balance_eth} eth</span>
                        <span className="border-bottom ml-5"><img
                            src="/resources/assets/frontend/img/ic_cash.png"
                            style={style}/> {Helper.formatMoney(wallet.balance_vnd)} vnd</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Wallet;