/**
 * Created by dinh on 12/18/17.
 */

import React, {Component} from 'react'

class Countdown extends Component {

    constructor(props) {
        super(props);
        this.state = {
            completed: false
        };
        this.getTimeRemaining = this.getTimeRemaining.bind(this);
        this.initializeClock = this.initializeClock.bind(this);
    }

    getTimeRemaining(endtime) {
        let t = Date.parse(endtime) - Date.parse(new Date());
        let seconds = Math.floor((t / 1000) % 60);
        let minutes = Math.floor((t / 1000 / 60) % 60);
        let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        let days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days >= 0 ? days : 0,
            'hours': hours >= 0 ? hours : 0,
            'minutes': minutes >= 0 ? minutes : 0,
            'seconds': seconds >= 0 ? seconds : 0
        };
    }

    initializeClock(id, endtime) {
        let clock = document.getElementById(id);
        let hoursSpan = clock.querySelector('.hour');
        let minutesSpan = clock.querySelector('.min');
        let secondsSpan = clock.querySelector('.sec');
        const {completed} = this.state;
        let updateClock = () => {
            let t = this.getTimeRemaining(endtime);

            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if(this.props.cb != undefined)
                this.props.cb(t);

            if (t.total <= 0) {
                if (completed == false) {
                    this.props.completed();
                    this.setState({
                        completed: true
                    })
                }
                clearInterval(timeinterval);
            }
        };

        updateClock();
        let timeinterval = setInterval(updateClock, 1000);
    }

    componentDidMount() {
        let deadline = new Date(this.props.time);
        this.initializeClock('time', deadline);
    }

    render() {
        return (
            <div className="time" id="time">
                <span className="hour">00</span>:<span className="min">11</span>:<span className="sec">22</span>
            </div>
        )
    }
}

export default Countdown;