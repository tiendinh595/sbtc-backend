/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import OfferItemSell from './OfferItemSell'
import * as ApiCaller from '../../utils/ApiCaller'
import Spinner from 'react-spinkit'
import {connect} from 'react-redux'
import * as AuthService from '../../utils/AuthService'


class ListOfferSell extends Component {

    constructor(props) {
        super(props)

        this.state = {
            list_offers: []
        }
    }

    componentDidMount() {
        const {coin_currency} = this.props.global;
        this.loadMoreOffer(`/offer/sell/${coin_currency}`)
    }

    componentWillReceiveProps(newProps) {
        if (newProps.global.coin_currency != this.props.global.coin_currency) {
            const {coin_currency} = newProps.global;
            this.loadMoreOffer(`/offer/sell/${coin_currency}`)
        }
    }

    loadMoreOffer(url) {
        ApiCaller.get(url)
            .then(res => {
                this.setState({
                    list_offers: res.data
                });
            })
            .catch(err => {
                console.error(err)
            })
    }

    render() {
        const {list_offers} = this.state;

        if (Object.keys(list_offers).length == 0) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn='none' name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="offers">
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col" width="50%">
                            <p className="title">danh sách người <span
                                className="highlight-green">bán</span>
                            </p>
                            <p className="sub-description">
                                Tỷ giá mua bán tùy thuộc vào người bán đặt ra
                            </p>
                        </th>
                        <th scope="col" width="50%">
                            <p className="title">thỏa thuận mua <img
                                src="/resources/assets/frontend/img/ic_deal.png" alt/></p>
                            <p className="sub-description">Mua với giá của bạn đưa ra trên từng giao
                                dịch</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        list_offers.data.map((offer, index) => <OfferItemSell key={index} offer={offer}/>)
                    }
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colSpan="2">
                            <div className="btn-group pagination" role="group">
                                <a onClick={this.loadMoreOffer.bind(this, list_offers.prev_page_url)} type="button"
                                   className={list_offers.prev_page_url === null ? 'btn btn-secondary disabled' : 'btn btn-secondary'}>TRANG
                                    TRƯỚC</a>
                                <a onClick={this.loadMoreOffer.bind(this, list_offers.next_page_url)} type="button"
                                   className={list_offers.next_page_url === null ? 'btn btn-secondary disabled' : 'btn btn-secondary'}>TRANG
                                    SAU</a>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        global: state.GlobalReducer
    }
};


export default connect(mapStateToProps, null)(ListOfferSell);