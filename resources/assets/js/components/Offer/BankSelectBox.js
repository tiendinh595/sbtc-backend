/**
 * Created by dinh on 12/16/17.
 */
import React from 'react'
import * as constants from '../../constants/index'

class BankSelectBox extends React.Component {

    constructor(props) {
        super(props);
        this._onChange = this._onChange.bind(this);
        this.disabled = this.props.disabled != undefined ? this.props.disabled : false;
        this.className = this.props.className != undefined ? this.props.className : "col-sm-4 no-pd";

        this.state = {
            select: this.props.select != undefined ? this.props.select : null
        };
    }

    _onChange(e) {
        if (!this.props.disabled) {
            this.props.change(e.target.value);
            this.setState({
                select: e.target.value
            })
        }
    }

    render() {
        let banks = constants.list_bank.map((bank, index) => <option value={index} key={index}>{bank}</option>);
        return (
            <div className={this.props.className}>
                <select name="" className="form-control no-radius" onChange={this._onChange} value={this.state.select} disabled={this.disabled}>
                    <option value="">----</option>
                    { banks }
                </select>
            </div>
        )
    }
}
export default BankSelectBox;