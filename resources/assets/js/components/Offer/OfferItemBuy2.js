import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, Link} from 'react-router-dom'
import * as AuthService from '../../utils/AuthService'
import * as Helper from '../../utils/Helper'
import FormDeal from '../../components/Offer/FormDeal'
import * as Button from '../../components/Offer/Button'

class OfferItemBuy2 extends Component {

    constructor(props) {
        super(props);
    }

    onDeal(e, id) {
        e.preventDefault();

        if(AuthService.requiredAuth()) {
            alert(id)
        }
    }

    render() {
        const {offer} = this.props;

        return (
            <tr>
                <td className="align-middle" width="50%">
                    <div className="row">
                        <div className="col-sm-9">
                            <p><b className="highlight-blue">{ Helper.formatMoney(offer.price_per_coin)} VND</b>/<b
                                className="highlight-orange">{ offer.type_money == 1 ? 'BTC' : 'ETH' }</b> qua <b>{ offer.bank_name.toUpperCase() }</b></p>
                            <p>Số lượng cần mua: <b>{ offer.current_amount } BTC</b></p>
                            <p>Tỷ giá: 1 USD = {Helper.formatMoney(offer.rate_vnd_usd)} VND</p>
                        </div>
                        <div className="col-sm-3">
                            <Button.ButtonSell to={`/offer/${offer.id}`} label="BÁN" activeOnlyWhenExact={true} type={offer.type_money} />
                        </div>
                    </div>
                </td>
                <td>
                    <FormDeal offer={offer} />
                </td>
            </tr>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    // return {
    //     ProfileAction: bindActionCreators(ProfileAction, dispatch)
    // }
};

export default connect(null, null)(OfferItemBuy2);