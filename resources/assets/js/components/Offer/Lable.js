/**
 * Created by dinh on 12/14/17.
 */
import React from 'react'

export const coin = (coin_type, short = false, upper = false) => {
    var coin_name = null;
    if (coin_type == 1) {
        coin_name = short === true ? 'btc' : 'bitcoin';
    } else {
        coin_name = short === true ? 'eth' : 'ethereum';
    }
    return upper === true ? coin_name.toUpperCase() : coin_name;
};
