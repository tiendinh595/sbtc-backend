/**
 * Created by dinh on 12/21/17.
 */
import React from 'react'
import * as AuthService from '../../utils/AuthService'
import Simplert from 'react-simplert'
import * as ApiCaller from '../../utils/ApiCaller'
import * as Helper from '../../utils/Helper'

class FormDeal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },

            offer_id: props.offer.id,
            amount: 0,
            rate_vnd_usd: 0,
            total_money: 0,

            loaded: true
        };

        this._onDeal = this._onDeal.bind(this);
        this._showMessage = this._showMessage.bind(this);
    }

    _onDeal(e) {
        e.preventDefault();
        if (!AuthService.isLogged())
            window.location.href = '/login';

        const deal = {
            offer_id: this.state.offer_id,
            amount: this.refs.amount.value,
            rate_vnd_usd: this.refs.rate_vnd_usd.value,
        };

        this.setState({
            loaded: false,
        });

        ApiCaller.post('/deal', deal)
            .then(res=>{
                console.log(res)
                if (res.code == 200) {
                    this.setState({
                        loaded: true,
                        alert: {
                            is_show: true,
                            type: 'success',
                            msg: res.msg
                        }
                    });
                    this.refs.amount.value = 0;
                    this.refs.rate_vnd_usd.value = 0;
                    this.setState({ total_money: 0 })
                } else {
                    this.setState({
                        loaded: true,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err=>{

            });
    }

    _onChange(e) {
        const total_money = Helper.formatMoney(this.refs.rate_vnd_usd.value * this.refs.amount.value * this.props.offer.rate_usd_coin);
        this.setState({
            total_money
        })
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    render() {
        const {loaded} = this.state;

        return (
            <div className="row">
                { this._showMessage() }
                <div className="col-sm-7">
                    <form className="form-inline">
                        <div className="form-group">
                            <label><b>TỶ GIÁ ĐỀ NGHỊ: </b></label>
                            <input type="number" className="input-deal" name="rate_vnd_usd" ref="rate_vnd_usd" onChange={this._onChange.bind(this)}/>
                        </div>
                        <div className="form-group">
                            <label><b>LƯỢNG SẼ {this.props.offer.type_offer == 1 ? 'MUA' : 'BÁN'}: </b></label>
                            <input type="number" ref="amount" className="input-deal" name="amount"
                                   onChange={this._onChange.bind(this)}/>
                        </div>
                        <div className="form-group">
                            <label><b>TỔNG TIỀN: </b></label>
                            <input type="text" className="input-deal" value={this.state.total_money}
                                   readOnly={true}/>
                        </div>
                    </form>
                </div>
                <div className="col-sm-5">
                    <a href
                       className={loaded == true ? 'bt bt-green-2 align-middle' : 'bt bt-green-2 align-middle disabled'}
                       onClick={this._onDeal}>TẠO THỎA
                        THUẬN</a>
                </div>
            </div>
        )
    }
}

export default FormDeal;