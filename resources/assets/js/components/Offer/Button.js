/**
 * Created by dinh on 12/14/17.
 */
import React from 'react'
import {
    Route,
    Link
} from 'react-router-dom'


export const ButtonBuy = ({ label, to, activeOnlyWhenExact, type }) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
        <div className="bt bt-green align-middle">
           <Link to={to} style={{color: '#fff'}}>
               MUA <img src={type == 1 ? '/resources/assets/frontend/img/ic_btc_2.png' : '/resources/assets/frontend/img/ic_eth_1x.png'} style={{marginTop: '-6px'}}/>
           </Link>
        </div>
    )}/>
);

export const ButtonSell = ({ label, to, activeOnlyWhenExact, type }) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
        <div className="bt bt-blue align-middle">
           <Link to={to} style={{color: '#fff'}}>
               BÁN <img src={type == 1 ? '/resources/assets/frontend/img/ic_btc_2.png' : '/resources/assets/frontend/img/ic_eth_1x.png'} style={{marginTop: '-6px'}}/>
           </Link>
        </div>
    )}/>
);

export const ButtonEdit = ({ label, to, activeOnlyWhenExact, type }) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
        <div className="bt bt-blue align-middle">
           <Link to={to} style={{color: '#fff'}}>
               Chỉnh sửa
           </Link>
        </div>
    )}/>
);

export const ButtonDetail = ({ label, to, activeOnlyWhenExact, type }) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
        <div className="bt bt-blue align-middle">
           <Link to={to} style={{color: '#fff'}}>
               Dến
           </Link>
        </div>
    )}/>
);