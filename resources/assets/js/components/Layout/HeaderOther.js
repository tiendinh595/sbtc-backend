/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react';
import {Link, Route} from 'react-router-dom'
import {connect} from 'react-redux'

const NavLink = ({className, label, to, activeOnlyWhenExact, child=null}) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({match}) => (
        <li className={match ? 'mr-4 active user-tabs' : 'mr-4 user-tabs'}>
            <Link to={to} className="text-uppercase"><i  className={className}/> {label} {child}</Link>
        </li>
    )}/>
);

class HeaderOther extends Component {
    render() {
        const {profile} = this.props;

        return (
            <div className="container-fluid">
                <div className="header-style2">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg">
                            <Link className="navbar-brand" to="/"><img src="/resources/assets/frontend/img/logo.png" alt=""/></Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"/>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#"><span className="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                                <ul className="nav navbar-nav navbar-right">
                                    <NavLink className="fa fa-envelope-o" label="hộp thư" to="/mail" activeOnlyWhenExact={true}/>
                                    <NavLink className="fa fa-file-text mr-1" label="mua bán" to="/create-offer" activeOnlyWhenExact={true}/>
                                    <NavLink className="fa fa-cog mr-1" label="cài đặt" to="/setting" activeOnlyWhenExact={true}/>
                                    <NavLink className="fa fa-user-circle-o mr-1" label={profile.username} to={`/profile/${profile.username}`} activeOnlyWhenExact={false} child={
                                        <ul className="actions">
                                            <li>
                                                <Link to={`/profile/${profile.username}`}>
                                                    <img src="/resources/assets/frontend/img/ic_user_tabs_profile.png" alt /> Cá nhân</Link>
                                            </li>
                                            <li><Link to="/create-offer"><img src="/resources/assets/frontend/img/ic_user_tabs_post.png" alt /> Đăng tin mua bán</Link></li>
                                            <li><Link to="/dashboard/trade/active"><img src="/resources/assets/frontend/img/ic_user_tabs_history1.png" alt /> Bảng điều khiển</Link></li>
                                            <li><Link to="/deal"><img src="/resources/assets/frontend/img/ic_user_tabs_history2.png" alt style={{marginRight: '9px'}} />   Lịch sử thoả thuận</Link></li>
                                            <li><Link to="/wallet/btc/deposit"><img src="/resources/assets/frontend/img/ic_user_tabs_wallet.png" alt /> Ví của bạn</Link></li>
                                            <li><Link to="/mail"><img src="/resources/assets/frontend/img/ic_user_tabs_inbox.png" alt /> Hộp thư</Link></li>
                                            <li><Link to="/setting"><img src="/resources/assets/frontend/img/ic_user_tabs_setting.png" alt /> Cài đặt</Link></li>
                                        </ul>
                                    }/>
                                    <NavLink className="fa fa-sign-out ml-1" label="đăng xuất" to="/logout" activeOnlyWhenExact={true}/>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

export default connect(mapStateToProps, null)(HeaderOther);