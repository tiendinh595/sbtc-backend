/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as GlobalAction from '../../actions/GlobalAction'
import * as Helper from '../../utils/Helper';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import * as AuthService from '../../utils/AuthService';

class HeaderHome extends Component {

    constructor(props) {
        super(props)

        this._onCopyToClipBoard = this._onCopyToClipBoard.bind(this)
    }

    _onChangeCurrency(e, currency) {
        e.preventDefault();
        this.props.GlobalAction.changeCoinCurrency(currency);
    }

    componentWillReceiveProps(newProps) {
    }

    showMenu() {
        const {profile} = this.props;

        if (profile.is_logged) {
            return (
                <ul className="nav navbar-nav navbar-right">
                    <li className="mr-4 text-white"><img src="/resources/assets/frontend/img/ic_btc_1x.png" alt
                                                         className="mr-1"/> {profile.wallet.balance_btc} BTC
                    </li>
                    <li className="mr-4 text-white"><img src="/resources/assets/frontend/img/ic_eth_1x.png" alt
                                                         className="mr-1"/> {profile.wallet.balance_eth} ETH
                    </li>
                    <li className="mr-4 text-white"><img src="/resources/assets/frontend/img/ic_cash.png" alt
                                                         className="mr-1"/> {Helper.formatMoney(profile.wallet.balance_vnd)} VND
                    </li>
                    <li className="mr-4  user-tabs">
                        <Link to={`/profile/${profile.username}`} className="text-white"><i
                        className="fa fa-user-circle-o mr-1"/>{profile.username.toUpperCase()}</Link>
                        <ul className="actions">
                            <li>
                                <Link to={`/profile/${profile.username}`}>
                                <img src="/resources/assets/frontend/img/ic_user_tabs_profile.png" alt /> Cá nhân</Link>
                            </li>
                            <li><Link to="/create-offer"><img src="/resources/assets/frontend/img/ic_user_tabs_post.png" alt /> Đăng tin mua bán</Link></li>
                            <li><Link to="/dashboard/trade/active"><img src="/resources/assets/frontend/img/ic_user_tabs_history1.png" alt /> Bảng điều khiển</Link></li>
                            <li><Link to="/deal"><img src="/resources/assets/frontend/img/ic_user_tabs_history2.png" alt style={{marginRight: '9px'}} />   Lịch sử thoả thuận</Link></li>
                            <li><Link to="/wallet/btc/deposit"><img src="/resources/assets/frontend/img/ic_user_tabs_wallet.png" alt /> Ví của bạn</Link></li>
                            <li><Link to="/mail"><img src="/resources/assets/frontend/img/ic_user_tabs_inbox.png" alt /> Hộp thư</Link></li>
                            <li><Link to="/setting"><img src="/resources/assets/frontend/img/ic_user_tabs_setting.png" alt /> Cài đặt</Link></li>
                        </ul>

                    </li>
                    <li><Link to="/logout" className="text-white">ĐĂNG XUẤT <span
                        className="fa fa-sign-out ml-1"/></Link></li>
                </ul>
            )
        } else {
            return (
                <ul className="nav navbar-nav navbar-right">
                    <li><Link to="/login" className="txt-login text-white">Đăng Nhập</Link></li>
                    <li><Link to="/register" className="bt bt-sm bt-s1 text-white">Đăng Ký</Link></li>
                </ul>
            )
        }
    }

    _onCopyToClipBoard(e, text) {
        e.preventDefault();
        let textField = document.createElement('textarea');
        textField.innerText = text;
        document.body.appendChild(textField);
        textField.select();
        document.execCommand('copy');
        textField.remove();

        NotificationManager.success('Sao chép link thành công')
    }

    render() {
        const {profile, global} = this.props;
        return (
            <div className="container-fluid">
                <NotificationContainer />
                <div className="header">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg bg-light nav-header ">
                            <Link className="navbar-brand" to="/"><img src="/resources/assets/frontend/img/logo.png" alt=""/></Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"/>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#"><span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#"><img
                                            src="/resources/assets/frontend/img/ic_etherum_2.png" alt/> ETHERIUM</a>
                                    </li>
                                </ul>
                                { this.showMenu() }
                            </div>
                        </nav>
                        <div className="line-top"/>
                        <div className="header-body text-center">
                            <div className="caption">
                                SIÊU THỊ BITCOIN HÀNG ĐẦU VIỆT NAM
                            </div>
                            <div className="slogan">SÀN GIAO DỊCH MUA BÁN BITCOIN - ETHEREUM AN TOÀN - NHANH CHÓNG - UY
                                TÍN
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-6">
                                    <div className="coin-wrap">
                                        <div className="block-coin">
                                            <div className="block-item">
                                                <img src="/resources/assets/frontend/img/ic_btc.png" alt />
                                                <div className="row mt-5">
                                                    <div className="col-6">
                                                        <p>GIÁ MUA BTC: <br /><span>{ Helper.formatMoney(global.system.price_bid_btc*global.system.rate_vnd_usd_btc) } VNĐ</span></p>
                                                        <p>
                                                            <Link to="/quick-buy/btc" className="bt bt-blue">Mua Ngay</Link>
                                                        </p>
                                                    </div>
                                                    <div className="col-6">
                                                        <p>GIÁ BÁN BTC: <br /><span>{ Helper.formatMoney(global.system.price_ask_btc*global.system.rate_vnd_usd_btc) } VNĐ</span></p>
                                                        <p>
                                                            <Link to="/quick-sell/btc" className="bt bt-green ml-2">Bán Ngay</Link>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="header-footer">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="block">
                                                    <img src="/resources/assets/frontend/img/ic_btc_2x.png" alt />
                                                    <div className="slash d-sm-none d-md-block" />
                                                    <p className="text-left">
                                                        <span>giá cao nhất trong ngày <span className="highlight-orange">{ Helper.formatMoney(global.system.price_high_btc, 2) }</span> <b>USD</b></span>
                                                        <br />
                                                        <span>giá thấp nhất trong ngày <span className="highlight-orange">{ Helper.formatMoney(global.system.price_low_btc, 2) }</span> <b>USD</b></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <div className="coin-wrap">
                                        <div className="block-coin">
                                            <div className="block-item">
                                                <img src="/resources/assets/frontend/img/ic_eth.png" alt />
                                                <div className="row mt-5">
                                                    <div className="col-6">
                                                        <p>GIÁ MUA ETH: <br /><span>{ Helper.formatMoney(global.system.price_bid_eth*global.system.rate_vnd_usd_eth) } VNĐ</span></p>
                                                        <p>
                                                            <Link to="/quick-buy/eth" className="bt bt-blue">Mua Ngay</Link>
                                                        </p>
                                                    </div>
                                                    <div className="col-6">
                                                        <p>GIÁ BÁN ETH: <br /><span>{ Helper.formatMoney(global.system.price_ask_eth*global.system.rate_vnd_usd_eth) } VNĐ</span></p>
                                                        <p>
                                                            <Link to="/quick-sell/eth" className="bt bt-green ml-2">Bán Ngay</Link>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="header-footer">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="block">
                                                    <img src="/resources/assets/frontend/img/ic_eth.png" alt />
                                                    <div className="slash d-sm-none d-md-block" />
                                                    <p className="text-left">
                                                        <span>giá cao nhất trong ngày <span className="highlight-orange">{ Helper.formatMoney(global.system.price_high_eth, 2) }</span> <b>USD</b></span>
                                                        <br />
                                                        <span>giá thấp nhất trong ngày <span className="highlight-orange">{ Helper.formatMoney(global.system.price_low_eth, 2) }</span> <b>USD</b></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="clearfix"/>

                    </div>
                </div>
                {/*end header*/}
                {/*start slide center*/}
                <div className="row slide-center text-center">
                    <div className="container">
                        <div className="wrap-description col-sm-12">
                            Nhận ngay 100$ số tiền cho mỗi một người được bạn giới thiệu tham gia STBC mua bán thành
                            công. <br />
                            Bạn vào hồ sơ cá nhân copy link giới thiệu bạn bè nhé!
                        </div>
                        <div className="col-sm-12" style={{overflowWrap: 'break-word'}}>
                            {
                                !profile.is_logged ?
                                    <Link to="/register" className="bt bt-lg bt-blue">BẠN CẦN ĐĂNG KÍ ĐỂ LẤY LINK</Link>
                                    :
                                    <a href="" onClick={(e)=>this._onCopyToClipBoard(e, `https://sieuthibtc.com/register?ref=${profile.username}`)}>https://sieuthibtc.com/register?ref={profile.username}</a>
                            }
                        </div>
                        <div className="hr"/>
                        <div className="row wrap-feature col-sm-12">
                            <div className="col-sm-4">
                                <img src="/resources/assets/frontend/img/ic_wallet.png" alt/> <span>Hướng dẫn giao dịch</span>
                            </div>
                            <div className="col-sm-4">
                                <img src="/resources/assets/frontend/img/ic_support.png" alt/> <span>Hỗ trợ trực tuyến</span>
                            </div>
                            <div className="col-sm-4">
                                <img src="/resources/assets/frontend/img/ic_email.png" alt/> <span>Liên hệ</span>
                            </div>
                        </div>

                        <div className="row wrap-feature col-sm-12 mt-3">
                            <ul className="nav nav-tabs nav-justified tabs-trade tabs-arrow">
                                <li className={this.props.global.coin_currency == 'btc' ? 'active' : ''}><a href="#" onClick={(e)=>this._onChangeCurrency(e, 'btc')}><embed src="/resources/assets/frontend/img/ic_tabs_home_btc.svg" alt style={{display: 'inline-block', marginBottom: '-5px', width: 17}} /> Giao dịch Bitcoin</a></li>
                                <li className={this.props.global.coin_currency == 'eth' ? 'active' : ''}><a href="#" onClick={(e)=>this._onChangeCurrency(e, 'eth')}><embed src="/resources/assets/frontend/img/ic_tabs_home_eth_active.svg" alt style={{display: 'inline-block', marginBottom: '-5px', width: 17}} /> Giao dịch ETHEREUM</a></li>
                            </ul>
                        </div>

                        
                    </div>
                </div>
                {/*end slide center*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer,
        global: state.GlobalReducer
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        GlobalAction: bindActionCreators(GlobalAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderHome);