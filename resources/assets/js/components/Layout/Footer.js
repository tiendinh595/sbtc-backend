/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'

class Footer extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid footer">
                    {/*start content*/}
                    <div className="container">
                        <div className="row block-1">
                            <div className="col-sm-6 col-md-3">
                                <img src="/resources/assets/frontend/img/logo_coinmarketcap.png" alt
                                     className="img-fluid"/>
                            </div>
                            <div className="col-sm-6 col-md-3">
                                <img src="/resources/assets/frontend/img/logo_bitshares.png" alt className="img-fluid"/>
                            </div>
                            <div className="col-sm-6 col-md-3">
                                <img src="/resources/assets/frontend/img/logo_bitconnect.png" alt
                                     className="img-fluid"/>
                            </div>
                            <div className="col-sm-6 col-md-3">
                                <img src="/resources/assets/frontend/img/logo_monero.png" alt className="img-fluid"/>
                            </div>
                        </div>
                        <div className="row block-2">
                            <div className="col-sm-12 col-md-6">
                                <p>
                                    THÔNG TIN LIÊN HỆ<br />
                                    Email: info@sieuthibitcoin.com <br />
                                </p>
                            </div>
                            <div className="col-sm-12 col-md-5 offset-md-1">
                                <p>Đăng ký nhận bản tin / Promotion </p>
                                <form className="form-inline">
                                    <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <input type="text" className="form-control" id="inlineFormInputGroupUsername2"
                                               placeholder=""/>
                                        <div className="input-group-addon"><i className="fa fa-envelope-o"
                                                                              aria-hidden="true"/> Đăng Ký
                                        </div>
                                    </div>
                                </form>
                                <p>Chúng tôi sẽ gửi đến các bạn những chương trình khuyến mãi, và các
                                    ưu đãi dành cho thành viên</p>
                            </div>
                        </div>
                        <div className="row block-3">
                            <div className="social">
                                <a href>FACEBOOK</a>
                                <a href>TWITTER</a>
                                <a href>GOOGLE+</a>
                            </div>
                            <p>© 2017 sieuthibtc (STB). All rights reserved.</p>
                        </div>
                    </div>
                    {/*start content*/}
                </div>

                <div className="quick-actions">
                    <ul>
                        <li><img src="/resources/assets/frontend/img/ic_quick_actions_cart.png" alt/><Link to="/create-offer">Đăng tin mua bán</Link></li>
                        <li><img src="/resources/assets/frontend/img/ic_quick_actions_wallet.svg" alt/><a href="#">Hướng dẫn giao dịch</a></li>
                        <li><img src="/resources/assets/frontend/img/ic_quick_actions_support.svg" alt/><a href="#">Hỗ trợ trực tuyến</a></li>
                        <li><img src="/resources/assets/frontend/img/ic_quick_actions_contact.svg" alt/><a href="#">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Footer;