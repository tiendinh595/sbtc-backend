/**
 * Created by dinh on 11/29/17.
 */
import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class HeaderLogin extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="header-style2">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg">
                            <Link className="navbar-brand" to="/"><img src="/resources/assets/frontend/img/logo.png" alt=""/></Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon" />
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#"><span className="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                                <ul className="nav navbar-nav navbar-right">
                                    <li className="text-uppercase"><Link to="/">TRANG CHỦ  <span className="fa fa-sign-out ml-1" /></Link></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                {/*end header*/}
            </div>
        )
    }
}
export default HeaderLogin;