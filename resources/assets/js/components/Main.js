import React, {Component} from 'react';
import {Switch, Link, BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import * as ProfileAction from '../actions/ProfileAction';
import * as GlobalAction from '../actions/GlobalAction';
import Pusher from 'pusher-js'
import * as constants from '../constants/index'

//import pages
import HomePage from  '../pages/HomePage';
import SettingPage from  '../pages/SettingPage';
import ProfilePage from  '../pages/ProfilePage';
import LoginPage from  '../pages/LoginPage';
import NoMatch from  '../pages/NoMatch';
import Logout from  '../pages/Logout';
import OfferPage from  '../pages/OfferPage';
import TradePage from  '../pages/TradePage';
import QuickBuyPage from  '../pages/QuickBuyPage';
import QuickSellPage from  '../pages/QuickSellPage';
import CreateOfferPage from  '../pages/CreateOfferPage';
import EditOfferPage from  '../pages/EditOfferPage';
import MailPage from  '../pages/MailPage';
import DealPage from  '../pages/DealPage';
import VerifyPage from  '../pages/VerifyPage';
import WalletPage from  '../pages/WalletPage';
import DashboardPage from  '../pages/DashboardPage';

//utils
import * as authService from '../utils/AuthService'
class Main extends Component {

    constructor(props) {
        super(props);
        this.props.ProfileAction.fetchProfile();
        this.props.GlobalAction.fetchGlobalData();
        this.channel_user = null;
        this.channel_system = null;
        this.is_bind = false;
    }

    componentWillMount() {
        // this.pusher = new Pusher(constants.slanger.key, {
        //     'wsHost' : constants.slanger.host,
        //     'wsPort': constants.slanger.wsPort, //port mặc định của slanger để chạy websocket
        //     'cluster' : 'mt1',
        //     enabledTransports: ['wss', 'flash'],
        //     'secret': constants.slanger.secret,
        //     'appId': 1
        // });
        this.pusher = new Pusher(constants.slanger.key, {
            cluster: 'ap1',
            encrypted: true
        });
        this.channel_user = this.pusher.subscribe('user');
        this.channel_system = this.pusher.subscribe('system');
    }

    componentWillUpdate (newProps, newState) {
        const {id} = newProps.profile;
        if(!this.is_bind && id != undefined) {
            this.channel_user.bind('update', data => {
                console.log(data)
                if(data.user_id == id)
                    this.props.ProfileAction.fetchProfile();
            }, this);

            this.channel_system.bind('update', data => {
                this.props.GlobalAction.updatePrice(data)
            }, this);
            this.is_bind = true;
        }
    }

    render() {

        return (
            <Router onUpdate={() => window.scrollTo(0, 0)}>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/login" exact component={LoginPage}/>
                    <Route path="/register" exact component={LoginPage}/>
                    <Route path="/login/confirm/:email/:token" exact component={LoginPage}/>
                    <Route path="/login/active/:email/:token" exact component={LoginPage}/>
                    <Route path="/logout" exact component={Logout}/>
                    <PrivateRoute path="/profile/:username" exact component={ProfilePage}/>
                    <PrivateRoute path="/setting" exact component={SettingPage}/>
                    <PrivateRoute path="/setting/verify" exact component={VerifyPage}/>
                    <PrivateRoute path="/offer/:id" exact component={OfferPage}/>
                    <PrivateRoute path="/offer/:id/edit" exact component={EditOfferPage}/>
                    <PrivateRoute path="/trade/:id" exact component={TradePage}/>
                    <PrivateRoute path="/quick-buy/:currency" component={QuickBuyPage}/>
                    <PrivateRoute path="/quick-sell/:currency" exact component={QuickSellPage}/>
                    <PrivateRoute path="/create-offer" exact component={CreateOfferPage}/>
                    <PrivateRoute path="/mail" exact component={MailPage}/>
                    <PrivateRoute path="/deal" exact component={DealPage}/>
                    <PrivateRoute path="/wallet/:coin/:page" exact component={WalletPage}/>
                    <PrivateRoute path="/dashboard/:page/:status?" exact component={DashboardPage}/>
                    <Route component={NoMatch}/>
                </Switch>
            </Router>
        );
    }
}

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        authService.isLogged() ? (
                <Component {...props}/>
            ) : (
                <Redirect to={{
                    pathname: '/login',
                    state: {from: props.location}
                }}/>
            )
    )}/>
);


const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ProfileAction: bindActionCreators(ProfileAction, dispatch),
        GlobalAction: bindActionCreators(GlobalAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);