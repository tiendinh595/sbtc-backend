/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderLogin from '../components/Layout/HeaderLogin'
import Footer from '../components/Layout/Footer'

//utils
import Simplert from 'react-simplert'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import * as Helper from '../utils/Helper'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as AuthAction from '../actions/AuthAction'
import * as ProfileAction from '../actions/ProfileAction'

import Cookies from 'universal-cookie';
const cookies = new Cookies();

import {Redirect} from 'react-router-dom'
import {showLoading, hideLoading} from 'react-redux-loading-bar'
import LoadingBar from 'react-redux-loading-bar'


class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.register_action = {
            is_processing: false,
                msg: '',
                is_show: false,
                status: 'success',
                text: 'Đăng Ký',
                errors: {}
        };

        this.init_state = {
            email_login: '',
            email: '',
            confirm_email: '',
            username: '',
            password: '',
            confirm_password: '',
            presenter: '',
            full_name: '',
            is_loading: false,
            token_invalid: false,
            token_valid: false,

            action_validate: {
                invalid: false,
                text: '',
                type: 'success'
            },

            auth: {
                register_success: false
            },
            register_action: this.register_action,
            login_action: {
                is_processing: false,
                msg: '',
                is_show: false,
                status: 'success',
                text: 'Tiếp Tục',
                login_success: false
            },
            errors: {}
        };
        this.state = this.init_state;
        AuthService.isLogged();
    }

    validateToken(email, token) {
        this.setState({is_loading: true});
        ApiCaller.post(`/login/confirm/${email}/${token}`)
            .then((res) => {
                if (res.code == 200) {
                    AuthService.login(res.data.token, res.data.profile);
                    this.props.ProfileAction.fetchProfile();
                } else {
                    this.setState({
                        is_loading: false, token_invalid: true,
                        action_validate: {
                            invalid: false,
                            text: "Liên kết đăng nhập của bạn đã hết hạn. Vui lòng đăng nhập lại.",
                            type: 'error'
                        }
                    });
                }
            })
    }

    onActiveAccount(email, token) {
        this.setState({is_loading: true});
        ApiCaller.post(`/login/active/${email}/${token}`)
            .then((res) => {
                if (res.code == 200) {
                    AuthService.login(res.data.token, res.data.profile);
                    this.props.ProfileAction.fetchProfile();
                } else {
                    this.setState({
                        is_loading: false, token_invalid: true,
                        action_validate: {
                            invalid: false,
                            text: "Có lỗi xảy ra trong quá trình kích hoạt tài khoản.",
                            type: 'error'
                        }
                    });
                }
            })
    }


    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
            register_action: this.register_action
        })

    }


    onLogin(e) {
        e.preventDefault();
        this.setState({token_invalid: false});
        this.setState({
            login_action: {
                login_success: false,
                is_processing: false,
                text: 'Đang Đăng Nhập'
            }
        });

        return ApiCaller.post('/login', {email_login: this.state.email_login}).then(res => {
            if (res.code == 200) {
                this.setState({
                    login_action: {
                        is_processing: false,
                        login_success: true
                    },
                })
            }
            else {
                this.setState({
                    login_action: {
                        is_processing: false,
                        login_success: false,
                        text: 'Tiếp Tục'
                    },
                    errors: res.msg
                })
            }
        }).catch(err => {
            this.setState({
                login_action: {
                    is_processing: false,
                    msg: 'Đăng nhập thất bại',
                    is_show: true,
                    status: 'error',
                    text: 'Tiếp Tục'
                }
            })
        });
    }

    onRegister(e) {
        e.preventDefault();
        this.setState({
            register_action: {
                ...this.state.register_action,
                is_processing: true,
                text: 'Đang Đăng Ký'
            }
        });
        let {full_name, email, confirm_email, presenter, username} = this.state;
        ApiCaller.post('/register', {full_name, email, confirm_email, presenter, username}).then(res => {
            if (res.code == 200) {
                console.log('ok')
                this.setState({
                    ...this.init_state,
                    register_action: {
                        is_processing: false,
                        msg: 'đăng kí thành công',
                        is_show: true,
                        status: 'success',
                        text: 'Đăng Ký'
                    }
                })
            }
            else {
                console.log('fail')
                this.setState({
                    register_action: {
                        is_processing: false,
                        status: 'error',
                        text: 'Đăng Ký'
                    },
                    errors: res.msg
                })
            }
        }).catch(err => {
            console.log(err)
            this.setState({
                register_action: {
                    is_processing: false,
                    msg: 'Đăng kí thất bại',
                    is_show: true,
                    status: 'error',
                    text: 'Đăng Ký'
                }
            })
        });

    }

    componentWillMount() {
        this.props.AuthAction.initLogin();
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        if (this.props.match.url == '/register')
            document.title = 'Đăng ký';
        else
            document.title = 'Đăng nhập';

        const {email, token} = this.props.match.params;
        if (email !== undefined && token !== undefined) {
            this.setState({
                email_login: email
            })
            if (this.props.match.path == '/login/active/:email/:token') {
                this.onActiveAccount(email, token);
            } else {
                this.validateToken(email, token);
            }

        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({is_login: 'access_token' in newProps.profile, ...newProps})
        const ref = Helper.getParameterByName('ref', newProps.location.search);
        if(ref != null)
            this.setState({presenter: ref,...newProps})
    }

    render() {
        return (
            <div>
                { this.state.is_login ? <Redirect to="/"/> : ''}
                <Simplert
                    showSimplert={this.state.register_action.is_show}
                    type={this.state.register_action.status}
                    title="Thông báo"
                    message={this.state.register_action.msg}
                />

                <Simplert
                    showSimplert={this.state.token_invalid}
                    type={this.state.action_validate.type}
                    title="Thông báo"
                    message={this.state.action_validate.text}
                />

                <LoadingBar className="loading_bar"/>

                <HeaderLogin/>
                <div className="container-fluid main" id="login">
                    {/*start content*/}
                    <div className="container">
                        <div className="row">
                            <p className="title-menu"><img src="/resources/assets/frontend/img/ic_login.png"
                                                           alt/><span>đăng nhập</span>
                            </p>
                            <div className="col-sm-12">
                                {
                                    this.state.login_action.login_success === false ?
                                        <form className=" frm">
                                            <p className="col-sm-12 col-md-4">
                                                <input type="email"
                                                       className="form-control text-center"
                                                       placeholder="Nhập email của bạn" name="email_login"
                                                       onChange={this.onChange.bind(this)}
                                                       value={this.state.email_login}
                                                />
                                                {
                                                    this.showError('email_login')
                                                }

                                            </p>
                                            <p className="col-sm-12 col-md-4"><input
                                                disabled={this.state.is_loading}
                                                onClick={this.onLogin.bind(this)}
                                                type="submit"
                                                className="btn btn-submit form-control  col-sm-12 text-center"
                                                defaultValue={this.state.is_loading ? 'Đang kiểm tra token' : 'TIẾP TỤC'}/>
                                            </p>
                                        </form>
                                        :
                                        <p className="text-center">
                                            Chúng tôi đã gửi liên kết để đăng nhập vào <a
                                            href="">{this.state.email_login}</a>.
                                            Bấm vào nút bên trong email là xong. Kiểm tra cả hộp thư rác nếu bạn
                                            không
                                            tìm thấy email trong hộp thư chính.
                                        </p>

                                }
                            </div>
                        </div>
                    </div>
                    <div className="container mt-5" id="register">
                        <div className="row">
                            <p className="title-menu"><img src="/resources/assets/frontend/img/ic_register.png"
                                                           alt/><span>đăng ký tài khoản mới</span></p>
                            <div className="col-sm-12">
                                <form onSubmit="" className="frm">
                                    <p className="col-sm-12 col-md-4">
                                        <input type="email" className="form-control text-center"
                                               placeholder="Định danh (không có khoảng trắng)" name="username"
                                               onChange={this.onChange.bind(this)}
                                               value={this.state.username}
                                        />
                                        { this.showError('username') }
                                    </p>
                                    <p className="col-sm-12 col-md-4"><input type="email"
                                                                             className="form-control text-center"
                                                                             placeholder="Email của bạn"
                                                                             name="email"
                                                                             onChange={this.onChange.bind(this)}
                                                                             value={this.state.email}/>
                                        { this.showError('email') }
                                    </p>
                                    <p className="col-sm-12 col-md-4"><input type="email"
                                                                             className="form-control text-center"
                                                                             placeholder="Xác nhận email"
                                                                             name="confirm_email"
                                                                             onChange={this.onChange.bind(this)}
                                                                             value={this.state.confirm_email}/>
                                        { this.showError('confirm_email') }
                                    </p>
                                    <p className="col-sm-12 col-md-4"><input type="email"
                                                                             className="form-control text-center"
                                                                             placeholder="Định danh người giới thiệu"
                                                                             name="presenter"
                                                                             onChange={this.onChange.bind(this)}
                                                                             value={this.state.presenter}/>
                                        { this.showError('presenter') }
                                    </p>
                                    <p className="col-sm-12 col-md-4"><input type="submit"
                                                                             className={this.state.register_action.is_processing ? 'btn btn-submit col-sm-12 text-center disabled' : "btn btn-submit col-sm-12 text-center"}
                                                                             defaultValue={this.state.register_action.text}
                                                                             onClick={this.onRegister.bind(this)}/>
                                    </p>
                                </form>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-12">
                                <p style={{margin: 0, color: 'gray'}}>* Vui lòng nhập email chính xác, chúng tôi sẽ
                                    gửi
                                    link xác nhận về email</p>
                            </div>
                        </div>
                    </div>
                    {/*start content*/}
                </div>
                <Footer/>
            </div>
        )
    }

    showError(field_name) {
        if (this.state.errors[field_name] === undefined)
            return null;
        return this.state.errors[field_name].map((error, index) => {
            return <span className="i_alert" key={index}>*{error}</span>
        })
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.AuthReducer,
        profile: state.ProfileReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        AuthAction: bindActionCreators(AuthAction, dispatch),
        ProfileAction: bindActionCreators(ProfileAction, dispatch),
        loading: bindActionCreators({showLoading, hideLoading}, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);