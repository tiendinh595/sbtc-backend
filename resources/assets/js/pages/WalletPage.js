/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import OfferItemSell2 from '../components/Offer/OfferItemSell2'
import OfferItemBuy2 from '../components/Offer/OfferItemBuy2'
import {connect} from 'react-redux'
import Spinner from 'react-spinkit'
import {Link} from 'react-router-dom'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import * as Helper from '../utils/Helper'
import Wallet from '../components/User/Wallet'
import $ from 'jquery'
import ReactHtmlParser from 'react-html-parser'
import BankSelectBox from '../components/Offer/BankSelectBox'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


class WalletPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            is_loading: false,
            address_wallet: null,
            transactions: {},

            alert: {
                is_show: false,
                type: 'danger',
                text: ''
            },

            is_processing: false,
            data_withdraw: {
                type_money: 'btc',
                amount: 0,
                to_address: ''
            },

            withdraw_fiat: {
                current_step: 1,
                banks: [],
                bank_code: 0,
                bank_number: '',
                bank_account_name: '',
                bank_selected: {}
            },
            deposit_fiat: {
                current_step: 1,
                is_processing: false,
                amount: 0,
                deposit_obj: {},
                upload_receipt: {
                    is_uploading: false,
                    percent: '',
                    is_error: false
                },
            }
        };

        this.coin_name = {
            btc: 'Bitcoin',
            eth: 'Ethereum',
            fiat: 'VND'
        };

        this._onChangeFormWithdraw = this._onChangeFormWithdraw.bind(this);
        this._onSendRequestWithdraw = this._onSendRequestWithdraw.bind(this);
        this._loadMoreTransactions = this._loadMoreTransactions.bind(this);
        this._showWithdrawFiat = this._showWithdrawFiat.bind(this);
        this._withdrawFiatStep1 = this._withdrawFiatStep1.bind(this);
        this._withdrawFiatStep2 = this._withdrawFiatStep2.bind(this);
        this._withdrawFiatStep3 = this._withdrawFiatStep3.bind(this);
        this._withdrawFiatStep4 = this._withdrawFiatStep4.bind(this);
        this._changeStepWithdrawFiat = this._changeStepWithdrawFiat.bind(this);
        this._nextStepWithdrawFiat = this._nextStepWithdrawFiat.bind(this);
        this._loadBanks = this._loadBanks.bind(this);
        this._onAddNewBank = this._onAddNewBank.bind(this);
        this._onRemoveBank = this._onRemoveBank.bind(this);
        this._showFormDepositFiat = this._showFormDepositFiat.bind(this);
        this._depositFiatStep1 = this._depositFiatStep1.bind(this);
        this._depositFiatStep2 = this._depositFiatStep2.bind(this);
        this._onInitDepositFiat = this._onInitDepositFiat.bind(this);
        this._onUpdateDepositFiat = this._onUpdateDepositFiat.bind(this);
        this._onUploadReceipt = this._onUploadReceipt.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const {coin, page} = this.props.match.params;

        document.title = `Ví ${this.coin_name[coin]}`;
    }

    componentWillReceiveProps(newProps) {
        const {coin, page} = newProps.match.params;

        if (this.props.match.params.page != page) {
            this.setState({
                withdraw_fiat: {
                    ...this.state.withdraw_fiat,
                    current_step: 1
                },
                alert: {
                    ...this.state.alert,
                    is_show: false,
                }
            });
        }
        if (page == 'transactions' && !this.state.is_loading) {
            this._loadMoreTransactions(`/wallet/transaction/${coin}`)
        } else if (page == 'withdraw' && coin == 'fiat') {
            this._loadBanks()
        }
    }

    _loadMoreTransactions(url) {
        this.setState({
            is_loading: true
        });

        ApiCaller.get(url)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        transactions: res.data,
                        is_loading: false
                    })
                } else {
                    this.setState({
                        is_loading: false
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false
                });
            })
    }

    _loadBanks() {
        this.setState({
            is_loading: true
        });

        ApiCaller.get('/bank')
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        withdraw_fiat: {
                            ...this.state.withdraw_fiat,
                            banks: res.data
                        },
                        is_loading: false
                    })
                } else {
                    this.setState({
                        is_loading: false
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false
                });
            })
    }

    _onRemoveBank(e, id) {
        e.preventDefault();

        ApiCaller.remove(`/bank/${id}`)
            .then(res => {
                if (res.code == 200) {
                    this._loadBanks()
                }
            })
            .catch(err => {

            })
    }

    _onAddNewBank(e) {
        e.preventDefault();
        const {bank_code, bank_number, bank_account_name} = this.state.withdraw_fiat;

        ApiCaller.post('/bank', {bank_code, bank_number, bank_account_name})
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        withdraw_fiat: {
                            ...this.state.withdraw_fiat,
                            banks: res.data
                        },
                        is_loading: false,
                        alert: {
                            is_show: true,
                            type: 'info',
                            text: res.msg
                        }
                    })
                } else {
                    this.setState({
                        is_loading: false,
                        alert: {
                            is_show: true,
                            type: 'danger',
                            text: res.msg
                        }
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                    alert: {
                        is_show: true,
                        type: 'danger',
                        text: 'Có lỗi xảy ra trong quá trinh thêm ngân hàng'
                    }
                });
            })
    }

    componentDidUpdate(prevProps, prevState) {
        const {coin, page} = this.props.match.params;
        document.title = `Ví ${this.coin_name[coin]}`;
    }

    _showMessage() {
        const {alert} = this.state;
        if (!alert.is_show)
            return null;

        let msg_show = typeof alert.text == 'string' ? alert.text : alert.text.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <div className={`alert alert-${alert.type}`}>
                {ReactHtmlParser(msg_show)}
            </div>
        )
    }

    _onCopyToClipBoard(e) {
        e.preventDefault();
        document.querySelector('#address_wallet').select();
        document.execCommand('copy');
    }

    _onChangeFormWithdraw(e) {
        const {name, value} = e.target;

        this.setState({
            data_withdraw: {
                ...this.state.data_withdraw,
                [name]: value,
            }
        })
    }

    _onSendRequestWithdraw(e) {
        e.preventDefault();
        const {data_withdraw} = this.state;
        const {coin} = this.props.match.params;

        this.setState({
            is_processing: true
        });

        data_withdraw.type_money = coin;
        ApiCaller.post('/wallet/withdraw', data_withdraw)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        is_processing: false,
                        alert: {
                            is_show: true,
                            type: 'info',
                            text: res.msg
                        }
                    });
                    if (coin == 'fiat')
                        this.setState({
                            withdraw_fiat: {
                                ...this.state.withdraw_fiat,
                                current_step: 4
                            },
                        })
                } else {
                    this.setState({
                        is_processing: false,
                        alert: {
                            is_show: true,
                            type: 'danger',
                            text: res.msg
                        }
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_processing: false,
                    alert: {
                        is_show: true,
                        type: 'danger',
                        text: 'Có lỗi trong quá trình xử lý'
                    }
                });
            })
    }



    _onInitDepositFiat(e) {
        e.preventDefault();
        const {amount} = this.state.deposit_fiat;

        this.setState({
            is_processing: true,
        });

        ApiCaller.post('/wallet/init-deposit-fiat', {amount})
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        deposit_fiat: {
                            ...this.state.deposit_fiat,
                            current_step: 2,
                            is_processing: false,
                            amount: 0,
                            deposit_obj: res.data,
                        },
                        alert: {
                            ...this.state.alert,
                            is_show: false,
                        }
                    })
                } else {
                    this.setState({
                        is_processing: false,
                        alert: {
                            is_show: true,
                            type: 'danger',
                            text: res.msg
                        }
                    });
                }
            })
            .catch(err => {
                this.setState({
                    is_processing: false,
                    alert: {
                        is_show: true,
                        type: 'danger',
                        text: 'Có lỗi trong quá trình xử lý'
                    }
                });
            })
    }
    _onUploadReceipt(e) {
        if(this.state.deposit_fiat.upload_receipt.is_uploading)
            return false;

        this.setState({
            deposit_fiat:{
                ...this.state.deposit_fiat,
                upload_receipt: {
                    is_uploading: true,
                    percent: '0/100',
                    is_error: false
                }
            }
        });

        let formData = new FormData();
        formData.append('receipt', e.target.files[0]);
        ApiCaller.post(`/wallet/transaction/${this.state.deposit_fiat.deposit_obj.id}/receipt`, formData, (progressEvent)=>{
            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
            this.setState({
                deposit_fiat:{
                    ...this.state.deposit_fiat,
                    upload_receipt: {
                        is_uploading: true,
                        percent: `${percentCompleted}/100`,
                        is_error: false
                    }
                }
            });
        })
            .then(res=>{
                if(res.code == 200) {
                    this.setState({
                        deposit_fiat: {
                            ...this.state.deposit_fiat,
                            deposit_obj: res.data,
                            upload_receipt: {
                                is_uploading: false,
                                percent: '',
                                is_error: false
                            },
                        },
                        is_processing: false,
                    });
                    NotificationManager.success(res.msg)
                } else {
                    this.setState({
                        deposit_fiat: {
                            ...this.state.deposit_fiat,
                            upload_receipt: {
                                is_uploading: false,
                                percent: '',
                                is_error: false
                            },
                        },
                        is_processing: false,
                    });
                    NotificationManager.error(res.msg)
                }
            })
            .catch(err=>{
                this.setState({
                    deposit_fiat: {
                        ...this.state.deposit_fiat,
                        upload_receipt: {
                            is_uploading: false,
                            percent: '',
                            is_error: false
                        },
                    },
                    is_processing: false,
                });
                NotificationManager.error('Có lỗi trong quá trình upload')
            })
    }
    _onUpdateDepositFiat(e, status) {
        e.preventDefault();
        ApiCaller.post(`/wallet/transaction/${this.state.deposit_fiat.deposit_obj.id}/status`, {status})
            .then(res=>{
                if(res.code == 200) {
                    this.setState({
                        deposit_fiat: {
                            ...this.state.deposit_fiat,
                            current_step: 1,
                            is_processing: false,
                            amount: 0,
                            deposit_obj: {},
                        },
                        is_processing: false,
                    });
                    NotificationManager.success(res.msg)
                } else {
                    NotificationManager.error(res.msg)
                }
            })
            .catch(err=>{
                NotificationManager.error('Có lỗi xảy ra trong quá trình xử lý')
            })
    }

    _showFormDeposit() {
        const {coin, page} = this.props.match.params;
        const {profile} = this.props;

        if (coin == 'fiat')
            return this._showFormDepositFiat();

        let address_wallet = coin == 'btc' ? profile.wallet.address_btc : profile.wallet.address_eth;
        $('body').find('#address_wallet').val(address_wallet);
        const link_address = coin == 'btc' ? `https://blockchain.info/address/${address_wallet}` : `https://etherscan.io/address/${address_wallet}`;

        return (
            <form action className="form form-inline text-center">
                <div className="form-group col-sm-12 col-sm-6 col-lg-6 offset-lg-3">
                    <div className="input-group" style={{width: '100%'}}>
                        <input type="text" id="address_wallet" value={address_wallet} className="form-control"
                               readOnly/>
                        <div className="input-group-append">
                            <span className="input-group-text"><a href={link_address} target="_blank"
                                                                  className="btn btn-gray" style={{
                                borderBottomLeftRadius: 0,
                                borderTopLeftRadius: 0
                            }}><img src="/resources/assets/frontend/img/ic_goto.png" alt/></a></span>
                            <span className="input-group-text ml-1"><a href="#" onClick={this._onCopyToClipBoard}
                                                                       className="btn btn-gray"><img
                                src="/resources/assets/frontend/img/ic_copy.png" alt/></a></span>
                        </div>
                    </div>
                    <p className="text-left">* Địa chỉ nạp tiền này chỉ chấp nhận {coin.toString().toUpperCase()}. Gửi
                        các loại đơn vị khác có thể
                        dẫn đến việc mất tiền gửi của bạn.</p>
                    <div style={{width: '100%'}}>
                        <img
                            src={`https://chart.googleapis.com/chart?cht=qr&chs=200x200&choe=UTF-8&chld=H&chl=${address_wallet}`}
                            alt width={190} height={190}/>
                    </div>
                </div>
            </form>
        )
    }

    _showFormDepositFiat() {
        const {deposit_fiat} = this.state;
        if (deposit_fiat.current_step == 1)
            return this._depositFiatStep1();
        else if (deposit_fiat.current_step == 2)
            return this._depositFiatStep2();
    }

    _depositFiatStep1() {
        return (
            <form action className="form">
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right">Số VNĐ muốn nạp</label>
                    <div className="col-sm-5 no-pd">
                        <input type="text" className="form-control " value={this.state.deposit_fiat.amount}
                               onChange={(e) => this.setState({
                                   deposit_fiat: {
                                       ...this.state.deposit_fiat,
                                       amount: e.target.value
                                   }
                               })}/>
                    </div>
                </div>
                {this._showMessage()}
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right"/>
                    <div className="col-sm-5 no-pd">
                        <button className="btn bg-primary no-radius" disabled={this.state.is_processing}
                                onClick={this._onInitDepositFiat}>Nạp
                        </button>
                    </div>
                </div>
            </form>

        )
    }

    _depositFiatStep2() {
        const {deposit_fiat} = this.state;
        const {deposit_obj} = deposit_fiat;
        return (
            <div>
                <div className="text-center panel-info-trade">
                    Thông tin thanh toán
                </div>
                <table className="table table-bordered">
                    <tbody className="text-left">
                    <tr>
                        <td>Phương thức thanh toán</td>
                        <td>Chuyển khoản ngân hàng trong nước</td>
                    </tr>
                    <tr>
                        <td>Số tiền nạp</td>
                        <td><span className="txt-red">{Helper.formatMoney(deposit_obj.amount_format)} VNĐ</span></td>
                    </tr>
                    <tr>
                        <td>Tên ngân hàng</td>
                        <td>Vietcombank</td>
                    </tr>
                    <tr>
                        <td>Tên chủ tài khoản</td>
                        <td>Vũ Tiến Định</td>
                    </tr>
                    <tr>
                        <td>Số tài khoản</td>
                        <td>01234567890</td>
                    </tr>
                    <tr>
                        <td>Nội dung chuyển khoản</td>
                        <td>{deposit_obj.payment_memo}</td>
                    </tr>
                    </tbody>
                </table>
                <div className="alert alert-success">
                    <b>Quan trọng:</b> Bạn phải bao gồm thông báo tham khảo ở trên trong nộid ung chuyển khoản để
                    ghi có tài khoản SieuThiBitcoin của bạn. Vui lòng gửi toàn bộ số tiền chỉ bằng 1 lần thanh toán.
                    Sau khi bạn gửi thanh toán vui lòng câp nhật bằng chứng thanh toán để chứng tôi có thể xử lý.
                </div>
                <div className="text-center panel-info-trade">
                    Bằng chứng thanh toán của bạn
                </div>
                <table className="table table-bordered">
                    <tbody className="text-left">
                    <tr className="text-center">
                        <td>
                            <div className="file-btn">
                                <input type="file" id="file2" onChange={this._onUploadReceipt} style={{display:'none'}}/>
                                {
                                    deposit_obj.extra.receipt != null
                                        ? <div className="file-group" style={{position: 'relative'}}>
                                            <div className="file-name" style={{width: '100%'}}>{deposit_obj.extra.receipt}</div>
                                            <label htmlFor="file2" className={deposit_fiat.upload_receipt.is_uploading ? "bg-blue disabled" : "bg-blue"} style={{
                                                position: 'absolute',
                                                top: 0,
                                                right: 0,
                                                background: '#2197ee'
                                            }}>Gửi lại {deposit_fiat.upload_receipt.percent}</label>
                                        </div>
                                        :
                                        <label htmlFor="file2" className={deposit_fiat.upload_receipt.is_uploading ? "btn btn-submit text-uppercase disabled" : "btn btn-submit text-uppercase"}>Gửi bằng chứng
                                            thanh toán {deposit_fiat.upload_receipt.percent}</label>

                                }

                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div>
                    <b>* Đổi ý?</b> Bạn có thể hủy lệnh nạp bấtk ỳ lúc nào, <a href="#"
                                                                               onClick={(e) => this._onUpdateDepositFiat(e, 4)}><b
                    className="txt-red">Hủy lệnh nạp của tôi!</b></a>
                </div>
                <div className="text-center mt-3">
                    <a href className="btn btn-submit text-uppercase" onClick={(e) => this._onUpdateDepositFiat(e, 1)}>Xác
                        nhận</a>
                </div>
            </div>

        )
    }

    _showFormWithdraw() {
        const {coin, page} = this.props.match.params;
        const {profile} = this.props;
        let balance = coin == 'btc' ? profile.wallet.balance_btc : profile.wallet.balance_eth;
        $('#txt_balance').val(balance);

        if (coin == 'fiat')
            return this._showWithdrawFiat();

        return (
            <form action className="form">
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right">Số {this.coin_name[coin]} khả dụng</label>
                    <div className="col-sm-5 no-pd">
                        <input type="text" className="form-control disabled" id="txt_balance" defaultValue={balance}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right">Ví {this.coin_name[coin]}</label>
                    <div className="col-sm-5 no-pd">
                        <input type="text" className="form-control " name="to_address" required={true}
                               onChange={this._onChangeFormWithdraw}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right">Lượng {this.coin_name[coin]}</label>
                    <div className="col-sm-5 no-pd">
                        <input type="number" className="form-control " name="amount" required={true}
                               onChange={this._onChangeFormWithdraw}/>
                    </div>
                </div>
                { this._showMessage() }
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right"/>
                    <div className="col-sm-5 no-pd">
                        <button className="btn bg-primary no-radius" onClick={this._onSendRequestWithdraw}
                                disabled={this.state.is_processing}>RÚT
                        </button>
                    </div>
                </div>
            </form>
        )
    }

    _changeStepWithdrawFiat(step) {
        this.setState({
            withdraw_fiat: {
                ...this.state.withdraw_fiat,
                current_step: step
            },
            alert: {
                ...this.state.alert,
                is_show: false,
            }
        });
    }

    _nextStepWithdrawFiat(e, step, extra = null) {
        e.preventDefault();

        this._changeStepWithdrawFiat(step);
        switch (step) {
            case 2:
                this.setState({
                    data_withdraw: {
                        ...this.state.data_withdraw,
                        to_address: extra.id,
                    },
                    withdraw_fiat: {
                        ...this.state.withdraw_fiat,
                        bank_selected: extra,
                        current_step: step
                    }
                });
                break;
            case 3:
                this.setState({
                    data_withdraw: {
                        ...this.state.data_withdraw,
                    }
                });
                break;
        }
    }


    _showWithdrawFiat() {
        const {withdraw_fiat} = this.state;

        let current_step = null;
        switch (withdraw_fiat.current_step) {
            case 1:
                current_step = this._withdrawFiatStep1();
                break;
            case 2:
                current_step = this._withdrawFiatStep2();
                break;
            case 3:
                current_step = this._withdrawFiatStep3();
                break;
            case 4:
                current_step = this._withdrawFiatStep4();
                break
        }
        return (
            <div>
                <div className=" text-center">
                    <ul className="nav nav-tabs step-anchor" style={{marginTop: 0, marginBottom: 20}}>
                        <li className={withdraw_fiat.current_step == 1 ? 'active' : ''}><a href="#">Chọn tài khoản</a>
                        </li>
                        <li className={withdraw_fiat.current_step == 2 ? 'active' : ''}><a href="#">Nhập thông tin</a>
                        </li>
                        <li className={withdraw_fiat.current_step == 3 ? 'active' : ''}><a href="#">Xác nhận</a></li>
                    </ul>
                </div>
                {
                    current_step
                }
            </div>
        )

    }

    _withdrawFiatStep1() {
        const {banks} = this.state.withdraw_fiat;

        return (
            <div>

                <div className="text-center panel-info-trade">
                    Tài khoản ngân hàng của bạn
                </div>
                <table className="table table-bordered">
                    <tbody className="text-left">
                    {
                        banks.length == 0
                            ?
                            <div className="alert alert-info">Chưa có dữ liệu</div>
                            :
                            banks.map((bank, index) => {
                                return (
                                    <tr key={index}>
                                        <td>
                                            <p><b>Tên ngân hàng</b>: {bank.bank_name}</p>
                                            <p><b>Tên chủ tài
                                                khoản</b>: {bank.bank_account_name.toString().toUpperCase()}
                                            </p>
                                            <p><b>Số tài khoản</b>: {bank.bank_number}</p>
                                        </td>
                                        <td className="text-center" style={{verticalAlign: 'middle'}}>
                                            <a href="#" className="btn btn-submit"
                                               onClick={(e) => this._nextStepWithdrawFiat(e, 2, bank)}>Chọn</a>
                                            <a href="#" onClick={(e) => this._onRemoveBank(e, bank.id)}
                                               className="btn btn-danger ml-1">Xoá</a>
                                        </td>
                                    </tr>
                                )
                            })
                    }
                    </tbody>
                </table>

                <div className="text-center panel-info-trade mt-3">
                    Tạo tài khoản mới
                </div>
                <form action className="form form-border pt-3">
                    { this._showMessage() }
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label text-right">Tên ngân hàng</label>
                        <BankSelectBox change={(bank) => {
                            this.setState({withdraw_fiat: {...this.state.withdraw_fiat, bank_code: bank}})
                            console.log(bank)
                        }} select={0} disabled={false} className="col-sm-4 no-pd"/>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label text-right">Tên chủ tài khoản</label>
                        <div className="col-sm-4 no-pd">
                            <input type="text" className="form-control " onChange={(e) => this.setState({
                                withdraw_fiat: {
                                    ...this.state.withdraw_fiat,
                                    bank_account_name: e.target.value
                                }
                            })}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label text-right">Số tài khoản</label>
                        <div className="col-sm-4 no-pd">
                            <input type="text" className="form-control " onChange={(e) => this.setState({
                                withdraw_fiat: {
                                    ...this.state.withdraw_fiat,
                                    bank_number: e.target.value
                                }
                            })}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label text-right"/>
                        <div className="col-sm-4 no-pd">
                            <button className="btn bg-primary no-radius" onClick={this._onAddNewBank}>THÊM MỚI</button>
                        </div>
                    </div>
                </form>
            </div>

        )
    }

    _withdrawFiatStep2() {
        return (
            <form action className="form">
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right">Số VNĐ muốn rút</label>
                    <div className="col-sm-5 no-pd">
                        <input type="text" className="form-control " name="amount"
                               defaultValue={this.state.data_withdraw.amount} onChange={this._onChangeFormWithdraw}/>
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-sm-4 col-form-label text-right"/>
                    <div className="col-sm-5 no-pd">
                        <a href="#" className="btn btn-gray" onClick={() => this._changeStepWithdrawFiat(1)}>Quay
                            lại</a>
                        <button className="btn bg-primary no-radius ml-1"
                                onClick={(e) => this._nextStepWithdrawFiat(e, 3)}>
                            Tiếp Tục
                        </button>
                    </div>
                </div>
            </form>
        )
    }

    _withdrawFiatStep3() {
        const {bank_selected} = this.state.withdraw_fiat;

        return (
            <div>
                <div>
                    {this._showMessage()}
                    <div className="text-center panel-info-trade">
                        Thông tin rút tiền
                    </div>
                    <table className="table table-bordered">
                        <tbody className="text-left">
                        <tr>
                            <td>Phương thức thanh toán</td>
                            <td>Chuyển khoản ngân hàng trong nước</td>
                        </tr>
                        <tr>
                            <td>Số tiền rút</td>
                            <td><span className="txt-red">{Helper.formatMoney(this.state.data_withdraw.amount)}
                                VNĐ</span></td>
                        </tr>
                        <tr>
                            <td>Tên ngân hàng</td>
                            <td>{bank_selected.bank_name}</td>
                        </tr>
                        <tr>
                            <td>Tên chủ tài khoản</td>
                            <td>{bank_selected.bank_account_name.toString().toUpperCase()}</td>
                        </tr>
                        <tr>
                            <td>Số tài khoản</td>
                            <td>{bank_selected.bank_number}</td>
                        </tr>
                        {/*<tr>*/}
                        {/*<td>Nội dung chuyển khoản</td>*/}
                        {/*<td>NAP12345</td>*/}
                        {/*</tr>*/}
                        </tbody>
                    </table>
                    <form action className="form">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label text-right"/>
                            <div className="col-sm-5 no-pd">
                                <a href="#" className="btn btn-gray" onClick={() => this._changeStepWithdrawFiat(2)}>Quay
                                    lại</a>
                                <button className="btn bg-primary no-radius ml-1" onClick={this._onSendRequestWithdraw}
                                        disabled={this.state.is_processing}>Xác nhận
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        )
    }

    _withdrawFiatStep4() {
        return (
            <div>
                {this._showMessage()}
                <form action className="form">
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label text-right"/>
                        <div className="col-sm-5 no-pd">
                            <a href="#" className="btn btn-gray" onClick={() => this._changeStepWithdrawFiat(1)}>Quay
                                lại</a>
                        </div>
                    </div>
                </form>
            </div>

        )
    }

    _showTransactions() {
        const {coin, page} = this.props.match.params;
        const {transactions, is_loading} = this.state;

        if (is_loading)
            return (
                <div className="container text-center mt-5 mb-5">
                    <div className="row">
                        <div className="col-12 justify-content-center text-center">
                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            );

        return [
            <table className="table table-bordered" key="1">
                <thead className="thead-light">
                <tr>
                    <td className="text-center bg-gray-dark">Thời gian</td>
                    <td className="text-center bg-gray-dark">Giao dịch</td>
                    <td className="text-center bg-gray-dark">Số {this.coin_name[coin]} giao dịch</td>
                    <td className="text-center bg-gray-dark">Trạng thái giao dịch</td>
                </tr>
                </thead>
                <tbody>

                {
                    transactions.data.length == 0
                        ? <tr>
                            <td colSpan="4">
                                <div className="alert alert-info">Chưa có dữ liệu</div>
                            </td>
                        </tr>
                        : transactions.data.map((transaction, index) => {
                            return (
                                <tr key={index}>
                                    <td className="text-center">{transaction.created_at}</td>
                                    <td className="text-center">{transaction.payment_memo}</td>
                                    <td className="text-center">{transaction.amount_format}</td>
                                    <td className="text-center">{transaction.status_name}</td>
                                </tr>
                            )
                        })
                }

                </tbody>
            </table>,
            <div className="text-center" key="2">
                <div className="btn-group pagination d-inline" role="group" aria-label="Basic example">
                    <a onClick={this._loadMoreTransactions.bind(this, transactions.prev_page_url)}
                       type="button"
                       className={transactions.prev_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG TRƯỚC</a>
                    <a onClick={this._loadMoreTransactions.bind(this, transactions.next_page_url)}
                       type="button"
                       className={transactions.next_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG SAU</a>
                </div>
            </div>
        ]
    }

    _showPage() {
        const {coin, page} = this.props.match.params;
        const {profile} = this.props;

        if (profile.is_logged == false)
            return null;

        if (coin == 'fiat' && profile.doc_status != 2)
            return <div className="alert alert-danger text-left">Bạn phải xác minh tài khoản để sử dụng ví VND. <Link
                to="/setting/verify">Xác minh ngay</Link></div>

        if (page == 'deposit')
            return this._showFormDeposit();
        else if (page == 'withdraw')
            return this._showFormWithdraw();
        return this._showTransactions();
    }

    _showMain() {
        const {coin, page} = this.props.match.params;

        return (
            <div className="row mt-5">
                <ul className="nav nav-tabs nav-justified tabs-trade">
                    <li className={coin == 'btc' ? 'active' : ''}><Link to={`/wallet/btc/${page}`}><img
                        src={coin == 'btc' ? "/resources/assets/frontend/img/wallet_btc_active.png" : "/resources/assets/frontend/img/wallet_btc.png"}
                        alt style={{display: 'inline-block', marginTop: '-5px'}}/> Ví Bitcoin</Link></li>
                    <li className={coin == 'eth' ? 'active' : ''}><Link to={`/wallet/eth/${page}`}><img
                        src={coin == 'eth' ? "/resources/assets/frontend/img/wallet_eth_active.png" : "/resources/assets/frontend/img/wallet_eth.png"}
                        alt style={{display: 'inline-block', marginTop: '-5px'}}/> Ví Ethereum</Link></li>
                    <li className={coin == 'fiat' ? 'active' : ''}><Link to={`/wallet/fiat/${page}`}><img
                        src={coin == 'fiat' ? "/resources/assets/frontend/img/wallet_vnd_active.png" : "/resources/assets/frontend/img/wallet_vnd.png"}
                        alt style={{display: 'inline-block', marginTop: '-5px'}}/> Ví VNĐ</Link></li>
                </ul>
                <div className="pn-active">
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-lg-3">
                                <ul className="nav flex-column menu-wallet-left">
                                    <li className={page == 'deposit' ? 'nav-item active' : 'nav-item'}>
                                        <Link to={`/wallet/${coin}/deposit`} className="nav-link"><img
                                            src={page == 'deposit' ? "/resources/assets/frontend/img/wallet_nap_active.png" : "/resources/assets/frontend/img/wallet_nap.png"}
                                            alt/> Nạp {this.coin_name[coin]}</Link>
                                    </li>
                                    <li className={page == 'withdraw' ? 'nav-item active' : 'nav-item'}>
                                        <Link to={`/wallet/${coin}/withdraw`} className="nav-link" href="#"><img
                                            src={page == 'withdraw' ? "/resources/assets/frontend/img/wallet_rut_active.png" : "/resources/assets/frontend/img/wallet_rut.png"}
                                            alt/> Rút {this.coin_name[coin]}</Link>
                                    </li>
                                    <li className={page == 'transactions' ? 'nav-item active' : 'nav-item'}>
                                        <Link to={`/wallet/${coin}/transactions`} className="nav-link" href="#"><img
                                            src={page == 'transactions' ? "/resources/assets/frontend/img/wallet_lichsu_active.png" : "/resources/assets/frontend/img/wallet_lichsu.png"}
                                            alt/> Lịch sử giao dịch</Link>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-9">
                                {this._showPage()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

    render() {
        const {profile} = this.props;

        return (
            <div>
                <NotificationContainer/>
                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={profile.wallet}/>

                        { this._showMain() }

                    </div>
                    {/*start content*/}
                </div>


                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

export default connect(mapStateToProps, null)(WalletPage);