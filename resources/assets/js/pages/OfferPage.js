/**
 * Created by dinh on 12/14/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import Wallet from '../components/User/Wallet'
import Spinner from 'react-spinkit'
import Simplert from 'react-simplert'
import {connect} from 'react-redux'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import ReactHtmlParser from 'react-html-parser'
import $ from 'jquery'
import * as Helper from '../utils/Helper'
import * as Label from '../components/Offer/Lable'
import BankSelectBox from '../components/Offer/BankSelectBox'
import {Redirect} from 'react-router-dom'
import Select from 'react-select';
import '../style/react-select.css';

class OfferPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            offer: {},
            amount_usd: '0 usd',
            amount_vnd: 0,
            amount_coin: 0,
            is_next: false,
            current_step: 1,
            loaded: false,
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },

            offer_id: 0,
            payment_method: '',
            bank_code: '',
            bank_number: '',
            bank_account_name: '',
            to_address: '',

            next_step: {
                text: 'TIẾP TỤC',
                processing: false
            },
            redirect: {
                is_redirect: false,
                link: ''
            },
            completed: false,
            trade: {}
        };

        this._onChange = this._onChange.bind(this);
        this._completeSellStep1 = this._completeSellStep1.bind(this);
        this._formSell = this._formSell.bind(this);
        this._formSellStep1 = this._formSellStep1.bind(this);
        this._formSellStep2 = this._formSellStep2.bind(this);
        this._createTrade = this._createTrade.bind(this);
        this._showMessage = this._showMessage.bind(this);
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const {id} = this.props.match.params;

        $('body').undelegate('.panel-header', 'click');
        $('body').on('click', '.panel-header', function (e) {
            e.preventDefault();

            let parent_el = $(this).parents('.pn');
            // $('.pn').removeClass('active');
            // $('.panel-body').slideUp('slow');
            $('.direct').html('+');
            parent_el.addClass('active');
            let body_el = parent_el.find('.panel-body');
            let direct_el = parent_el.find('.direct');
            if (body_el.is(':visible')) {
                body_el.slideUp('slow');
                direct_el.html('+')
                parent_el.removeClass('active');
            }
            else {
                parent_el.addClass('active');
                body_el.slideDown('slow');
                direct_el.html('-')
            }
        });

        ApiCaller.get(`/offer/${id}?token=${AuthService.getToken()}`)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        offer: res.data,
                        offer_id: res.data.id,
                        amount_coin: res.data.min_amount,
                        is_next: res.data.status == 1,
                        loaded: true,
                        bank_code: res.data.bank_code,
                        next_step: {
                            text: res.data.type_offer == 1 ? `Mua ${Label.coin(res.data.type_money, true, true)}` : 'TIẾP TỤC',
                            processing: false
                        },
                    })
                } else {
                    this.setState({
                        loaded: true,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loaded: true,
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi xảy ra trong quá trình tải dữ liệu'
                    }
                })
            })
    }

    _onChange(e) {
        const name = e.target.name;
        this.setState({alert: {is_show: false, type: '', msg: ''}});

        if (name == 'amount_coin') {
            const deal_id = parseInt(this.state.offer.deal_id);
            if(deal_id > 0)
                return false;

            const amount_coin = e.target.value;
            const amount_coin_parse = parseFloat(amount_coin).toFixed(8);
            let {offer, is_next, amount_usd} = this.state;
            if (amount_coin_parse > parseFloat(offer.current_amount) || amount_coin_parse < parseFloat(offer.min_amount)) {
                is_next = false;
                amount_usd = amount_coin_parse > parseFloat(offer.current_amount) ? `Lượng ${Label.coin(offer.type_money)} tối đa ${offer.current_amount}` : `Lượng ${Label.coin(offer.type_money)} tối thiểu ${offer.min_amount}`;
            } else {
                is_next = true;
            }
            this.setState({
                amount_coin,
                is_next,
                amount_usd
            })
        } else {
            this.setState({
                [name]: e.target.value
            })
        }
    }

    _calAmount() {
        if (this.state.is_next) {
            const amount_usd = Helper.formatMoney(this.state.amount_coin * this.state.offer.rate_usd_coin, 2) + ' USD';
            const amount_vnd = Helper.formatMoney(this.state.amount_coin * this.state.offer.price_per_coin, 0);

            this.setState({
                amount_usd,
                amount_vnd
            })
        }
    }

    componentDidUpdate(prevProps, prevSate) {
        if (prevSate.amount_coin != this.state.amount_coin)
            this._calAmount();
    }

    _completeSellStep1(e) {
        e.preventDefault();
        this.setState({
            current_step: 2
        })
    }

    _createTrade(e) {
        e.preventDefault();
        const {payment_method, bank_code, bank_number, bank_account_name, offer_id, amount_coin, offer, to_address} = this.state;
        this.setState({
            next_step: {
                text: 'ĐANG KHỞI TẠO',
                processing: true
            }
        });

        if (offer.type_offer == 2) {
            ApiCaller.post(`/trade/create?token=${AuthService.getToken()}`, {
                payment_method,
                bank_code,
                bank_number,
                bank_account_name,
                offer_id,
                amount_coin
            })
                .then(res => {
                    if (res.code == 200) {
                        this.setState({
                            redirect: {
                                is_redirect: true,
                                link: `/trade/${res.data.id}`
                            }
                        })
                    } else {
                        this.setState({
                            loaded: true,
                            alert: {
                                is_show: true,
                                type: 'error',
                                msg: res.msg
                            },
                            next_step: {
                                text: 'TIẾP TỤC',
                                processing: false
                            }
                        });
                    }
                })
                .catch(err => {

                })
        } else {
            ApiCaller.post(`/trade/create?token=${AuthService.getToken()}`, {
                to_address: typeof to_address === 'object' && to_address !== null ? to_address.value : to_address,
                offer_id,
                amount_coin
            })
                .then(res => {
                    if (res.code == 200) {
                        this.setState({
                            redirect: {
                                is_redirect: true,
                                link: `/trade/${res.data.id}`
                            }
                        })
                    } else {
                        this.setState({
                            loaded: true,
                            alert: {
                                is_show: true,
                                type: 'error',
                                msg: res.msg
                            },
                            next_step: {
                                text: `Mua ${Label.coin(offer.type_money, true, true)}`,
                                processing: false
                            }
                        });
                    }
                })
                .catch(err => {

                })
        }

    }

    _formSell(offer) {
        const {current_step} = this.state;
        return current_step == 1 ? this._formSellStep1(offer) : this._formSellStep2(offer)
    }

    _formSellStep1(offer) {
        const deal_id = parseInt(offer.deal_id);
        const is_change_amount = deal_id == 0 || isNaN(deal_id);

        return (
            <div className="row mt-5">
                { offer.status != 1
                    ? <div className="col-12 no-pd">
                        <div className="alert alert-danger">Offer này đã đóng</div>
                    </div>
                    : ''
                }

                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(offer.title_html)}
                    </div>
                    <div className="col-12 panel-body-active text-center" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <form action className="form form-inline">
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="btc" className="col-sm-5">Số
                                            lượng {Label.coin(offer.type_money, true, true)} </label>
                                        <div className="col-sm-7">
                                            <input type="number" name="amount_coin" className="form-control"
                                                   id="btc"
                                                   value={this.state.amount_coin}
                                                   onChange={this._onChange} disabled={!is_change_amount}/>
                                            <small className="form-text text-left txt-red">≈ {this.state.amount_usd}
                                            </small>
                                        </div>
                                    </div>
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="vnd" className="col-sm-5">Số lượng VNĐ </label>
                                        <div className="col-sm-7">
                                            <input type="text" className="form-control" id="vnd" name="amount"
                                                   value={this.state.amount_vnd} readOnly={true}/>
                                            <small
                                                className="form-text text-left text-danger">&nbsp;</small>
                                        </div>
                                    </div>
                                    <div className="form-group col-sm-12 col-sm-12 col-lg-12 mt-2">
                                        <div className="col-sm-7">
                                            <button className="btn bg-primary no-radius"
                                                    onClick={this._completeSellStep1} disabled={!this.state.is_next}
                                                    style={{marginLeft: '-8px'}}>
                                                Bán {Label.coin(offer.type_money, true, true)}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-sm-12 col-md-8 offset-md-2 mt-5">
                                <div className="text-center panel-info-trade">
                                    Thông tin chi tiết quảng cáo
                                </div>
                                <table className="table table-bordered">
                                    <tbody className="text-left">
                                    <tr>
                                        <td>Mua từ</td>
                                        <td>{offer.username}</td>
                                    </tr>
                                    <tr>
                                        <td>Tỉ Giá</td>
                                        <td><span className="txt-red">{offer.rate_vnd_usd_format}</span>/USD</td>
                                    </tr>
                                    <tr>
                                        <td>Lượng giới hạn</td>
                                        <td><span
                                            className="badge badge-btc">{offer.min_amount} {Label.coin(offer.type_money, true, true)}</span>
                                            - <span
                                                className="badge badge-btc">{offer.current_amount} {Label.coin(offer.type_money, true, true)}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phương thức thanh toán</td>
                                        <td>{offer.payment_method_name} {offer.bank_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Thời gian thanh toán</td>
                                        <td>{offer.payment_time} phút</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div className="pn mt-2">
                                    <div className="panel-header bg-primary">
                                        Giao dịch an toàn không?
                                        <span className="direct open">+</span>
                                    </div>
                                    <div className="col-12 panel-body border text-left">
                                        <p>{Label.coin(offer.type_money, true, true)} của bạn sẽ được khoá lại và sẽ
                                            không được giải phóng cho
                                            đến khi bạn nhận được thanh toán từ người mua</p>
                                        <p><a href>Bấm vào đây để đọc thêm về Giao Dịch Đảm Bảo</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    _formSellStep2(offer) {
        if (typeof offer != 'object')
            return null;
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(offer.title_html)}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_load.png"
                                         style={{display: 'inline-block', marginTop: '-30px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="active"><a href="#">Cập nhật thông tin thanh toán</a></li>
                                        <li><a href="">Giải phóng {Label.coin(offer.type_money, false, true)} của
                                            bạn</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">Cập nhật thông tin thanh toán</p>
                                <p>Bạn đang bán {this.state.amount_coin} cho <span
                                    className="txt-red">{offer.username.toUpperCase()}</span>.<br />
                                    Vui lòng cung cấp thông tin nhận tiền của bạn dưới đây để người mua có thể thanh
                                    toán cho bạn</p>
                            </div>
                            <div className="col-sm-12 col-md-8 offset-md-2 mt-3">
                                <div className="text-center panel-info-trade">
                                    Thông tin thanh toán
                                </div>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12">
                                        <form action className="form form-border pt-3">
                                            <div className="form-group row">
                                                <label className="col-sm-4 col-form-label text-right">Tên ngân
                                                    hàng</label>
                                                <BankSelectBox change={(bank) => {
                                                    this.setState({bank_code: bank})
                                                }} select={offer.bank_code} disabled={false}/>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4 col-form-label text-right">Tên chủ tài
                                                    khoản</label>
                                                <div className="col-sm-4 no-pd">
                                                    <input type="text" className="form-control "
                                                           name="bank_account_name" onChange={this._onChange}/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4 col-form-label text-right">Số tài
                                                    khoản</label>
                                                <div className="col-sm-4 no-pd">
                                                    <input type="text" className="form-control " name="bank_number"
                                                           onChange={this._onChange}/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4 col-form-label text-right"/>
                                                <div className="col-sm-4 no-pd">
                                                    <button className="btn bg-primary no-radius"
                                                            onClick={this._createTrade.bind(this)}
                                                            disabled={this.state.next_step.processing}>{this.state.next_step.text}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <small className="form-text text-left text-danger mt-3">* Vui lòng cập nhật
                                            đúng thông tin để cập nhật nhanh chóng
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    _autoCompleteAddress(e) {
        const to_address = this.state.offer.type_money == 1 ? this.props.profile.wallet.address_btc : this.props.profile.wallet.address_eth;
        this.refs.to_address.value = to_address;
        this.setState({
            to_address
        })
    }

    _formBuy(offer) {
        const {deal_id} = offer;
        const is_change_amount = deal_id == 0 || isNaN(deal_id);

        const to_address = typeof this.state.to_address === 'object' && this.state.to_address != null ? this.state.to_address.value : this.state.to_address;
        const value = {value: to_address, label:to_address};

        let to_address_default = null;
        if(this.props.profile.wallet !== undefined)
            to_address_default = offer.type_money == 1 ? this.props.profile.wallet.address_btc : this.props.profile.wallet.address_eth;

        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(offer.title_html)}
                    </div>
                    <div className="col-12 panel-body-active text-center" style={{display: 'block'}}>
                        <div className="row">

                            <div className="col-md-8 offset-md-2">
                                <form action className="form form-inline">
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="btc" className="col-sm-5">Số
                                            lượng {Label.coin(offer.type_money, true, true)} </label>
                                        <div className="col-sm-7">
                                            <input type="number" className="form-control" id="btc" placeholder={0}
                                                   name="amount_coin"
                                                   value={this.state.amount_coin}
                                                   onChange={this._onChange} disabled={!is_change_amount}/>
                                            <small className="form-text text-left txt-red">
                                                ≈ {this.state.amount_usd} </small>
                                        </div>
                                    </div>
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="vnd" className="col-sm-5">Số lượng VNĐ </label>
                                        <div className="col-sm-7">
                                            <input type="text" className="form-control" id="vnd" placeholder={0}
                                                   value={this.state.amount_vnd} readOnly={true}/>
                                            <small className="form-text text-left text-danger">&nbsp;</small>
                                        </div>
                                    </div>
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="vnd" className="col-sm-5">Địa
                                            chỉ {Label.coin(offer.type_money, true, true)} </label>
                                        <div className="col-sm-7">
                                            <input defaultValue={to_address_default} className="form-control" readOnly={true}/>
                                            {/*<Select.Creatable*/}
                                                {/*className="form-control w-195"*/}
                                                {/*placeholder=""*/}
                                                {/*multi={false}*/}
                                                {/*promptTextCreator={(label)=>`Chọn ví: ${label}`}*/}
                                                {/*options={[{value: to_address_default, label:`Ví của bạn tại Sieuthibtc ${to_address_default}`}]}*/}
                                                {/*onChange={(val)=>this.setState({to_address: val})}*/}
                                                {/*value={value}*/}
                                            {/*/>*/}
                                        </div>
                                    </div>
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <div className="col-sm-5">
                                            <button className="btn bg-primary no-radius"
                                                    onClick={this._createTrade.bind(this)}
                                                    disabled={this.state.next_step.processing}>{this.state.next_step.text}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div className="col-sm-12 col-md-8 offset-md-2 mt-5">
                                <div className="text-center panel-info-trade">
                                    Thông tin chi tiết quảng cáo
                                </div>
                                <table className="table table-bordered">
                                    <tbody className="text-left">
                                    <tr>
                                        <td>Mua từ</td>
                                        <td>{offer.username}</td>
                                    </tr>
                                    <tr>
                                        <td>Giá</td>
                                        <td><span className="txt-red">{Helper.formatMoney(offer.rate_vnd_usd_format)}</span>/USD</td>
                                    </tr>
                                    <tr>
                                        <td>Lượng giới hạn</td>
                                        <td><span
                                            className="badge badge-btc">{offer.min_amount} {Label.coin(offer.type_money, true, true)}</span>
                                            - <span
                                                className="badge badge-btc">{offer.current_amount} {Label.coin(offer.type_money, true, true)}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phương thức thanh toán</td>
                                        <td>{offer.bank_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Thời gian thanh toán</td>
                                        <td>{offer.payment_time} phút</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div>
                                    <div className="pn mt-2">
                                        <div className="panel-header bg-primary">
                                            Giao dịch an toàn không?
                                            <span className="direct open">+</span>
                                        </div>
                                        <div className="col-12 panel-body border text-left">
                                            <p>Khi bạn mở giao dịch, {Label.coin(offer.type_money, true, true)} của người bán sẽ được khoá trong hệ thống,
                                                điều này
                                                giúp bạn yên tâm khi chuyển tiền cho người bán mà không sợ bị lừa đảo.
                                                Nếu người
                                                bán không chịu giải phóng {Label.coin(offer.type_money, true, true)} cho bạn sau khi đã nhận thanh toán,
                                                bạn chỉ việc
                                                nhấn vào nút "Tranh chấp giao dịch" và đội ngũ hỗ trợ của chúng tôi sẽ
                                                xuất hiện để
                                                trợ giúp.</p>
                                            <p><a href>Bấm vào đây để đọc thêm về Giao Dịch Đảm Bảo</a></p>
                                        </div>
                                    </div>
                                    <div className="pn mt-2">
                                        <div className="panel-header bg-primary">
                                            Khi nào tôi nhận được {Label.coin(offer.type_money, true, true)}?
                                            <span className="direct open">+</span>
                                        </div>
                                        <div className="col-12 panel-body border text-left">
                                            <p>Bạn sẽ nhận được {Label.coin(offer.type_money, true, true)} ngay lập tức sau khi người bán xác nhận đã nhận
                                                được thanh
                                                toán của bạn.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const {offer, loaded, redirect} = this.state;
        const {wallet} = this.props.profile;

        if (loaded == false) {
            return (
                <div>
                    <HeaderOther/>
                    <div className="container text-center mt-5 mb-5">
                        <div className="row">
                            <div className="col-12 justify-content-center text-center">
                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            )
        }

        document.title = offer.title;

        return (
            <div>

                {
                    redirect.is_redirect === true ? <Redirect to={redirect.link}/> : ''
                }

                {this._showMessage()}

                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        { offer.type_offer == 2 ? this._formSell(offer) : this._formBuy(offer) }

                    </div>
                    {/*start content*/}
                </div>

                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer,
    }
};

export default connect(mapStateToProps, null)(OfferPage);