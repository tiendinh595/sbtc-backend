/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import {connect} from 'react-redux'
import * as ApiCaller from '../utils/ApiCaller'
import Spinner from 'react-spinkit'
import Simplert from 'react-simplert'
import Wallet from '../components/User/Wallet'
import '../style/verify.css'
import {Link} from 'react-router-dom'
import * as ProfileAction from '../actions/ProfileAction'
import {bindActionCreators} from 'redux'
import {NotificationContainer, NotificationManager} from 'react-notifications';


class VerifyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },

            progress: {
                img1: {
                    className: 'progressbar-blue',
                    percent: 0,
                    img: null
                },
                img2: {
                    className: 'progressbar-blue',
                    percent: 0,
                    img: null
                },
                img3: {
                    className: 'progressbar-blue',
                    percent: 0,
                    img: null
                },
                img4: {
                    className: 'progressbar-blue',
                    percent: 0,
                    img: null
                }
            }
        };

        this._showFormVerify = this._showFormVerify.bind(this);
        this._onConfirmVerify = this._onConfirmVerify.bind(this);
        this._onUploadImage = this._onUploadImage.bind(this);
    }

    componentWillMount() {
        ApiCaller.get('/profile-extra')
            .then(res => {
                this.setState({
                    loaded: true
                });
                this.setState({
                    progress: {
                        img1: {
                            img: res.data.img1 == undefined || res.data.img1 == null ? null : res.data.img1,
                            percent: res.data.img1 == undefined || res.data.img1 == null ? 0 : 100,
                            className: res.data.img1 == undefined || res.data.img1 == null ? 'progressbar-blue' : 'progressbar-yellow',
                        },
                        img2: {
                            img: res.data.img2 == undefined || res.data.img2 == null ? null : res.data.img2,
                            percent: res.data.img2 == undefined || res.data.img2 == null ? 0 : 100,
                            className: res.data.img2 == undefined || res.data.img2 == null ? 'progressbar-blue' : 'progressbar-yellow',
                        },
                        img3: {
                            img: res.data.img3 == undefined || res.data.img3 == null ? null : res.data.img3,
                            percent: res.data.img3 == undefined || res.data.img3 == null ? 0 : 100,
                            className: res.data.img3 == undefined || res.data.img3 == null ? 'progressbar-blue' : 'progressbar-yellow',
                        },
                        img4: {
                            img: res.data.img4 == undefined || res.data.img4 == null ? null : res.data.img4,
                            percent: res.data.img4 == undefined ? 0 : 100,
                            className: res.data.img4 == undefined || res.data.img4 == null ? 'progressbar-blue' : 'progressbar-yellow',
                        }
                    }
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    componentDidMount() {
        document.title = 'Xác minh tài khoản';
        window.scrollTo(0, 0);
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _onConfirmVerify(e) {
        e.preventDefault();

        ApiCaller.post('/verify/confirm')
            .then(res => {
                if (res.code == 200) {
                    this.props.ProfileAction.fetchProfile();
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        },
                    })
                }
            })
            .catch(err => {
                this.setState({
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi xẩy ra trong quá trình xác minh'
                    },
                })
            })
    }

    _onUploadImage(e) {
        const {name} = e.target;

        if (this.state.progress[name].img != null)
            return false;

        let formData = new FormData();
        formData.append('file', e.target.files[0]);
        formData.append('name', name);

        ApiCaller.post('/verify/upload', formData, (progressEvent) => {
            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
            this.setState({
                progress: {
                    ...this.state.progress,
                    [name]: {
                        className: 'progressbar-blue',
                        percent: percentCompleted,
                        img: null
                    }
                }
            })
        })
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        progress: {
                            ...this.state.progress,
                            [name]: {
                                className: 'progressbar-blue',
                                percent: 100,
                                img: res.data[name]
                            }
                        }
                    });
                    NotificationManager.success('Tải lên thành công')
                } else {
                    NotificationManager.error(res.msg)
                    this.setState({
                        progress: {
                            ...this.state.progress,
                            [name]: {
                                className: 'progressbar-red',
                                percent: 100,
                                img: null
                            }
                        }
                    })
                }
            })
            .catch(err => {
                NotificationManager.error('Có lỗi xảy ra')
                this.setState({
                    progress: {
                        ...this.state.progress,
                        [name]: {
                            className: 'progressbar-red',
                            percent: 100,
                            img: null
                        }
                    }
                })
            })
    }


    _showFormVerify() {
        const {profile} = this.props;

        if (profile.is_logged == false)
            return null;

        return (
            <div className="row mt-5">
                <p className="title-menu"><img src="/resources/assets/frontend/img/ic_setting.png"
                                               alt/><span>cài đặt</span></p>
                <div className="pn-active">
                    <div className="panel-header-active">
                        Xác minh tài khoản
                    </div>

                    <div className="col-12 panel-body-active text-center" style={{display: 'block'}}>
                        {
                            profile.doc_status == 3
                                ? <div className="alert alert-danger text-left">Đã xảy ra lỗi trong quá trình xác
                                    thực.</div>
                                :
                                (
                                    profile.doc_status == 1
                                        ? <div className="alert alert-success text-left mt-2">Tài liệu của bạn hiện đang
                                            được phân tích. Điều này có thể mất 2-3 phút.</div>
                                        :
                                        (
                                            profile.doc_status == 2 ?
                                                <div className="alert alert-success text-left mt-2">Tài khoản đã được
                                                    xác minh</div>
                                                :
                                                <div>
                                                    <p style={{margin: 0}} className="text-uppercase">Trạng thái xác
                                                        thực: <span
                                                            className="txt-red">{profile.doc_status_name}</span></p>
                                                    <div className="alert alert-success text-left mt-2">
                                                        <p>Hình ảnh xác minh cần thoả mản các điều kiện sau:</p>
                                                        <p />
                                                        <p>- Chưa qua chỉnh sửa</p>
                                                        <p>- Có độ phân giải tối thiểu 2 megapixels</p>
                                                        <p>- Rõ nét, không bị mờ</p>
                                                        <p />
                                                        <p>Hình ảnh xác minh không thoả mãn các điều kiện trên sẽ không
                                                            bao giờ được chấp nhận.</p>
                                                        <p />
                                                        <p><b>Quá trình xác minh là ngay lập tức. Nó chỉ mất 2-3 phút để
                                                            có được tài khoản của bạn
                                                            được xác nhận.</b></p>
                                                        <p />
                                                    </div>
                                                    <p style={{}} className="text-uppercase">Để được xác thực tài khoản
                                                        bạn cần tải lên</p>
                                                    <div className="row text-left row-middle mt-2">
                                                        <div className="col-md-6">
                                                            <div className="row row-middle">
                                                                <div className="col-md-4">
                                                                    <img
                                                                        src="/resources/assets/frontend/img/ic_xacminh_1.png"
                                                                        alt
                                                                        className="img-fluid"/>
                                                                </div>
                                                                <div className="col-md-8">
                                                                    <div className="text-uppercase">Ảnh CMND của bạn
                                                                    </div>
                                                                    <div className>Ảnh chụp chứng minh nhân dân của
                                                                        bạn
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="file-btn">
                                                                <input type="file" id="file1" name="img1"
                                                                       onChange={this._onUploadImage}/>
                                                                <label htmlFor="file1">Tải ảnh lên</label>
                                                                <p className="help-text">Chỉ jpg, jpeg, png</p>
                                                            </div>
                                                            <div
                                                                className={`progressbar ${this.state.progress.img1.className}`}>
                                                                <div className="progressbar-inner"
                                                                     style={{width: `${this.state.progress.img1.percent}%`}}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row text-left row-middle">
                                                        <div className="col-md-6">
                                                            <div className="row row-middle">
                                                                <div className="col-md-4">
                                                                    <img
                                                                        src="/resources/assets/frontend/img/ic_xacminh_2.png"
                                                                        alt
                                                                        className="img-fluid"/>
                                                                </div>
                                                                <div className="col-md-8">
                                                                    <div className="text-uppercase">Ảnh bạn giữ CMND
                                                                    </div>
                                                                    <div className>Ảnh bạn giữ chứng minh nhân dân đặt
                                                                        trước khuôn
                                                                        mặt của bạn
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="file-btn">
                                                                <input type="file" id="file2" name="img2"
                                                                       onChange={this._onUploadImage}/>
                                                                <label htmlFor="file2" name="img2">Tải ảnh lên</label>
                                                                <p className="help-text">Chỉ jpg, jpeg, png</p>
                                                            </div>
                                                            <div
                                                                className={`progressbar ${this.state.progress.img2.className}`}>
                                                                <div className="progressbar-inner"
                                                                     style={{width: `${this.state.progress.img2.percent}%`}}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row text-left row-middle mt-3">
                                                        <div className="col-md-6">
                                                            <div className="row row-middle">
                                                                <div className="col-md-4">
                                                                    <img
                                                                        src="/resources/assets/frontend/img/ic_xacminh_3.png"
                                                                        alt
                                                                        className="img-fluid"/>
                                                                </div>
                                                                <div className="col-md-8">
                                                                    <div className="text-uppercase">ẢNH CMND ĐẶT TRƯỚC
                                                                        NỀN SIÊU THỊ BITCOIN
                                                                    </div>
                                                                    <div className>Ảnh chụp chứng minh nhân dân đặt
                                                                        trước nền giấy
                                                                        viết tay “Siêu Thị Bitcoin”
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="file-btn">
                                                                <input type="file" id="file3" name="img3"
                                                                       onChange={this._onUploadImage}/>
                                                                <label htmlFor="file3">Tải ảnh lên</label>
                                                                <p className="help-text">Chỉ jpg, jpeg, png</p>
                                                            </div>
                                                            <div
                                                                className={`progressbar ${this.state.progress.img3.className}`}>
                                                                <div className="progressbar-inner"
                                                                     style={{width: `${this.state.progress.img3.percent}%`}}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row text-left row-middle">
                                                        <div className="col-md-6">
                                                            <div className="row row-middle">
                                                                <div className="col-md-4">
                                                                    <img
                                                                        src="/resources/assets/frontend/img/ic_xacminh_1.png"
                                                                        alt
                                                                        className="img-fluid"/>
                                                                </div>
                                                                <div className="col-md-8">
                                                                    <div className="text-uppercase">ẢNH GIẤY TỜ TÙY THÂN
                                                                        CÓ THÔNG TIN CÁ NHÂN
                                                                    </div>
                                                                    <div className>
                                                                        Ảnh giấy tờ tùy thân khác (Hộ chiếu, Hộ Khẩu...)
                                                                        có
                                                                        thông tin cá nhân của bạn
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="file-btn">
                                                                <input type="file" id="file4" name="img4"
                                                                       onChange={this._onUploadImage}/>
                                                                <label htmlFor="file4">Tải ảnh lên</label>
                                                                <p className="help-text">Chỉ jpg, jpeg, png</p>
                                                            </div>
                                                            <div
                                                                className={`progressbar ${this.state.progress.img4.className}`}>
                                                                <div className="progressbar-inner"
                                                                     style={{width: `${this.state.progress.img4.percent}%`}}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        )

                                )
                        }

                        {
                            profile.doc_status == 0
                                ? <div className="row mt-5">
                                    <div className="col-md-12">
                                        <a href="" onClick={this._onConfirmVerify} className="btn btn-submit"
                                           style={{width: 200}}>Tiếp tục</a>
                                    </div>
                                </div>
                                : <div className="row mt-5">
                                    <div className="col-md-12">
                                        <Link to="/setting" className="btn btn-submit" style={{width: 200}}>Trở
                                            về</Link>
                                    </div>
                                </div>

                        }
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const {loaded} = this.state;
        const {wallet} = this.props.profile;

        if (loaded == false) {
            return (
                <div>
                    <HeaderOther/>
                    <div className="container text-center mt-5 mb-5">
                        <div className="row">
                            <div className="col-12 justify-content-center text-center">
                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            )
        }

        return (
            <div>
                <NotificationContainer/>
                <HeaderOther/>
                { this._showMessage() }

                <div className="container-fluid main">
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        {this._showFormVerify()}
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ProfileAction: bindActionCreators(ProfileAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyPage);