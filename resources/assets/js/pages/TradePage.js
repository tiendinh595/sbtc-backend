/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import {connect} from 'react-redux'
import * as ApiCaller from '../utils/ApiCaller'
import Spinner from 'react-spinkit'
import Simplert from 'react-simplert'
import * as AuthService from '../utils/AuthService'
import Wallet from '../components/User/Wallet'
import ReactHtmlParser from 'react-html-parser'
import * as Label from '../components/Offer/Lable'
import Countdown from '../components/Global/Countdown'
import $ from 'jquery'
import Pusher from 'pusher-js'
import * as constants from '../constants/index'
import * as Helper from '../utils/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';


class TradePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            trade: {},
            loaded: false,
            is_show_not_received: false,
            file_receipt: null,

            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },
            redirect: {
                is_redirect: false,
                link: ''
            },

            confirm_paid: {
                status: false,
                text: 'Xác nhận thanh toán'
            },

            upload_receipt: {
                status: false,
                text: 'Gửi biên lai'
            },

            feedback: {
                trade_id: 0,
                rating: 1,
                content: ''
            }
        };

        this._countdownCompleted = this._countdownCompleted.bind(this);
        this._onUpdateStatus = this._onUpdateStatus.bind(this);
        this._cbCountDown = this._cbCountDown.bind(this);
        this._onUploadReceipt = this._onUploadReceipt.bind(this);
        this._onChangeFileReceipt = this._onChangeFileReceipt.bind(this);
        this._onChangeVote = this._onChangeVote.bind(this);
        this._onSendFeedback = this._onSendFeedback.bind(this);
        this._feedback = this._feedback.bind(this);
        this._fetchTrade = this._fetchTrade.bind(this);
    }

    componentWillMount() {
        // this.pusher = new Pusher(constants.slanger.key, {
        //     'wsHost' : constants.slanger.host,
        //     'wsPort': constants.slanger.wsPort,
        //     'cluster' : 'mt1',
        //     enabledTransports: ['wss', 'flash'],
        //     'secret': constants.slanger.secret,
        //     'appId': 1
        // });
        this.pusher = new Pusher(constants.slanger.key, {
            cluster: 'ap1',
            encrypted: true
        });
        this.channel = this.pusher.subscribe('trade');

    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const {id} = this.props.match.params;
        this._fetchTrade(id);
        this.channel.bind('update', data => {
            console.log('data')
            console.log(data)
            if(data.trade_id == id)
                this._fetchTrade(id);
        }, this);
    }

    _fetchTrade(id) {
        ApiCaller.get(`/trade/${id}?token=${AuthService.getToken()}`)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        loaded: true,
                        trade: res.data,
                    });

                    if(this.state.trade.feedback != null) {
                        this.setState({
                            feedback: this.state.trade.feedback
                        })
                    } else {
                        this.setState({
                            feedback: {...this.state.feedback, trade_id: res.data.id}
                        })
                    }
                } else {
                    this.setState({
                        loaded: true,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err => {
                console.error('tradePage', err)
            });
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _onChangeVote(vote) {
        if (this.state.trade.feedback != null)
            return;
        this.setState({
            feedback: {...this.state.feedback, rating: vote}
        });
        $('.vote').removeClass('active');
    }

    _onSendFeedback() {
        if (this.state.trade.feedback != null)
            return alert('Không thể chỉnh sửa đáng giá');

        ApiCaller.post(`/feedback?token=${AuthService.getToken()}`, this.state.feedback)
            .then(res=>{
                if (res.code == 200) {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'success',
                            msg: res.msg
                        }
                    })
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err=>{

            })
    }

    _onUpdateStatus(e, status) {
        e.preventDefault();
        const {trade} = this.state;

        if (status == 'delivered') {
            this.setState({
                confirm_paid: {
                    status: true,
                    text: 'Đang xác nhận'
                }
            });
        }

        ApiCaller.put(`/trade/${trade.id}/update-state?token=${AuthService.getToken()}`, {status})
            .then(res => {
                if (status == 'delivered') {
                    this.setState({
                        confirm_paid: {
                            status: false,
                            text: 'Xác nhận thanh toán'
                        }
                    });
                    $('#modal_verify').modal('hide');
                    $('.modal-backdrop').remove();
                }
                if (res.code == 200) {
                    this.setState({
                        trade: res.data
                    })
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err => {

            })
    }

    _onChangeFileReceipt(e) {
        this.setState({file_receipt: e.target.files[0]})
    }

    _onUploadReceipt(e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append('image', this.state.file_receipt);
        const {id} = this.state.trade;

        this.setState({
            upload_receipt: {
                status: true,
                text: 'Đang gửi biên lai'
            }
        });
        ApiCaller.post(`/trade/${id}/upload-receipt?token=${AuthService.getToken()}`, formData, (progressEvent)=>{
            console.log('upload-receipt');
            console.log(progressEvent);
        })
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        trade: res.data
                    });
                    NotificationManager.success('Gửi biên lai thành công')
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        },
                        upload_receipt: {
                            status: false,
                            text: 'Gửi biên lai'
                        }
                    });
                }
            })
            .catch(err => {
                NotificationManager.error('Có lỗi xảy ra')
            });

    }

    _genTitle(trade) {
        let title = '';
        if (trade.role == 'seller')
            title = `bán ${Label.coin(trade.type_money)} qua ${trade.bank_name} cho ${trade.buyer_username}`;
        else
            title = `mua ${Label.coin(trade.type_money)} qua ${trade.bank_name} từ ${trade.seller_username}`;
        return title;
    }

    _countdownCompleted() {
        let {trade} = this.state;
        if (trade.time_up != 1) {
            ApiCaller.put(`/trade/${trade.id}/time-up?token=${AuthService.getToken()}`)
                .then(res => {
                    if (res.code == 200) {
                        this.setState({
                            trade: res.data
                        })
                    } else {
                        this.setState({
                            alert: {
                                is_show: true,
                                type: 'error',
                                msg: res.msg
                            }
                        })
                    }

                })
                .catch(err => {

                })
        }

    }

    _cbCountDown(t) {
        if (t.minutes <= 2 && this.state.is_show_not_received == false) {
            this.setState({
                is_show_not_received: true
            })
        }
    }

    _infoPayment(trade) {
        if (Object.keys(trade).length == 0)
            return null;

        return (
            <div className="col-sm-12 col-md-8 offset-md-2 mt-3">
                <div className="text-center panel-info-trade">
                    Thông tin thanh toán
                </div>
                <table className="table table-bordered">
                    <tbody className="text-left">
                    <tr>
                        <td>Phương thức thanh toán</td>
                        <td>{trade.payment_method_name}</td>
                    </tr>
                    <tr>
                        <td>Số tiền phải thanh toán</td>
                        <td><span className="txt-red">{Helper.formatMoney(trade.buyer_sending_vnd_amount)} VND</span></td>
                    </tr>
                    <tr>
                        <td>Tên ngân hàng</td>
                        <td>{trade.bank_name}</td>
                    </tr>
                    <tr>
                        <td>Tên chủ tài khoản</td>
                        <td>{trade.bank_account_name}</td>
                    </tr>
                    <tr>
                        <td>Số tài khoản</td>
                        <td>{trade.bank_number}</td>
                    </tr>
                    <tr>
                        <td>Nội dung chuyển khoản</td>
                        <td>{trade.payment_memo}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }

    _formTradeBuy(trade) {

        if (Object.keys(trade).length == 0)
            return null;

        if ((trade.status == 1 || trade.status == 5) && trade.action_buyer == 0)
            return this._formTradeBuyStep1(trade);

        if ((trade.status == 1 || trade.status == 5) && (trade.action_buyer == 1 || trade.action_buyer == 3))
            return this._formTradeBuyStep2(trade);

        if (trade.status == 3 || trade.status == 4)
            return this._formTradeCancel(trade);

        return this._formFeedbackBuy(trade);
    }

    _formTradeBuyStep1(trade) {
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(this._genTitle(trade))}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_lock.png"
                                         style={{display: 'inline-block', marginTop: '-30px', marginRight: '5px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="active"><a href="#">Thanh toán cho người bán</a></li>
                                        <li><a href="#">Nhận {Label.coin(trade.type_money, false, true)}</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">Cập nhật thông tin thanh toán</p>
                                <p>Bạn đang mua {trade.buyer_receiving_coin_amount} từ <span
                                    className="txt-red">{trade.seller_username}</span>. {Label.coin(trade.type_money, false, true)} của người bán đã được khóa lại để đảm bảo
                                    cho giao dịch này. Vui lòng chờ người bán xác nhận đã nhận được thanh toán của bạn.
                                </p>
                                <div className="countdown">
                                    <p className="text">Thời gian còn lại để thanh toán</p>
                                    <Countdown time={trade.payment_time} completed={this._countdownCompleted}/>
                                </div>
                                <div className="alert alert-danger">
                                    Hết thời gian thanh toán giao dịch sẽ bị hủy và chúng tôi sẽ gửi
                                    trả {Label.coin(trade.type_money, false, true)} lại cho người bán
                                </div>
                                <div className="text-center">
                                    <div className="button-confirm">
                                        <a href="#" data-toggle="modal" data-target="#modal_verify" className="success">Tôi
                                            đã thanh toán</a>
                                        <a href className="cancel ml-1" onClick={(e) => {
                                            this._onUpdateStatus(e, 'cancel')
                                        }}>Huỷ bỏ giao dịch</a>
                                    </div>
                                </div>
                            </div>
                            { this._infoPayment(trade) }
                        </div>
                    </div>
                </div>

                {/*modal*/}
                <div className="modal fade vertically-modal" id="modal_verify" tabIndex={-1} role="dialog"
                     aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Thông báo</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="text-center">
                                        <img
                                            src={trade.receipt != null ? trade.receipt : '/resources/assets/frontend/img/ic_image.png'}
                                            style={{maxWidth: '100%', maxHeight: '200px'}}/>
                                        <input accept="image/x-png,image/gif,image/jpeg"
                                               onChange={this._onChangeFileReceipt} type="file" style={{
                                            height: 100,
                                            width: 150,
                                            position: 'absolute',
                                            left: 170,
                                            opacity: 0
                                        }}/>
                                    </div>
                                    <div className="alert alert-danger text-center mt-3">
                                        Khuyến cáo nên gửi biên lại thanh toán trước khi xác nhận đã thanh toán để đảm
                                        bảo an toàn. Nếu có tranh chấp, bạn không chứng minh được bằng chứng thanh toán,
                                        sàn sẽ không hỗ trợ bạn giải quyết.
                                    </div>
                                    <div className="form-group text-center mt-3">
                                        <div style={{display: 'inline-block'}}>
                                            {
                                                trade.receipt != null
                                                    ? <button className="btn no-radius bg-green"
                                                              onClick={(e) => e.preventDefault()}><img
                                                        src="/resources/assets/frontend/img/ic_checked_white.png"
                                                        style={{
                                                            display: 'inline-block',
                                                            marginTop: '-2px',
                                                            marginRight: 7,
                                                            width: 20
                                                        }} alt/>Đã gửi biên lai</button>
                                                    : <button
                                                        className={this.state.upload_receipt.status == true ? 'btn no-radius bg-blue disabled' : 'btn no-radius bg-blue'}
                                                        onClick={this._onUploadReceipt}>{this.state.upload_receipt.text}</button>
                                            }
                                            <button
                                                className={this.state.confirm_paid.status == true ? 'btn no-radius bg-blue ml-1 disabled' : 'btn no-radius bg-blue ml-1'}
                                                onClick={(e) => {
                                                    this._onUpdateStatus(e, 'delivered')
                                                }}>{this.state.confirm_paid.text}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {/*end modal*/}

            </div>
        )
    }

    _formTradeBuyStep2(trade) {
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(this._genTitle(trade))}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_lock.png"
                                         style={{display: 'inline-block', marginTop: '-30px', marginRight: '5px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="done"><a href="#">Thanh toán cho người bán</a></li>
                                        <li className="active"><a
                                            href="#">Nhận {Label.coin(trade.type_money, false, true)}</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">Cập nhật thông tin thanh toán</p>
                                <p>Bạn đang mua {trade.buyer_receiving_coin_amount} từ <span
                                    className="txt-red">{trade.seller_username}</span>. {Label.coin(trade.type_money, false, true)} của người bán đã được khóa lại để đảm bảo
                                    cho giao dịch này. Vui lòng chờ người bán xác nhận đã nhận được thanh toán của bạn.
                                </p>
                                <div className="text-center">
                                    <div className="button-confirm">
                                        <a href className={trade.action_buyer == 3 ? 'success disabled' : 'success'}
                                           onClick={(e) => {
                                               this._onUpdateStatus(e, 'dispute')
                                           }}>Tranh chấp</a>
                                        <a href className="cancel ml-1" onClick={(e) => {
                                            this._onUpdateStatus(e, 'cancel')
                                        }}>Huỷ bỏ giao dịch</a>
                                    </div>
                                </div>
                            </div>
                            { this._infoPayment(trade) }
                        </div>
                    </div>
                </div>
            </div>

        )
    }

    _formFeedbackBuy(trade) {
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(this._genTitle(trade))}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_checked.png"
                                         style={{display: 'inline-block', marginTop: '-30px', marginRight: '5px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="done"><a href="#">Thanh toán cho người bán</a></li>
                                        <li className="done"><a
                                            href="#">Nhận {Label.coin(trade.type_money, false, true)}</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">GIAO DỊCH HOÀN TẤT</p>
                                <p>Bạn đã mua <span className="txt-red">{trade.buyer_receiving_coin_amount}</span>{Label.coin(trade.type_money, true, true)} từ {trade.seller_username}.</p>
                                <p>{Label.coin(trade.type_money, false, true)} đã được gửi vào địa chỉ
                                    này: {trade.to_address}</p>
                                { this._feedback(trade) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

    _feedback(trade) {
        const {feedback} = this.state.trade;
        const vote = feedback != null ? feedback.rating : this.state.feedback.rating;

        for (let i = 1; i <= vote; i++) {
            $('.vote_' + i).addClass('active');
        }
        return (
            <form action>
                <div className="text-center">
                    <div className="vote-title">Đánh
                        giá {trade.role == 'buyer' ? trade.seller_username : trade.buyer_username}</div>
                    <div className="list-vote">
                        <span className="vote active vote_1" onMouseOver={() => this._onChangeVote(1)}/>
                        <span className="vote vote_2" onMouseOver={() => this._onChangeVote(2)}/>
                        <span className="vote vote_3" onMouseOver={() => this._onChangeVote(3)}/>
                        <span className="vote vote_4" onMouseOver={() => this._onChangeVote(4)}/>
                        <span className="vote vote_5" onMouseOver={() => this._onChangeVote(5)}/>
                    </div>
                </div>
                <div className="input-group mt-3">
                    <input type="text" className="form-control bg-gray-dark" placeholder="Gửi phản hồi"
                           value={this.state.feedback.content}
                           onChange={(e) => {
                               if (this.state.trade.feedback != null) return;
                               this.setState({
                                   feedback: {...this.state.feedback, content: e.target.value}
                               })
                           }}/>
                    <span className="input-group-btn">
                                            <button className="btn btn-default bg-blue" onClick={this._onSendFeedback}
                                                    type="button">Gửi</button>
                                        </span>
                </div>
            </form>
        )
    }

    _formTradeSell(trade) {

        if (Object.keys(trade).length == 0)
            return null;

        if (trade.status == 1 || trade.status == 5)
            return this._formConfirmTradeSell(trade);

        if (trade.status == 3 || trade.status == 4)
            return this._formTradeCancel(trade);

        return this._formFeedbackSell(trade);
    }

    _formConfirmTradeSell(trade) {
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(this._genTitle(trade))}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_lock.png"
                                         style={{display: 'inline-block', marginTop: '-30px', marginRight: '5px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="done"><a href="#">Cập nhật thông tin thanh toán</a></li>
                                        <li className="active"><a href="#">Giải phóng {Label.coin(trade.type_money, false, true)} của bạn</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">Chờ người mua thanh toán</p>
                                <p>Bạn đã bán <span className="txt-red">{trade.seller_sending_coin_amount}</span>{Label.coin(trade.type_money, true, true)} cho {trade.buyer_username}. {Label.coin(trade.type_money, true, true)} của bạn đã
                                    được đóng băng cho giao dịch này
                                    Sau khi nhận được thanh toán từ người mua. Vui lòng xác nhận dưới đây
                                    để {Label.coin(trade.type_money, true, true)} được giải phóng cho
                                    người mua.</p>
                                <div className="countdown">
                                    <p className="text">Thời gian còn lại để thanh toán</p>
                                    <Countdown time={trade.payment_time} completed={this._countdownCompleted}
                                               cb={(t) => this._cbCountDown(t)}/>
                                </div>
                                <div className="alert alert-danger">
                                    Hết thời gian thanh toán giao dịch sẽ bị hủy và chúng tôi sẽ gửi
                                    trả {Label.coin(trade.type_money, false, true)} lại cho người bán
                                </div>
                                <div className="text-center">
                                    <div className="button-confirm">
                                        <a href className="success" onClick={(e) => {
                                            this._onUpdateStatus(e, 'received')
                                        }}>Tôi đã nhận được tiền</a>
                                        <a href="#"
                                           className={this.state.is_show_not_received == false || trade.action_buyer == 2 ? 'cancel disabled ml-1' : 'cancel ml-1'}
                                           onClick={(e) => {
                                               this._onUpdateStatus(e, 'not-received')
                                           }}>Tôi
                                            chưa nhận được
                                            tiền</a>
                                    </div>
                                </div>
                            </div>
                            { this._infoPayment(trade) }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    _formFeedbackSell(trade) {
        return (
            <div className="row mt-5">
                <div className="pn-active">
                    <div className="panel-header-active text-center">
                        {ReactHtmlParser(this._genTitle(trade))}
                    </div>
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className=" text-center">
                                    <img src="/resources/assets/frontend/img/ic_checked.png"
                                         style={{display: 'inline-block', marginTop: '-30px', marginRight: '5px'}} alt/>
                                    <ul className="nav nav-tabs step-anchor">
                                        <li className="done"><a href="#">Cập nhật thông tin thanh toán</a></li>
                                        <li className="done"><a href="#">Giải phóng {Label.coin(trade.type_money, false, true)} của bạn</a></li>
                                    </ul>
                                </div>
                                <p className="text-uppercase txt-blue mt-3">Giao dịch thành công</p>
                                <p>Bạn đã bán <span className="txt-red">{trade.seller_sending_coin_amount}</span>{Label.coin(trade.type_money, true, true)} cho {trade.buyer_username}.</p>
                                { this._feedback(trade) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

    _formTradeCancel(trade) {
        return (
            <div className="row mt-5">
                <div className="col-12 no-pd">
                    <div className="alert alert-danger">Giao
                        dịch #{trade.id}: {trade.role == 'buyer' ? `${trade.buyer_receiving_coin_amount}${Label.coin(trade.type_money, true, true)} với ${trade.seller_username}` : `${trade.seller_sending_coin_amount}${Label.coin(trade.type_money, true, true)} với ${trade.buyer_username}`} đã bị huỷ
                    </div>
                </div>
            </div>
        )
    }

    _showNotiTrade(trade) {
        if (trade.status == 2)
            return null;

        let msg = '';
        if (trade.status == 5)
            msg =
                <div className="alert alert-warning">Giao dịch đang trong trạng thái tranh chấp, vui lòng chờ support hỗ
                    trợ giải truyết</div>
        else if (trade.action_seller == 2 && trade.role == 'buyer')
            msg =
                <div className="alert alert-warning">Người bán báo chưa nhận được tiền, vui lòng kiểm tra lại, nếu không
                    xác nhận thì giao dịch sẽ bị huỷ khi hết thời gian giao dịch</div>
        else if ((trade.action_buyer == 1 || trade.action_buyer == 3) && trade.role == 'seller')
            msg = <div className="alert alert-warning">
                Người mua báo đã chuyển tiền, vui lòng xác nhận, nếu hết thời gian giao dịch thì giao dịch này sẽ được
                đưa vào trạng thái tranh chấp.
                <br/>
            </div>;

        if (msg == '')
            return null;

        return (
            <div className="row mt-5">
                <div className="col-12 no-pd">
                    {msg}
                </div>
            </div>
        )
    }


    render() {
        const {loaded, redirect, trade} = this.state;
        const {wallet} = this.props.profile;

        if (loaded == false) {
            return (
                <div>
                    <HeaderOther/>
                    <div className="container text-center mt-5 mb-5">
                        <div className="row">
                            <div className="col-12 justify-content-center text-center">
                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            )
        }

        window.document.title = this._genTitle(trade);

        return (
            <div>
                <NotificationContainer/>
                <HeaderOther/>
                { redirect.is_redirect === true ? <Redirect to={redirect.link}/> : '' }
                { this._showMessage() }

                <div className="container-fluid main">
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        { this._showNotiTrade(trade) }

                        {trade.role == 'seller' ? this._formTradeSell(trade) : this._formTradeBuy(trade) }
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

export default connect(mapStateToProps)(TradePage);