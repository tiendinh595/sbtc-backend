/**
 * Created by dinh on 12/14/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import Wallet from '../components/User/Wallet'
import Spinner from 'react-spinkit'
import Simplert from 'react-simplert'
import {connect} from 'react-redux'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import ReactHtmlParser from 'react-html-parser'
import $ from 'jquery'
import * as Helper from '../utils/Helper'
import * as Label from '../components/Offer/Lable'
import BankSelectBox from '../components/Offer/BankSelectBox'
import {Redirect} from 'react-router-dom'
import '../style/mail.css'

class MailPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            is_loading: true,
            mails: {},
            current_tab: 1,
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },
            action_send: {
                sending: false,
                text: 'Gửi'
            }
        };

        this._showMessage = this._showMessage.bind(this);
        this._sendMail = this._sendMail.bind(this);
    }


    componentDidMount() {
        window.scrollTo(0, 0);
        document.title = 'Hộp thư';

        $('body').undelegate('.title-mail', 'click');
        $('body').delegate('.title-mail', 'click', function (e) {
            e.preventDefault();
            $(this).parents('tr').removeClass('active');
            let id = $(this).parents('tr').attr('id');
            ApiCaller.get(`/mail/mark-read/${id}`);
            $('body').find('.content-mail').fadeOut();
            $(this).parents('tr').next().find('.content-mail').fadeToggle();
        });

        $('body').undelegate('#btn-action', 'click');
        $('body').delegate('#btn-action', 'click', function (e) {
            e.preventDefault();
            let list_id = $('[name="mail_selected[]"]:checked').map(function () {
                return $(this).data('id');
            }).get();
            ApiCaller.post('/mail/delete', {list_id})
                .then(res => {
                    list_id.map(id => {
                        $(`.item-m-${id}`).remove();
                    })
                })
                .catch(err => {

                })
        });


        $('body').undelegate('#check_all', 'click');
        $('body').delegate('#check_all', 'click', function (e) {
            $('[name="mail_selected[]"]').not(this).prop('checked', this.checked);
        });

        this._getMails('/mail/1')
    }

    _sendMail() {
        const category = this.refs.category.value;
        const content = this.refs.content.value;

        this.setState({
            action_send: {
                sending: true,
                text: 'Đang gửi'
            }
        });

        ApiCaller.post('/mail/send', {category, content})
            .then(res => {
                this.setState({
                    action_send: {
                        sending: false,
                        text: 'Gửi'
                    }
                });

                if(res.code == 200) {

                    this.refs.content.value = '';

                    this.setState({
                        is_loading: false,
                        alert: {
                            is_show: true,
                            type: 'success',
                            msg: res.msg
                        }
                    })
                } else {
                    this.setState({
                        is_loading: false,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }

            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi trong quá trình gửi phản hồi'
                    }
                })
            })
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _getMails(url) {
        this.setState({
            is_loading: true,
        });

        ApiCaller.get(url)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        is_loading: false,
                        mails: res.data
                    })
                } else {
                    this.setState({
                        is_loading: false,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loaded: true,
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi xảy ra trong quá trình tải dữ liệu'
                    }
                })
            })
    }

    _renderList() {
        const {mails, is_loading} = this.state;

        if (is_loading)
            return (
                <tbody>
                <tr>
                    <td width="100%" colSpan="4">
                        <div className="container text-center mt-5 mb-5">
                            <div className="row">
                                <div className="col-12 justify-content-center text-center">
                                    <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                </tbody>
            )

        if (mails.data.length == 0)
            return (
                <tbody>
                <tr>
                    <td colSpan="4" className="alert alert-success">
                        Hộp thư rỗng
                    </td>
                </tr>
                </tbody>
            )

        return (
            <tbody>
            <tr className="header-control">
                <td colSpan={3} className="text-right">Thư hiện tại: {mails.data.length}</td>
                <td width="10px"><input type="checkbox" id="check_all"/></td>
            </tr>
            {
                mails.data.map(mail => {
                    return [
                        <tr className={mail.status == 0 && mail.type == 1 ? `item-mail active item-m-${mail.id}` : `item-mail item-m-${mail.id}`}
                            id={mail.id}>
                            <td className="ic-mail"><i
                                className={mail.status == 0 && mail.type == 1 ? "fa fa-envelope" : "fa fa-envelope-o"}
                                aria-hidden="true"/>
                            </td>
                            <td className="title-mail">{mail.title}</td>
                            <td className="time-mail">{mail.date} <br />{mail.time}</td>
                            <td className="control-mail"><input type="checkbox" name={`mail_selected[]`}
                                                                data-id={mail.id}/></td>
                        </tr>,
                        <tr className={`item-m-${mail.id}`}>
                            <td colSpan={4} className="content-mail">
                                <div>
                                    {ReactHtmlParser(mail.content)}
                                </div>
                            </td>
                        </tr>
                    ]
                })
            }

            <tr className="footer-control">
                <td colSpan={2} className="no-border">
                    <div className="btn-group pagination no-mg" role="group"
                         aria-label="Basic example">
                        <a onClick={this._getMails.bind(this, mails.prev_page_url)}
                           type="button"
                           className={mails.prev_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                           style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                            TRƯỚC</a>
                        <a onClick={this._getMails.bind(this, mails.next_page_url)}
                           type="button"
                           className={mails.next_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                           style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                            SAU</a>
                    </div>
                </td>
                <td colSpan={2} className="text-right">
                    Thư đã chọn:
                    <select name="action">
                        <option value="delete">Xoá thư</option>
                    </select>
                    <button id="btn-action">GO</button>
                </td>
            </tr>
            </tbody>
        )

    }

    render() {

        return (
            <div>

                {this._showMessage()}

                <HeaderOther/>

                <div className="container mt-5">
                    <div className="row">
                        <p className="title-menu"><i className="fa fa-envelope-o"
                                                     aria-hidden="true"/><span>hộp thư</span></p>
                        <div className="col-12 no-mg no-pd">
                            <ul className="nav nav-tabs" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" href="#"
                                       onClick={this._getMails.bind(this, '/mail/1')} role="tab" data-toggle="tab">Hộp
                                        thư đến</a>
                                </li>
                                <li className="nav-item ml-1">
                                    <a className="nav-link" role="tab" href="#"
                                       onClick={this._getMails.bind(this, '/mail/2')} data-toggle="tab">Thư đã gửi</a>
                                </li>
                            </ul>
                            {/* Tab panes */}
                            <div className="tab-content">
                                <div role="tabpanel" className="tab-pane fade in active show" id="tab_receive">
                                    <table className="table-mail">

                                        { this._renderList() }

                                    </table>
                                </div>
                            </div>

                            <hr />
                        </div>
                    </div>
                    <div className="row mt-4 mb-5">
                        <p className="title-menu"><i className="fa fa-envelope-o"
                                                     aria-hidden="true"/><span>gửi hỗ trợ</span></p> <br />
                        <div className="col-12 no-mg no-pd">
                            <form action className="col-sm-12 col-md-5 no-mg no-pd">
                                <select ref="category" className="form-control">
                                    <option>Chọn mục bạn cần hỗ trợ</option>
                                    <option value="0">Nạp / Rút </option>
                                    <option value="1">Giao dịch</option>
                                    <option value="2">Tài khoản</option>
                                    <option value="3">Vấn đề khác</option>
                                </select>
                                <textarea ref="content" className="form-control mt-2" rows={10}
                                          placeholder="Nội dung kèm theo"
                                          defaultValue={""}/>
                                <input type="button" defaultValue={this.state.action_send.text}
                                       className={this.state.action_send.sending ? 'float-right mt-2 bg-primary no-border disabled' : 'float-right mt-2 bg-primary no-border'} onClick={this._sendMail}/>
                            </form>
                        </div>
                    </div>
                </div>

                <Footer/>
            </div>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer,
    }
};

export default connect(mapStateToProps, null)(MailPage);