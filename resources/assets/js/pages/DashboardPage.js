/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import OfferItemSell2 from '../components/Offer/OfferItemSell2'
import OfferItemBuy2 from '../components/Offer/OfferItemBuy2'
import {connect} from 'react-redux'
import Spinner from 'react-spinkit'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import * as Helper from '../utils/Helper'
import Wallet from '../components/User/Wallet'
import $ from 'jquery'
import ReactHtmlParser from 'react-html-parser'
import BankSelectBox from '../components/Offer/BankSelectBox'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import {Redirect, Link} from 'react-router-dom'
import * as Button from '../components/Offer/Button'

class ItemRef extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            referral: props.referral
        };

        this._onReward = this._onReward.bind(this);
    }

    _onReward() {
        const {id} = this.state.referral;

        ApiCaller.put(`/referrals/${id}`)
            .then(res=>{
               if(res.code == 200) {
                   this.setState({
                       referral: res.data
                   });
                   NotificationManager.success(res.msg);
               } else {
                   NotificationManager.error(res.msg);
               }
            })
            .catch(err=>{
                NotificationManager.error('Có lỗi xảy ra trong quá trình xử lý');
            })
    }

    render() {
        const {referral} = this.state;
        let btn, status = null;
        switch (parseInt(referral.status)) {
            case 0:
                btn = <button className="btn btn-sm btn-primary disabled">Nhận</button>;
                status = 'chưa đạt';
                break;
            case 1:
                btn = <button className="btn btn-sm btn-primary" onClick={this._onReward}>Nhận</button>;
                status = 'chưa nhận';
                break;
            case 2:
                btn = <button className="btn btn-sm btn-primary disabled">Đã nhận</button>;
                status = 'đã nhận';
                break;
        }
        return (
            <tr>
                <td className="text-center"><Link to={`/profile/${referral.username}`}>{referral.username}</Link></td>
                <td className="text-center">{referral.created_at}</td>
                <td className="text-center">{status}</td>
                <td className="text-center">{btn}</td>
            </tr>
        )
    }
}

class ItemTrade extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const className = this.props.username == this.props.trade.buyer_username ? 'green' : 'blue';
        const label = this.props.username == this.props.trade.buyer_username ? 'Mua' : 'Bán';
        const icon = this.props.trade.type_money == 1 ? '/resources/assets/frontend/img/ic_btc_2.png' : '/resources/assets/frontend/img/ic_eth_1x.png';
        const amount_vnd = this.props.username == this.props.trade.buyer_username ? this.props.trade.buyer_sending_vnd_amount : this.props.trade.seller_receiving_vnd_amount;
        const amount_coin = this.props.username == this.props.trade.buyer_username ? this.props.trade.buyer_receiving_coin_amount : this.props.trade.seller_sending_coin_amount;
        const currency = this.props.trade.type_money == 1 ? 'BTC' : 'ETH';
        const partner = this.props.username == this.props.trade.buyer_username ? this.props.trade.seller_username : this.props.trade.buyer_username;

        return (
            <tr>
                <td>
                    <div className="row">
                        <div className="col-sm-2 text-center">
                            <p><Link to={`/trade/${this.props.trade.id}`} className="txt-border"
                                     style={{width: '70%'}}>{this.props.trade.id}</Link></p>
                            <p>{this.props.trade.status_name}</p>
                        </div>
                        <div className="col-sm-1 text-center">
                            <a href className={`bt small bt-${className} align-middle`}>{label} <img src={icon}
                                                                                                     style={{marginTop: '-2px'}}
                                                                                                     alt/></a>
                        </div>
                        <div className="col-sm-9">
                            <p><b className={`highlight-${className}`}> {amount_coin} {currency}</b> giao dịch qua <b>{this.props.trade.bank_name}</b> với <b>{partner}</b></p>
                            <p>Giá trị giao dịch: <b className="txt-red">{Helper.formatMoney(amount_vnd)} VNĐ</b></p>
                        </div>
                    </div>

                </td>
            </tr>
        )
    }
}

class ItemOffer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offer: props.offer,
        };

        this._changeStatus = this._changeStatus.bind(this)
    }

    _changeStatus(e, status) {
        e.preventDefault();
        const {offer} = this.state;

        ApiCaller.put(`/offer/${offer.id}`, {...offer, status})
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        offer: res.data
                    })
                    if(res.data.status == 5)
                        NotificationManager.success('Xoá bài đăng thành công');

                } else {
                    NotificationManager.error(res.msg);
                }
            })
            .catch(err => {
                NotificationManager.error('Có lỗi xảy ra trong quá trình xử lý');
            })
    }


    render() {
        const {offer} = this.state;
        if(offer.status == 5)
            return null;
        let btn_stop, btn_delete = null;
        const className = offer.type_offer == 1 ? 'highlight-green' : 'highlight-blue';
        switch (parseInt(offer.status)) {
            case 1:
                btn_stop = <a href className="btn btn-danger text-uppercase"
                              onClick={(e) => this._changeStatus(e, 4)}>Tạm dừng</a>;
                btn_delete = <a href className="btn btn-submit text-uppercase ml-1"
                                onClick={(e) => this._changeStatus(e, 5)}>Xoá</a>;
                break;

            case 2:
                btn_stop = <a href className="btn btn-danger text-uppercase disabled"
                              onClick={(e) => e.preventDefault()}>Tạm dừng</a>;
                btn_delete = <a href className="btn btn-submit text-uppercase ml-1"
                                onClick={(e) => this._changeStatus(e, 5)}>Xoá</a>;
                break;

            case 3:
                btn_stop = <a href className="btn btn-danger text-uppercase disabled"
                              onClick={(e) => e.preventDefault()}>Tạm dừng</a>;
                btn_delete = <a href className="btn btn-submit text-uppercase ml-1"
                                onClick={(e) => this._changeStatus(e, 5)}>Xoá</a>;
                break;

            case 4:
                btn_stop = <a href className="btn btn-success text-uppercase"
                              onClick={(e) => this._changeStatus(e, 1)}>Kích hoạt</a>;
                btn_delete = <a href className="btn btn-submit text-uppercase ml-1"
                                onClick={(e) => this._changeStatus(e, 5)}>Xoá</a>;
                break;

            case 5:
                btn_stop = <a href className="btn btn-danger text-uppercase disabled"
                              onClick={(e) => e.preventDefault()}>Tạm dừng</a>;
                btn_delete = <a href className="btn btn-submit text-uppercase ml-1"
                                onClick={(e) => e.preventDefault()}>Xoá</a>;
                break;
        }
        return (

            <tr>
                <td className="align-middle">
                    <div className="row">
                        <div className="col-sm-12">
                            <p><b className={className}>{Helper.formatMoney(offer.price_per_coin * offer.current_amount)} VND</b> chuyển khoản
                                qua <b>{offer.bank_name}</b></p>
                            <p>Giá bạn thực nhận <b
                                className={className}>{Helper.formatMoney(offer.price_per_coin)}
                                VND</b>/<b className="highlight-orange">{ offer.type_money == 1 ? 'BTC' : 'ETH' }</b>
                            </p>
                            <p>Tối đa: <b>{offer.max_amount} { offer.type_money == 1 ? 'BTC' : 'ETH' }</b></p>
                            <p>Trạng thái: <b>{offer.status_name}</b>
                                <span className="ml-2">
                                    {btn_stop}
                                    <Link to={`/offer/${offer.id}/edit`} className={offer.status == 1 || offer.status == 4 ? "btn btn-submit text-uppercase ml-1" : "btn btn-submit text-uppercase ml-1 disabled"}>Chỉnh sửa</Link>
                                    {btn_delete}
                                  </span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
        )
    }
}

class DashboardPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            is_loading: false,
            list: {}
        };

        this._getTitle = this._getTitle.bind(this);
        this._showMain = this._showMain.bind(this);
        this._renderOffers = this._renderOffers.bind(this);
        this._renderReferrals = this._renderReferrals.bind(this);
        this._renderTrade = this._renderTrade.bind(this);
        this._fetchData = this._fetchData.bind(this);
        this._preFetchData = this._preFetchData.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const {page, status} = this.props.match.params;
        document.title = this._getTitle();
        this._preFetchData(this.props)
    }

    _getTitle() {
        const {page, status} = this.props.match.params;
        let title = '';
        if (page == 'trade') {
            title = status == 'active' ? 'Giao dịch đang mở' : 'Giao dịch đã đóng'
        } else if (page == 'offers') {
            title = 'Bài đăng của bạn'
        } else if (page == 'referrals') {
            title = 'Danh sách giới thiệu'
        }
        return title;
    }

    componentWillReceiveProps(newProps) {
        const {page, status} = this.props.match.params;
        if (!this.state.is_loading && (page != newProps.match.params.page || status != newProps.match.params.status)) {
            this._preFetchData(newProps)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        document.title = this._getTitle();

        const {page, status} = this.props.match.params;

    }

    _preFetchData(props) {
        const {page, status} = props.match.params;
        if (page == 'trade') {
            this._fetchData(`/trade/my-list/${status}`)
        } else if (page == 'offers') {
            let url = status == 'sell' ? '/offer/my/1' : '/offer/my/2';
            this._fetchData(url)
        } else if (page == 'referrals') {
            this._fetchData('/referrals')
        }
    }

    _fetchData(url) {
        this.setState({
            is_loading: true,
            list: {}
        });
        ApiCaller.get(url)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        is_loading: false,
                        list: res.data
                    });
                } else {
                    this.setState({
                        is_loading: false
                    });
                    NotificationManager.error(res.msg);
                }
            })
            .catch(err => {
                this.setState({
                    is_loading: false
                });
                NotificationManager.error('Có lỗi xảy ra trong quá trình tải dữ liệu');
            })
    }

    _renderReferrals() {
        const {is_loading, list} = this.state;

        return (
            <div className="row">
                <div className="col-12" style={{background: '#fff', padding: '20px 40px 0px 40px'}}>
                    <h5 className="mt-1">DANH SÁCH GIỚI THIỆU</h5>
                    {
                        is_loading
                            ? <div className="container text-center mt-5 mb-5">
                                <div className="row">
                                    <div className="col-12 justify-content-center text-center">
                                        <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                                    </div>
                                </div>
                            </div>
                            : (
                                list.data == undefined || list.data.length == 0
                                    ? <div className="alert alert-info">chưa có dữ liệu</div>
                                    : <table className="table table-bordered no-border">
                                        <thead>
                                        <tr>
                                            <td className="text-center bg-gray-dark">Tài khoản giới thiệu</td>
                                            <td className="text-center bg-gray-dark">Thời gian tạo</td>
                                            <td className="text-center bg-gray-dark">Trạng thái</td>
                                            <td className="text-center bg-gray-dark"></td>
                                        </tr>
                                        </thead>
                                        <tbody style={{boxShadow:'none'}}>
                                        {
                                            list.data.map((ref, index)=><ItemRef referral={ref} key={index}></ItemRef>)
                                        }
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colSpan={2} className="no-border">
                                                <div className="btn-group pagination" role="group"
                                                     aria-label="Basic example">
                                                    <a onClick={this._fetchData.bind(this, list.prev_page_url)}
                                                       type="button"
                                                       className={list.prev_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        TRƯỚC</a>
                                                    <a onClick={this._fetchData.bind(this, list.next_page_url)}
                                                       type="button"
                                                       className={list.next_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        SAU</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                            )
                    }
                </div>
            </div>

        )
    }

    _renderOffers() {
        const {is_loading, list} = this.state;
        const {status} = this.props.match.params;

        return (
            <div>
                <table className="table">
                    <tr>
                        <td colSpan="2" className="text-uppercase">
                            <Link to="/dashboard/offers/buy" className={status == 'buy' ? "txt-gray" : ''}>Bài đăng mua
                                của bạn</Link> | <Link to="/dashboard/offers/sell"
                                                       className={status == 'sell' ? "txt-gray" : ''}>Bài đăng bán của
                            bạn</Link>
                        </td>
                    </tr>
                    {
                        is_loading
                            ? <tbody>
                            <tr>
                                <td>
                                    <div className="container text-center mt-5 mb-5">
                                        <div className="row">
                                            <div className="col-12 justify-content-center text-center">
                                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            : (
                                list.data == undefined || list.data.length == 0
                                    ? <tbody>
                                    <tr>
                                        <td>
                                            <div className="alert alert-info">chưa có dữ liệu</div>
                                        </td>
                                    </tr>
                                    </tbody>
                                    : [
                                        <tbody key="1">
                                        {
                                            list.data.map((offer, index) => <ItemOffer key={index} offer={offer}/>)
                                        }
                                        </tbody>,
                                        <tfoot key={2}>
                                        <tr>
                                            <td colSpan={2}>
                                                <div className="btn-group pagination" role="group"
                                                     aria-label="Basic example">
                                                    <a onClick={this._fetchData.bind(this, list.prev_page_url)}
                                                       type="button"
                                                       className={list.prev_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        TRƯỚC</a>
                                                    <a onClick={this._fetchData.bind(this, list.next_page_url)}
                                                       type="button"
                                                       className={list.next_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        SAU</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    ]

                            )
                    }

                </table>

            </div>
        )
    }

    _renderTrade(status) {
        const {is_loading, list} = this.state;
        const {username} = this.props.profile;

        return (
            <div>
                {
                    is_loading
                        ? <div className="container text-center mt-5 mb-5">
                            <div className="row">
                                <div className="col-12 justify-content-center text-center">
                                    <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                                </div>
                            </div>
                        </div>
                        : (
                            list.data == undefined || list.data.length == 0
                                ? <div className="alert alert-info">chưa có dữ liệu</div>
                                : <table className="table">

                                    <tbody>
                                    {
                                        list.data.map((trade, index) => <ItemTrade trade={trade} username={username}
                                                                                   key={index}/>)
                                    }
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colSpan={2}>
                                            <div className="btn-group pagination" role="group"
                                                 aria-label="Basic example">
                                                <a onClick={this._fetchData.bind(this, list.prev_page_url)}
                                                   type="button"
                                                   className={list.prev_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                   style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                    TRƯỚC</a>
                                                <a onClick={this._fetchData.bind(this, list.next_page_url)}
                                                   type="button"
                                                   className={list.next_page_url !== null ? 'btn btn-secondary btn-sm' : 'btn btn-secondary btn-sm disabled'}
                                                   style={{background: "#fff", border: "1px solid #ccc"}}>TRANG SAU</a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>

                        )
                }
            </div>
        )
    }


    _showMain() {
        const {page, status} = this.props.match.params;

        let content_page = null;

        if (page == 'referrals')
            content_page = this._renderReferrals();
        else if (page == 'offers')
            content_page = this._renderOffers();
        else if (page == 'trade')
            content_page = this._renderTrade(status);


        return (
            <div>
                <div className="row mt-5">
                    <p className="title-menu"><img src="/resources/assets/frontend/img/ic_register.png" alt/><span>Danh sách <a
                        href>Mua / Bán</a></span></p>
                    <div className="offers" style={{width: '100%'}}>
                        <ul className="nav nav-tabs tabs-profile">
                            <li className={page == 'trade' && status == 'active' ? 'active' : ''}><Link
                                to="/dashboard/trade/active"><img
                                src="/resources/assets/frontend/img/ic_tabs_trans_open.png" alt/> Giao dịch đang
                                mở</Link></li>
                            <li className={page == 'trade' && status == 'closed' ? 'active' : ''}><Link
                                to="/dashboard/trade/closed"><img
                                src="/resources/assets/frontend/img/ic_tabs_trans_close.png" alt/> Giao dịch đã
                                chốt</Link></li>
                            <li className={page == 'offers' ? 'active' : ''}><Link to="/dashboard/offers/buy"><img
                                src="/resources/assets/frontend/img/ic_tabs_post.png" alt/> Bài đăng của bạn</Link></li>
                            <li className={page == 'referrals' ? 'active' : ''}><Link to="/dashboard/referrals"><img
                                src="/resources/assets/frontend/img/ic_tabs_list.png" alt/> Danh sách giới thiệu</Link>
                            </li>
                        </ul>
                        { content_page }
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const {profile} = this.props;

        return (
            <div>
                <NotificationContainer/>

                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={profile.wallet}/>

                        { this._showMain() }

                    </div>
                    {/*start content*/}
                </div>


                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

export default connect(mapStateToProps, null)(DashboardPage);