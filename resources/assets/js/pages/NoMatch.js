/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import { PacmanLoader} from 'react-spinners';

class NoMatch extends Component {
    render() {
        return (
            <Redirect to="/"/>
        )
    }
}

export default NoMatch;