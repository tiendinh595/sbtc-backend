/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import * as ApiCaller from '../utils/ApiCaller'
import $ from 'jquery'
import * as AuthService from '../utils/AuthService'
import Spinner from 'react-spinkit'
import * as Helper from '../utils/Helper'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Simplert from 'react-simplert'
import * as ProfileAction from '../actions/ProfileAction'
import {Link} from 'react-router-dom'
import Wallet from '../components/User/Wallet'
import {NotificationContainer, NotificationManager} from 'react-notifications';


class ProfilePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wallet: {},
            info: {},
            showModal: false,
            alert: {
                type: 'success',
                is_show: false,
                msg: []
            },
            histories_login: {}
        };

    }


    componentDidMount() {
        document.title = 'Cài đặt';
        window.scrollTo(0, 0);
        this._fetchProfile();

        console.log('componentDidMount');

        $('body').undelegate('.panel-header', 'click');
        $('body').delegate('.panel-header', 'click', function (e) {
            e.preventDefault();
            console.log('clicked')
            let parent_el = $(this).parents('.pn');
            // $('.pn').removeClass('active');
            // $('.panel-body').slideUp('slow');
            $('.direct').html('+');
            parent_el.addClass('active');
            let body_el = parent_el.find('.panel-body');
            let direct_el = parent_el.find('.direct');
            if (body_el.is(':visible')) {
                body_el.slideUp('slow');
                direct_el.html('+');
                parent_el.removeClass('active');
            }
            else {
                parent_el.addClass('active');
                body_el.slideDown('slow');
                direct_el.html('-')
            }
            console.log('end click')
        });

        this._fetchHistoryLogin('/profile/history-login');
    }

    _fetchHistoryLogin(url) {
        ApiCaller.get(url)
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        histories_login: res.data
                    })
                } else {
                    this.setState({
                        histories_login: {}
                    })
                }
            })
            .catch(err => {

            })
    }

    _fetchProfile() {
        ApiCaller.get(`/profile?full&token=${AuthService.getToken()}`)
            .then((res) => {
                this.setState({
                    ...res.data
                });
            })
            .catch(err => {
                console.error('SettingPage' + err)
            });
    }

    _addPhone() {
        const phone = this.refs.txt_phone.value;
        ApiCaller.post('/profile/add-phone', {phone})
            .then(res=>{
                if(res.code == 200) {
                    this.setState({
                        info: {
                            ...this.state.info,
                            phone_status: 1,
                            phone
                        }
                    });
                    NotificationManager.success(res.msg)
                } else {
                    NotificationManager.error(res.msg)
                }
            })
            .catch(err=>{
                NotificationManager.error('Có lỗi xảy ra')
            })
    }

    _sendCodeVerifyPhone() {
        ApiCaller.post('/profile/send-code-verify-phone')
            .then(res=>{
                if(res.code == 200) {
                    NotificationManager.success(res.msg)
                } else {
                    NotificationManager.error(res.msg)
                }
            })
            .catch(err=>{
                NotificationManager.error('Có lỗi xảy ra')
            })
    }

    _verifyPhone() {
        const code = this.refs.txt_code.value;
        ApiCaller.post('/profile/verify-phone', {code})
            .then(res=>{
                if(res.code == 200) {
                    this.setState({
                        info: {
                            ...this.state.info,
                            phone_status: 2
                        }
                    });
                    NotificationManager.success(res.msg)
                } else {
                    NotificationManager.error(res.msg)
                }
            })
            .catch(err=>{
                NotificationManager.error('Có lỗi xảy ra')
            })
    }

    _showMessage() {
        let msg = this.state.alert.msg.map(val => `${val}<br>`);
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg}
            />
        )
    }

    _updateAuthenticator(e) {
        e.preventDefault();
        ApiCaller.put(`/profile/UpdateAuthenticator?token=${AuthService.getToken()}`, {'2fa': this.refs.txt_2fa.value})
            .then(res => {
                if (res.code != 200) {
                    this.setState({
                        alert: {
                            type: 'error',
                            is_show: true,
                            msg: res.msg
                        }
                    })
                    this.refs.txt_2fa.value = '';
                } else {
                    this._fetchProfile();
                    this.props.ProfileAction.fetchProfile();
                    this.setState({
                        alert: {
                            type: 'success',
                            is_show: true,
                            msg: res.data
                        }
                    })
                }
            })
            .catch(err => {
                console.log(err)
            });
    }

    render() {
        const {wallet, info, histories_login} = this.state;
        if (Object.keys(info).length == 0) {
            return (
                <div>
                    <HeaderOther/>
                    <div className="container text-center">
                        <div className="row">
                            <div className="col-12 loading">
                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            )
        }

        return (
            <div>
                {this._showMessage()}

                <NotificationContainer/>
                <HeaderOther/>

                <div>

                    {/*step 1*/}
                    <div className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true" id="md_phone_st_1">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content no-radius no-border">
                                <div className="modal-header no-border bg-primary no-radius justify-content-center">
                                    <h5 className="modal-title text-uppercase text-center" style={{width: '100%'}}>
                                        cập nhật số điện thoại
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" className="text-white"
                                                  style={{opacity: 1}}>×</span>
                                        </button>
                                    </h5>
                                </div>
                                {
                                    info.phone_status == 0
                                        ? (
                                            <div>
                                                <div className="modal-body text-center no-border">
                                                    <div className="row">
                                                        <div className="col-sm-2"/>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" ref="txt_phone"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="modal-footer no-border justify-content-center">
                                                    <button type="button"
                                                            className="btn btn-primary text-uppercase no-radius" onClick={this._addPhone.bind(this)}>
                                                        tiếp
                                                        tục
                                                    </button>
                                                </div>
                                            </div>
                                        )
                                        : (
                                            info.phone_status == 2
                                                ? (
                                                    <div>
                                                        {/*step 3*/}
                                                        <div className="modal-body text-center no-border"
                                                             >
                                                            <div className="row justify-content-center">
                                                                <div className="col-sm-12">
                                                                    <div className="alert alert-success" role="alert">
                                                                        Thêm số điện thoại thành công
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {/*end step 3*/}
                                                    </div>
                                                )
                                                : (
                                                    <div>
                                                        {/*step 2*/}
                                                        <div className="modal-body text-center no-border"
                                                             >
                                                            <div className="row justify-content-center">
                                                                <div className="col-sm-12">
                                                                    <div className="alert alert-success" role="alert">
                                                                        mã xác nhận đã được gửi tới email. <br />
                                                                        hãy nhập mã xác nhận vào ô bên dưới để xác minh
                                                                        số điện thoại
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-sm-2"/>
                                                                <div className="col-sm-8">
                                                                    <input type="text" className="form-control"
                                                                           placeholder="nhập mã" ref="txt_code"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="modal-footer no-border justify-content-center">
                                                            <button type="button" className="btn btn-info no-radius" onClick={this._sendCodeVerifyPhone.bind(this)}>
                                                                Chưa nhận được mã
                                                            </button>
                                                            <button type="button"
                                                                    className="btn btn-primary text-uppercase no-radius" onClick={this._verifyPhone.bind(this)}>
                                                                xác
                                                                nhận
                                                            </button>

                                                        </div>
                                                        {/*end step 2*/}
                                                    </div>
                                                )
                                        )
                                }


                            </div>
                        </div>
                    </div>
                    {/*end step 1*/}

                </div>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        <div className="row mt-5">
                            <p className="title-menu"><img src="/resources/assets/frontend/img/ic_setting.png"
                                                           alt/><span>cài đặt</span></p>
                            <div className="pn">
                                <div className="panel-header">
                                    Bảo mật Google Authenticator
                                    <span className="direct">+</span>
                                </div>
                                <div className="col-12 panel-body text-center" style={{}}>
                                    <p style={{margin: 0}} className="text-uppercase">Bảo mật google
                                        authenticator {info.enabled_2fa == 0 ? 'chưa được cài đặt' : 'đã được cài đặt'}</p>
                                    <p style={{}} className="text-uppercase">download GG AuThen</p>
                                    <p>
                                        <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2"
                                           target="_blank"><img src="/resources/assets/frontend/img/ic_gg_play.png"
                                                                alt/></a>
                                        <a href="https://itunes.apple.com/vn/app/google-authenticator/id388497605?mt=8"
                                           target="_blank" className="ml-2"><img
                                            src="/resources/assets/frontend/img/ic_store.png"
                                            alt/></a>
                                    </p>
                                    {
                                        info.enabled_2fa == 0
                                            ? <div>
                                                <p className="text-uppercase">hoặc nếu bạn đã có quét mã QR &amp; nhập
                                                    CODE</p>
                                                <p><a rel="nofollow" href="#"
                                                      style={{cursor: 'default'}}><img src={info.url_qr_code} alt/></a>
                                                </p>
                                                <p className="text-uppercase">hashkey: {info.secret_2fa}</p>
                                            </div>
                                            : ''
                                    }

                                    <div className="text-center mb-2">
                                        <form className="form-inline " style={{
                                            margin: '0 auto',
                                            width: 'auto',
                                            display: 'inline-block',
                                            height: 10
                                        }}>
                                            <div className="form-group" style={{display: 'inline-block'}}>
                                                <input ref="txt_2fa" type="text" className="no-radius form-control"
                                                       placeholder="Nhập code từ gg authen"/>
                                            </div>
                                            <button onClick={this._updateAuthenticator.bind(this)} type="submit"
                                                    className="btn btn-primary no-radius ml-2">{info.enabled_2fa == 0 ? 'xác thực' : 'tắt xác thực'}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="pn mt-2">
                                <div className="panel-header">
                                    Thông báo giao dịch
                                    <span className="direct open">+</span>
                                </div>
                                <div className="col-12 panel-body">
                                    <p className="text-center b500">THÔNG BÁO QUA MAIL</p>
                                    <div className="row text-center">
                                        <div className="col-sm-12 col-md-4">
                                            Có giao dịch mới
                                            <label className="switch">
                                                <input type="checkbox"/>
                                                <span className="slider round"/>
                                            </label>
                                        </div>
                                        <div className="col-sm-12 col-md-4">
                                            Trạng thái giao dịch thay đổi
                                            <label className="switch">
                                                <input type="checkbox" defaultChecked/>
                                                <span className="slider round"/>
                                            </label>
                                        </div>
                                        <div className="col-sm-12 col-md-4">
                                            Tin nhắn từ giao dịch
                                            <label className="switch">
                                                <input type="checkbox"/>
                                                <span className="slider round"/>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pn mt-2">
                                <div className="panel-header">
                                    hồ sơ của bạn
                                    <span className="direct open">+</span>
                                </div>
                                <div className="col-12 panel-body">
                                    <div className="row">
                                        <div className="col-sm-12 col-md-5">
                                            <form action>
                                                <div className="form-group row">
                                                    <label className="col-sm-5 col-form-label text-right">Email</label>
                                                    <div className="col-sm-7 no-pd">
                                                        <input type="email" className="form-control " readOnly={true}
                                                               value={info.email}/>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-sm-5 col-form-label text-right">Bí
                                                        danh</label>
                                                    <div className="col-sm-7 no-pd">
                                                        <input type="text" className="form-control " readOnly={true}
                                                               value={info.username}/>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-sm-5 col-form-label text-right">Có giao dich
                                                        mới</label>
                                                    <div className="col-sm-7 no-pd">
                                                        <label className="switch">
                                                            <input type="checkbox" defaultChecked/>
                                                            <span className="slider round"/>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-sm-5 col-form-label text-right">Số điện
                                                        thoại</label>
                                                    <div className="col-sm-7 no-pd">
                                                        <input type="text" className="form-control "
                                                               defaultValue={info.phone}/>
                                                        <a href="#" data-toggle="modal" data-target="#md_phone_st_1"
                                                           style={{
                                                               position: 'absolute',
                                                               right: 11,
                                                               top: 8,
                                                               textDecoration: 'underline'
                                                           }}>Cập
                                                            nhật</a>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-sm-5 col-form-label text-right">Tình trạng tài
                                                        khoản</label>
                                                    <div className="col-sm-7 no-pd">
                                                        <input type="email" className="form-control" readOnly
                                                               style={{backgroundColor: '#fff'}}/>
                                                        <span style={{
                                                            position: 'absolute',
                                                            top: 7,
                                                            left: 10,
                                                            color: 'red'
                                                        }}>
                                                            {
                                                                info.doc_status == 0 ? 'chưa gửi' : (info.doc_status == 1 ? 'đang chờ duyệt' : (info.doc_status == 2 ? 'đã duyệt' : 'bị từ chối'))
                                                            }
                                                        </span>
                                                        <Link to="/setting/verify" style={{
                                                            position: 'absolute',
                                                            right: 11,
                                                            top: 8,
                                                            textDecoration: 'underline'
                                                        }}>Tải
                                                            tài liệu lên</Link>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pn mt-2">
                                <div className="panel-header">
                                    lịch sử truy cập
                                    <span className="direct open">+</span>
                                </div>
                                <div className="col-12 panel-body">
                                    <div className="row">
                                        <div className="col-md-2"/>
                                        <div className="col-sm-12 col-md-8">
                                            <table className="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <td scope="col" className="text-uppercase text-center b600">địa điểm
                                                        truy cập(ip)
                                                    </td>
                                                    <td scope="col" className="text-uppercase text-center b600">
                                                        ngày/giờ
                                                    </td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    Object.keys(histories_login).length == 0
                                                        ? 'chưa có dữ liệu'
                                                        : histories_login.data.map((history, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td className=" text-center">{`${history.city}(${history.ip})`}</td>
                                                                    <td className=" text-center">{history.created_at}</td>
                                                                </tr>
                                                            )
                                                        })
                                                }
                                                <tr>
                                                    <td className="no-border" colSpan="2">
                                                        <div className="btn-group pagination no-mg text-center"
                                                             role="group"
                                                             aria-label="Basic example">
                                                            <a onClick={this._fetchHistoryLogin.bind(this, histories_login.prev_page_url)}
                                                               type="button"
                                                               className={histories_login.prev_page_url !== null ? 'btn btn-sm btn-secondary' : 'btn btn-sm btn-secondary disabled'}
                                                               style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                                TRƯỚC</a>
                                                            <a onClick={this._fetchHistoryLogin.bind(this, histories_login.next_page_url)}
                                                               type="button"
                                                               className={histories_login.next_page_url !== null ? 'btn btn-sm btn-secondary' : 'btn btn-sm btn-secondary disabled'}
                                                               style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                                SAU</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/*start content*/}
                </div>


                <Footer/>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        auth: state.AuthReducer,
        profile: state.ProfileReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ProfileAction: bindActionCreators(ProfileAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);