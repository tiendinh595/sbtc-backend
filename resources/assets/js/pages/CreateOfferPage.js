/**
 * Created by dinh on 12/22/17.
 */
import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import Wallet from '../components/User/Wallet'
import OfferItemSell3 from '../components/Offer/OfferItemSell3'
import Spinner from 'react-spinkit'
import * as ApiCaller from '../utils/ApiCaller'
import * as Label from '../components/Offer/Lable'
import {Link} from 'react-router-dom'
import * as GlobalAction from '../actions/GlobalAction'
import  * as Helper from '../utils/Helper'
import BankSelectBox from '../components/Offer/BankSelectBox'
import Simplert from 'react-simplert'
import Select from 'react-select';
import '../style/react-select.css';

class CreateOfferPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currency: 'Bitcoin',
            currency_short: 'BTC',
            action: 'buy_btc',
            rate_vnd_usd: 0,
            rate_usd_coin: 0,
            price_per_coin: 0,
            min_amount: 0,
            max_amount: 0,
            payment_time: 15,
            allow_not_verify: false,
            payment_method: 1,
            bank_code: 0,
            bank_number: '',
            bank_account_name: '',
            address_wallet: '',

            completed: true,
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },
        };

        this._formCreate = this._formCreate.bind(this);
        this._onChange = this._onChange.bind(this);
        this._getActionName = this._getActionName.bind(this);
        this._onCreate = this._onCreate.bind(this);
        this._onSelectAll = this._onSelectAll.bind(this);
    }

    componentDidMount() {
        document.title = 'Đăng tin mua bán';
        window.scrollTo(0, 0);
        this.setState({
            rate_usd_coin: this.state.currency_short == 'BTC' ? this.props.global.system.price_last_btc : newProps.global.system.price_last_eth
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            rate_usd_coin: this.state.currency_short == 'BTC' ? newProps.global.system.price_last_btc : newProps.global.system.price_last_eth
        });

    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _onCreate(e) {
        e.preventDefault();

        const {action, rate_vnd_usd, rate_usd_coin, price_per_coin, min_amount, max_amount, payment_time, allow_not_verify, payment_method, bank_code, bank_number, bank_account_name, address_wallet} = this.state;

        this.setState({
            completed: false,
        });

        ApiCaller.post('/offer', {
            action,
            rate_vnd_usd,
            rate_usd_coin,
            price_per_coin,
            min_amount,
            max_amount,
            payment_time,
            allow_not_verify,
            payment_method,
            bank_code,
            bank_number,
            bank_account_name,
            address_wallet:  typeof address_wallet === 'object' && address_wallet !== null ? address_wallet.value : address_wallet,
        })
            .then(res => {
                if (res.code == 200) {
                    this.setState({
                        completed: true,
                        alert: {
                            is_show: true,
                            type: 'success',
                            msg: res.msg
                        },
                    });
                    this.refs.frm_offer.reset();
                } else {
                    this.setState({
                        completed: true,
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        }
                    })
                }
            })
            .catch(err => {

            })
    }

    _onSelectAll(e) {
        e.preventDefault();
        const max_amount = this.state.currency_short.toLowerCase() == 'btc' ? this.props.profile.wallet.balance_btc : this.props.profile.wallet.balance_eth;
        this.refs.max_amount.value = max_amount;
        this.setState({
            max_amount
        })
    }

    _onChange(e) {
        let {name, value} = e.target;
        if (name == 'action') {
            const {currency, currency_short} = value.indexOf('btc') != -1 ? {
                    currency: 'Bitcoin',
                    currency_short: 'BTC'
                } : {currency: 'Ethereum', currency_short: 'ETH'};
            const rate_usd_coin = value.indexOf('btc') != -1 ? this.props.global.system.price_last_btc : this.props.global.system.price_last_eth;
            this.setState({
                currency,
                currency_short,
                rate_usd_coin
            })
        } else if (name == 'allow_not_verify') {
            value = !this.state.allow_not_verify;
        }
        this.setState({
            [name]: value
        });
    }

    _autoCompleteAddress(e) {
        const address_wallet = this.state.currency_short.toLowerCase() == 'btc' ? this.props.profile.wallet.address_btc : this.props.profile.wallet.address_eth;
        this.refs.address_wallet.value = address_wallet;
        this.setState({
            address_wallet
        })
    }

    _getActionName() {
        return (this.state.action == 'sell_btc' || this.state.action == 'sell_eth') ? 'bán' : 'mua';
    }

    _formCreate() {
        const {currency_short, currency, action, rate_usd_coin, rate_vnd_usd, completed} = this.state;

        const address_wallet = typeof this.state.address_wallet === 'object' && this.state.address_wallet != null ? this.state.address_wallet.value : this.state.address_wallet;
        const value = {value: address_wallet, label:address_wallet};

        let address_wallet_default = null;
        if(this.props.profile.wallet !== undefined)
            address_wallet_default = this.state.currency_short.toLowerCase() == 'btc' ? this.props.profile.wallet.address_btc : this.props.profile.wallet.address_eth;


        return (
            <form action className="form" id="frm_offer" ref="frm_offer">
                <div className="row mt-5">
                    <p className="title-menu"><img src="/resources/assets/frontend/img/ic_cart.png" alt/><span>Đăng tin mua/bán</span>
                    </p>
                    <div className="pn-active">
                        <div className="panel-header-active text-center">
                            Thông tin mua/bán
                        </div>
                        <div className="col-12 panel-body-active" style={{display: 'block'}}>
                            <div className="row row-middle">
                                <div className="col-lg-2 col-md-3">
                                    <label>Tôi muốn</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <select className="form-control" name="action" onChange={this._onChange}>
                                        <option value="buy_btc">Mua Bitcoin</option>
                                        <option value="sell_btc">Bán Bitcoin</option>
                                        <option value="" disabled="disabled">---</option>
                                        <option value="buy_eth">Mua Ethereum</option>
                                        <option value="sell_eth">Bán Ethereum</option>
                                    </select>
                                </div>
                                <div className="col"/>
                            </div>
                            { action == 'buy_btc' || action == 'buy_eth'
                                ? <div className="row row-middle mt-2">
                                    <div className="col-lg-2 col-md-3">
                                        <label>Địa chỉ nhận</label>
                                    </div>
                                    <div className="col-lg-4 col-md-4">
                                        <input type="text" value={address_wallet_default} className="form-control" readOnly={true}/>

                                        {/*<Select.Creatable*/}
                                            {/*className="form-control"*/}
                                            {/*placeholder=""*/}
                                            {/*multi={false}*/}
                                            {/*promptTextCreator={(label)=>`Chọn ví: ${label}`}*/}
                                            {/*options={[{value: address_wallet_default, label:`Ví của bạn tại Sieuthibtc ${address_wallet_default}`}]}*/}
                                            {/*onChange={(val)=>this.setState({address_wallet: val})}*/}
                                            {/*value={value}*/}
                                        {/*/>*/}
                                    </div>
                                    <div className="col"/>
                                </div>
                                : ''
                            }

                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Tỉ giá USD</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <input type="text" className="form-control" name="rate_vnd_usd"
                                               onChange={this._onChange}/>
                                        <span className="input-group-addon no-radius"
                                              style={{background: 'transparent'}}><span>VNĐ</span></span>
                                    </div>
                                </div>
                                <div className="col">
                                    1USD = {Helper.formatMoney(rate_vnd_usd, 3)}
                                </div>
                            </div>
                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Giá trị thực nhận</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <b>{Helper.formatMoney(rate_vnd_usd*rate_usd_coin)} VND/{currency_short}</b>
                                </div>
                                <div className="col">
                                    Đây là giá tương ứng với giá BitStamp (Blockchain) hiện tại
                                    (<b>{rate_usd_coin}</b>) Giá sẽ biến động khi tỉ giá Bitstamp (Blockchain) thay đổi
                                </div>
                            </div>
                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Số {currency_short} tối thiểu</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <input type="text" className="form-control" name="min_amount"
                                               onChange={this._onChange}/>
                                        <span className="input-group-addon no-radius"
                                              style={{background: 'transparent'}}><span>{currency_short}</span></span>
                                    </div>
                                </div>
                                <div className="col">
                                    Hạn mức giao dịch tối thiểu trong một giao dịch. (Ít nhất là 0.001
                                    {currency})
                                </div>
                            </div>
                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Số {currency_short} muốn {this._getActionName()}</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <input type="text" className="form-control" name="max_amount"
                                               onChange={this._onChange} ref="max_amount"/>
                                        <span className="input-group-btn no-radius">
                                            <a href="#" className="btn btn-primary bg-blue no-radius" onClick={this._onSelectAll}>All</a>
                                        </span>
                                    </div>
                                </div>
                                <div className="col">
                                    Số {currency_short} muốn {this._getActionName()}
                                </div>
                            </div>
                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Thời gian thanh toán</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <select className="form-control" name="payment_time" onChange={this._onChange}>
                                        <option value="15">15 phút</option>
                                        <option value="30">30 phút</option>
                                    </select>
                                </div>
                                <div className="col">
                                    Nếu người mua không hoàn thành thanh toán trong thời gian này, giao
                                    dịch sẽ bị huỷ tự động
                                </div>
                            </div>
                            <div className="row row-middle mt-2">
                                <div className="col-lg-2 col-md-3">
                                    <label>Từ chối người mua chưa xác minh</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <label className="switch">
                                        <input type="checkbox" value="on"
                                               name="allow_not_verify" onChange={this._onChange}/>
                                        <span className="slider round"></span>
                                    </label>
                                </div>
                                <div className="col">
                                    Chỉ chấp nhận người mua đã xác minh để làm cho các giao dịch của
                                    bạn trở nên an toàn hơn
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pn-active mt-3">
                        <div className="panel-header-active text-center">
                            Thông tin thanh toán
                        </div>
                        <div className="col-12 panel-body-active" style={{display: 'block'}}>
                            <div className="row row-middle">
                                <div className="col-lg-2 col-md-3">
                                    <label>Phương thức thanh toán</label>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <select className="form-control" name="payment_method" onChange={this._onChange}>
                                        <option value="1">Chuyển khoản ngân hàng</option>
                                        <option value="2">Nộp tiền mặt vào tài khoản ngân hàng</option>
                                    </select>
                                </div>
                                <div className="col">
                                    Phương thức thanh toán cho quảng cáo này
                                </div>
                            </div>
                            <div className="row row-middle mt-3">
                                <div className="col-lg-2 col-md-3">
                                    <label>Tên ngân hàng</label>
                                </div>
                                <BankSelectBox change={(bank) => {
                                    this.setState({bank_code: bank})
                                }} select={0} disabled={false} className="col-lg-4 col-md-4"/>
                                <div className="col"/>
                            </div>
                            { action == 'sell_btc' || action == 'sell_eth'

                                ? <span>
                                        <div className="row row-middle mt-2">
                                            <div className="col-lg-2 col-md-3">
                                                <label>Số tài khoản</label>
                                            </div>
                                            <div className="col-lg-4 col-md-4">
                                                <input type="text" className="form-control" name="bank_number"
                                                       onChange={this._onChange}/>
                                            </div>
                                            <div className="col"/>
                                        </div>
                                        <div className="row row-middle mt-2">
                                            <div className="col-lg-2 col-md-3">
                                                <label>Tên tài khoản</label>
                                            </div>
                                            <div className="col-lg-4 col-md-4">
                                                <input type="text" className="form-control" name="bank_account_name"
                                                       onChange={this._onChange}/>
                                            </div>
                                            <div className="col"/>
                                        </div>
                                    </span>
                                : null
                            }

                            <div className="row row-middle mt-2">
                                <div className="col-12 text-center">
                                    <button type="submit"
                                            className={completed == false ? "btn btn-primary bg-blue no-radius disabled" : "btn btn-primary bg-blue no-radius"}
                                            onClick={this._onCreate}>
                                        {completed == false ? <i className="fa fa-spinner fa-spin fa-fw"></i> : ''} Tạo
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        )
    }

    render() {
        const {wallet} = this.props.profile;
        return (
            <div>
                { this._showMessage() }
                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        { this._formCreate() }

                    </div>
                    {/*start content*/}
                </div>

                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        global: state.GlobalReducer,
        profile: state.ProfileReducer,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        GlobalAction: bindActionCreators(GlobalAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateOfferPage);