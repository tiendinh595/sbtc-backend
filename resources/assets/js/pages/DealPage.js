/**
 * Created by dinh on 12/22/17.
 */
import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import Wallet from '../components/User/Wallet'
import OfferItemSell3 from '../components/Offer/OfferItemSell3'
import Spinner from 'react-spinkit'
import * as ApiCaller from '../utils/ApiCaller'
import * as Label from '../components/Offer/Lable'
import {Link} from 'react-router-dom'
import * as GlobalAction from '../actions/GlobalAction'
import  * as Helper from '../utils/Helper'
import BankSelectBox from '../components/Offer/BankSelectBox'
import Simplert from 'react-simplert'
import ReactHtmlParser from 'react-html-parser'
import {Redirect} from 'react-router-dom'


class ItemDeal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_loading: false,
            removed: false,
            approved: {
                is_success: false,
                trade: {}
            },
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },
        };

        this._onCancel = this._onCancel.bind(this);
        this._onApprove = this._onApprove.bind(this);
        this._showMessage = this._showMessage.bind(this);
    }

    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _onApprove(e) {
        e.preventDefault();

        this.setState({
            is_loading: true
        });

        ApiCaller.post(`/deal/approve/${this.props.deal.id}`)
            .then(res => {
                this.setState({
                    is_loading: true
                });

                if (res.code == 200) {
                    this.setState({
                        approved: {
                            is_success: true,
                            trade: res.data
                        },
                    });
                    console.log(res)
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        },
                    })
                }
            })
            .catch(err => {
                this.setState({
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi trong quá trình xử lý'
                    },
                })
            })
    }

    _onCancel(e) {
        e.preventDefault();

        this.setState({
            is_loading: true
        });

        ApiCaller.post(`/deal/cancel/${this.props.deal.id}`)
            .then(res => {
                this.setState({
                    is_loading: true
                });

                if (res.code == 200) {
                    this.setState({
                        removed: true
                    })
                } else {
                    this.setState({
                        alert: {
                            is_show: true,
                            type: 'error',
                            msg: res.msg
                        },
                    })
                }
            })
            .catch(err => {
                this.setState({
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi trong quá trình xử lý'
                    },
                })
            })
    }

    render() {
        const {deal} = this.props;
        const {is_loading, removed, approved} = this.state;

        if (removed)
            return null;

        if (approved.is_success)
            return <Redirect to={`/trade/${approved.trade.id}`}/>

        let title = '';
        let is_owner = false;
        if (deal.username == this.props.username) {
            title += 'Bạn đang thoả thuận ';
            title += deal.type == 1 ? `<b className="txt-green">Bán ${Label.coin(deal.offer.type_money)}</b>` : `<b className="txt-green">Mua ${Label.coin(deal.offer.type_money)}</b>`
            title += ` với <b>${deal.owner_offer}</b>`;

            is_owner = true;
        } else {
            title += `${deal.username} đang thoả thuận `;
            title += deal.type == 1 ? `<b className="txt-green">Bán ${Label.coin(deal.offer.type_money)}</b>` : `<b className="txt-green">Mua ${Label.coin(deal.offer.type_money)}</b>`
            title += ` với bạn`;

            is_owner = false;
        }

        return (
            <tr>
                <td>
                    {this._showMessage() }
                    <div className="row">
                        <div className="col-sm-12">
                            <p>
                                <i className="fa fa-circle-o txt-green mr-1" aria-hidden="true"/>
                                {ReactHtmlParser(title)}
                            </p>
                            <p className="ml-3">
                                <label style={{width: 160}}><b>TỶ GIÁ ĐỀ NGHỊ: </b></label>
                                <label>{Helper.formatMoney(deal.rate_vnd_usd, 2)}</label>
                            </p>
                            <p className="ml-3">
                                <label style={{width: 160}}><b>LƯỢNG GIAO DỊCH: </b></label>
                                <label>{Helper.formatMoney(deal.amount, 8)} {Label.coin(deal.offer.type_money, true, true)}</label>
                            </p>
                            <p className="ml-3">
                                <label style={{width: 160}}><b>TỔNG TIỀN: </b></label>
                                <label>{Helper.formatMoney(deal.total_money)} VND</label>
                            </p>
                        </div>
                    </div>
                </td>
                {
                    deal.status == 2
                        ? <td width="30%">
                            <div className="row text-right">
                                <div className="col-sm-12">
                                    <Link to={deal.trade_id == null || deal.trade_id == 0 ? `/offer/${deal.offer_ref.id}` : `/trade/${deal.trade_id}`}
                                       className="bt bt-blue align-middle ml-2">Chi tiết</Link>
                                </div>
                            </div>
                        </td>
                        : <td width="30%">
                            <div className="row text-right">
                                <div className="col-sm-12">
                                    <a href onClick={this._onCancel}
                                       className={is_loading || deal.status != 1 ? "bt btn-danger align-middle disabled" : "bt btn-danger align-middle"}>Huỷ</a>
                                    <a href onClick={this._onApprove}
                                       className={is_owner == true || is_loading == true || deal.status != 1 ? "bt bt-blue align-middle ml-2 disabled" : "bt bt-blue align-middle ml-2"}>Chấp
                                        nhận</a>
                                </div>
                            </div>
                        </td>
                }
            </tr>
        )
    }
}

class DealPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            is_loading: true,
            alert: {
                is_show: false,
                type: 'success',
                msg: []
            },
            current_status: 2,
            deals: {}
        };

        this._loadDeals = this._loadDeals.bind(this);
        this._renderList = this._renderList.bind(this);
    }

    componentDidMount() {
        document.title = 'Lịch sử thoả thuận';
        window.scrollTo(0, 0);

        const {current_status} = this.state;
        const url = `/deal/${current_status}`;
        this._loadDeals(url);

    }

    _loadDeals(url) {
        this.setState({
            is_loading: true
        });

        ApiCaller.get(url)
            .then(res => {
                this.setState({
                    is_loading: false,
                    deals: res.data
                })
            })
            .catch(err => {
                this.setState({
                    is_loading: false,
                    alert: {
                        is_show: true,
                        type: 'error',
                        msg: 'Có lỗi xảy ra trong quá trình tải dữ liệu'
                    }
                })
            })
    }


    _showMessage() {
        const {msg} = this.state.alert;
        let msg_show = typeof msg == 'string' ? msg : this.state.alert.msg.map(val => val + '<br>');
        msg_show = typeof msg_show == 'object' ? msg_show.join("") : msg_show;
        return (
            <Simplert
                showSimplert={this.state.alert.is_show}
                type={this.state.alert.type}
                title="Thông báo"
                message={msg_show}
                onClose={() => this.setState({alert: {is_show: false, type: '', msg: ''}}) }
            />
        )
    }

    _changeTab(e, current_status) {
        e.preventDefault();
        this.setState({
            current_status
        });

        const url = `/deal/${current_status}`;
        this._loadDeals(url);
    }

    _renderList() {
        const {deals, is_loading, current_status} = this.state;


        return (
            <div className="row mt-5">
                <p className="title-menu" style={{display: 'block', width: '100%'}}><img
                    src="/resources/assets/frontend/img/ic_thoathuan.png" alt/><span>Lịch sử thoả thuận</span></p>

                <div className="offers" style={{width: '100%'}}>
                    <ul className="nav nav-tabs tabs-profile">
                        <li className={current_status == 2 ? 'active' : ''}><a href=""
                                                                               onClick={(e) => this._changeTab(e, 2)}>Đã
                            thoả thuận</a></li>
                        <li className={current_status == 1 ? 'active' : ''}><a href=""
                                                                               onClick={(e) => this._changeTab(e, 1)}>Đang
                            thoả thuận</a></li>
                        <li className={current_status == 3 ? 'active' : ''}><a href=""
                                                                               onClick={(e) => this._changeTab(e, 3)}>Đã
                            huỷ</a></li>
                    </ul>

                    {
                        is_loading
                            ? <div>
                                <div className="container text-center mt-5 mb-5">
                                    <div className="row">
                                        <div className="col-12 justify-content-center text-center">
                                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : (
                                Object.keys(deals.data).length == 0
                                    ? <div className="alert alert-info">Chưa có dữ liệu</div>
                                    : <table className="table">
                                        <tbody>

                                        {deals.data.map((deal, index) => <ItemDeal key={index} deal={deal}
                                                                                   username={this.props.profile.username}/>)}

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colSpan={2}>
                                                <div className="btn-group pagination" role="group"
                                                     aria-label="Basic example">
                                                    <a onClick={this._loadDeals.bind(this, deals.prev_page_url)}
                                                       type="button"
                                                       className={deals.prev_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        TRƯỚC</a>
                                                    <a onClick={this._loadDeals.bind(this, deals.next_page_url)}
                                                       type="button"
                                                       className={deals.next_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG
                                                        SAU</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                            )
                    }

                </div>

            </div>
        )
    }

    render() {
        const {wallet} = this.props.profile;
        return (
            <div>
                { this._showMessage() }
                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        { this._renderList() }

                    </div>
                    {/*start content*/}
                </div>

                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer,
    }
};

export default connect(mapStateToProps, null)(DealPage);