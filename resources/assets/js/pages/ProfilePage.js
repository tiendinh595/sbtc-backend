/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import OfferItemSell2 from '../components/Offer/OfferItemSell2'
import OfferItemBuy2 from '../components/Offer/OfferItemBuy2'
import {connect} from 'react-redux'
import Spinner from 'react-spinkit'

import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import * as Helper from '../utils/Helper'
import Wallet from '../components/User/Wallet'


class SettingPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            'wallet': {},
            'info': {},
            'list_feedback': [],
            'offers_sell': [],
            'offers_buy': [],
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const {username} = this.props.match.params;

        document.title = `Trang cá nhân ${username}`;

        ApiCaller.get(`/profile/${username}?token=${AuthService.getToken()}`)
            .then((res) => {
                this.setState({
                    ...res.data
                });
            })
            .catch(err => {
                console.error('SettingPage' + err)
            });

        ApiCaller.get(`/profile/${username}/feedback`)
            .then((res) => {
                this.setState({
                    ...res.data
                });
            })
            .catch(err => {
                console.log(err)
            });

        this._loadOfferSell(`/offer/sell/all/${username}`);

        this._loadOfferBuy(`/offer/buy/all/${username}`);
    }

    _loadOfferSell(url) {
        ApiCaller.get(url)
            .then((res) => {
                this.setState({
                    offers_sell: res.data
                });
            })
            .catch(err => {
                console.log(err)
            });
    }

    _loadOfferBuy(url) {
        ApiCaller.get(url)
            .then((res) => {
                this.setState({
                    offers_buy: res.data
                });
            })
            .catch(err => {
                console.log(err)
            });
    }

    _showInfo() {
        const {info, wallet} = this.state;

        if (Object.keys(info).length == 0) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="col-sm-12 col-md-6 no-pd no-mg">
                    <p className="title-menu pt-5 bg-gray pb-1"><span className="no-mg">Thông tin của <a
                        href="">{info.full_name}</a></span></p>
                    <div className="pn active">
                        <div className="col-12 panel-body" style={{display: 'block'}}>
                            <table className="table">
                                <tbody>
                                <tr>
                                    <td className="no-border">Đã giao dịch BTC</td>
                                    <td className="no-border">{Helper.formatMoney(wallet.traded_btc, 8)} <b
                                        className="highlight-orange">BTC</b></td>
                                </tr>
                                <tr>
                                    <td className="no-border">Đã giao dịch ETH</td>
                                    <td className="no-border">{Helper.formatMoney(wallet.traded_eth, 8)} <b>ETH</b></td>
                                </tr>
                                <tr>
                                    <td className="no-border">Số giao dịch thành công</td>
                                    <td className="no-border"> {info.trade_in_count+info.trade_out_count}</td>
                                </tr>
                                <tr>
                                    <td className="no-border">Điểm số phản hồi</td>
                                    <td className="no-border"> {info.rate}/5</td>
                                </tr>
                                <tr>
                                    <td className="no-border">Tài khoản được tạo</td>
                                    <td className="no-border"> {info.created_at}</td>
                                </tr>
                                <tr>
                                    <td className="no-border">Lần truy cập cuối</td>
                                    <td className="no-border"> {info.latest_login_at}</td>
                                </tr>
                                <tr>
                                    <td className="no-border">Tải tài liệu lên</td>
                                    <td className="no-border">
                                        <span className={info.doc_status == 2 ? "highlight-green" : "highlight-orange"}>{info.doc_status_name}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="no-border">Số điện thoại</td>
                                    <td className="no-border">
                                        {info.phone_status != 2 ? <span className="highlight-orange">Chưa xác thực</span> : <span className="highlight-green">{info.phone}</span>}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="no-border"/>
                                    <td className="no-border"/>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            )
        }
    }

    loadMoreFeedback(url) {
        this.setState({
            list_feedback: {}
        });
        ApiCaller.get(url)
            .then((res) => {
                this.setState({
                    ...res.data
                });
            })
            .catch(err => {
                console.log(err)
            });
    }

    _showFeedback() {
        const {list_feedback, info} = this.state;
        if (Object.keys(list_feedback).length == 0) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="col-sm-12 col-md-6 no-pd no-mg">
                    <p className="title-menu pt-5 bg-gray pb-1"><span className="no-mg">phản hồi về <a
                        href="">{info.full_name}</a></span>
                    </p>
                    <div className="pn active">
                        <div className="col-12 panel-body" style={{display: 'block'}}>

                            <table className="table">
                                <tbody>

                                {
                                    list_feedback.data.map((feedback, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className="no-border">"{feedback.content}"
                                                    - {feedback.reporter}</td>
                                            </tr>
                                        );
                                    })
                                }
                                <tr>
                                    <td className="no-border">
                                        <div className="btn-group pagination no-mg" role="group"
                                             aria-label="Basic example">
                                            <a onClick={this.loadMoreFeedback.bind(this, list_feedback.prev_page_url)}
                                               type="button"
                                               className={list_feedback.prev_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                               style={{background: "#fff", border: "1px solid #ccc"}}>TRANG TRƯỚC</a>
                                            <a onClick={this.loadMoreFeedback.bind(this, list_feedback.next_page_url)}
                                               type="button"
                                               className={list_feedback.next_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                               style={{background: "#fff", border: "1px solid #ccc"}}>TRANG SAU</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            )
        }
    }

    _showListOfferSell() {
        if(this.props.match.params.username == this.props.profile.username)
            return null;
        const {offers_sell, info} = this.state;
        if (Object.keys(offers_sell).length == 0) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="row mt-5">
                    <p className="title-menu" style={{width:"100%"}}><span className="no-mg">MUA TỪ <a href="">{info.full_name}</a></span></p>
                    <div className="offers">
                        <table className="table">
                            <tbody>
                            {
                                offers_sell.data.map((offer, index)=> <OfferItemSell2 key={index} offer={offer}/>)
                            }
                            </tbody>
                            {
                                offers_sell.data.length == 0
                                ?   <p>Chưa có dữ liệu</p>
                                :   <tfoot>
                                        <tr>
                                            <td colSpan={2}>
                                                <div className="btn-group pagination" role="group"
                                                     aria-label="Basic example">
                                                    <a onClick={this._loadOfferSell.bind(this, offers_sell.prev_page_url)}
                                                       type="button"
                                                       className={offers_sell.prev_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG TRƯỚC</a>
                                                    <a onClick={this._loadOfferSell.bind(this, offers_sell.next_page_url)}
                                                       type="button"
                                                       className={offers_sell.next_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                       style={{background: "#fff", border: "1px solid #ccc"}}>TRANG SAU</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                            }

                        </table>
                    </div>
                </div>
            )
        }
    }

    _showListOfferBuy() {
        if(this.props.match.params.username == this.props.profile.username)
            return null;
        const {offers_buy, info} = this.state;
        if (Object.keys(offers_buy).length == 0) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="row mt-5">
                    <p className="title-menu" style={{width:"100%"}}><span className="no-mg">BÁN CHO <a href="">{info.full_name}</a></span></p>
                    <div className="offers">
                        <table className="table">
                            <tbody>
                            {
                                offers_buy.data.map((offer, index)=> <OfferItemBuy2 key={index} offer={offer}/>)
                            }
                            </tbody>
                            {
                                offers_buy.data.length === 0
                                    ?   <p>Chưa có dữ liệu</p>
                                    :   <tfoot>
                                            <tr>
                                                <td colSpan={2}>
                                                    <div className="btn-group pagination" role="group"
                                                         aria-label="Basic example">
                                                        <a onClick={this._loadOfferBuy.bind(this, offers_buy.prev_page_url)}
                                                           type="button"
                                                           className={offers_buy.prev_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                           style={{background: "#fff", border: "1px solid #ccc"}}>TRANG TRƯỚC</a>
                                                        <a onClick={this._loadOfferBuy.bind(this, offers_buy.next_page_url)}
                                                           type="button"
                                                           className={offers_buy.next_page_url !== null ? 'btn btn-secondary' : 'btn btn-secondary disabled'}
                                                           style={{background: "#fff", border: "1px solid #ccc"}}>TRANG SAU</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                            }
                        </table>
                    </div>
                </div>
            )
        }
    }


    render() {
        const {profile} = this.props;

        if (!profile.is_logged)
            return (
                <div>
                    <HeaderOther/>
                    <div className="container text-center">
                        <div className="row">
                            <div className="col-12 loading">
                                <Spinner fadeIn="none" name='pacman' color="rgb(54, 215, 183)"/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );

        return (
            <div>
                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={profile.wallet}/>

                        <div className="row bg-white pt-0">
                            {this._showInfo()}

                            {this._showFeedback()}
                        </div>

                        { this._showListOfferSell() }

                        { this._showListOfferBuy() }

                    </div>
                    {/*start content*/}
                </div>


                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};

export default connect(mapStateToProps, null)(SettingPage);