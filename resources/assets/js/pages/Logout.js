/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderLogin from '../components/Layout/HeaderLogin'
import Footer from '../components/Layout/Footer'

//utils
import Simplert from 'react-simplert'
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as AuthAction from '../actions/AuthAction'
import * as ProfileAction from '../actions/ProfileAction'

import Cookies from 'universal-cookie';
const cookies = new Cookies();

import {Redirect} from 'react-router-dom'


class Logout extends Component {

    constructor(props) {
        super(props);
        this.props.AuthAction.logout();
    }

    componentWillReceiveProps(newProps) {
        
    }

    render() {
        return (
            <Redirect to="/login"/>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        profile: state.ProfileReducer
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        AuthAction: bindActionCreators(AuthAction, dispatch),
        ProfileAction: bindActionCreators(ProfileAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);