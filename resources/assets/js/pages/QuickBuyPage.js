/**
 * Created by dinh on 12/22/17.
 */
import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import HeaderOther from '../components/Layout/HeaderOther'
import Footer from '../components/Layout/Footer'
import Wallet from '../components/User/Wallet'
import OfferItemSell3 from '../components/Offer/OfferItemSell3'
import Spinner from 'react-spinkit'
import * as ApiCaller from '../utils/ApiCaller'
import * as Label from '../components/Offer/Lable'
import {Link} from 'react-router-dom'
import * as GlobalAction from '../actions/GlobalAction'
import  * as Helper from '../utils/Helper'


class QuickBuyPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            list_offers: {},
            total_money: '',
            loading: false,
            is_search: false
        };

        this._formInput = this._formInput.bind(this);
        this._showAlert = this._showAlert.bind(this);
        this._formInput = this._formInput.bind(this);
        this.loadMoreOffer = this.loadMoreOffer.bind(this);
        this._onSearch = this._onSearch.bind(this);
    }

    componentWillMount() {
        const currency = this.props.match.params.currency
        this.props.GlobalAction.changeCoinCurrency(currency);
    }

    componentDidMount() {
        document.title = 'Mua nhanh';
        window.scrollTo(0, 0);
    }

    loadMoreOffer(url) {
        ApiCaller.get(url)
            .then(res => {
                this.setState({
                    list_offers: res.data,
                    loading: false
                });
            })
            .catch(err => {
                console.error(err)
            })
    }


    _onSearch(e) {
        const {coin_currency} = this.props.global;
        const amount = this.refs.amount.value;
        let total_money = '';
        if (coin_currency == 'btc')
            total_money = Helper.formatMoney(this.props.global.system.price_last_btc * amount, 2);
        else
            total_money = Helper.formatMoney(this.props.global.system.price_last_eth * amount, 2);

        total_money = `≈ ${total_money} USD`;
        this.setState({
            loading: true,
            is_search: true,
            total_money
        });
        setTimeout(() => {

            this.loadMoreOffer(`/offer/sell/${coin_currency}?amount=${amount}`);
        }, 500);
    }

    _formInput() {
        const ic = this.props.global.coin_currency == 'btc' ? '/resources/assets/frontend/img/ic_btc_1x.png' : '/resources/assets/frontend/img/ic_eth_1x.png'
        const {total_money} = this.state;
        const {coin_currency} = this.props.global;

        return (
            <div className="row mt-5">
                <ul className="nav nav-tabs nav-justified tabs-trade">
                    <li className="active"><Link to={"/quick-buy/" + coin_currency}><img src={ic} alt style={{
                        display: 'inline-block',
                        marginTop: '-5px'
                    }}/> Mua ngay</Link></li>
                    <li><Link to={"/quick-sell/" + coin_currency}><img src={ic} alt style={{
                        display: 'inline-block',
                        marginTop: '-5px'
                    }}/> Bán ngay</Link></li>
                </ul>
                <div className="pn-active">
                    <div className="col-12 panel-body-active" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col-lg-10 offset-lg-1">
                                <form action className="form form-inline text-left">
                                    <div className="form-group col-sm-12 col-sm-6 col-lg-6">
                                        <label htmlFor="btc" className="col-sm-12"
                                               style={{alignItems: 'left', justifyContent: 'left'}}>Số
                                            lượng {coin_currency.toUpperCase()} </label>
                                        <div className="col-sm-12">
                                            <input type="number" className="form-control" placeholder={0}
                                                   style={{width: '100%'}} ref="amount" onChange={this._onSearch}/>
                                            <small className="form-text text-left txt-red">{total_money}</small>
                                        </div>
                                    </div>
                                    {/*<div className="form-group col-sm-12 col-sm-6 col-lg-6">*/}
                                    {/*<label htmlFor="wallet" className="col-sm-12" style={{alignItems: 'left', justifyContent: 'left'}}>Địa chỉ {coin_currency.toUpperCase()} của bạn </label>*/}
                                    {/*<div className="col-sm-12">*/}
                                    {/*<input type="text" className="form-control" id="wallet" style={{width: '100%'}} />*/}
                                    {/*<small className="form-text text-left text-danger">&nbsp;</small>*/}
                                    {/*</div>*/}
                                    {/*</div>*/}
                                </form>
                            </div>
                        </div>
                        { this._showAlert() }
                        { this._showList() }
                    </div>
                </div>
            </div>

        )
    }

    _showAlert() {
        const {coin_currency} = this.props.global;
        const {list_offers, loading, is_search} = this.state;

        if (loading == false && is_search) {
            if (list_offers.data.length == 0) {
                return (
                    <div className="row mt-5">
                        <div className="col-12">
                            <div className="alert alert-danger">
                                Không có người nào bán {this.refs.amount.value} {coin_currency.toUpperCase()}.
                                <Link to="/create-offer" className="btn btn-info ml-2">Đăng tin mua</Link>
                            </div>
                        </div>
                    </div>
                )
            }
        }

        return null;
    }

    _showList() {
        const {list_offers, loading} = this.state;

        if (loading) {
            return (
                <div className="container text-center">
                    <div className="row">
                        <div className="col-12 loading">
                            <Spinner fadeIn='none' name='pacman' color="rgb(54, 215, 183)"/>
                        </div>
                    </div>
                </div>
            )
        }

        if (Object.keys(list_offers).length == 0)
            return null;

        return (
            <div className="row mt-5">
                <div className="col-12">
                    <div className="offers">
                        <p className="title">danh sách người <span className="highlight-green">bán</span></p>
                        <p className="sub-description">
                            tỷ giá mua bán tùy thuộc vào người bán đặt ra
                        </p>
                        <table className="table">
                            <tbody style={{boxShadow: 'none'}}>
                            {
                                list_offers.data.map((offer, index) => <OfferItemSell3 key={index} offer={offer}/>)
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        // const {offer, loaded, redirect} = this.state;
        const {wallet} = this.props.profile;
        return (
            <div>

                <HeaderOther/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <Wallet wallet={wallet}/>

                        { this._formInput() }

                    </div>
                    {/*start content*/}
                </div>

                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        global: state.GlobalReducer,
        profile: state.ProfileReducer,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        GlobalAction: bindActionCreators(GlobalAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(QuickBuyPage);