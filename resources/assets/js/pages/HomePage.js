/**
 * Created by dinh on 11/29/17.
 */
import React, {Component} from 'react'
import HeaderHome from '../components/Layout/HeaderHome'
import Footer from '../components/Layout/Footer'
import ListOfferSell from '../components/Offer/ListOfferSell'
import ListOfferBuy from '../components/Offer/ListOfferBuy'


class HomePage extends Component {


    componentDidMount() {
        document.title = 'SieuThiBTC Mua Bitcoin và Ethereum nhanh chóng và an toàn';
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <HeaderHome/>

                <div className="container-fluid main">
                    {/*start content*/}
                    <div className="container">

                        <ListOfferSell/>
                        <div className="mt-4"></div>
                        <ListOfferBuy/>

                    </div>
                    {/*start content*/}
                </div>

                <Footer/>
            </div>
        )
    }
}

export default HomePage;