/**
 * Created by dinh on 11/30/17.
 */
import React from 'react'
import Simplert from 'react-simplert'

export function success(title, msg) {
    console.log(title)
    console.log(msg)
    return showAlert('success', title, msg)
}

export function info(title, msg) {
    return showAlert('info', title, msg)
}

export function warning(title, msg) {
    return showAlert('warning', title, msg)
}

export function error(title, msg) {
    return showAlert('error', title, msg)
}

function showAlert(type, title, msg) {
    return <Simplert
        showSimplert={true}
        type={type}
        title={title}
        message={msg}
    />
}