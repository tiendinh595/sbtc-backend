/**
 * Created by dinh on 12/21/17.
 */
import * as ActionTypes from '../actions/ActionTypes'
const initialState = {
    coin_currency: 'btc',
    system: {}
};

const GlobalReducer = (state=initialState, action) => {
    let new_state = {...state};
    switch (action.type) {
        case ActionTypes.CHANGE_COIN_CURRENCY:
            new_state.coin_currency = action.currency;
            return new_state;

        case ActionTypes.FETCH_GLOBAL_DATA:
            new_state.system = action.data;
            return new_state;

        default:
            return new_state;
    }
};
export default GlobalReducer;