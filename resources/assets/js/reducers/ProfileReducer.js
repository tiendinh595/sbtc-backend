/**
 * Created by dinh on 11/30/17.
 */
import * as ActionTypes from '../actions/ActionTypes'

const initialState = {
    is_logged: false
};

const ProfileReducer = (state = initialState, action) => {
    let newState = {...state};
    switch (action.type) {
        case ActionTypes.FETCH_PROFILE_SUCCESS:
            newState = {is_logged: true, ...action.profile};

            return newState;
        case ActionTypes.FETCH_PROFILE_FAILED:
            newState = initialState;
            return newState;
        default:
            return newState;
    }
};

export default ProfileReducer;
