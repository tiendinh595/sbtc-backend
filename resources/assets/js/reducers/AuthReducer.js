/**
 * Created by dinh on 11/29/17.
 */
import * as ActionTypes from '../actions/ActionTypes'

const initialState = {
    login_success: false,
    register_success: false,
    errors: {}
};

const AuthReducer = (state = initialState, action) => {

    let newState = {...state};
    switch (action.type) {
        case ActionTypes.LOGIN_SUCCESS:
            newState.login_success = true;
            return newState;
        case ActionTypes.LOGIN_FAILED:
            newState.login_success = false;
            newState.errors = action.errors;
            return newState;
        case ActionTypes.LOGIN_INIT:
            newState = {...initialState};
            return newState;

        case ActionTypes.REGISTER_SUCCESS:
            newState = {...initialState};
            newState.register_success = true;
            return newState;

        case ActionTypes.REGISTER_FAILED:
            newState.register_success = false;
            newState.errors = action.errors;
            return newState;
        default:
            return newState;
    }
};

export default AuthReducer;