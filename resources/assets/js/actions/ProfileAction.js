/**
 * Created by dinh on 11/29/17.
 */

import * as ActionTypes from './ActionTypes';
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'

export function fetchProfile() {
    return (dispatch) => {
        if (AuthService.isLogged()) {
            const token = AuthService.getToken();
            return ApiCaller.get(`/token`).then(res => {
                console.log('fetchProfile')
                console.log(res.data)
                if (res.code == 200) {
                    dispatch({type: ActionTypes.FETCH_PROFILE_SUCCESS, profile: res.data});
                }
                else {
                    AuthService.logout();
                    dispatch({errors: res.msg, type: ActionTypes.FETCH_PROFILE_FAILED})
                }
            }).catch(err => {
                console.error('fetchProfile', err)
                // AuthService.logout();
                dispatch({errors: err.msg, type: ActionTypes.FETCH_PROFILE_FAILED})
            })
        } else {
            AuthService.logout();
            dispatch({type: ActionTypes.FETCH_PROFILE_FAILED});
        }

    }
}
