/**
 * Created by dinh on 12/21/17.
 */

import * as ActionTypes from './ActionTypes'
import * as ApiCaller from '../utils/ApiCaller'

export function changeCoinCurrency(currency) {
    if(currency != 'btc' && currency != 'eth')
        currency = 'btc';

    return {currency, type: ActionTypes.CHANGE_COIN_CURRENCY}
}

export function fetchGlobalData() {
    return (dispatch) => {
        ApiCaller.get('/system')
            .then(res=>{
                dispatch({type: ActionTypes.FETCH_GLOBAL_DATA, data: res.data});
            })
            .catch(err=>{

            })
    }
}

export function updatePrice(data) {
    return (dispatch)=> {
        dispatch({type: ActionTypes.FETCH_GLOBAL_DATA, data});
    }
}