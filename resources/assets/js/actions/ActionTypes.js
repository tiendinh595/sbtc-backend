/**
 * Created by dinh on 11/29/17.
 */
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_INIT = 'LOGIN_INIT';

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';


export const FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS';
export const FETCH_PROFILE_FAILED = 'FETCH_PROFILE_FAILED';

export const CHANGE_COIN_CURRENCY = 'CHANGE_COIN_CURRENCY';
export const FETCH_GLOBAL_DATA = 'FETCH_GLOBAL_DATA';