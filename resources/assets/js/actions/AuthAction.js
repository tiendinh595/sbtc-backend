/**
 * Created by dinh on 11/29/17.
 */

import * as ActionTypes from './ActionTypes';
import * as ApiCaller from '../utils/ApiCaller'
import * as AuthService from '../utils/AuthService'
import { showLoading, hideLoading } from 'react-redux-loading-bar'


export function initLogin() {
    return {type: ActionTypes.LOGIN_INIT}
}


export function loginUser(credentials) {
    return (dispatch) => {
        dispatch(showLoading());
        return ApiCaller.post('/login', credentials).then(res => {
            if (res.code == 200) {
                dispatch({type: ActionTypes.LOGIN_SUCCESS});
            }
            else {
                dispatch({errors: res.msg, type: ActionTypes.LOGIN_FAILED})
            }
            setTimeout(()=>dispatch(hideLoading()), 1000);
        }).catch(err => {
            setTimeout(()=>dispatch(hideLoading()), 1000);
            dispatch({errors: {email_login: ['Đăng nhập thất bại']}, type: ActionTypes.LOGIN_FAILED})
        })
    }
}

export function logout() {
    return (dispatch) => {
        if (AuthService.isLogged()) {
            const token = AuthService.getToken();
            return ApiCaller.get('/logout?token='+token).then(res => {
                AuthService.logout();
                dispatch({errors: res.msg, type: ActionTypes.FETCH_PROFILE_FAILED})
            }).catch(err => {
                AuthService.logout();
                dispatch({errors: res.msg, type: ActionTypes.FETCH_PROFILE_FAILED})
            })
        }
    }
}

export function validateToken(email, token) {
    return dispatch => {
        return ApiCaller.post('/register', data).then(res=> {
            if (res.code == 200)
                dispatch({type: ActionTypes.REGISTER_SUCCESS});
            else {
                dispatch({errors: res.msg, type: ActionTypes.REGISTER_FAILED})
            }
        }).catch(err => {
            dispatch({errors: {email: ['Đăng ký thất bại']}, type: ActionTypes.REGISTER_FAILED})
        })
    }
}

