/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');
import ReactDOM from 'react-dom'
import React from 'react'
import Main from './components/Main'

//redux
import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunk from 'redux-thunk'
import {Provider} from 'react-redux'

import AuthReducer from './reducers/AuthReducer'
import ProfileReducer from './reducers/ProfileReducer'
import GlobalReducer from './reducers/GlobalReducer'
import { loadingBarReducer } from 'react-redux-loading-bar'
import { loadingBarMiddleware } from 'react-redux-loading-bar'

const reduces = combineReducers({
    AuthReducer,
    ProfileReducer,
    loadingBar: loadingBarReducer,
    GlobalReducer
});

const middleware = applyMiddleware(thunk);

const store = createStore(
    reduces,
    compose(middleware),
);


ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>,
    document.getElementById('main'));

