/**
 * Created by dinh on 11/28/17.
 */
$(document).ready(function () {
    $('.panel-header').click(function (e) {
        e.preventDefault();

        var parent_el = $(this).parents('.pn');
        $('.pn').removeClass('active');
        $('.panel-body').slideUp('slow');
        $('.direct').html('+');
        parent_el.addClass('active');
        var body_el = parent_el.find('.panel-body');
        var direct_el = parent_el.find('.direct');
        if(body_el.is(':visible')) {
            body_el.slideUp('slow');
            direct_el.html('+')
        }
        else {
            body_el.slideDown('slow');
            direct_el.html('-')
        }

    })
});