@extends('backend.layouts.dashboard')
@section('title', 'Sửa quản trị viên')
@section('content')
    <div class="m-portlet m-portlet--tab">
        <form class="m-form m-form--fit m-form--label-align-right" method="post" autocomplete="off">
            @include('backend.includes.errors')
            @include('backend.includes.alert')
            {{ csrf_field() }}
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>
                        Name
                    </label>
                    <input type="text" name="name" class="form-control m-input m-input--square" placeholder="Display name" value="{{ old('name', $user->name) }}">
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Email address
                    </label>
                    <input type="email" class="form-control m-input m-input--square" placeholder="Enter email" value="{{ $user->email }}" readonly disabled>
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Password
                    </label>
                    <input type="text" name="password" class="form-control m-input m-input--square" placeholder="Password" value="{{ old('password') }}">
                    <p class="m-form__help">Để trống nếu không thay đổi mật khẩu</p>
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Status
                    </label>
                    <select name="active" class="form-control m-bootstrap-select m_selectpicker">
                        <option value="1" {{ old('active', $user->active)==1?'selected':'' }}>Hoạt động</option>
                        <option value="2" {{ old('active', $user->active)==2?'selected':'' }}>Bị khoá</option>
                    </select>
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Permission
                    </label>
                    <label class="pull-right">
                        <a href="" class="text-primary checkall">Check all</a> /
                        <a href="" class="text-primary uncheckall">Uncheck all</a>
                    </label>
                    <table class="table table-bordered">
                        <thead>
                        <th></th>
                        @foreach(\App\Admin::$permission_performs as $key => $perform)
                            <th class="text-center" >{{ $perform }}</th>
                        @endforeach
                        </thead>
                        <tbody id="list_permission">
                        @foreach($permissions as $key => $permission_perfroms)
                            <tr>
                                <?php
                                    $performs = array();
                                    foreach ($permission_perfroms as $perfrom){
                                        $performs[] = explode(' ', $perfrom->name)[0];
                                    }
                                ?>
                                <td width="20%"><b>{{ ucfirst($key) }}</b></td>
                                @foreach(\App\Admin::$permission_performs as $perform => $perform_name)
                                <td style="padding:5px" class="text-center">
                                    @if(in_array($perform, $performs))
                                        <label class="m-checkbox m-checkbox--success">
                                            <input type="checkbox" name="permissions[{{ $key }}][{{ $perform }}]" value="1" {{ old('permissions.'.$key.'.'.$perform, isset($user_has_permissions[$key])&&in_array($perform, $user_has_permissions[$key])?1:0)==1?'checked':'' }} >
                                            <span></span>
                                        </label>
                                    @else
                                        <label>--</label>
                                    @endif
                                </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">
                        Lưu
                    </button>
                    <a href="{{ route('administrator') }}" class="btn btn-secondary">
                        Huỷ
                    </a>
                </div>
            </div>
        </form>
    </div>
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.checkall').click(function (e) {
               e.preventDefault();
               $('#list_permission input[type="checkbox"]').attr('checked', true);
            });
            $('.uncheckall').click(function (e) {
               e.preventDefault();
               $('#list_permission input[type="checkbox"]').removeAttr('checked');
            });
        });
    </script>
@stop