@extends('backend.layouts.dashboard')
@section('title', 'Quản trị viên')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <form action="" method="get" autocomplete="off">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-5">
                                    <div class="m-form__group m-form__group--inline">
                                        <div class="m-form__label">
                                            <label>
                                                Status:
                                            </label>
                                        </div>
                                        <div class="m-form__control">
                                            <select name="active" class="form-control m-bootstrap-select m_selectpicker">
                                                <option value="">All</option>
                                                <option value="1" {{ app('request')->input('active')==1?'selected':'' }}>Hoạt động</option>
                                                <option value="2" {{ app('request')->input('active')==2?'selected':'' }}>Bị khoá</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="m-form__control">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." name="key" value="{{ app('request')->input('key') }}">
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="m-input-icon m-input-icon--left">
                                        <button type="submit" class="btn btn-primary">
                                            Search
                                        </button>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{ route('add-administrator') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la la-user-plus"></i>
                                <span>
                                    Thêm
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered m-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>
                            @if($item->active==1)
                                <span class="m-badge  m-badge--success m-badge--wide">Hoạt động</span>
                            @else
                                <span class="m-badge  m-badge--danger m-badge--wide">Bị khoá</span>
                            @endif
                        </td>
                        <td>
                            @if(auth()->guard('admin')->user()->can('edit administrator'))
                                <a href="{{ route('edit-administrator', [$item->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
                                    <i class="la la-edit"></i>
                                </a>
                            @endif
                            @if(auth()->guard('admin')->user()->can('delete administrator'))
                                <a href="#" data-id="{{ $item->id }}" data-name="{{ $item->name }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete-item">
                                    <i class="la la-remove"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $list->links() }}
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
        $('.delete-item').click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $parent = $(this).parents('tr');
            $.confirm.show({
                'title': 'Xoá quản trị viên',
                'message': 'Xác nhận xoá vĩnh viễn quản trị viên '+$(this).data('name')+'?',
                'yesText': 'Đồng ý',
                'noText': 'Huỷ',
                'type': 'warning',
                'yes': function (){
                    $.get('{{ route('delete-administrator') }}?id=' + $id).done(function (data) {
                        if (data['status'] === 'success') {
                            toastr.success(data['msg'], data['title']);
                            $parent.remove();
                        } else {
                            toastr.error(data['msg'], data['title']);
                        }
                    });
                }
            });

        });
    </script>
@stop