@extends('backend.layouts.dashboard')
@section('title', 'Trang cá nhân')
@section('content')
    <div class="m-portlet m-portlet--tab">
        <form class="m-form m-form--fit m-form--label-align-right" method="post" autocomplete="off">
            @include('backend.includes.errors')
            @include('backend.includes.alert')
            {{ csrf_field() }}
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>
                        Name
                    </label>
                    <input type="text" name="name" class="form-control m-input m-input--square" placeholder="Display name" value="{{ old('name', auth()->guard('admin')->user()->name) }}">
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Email address
                    </label>
                    <input type="email" class="form-control m-input m-input--square" placeholder="Enter email" value="{{ auth()->guard('admin')->user()->email }}" readonly disabled>
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Password
                    </label>
                    <input type="text" name="password" class="form-control m-input m-input--square" placeholder="Password">
                    <p class="m-form__help">Để trống nếu không thay đổi mật khẩu</p>
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Confirm password
                    </label>
                    <input type="text" name="cfg_password" class="form-control m-input m-input--square" placeholder="Confirm password">
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">
                        Cập nhật
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@stop