@extends('backend.layouts.dashboard')
@section('title', 'Thêm quyền')
@section('content')
    <div class="m-portlet m-portlet--tab">
        <form class="m-form m-form--fit m-form--label-align-right" action="" method="post" autocomplete="off">
            @include('backend.includes.errors')
            @include('backend.includes.alert')
            {{ csrf_field() }}
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>
                        Name
                    </label>
                    <input type="text" name="name" class="form-control m-input m-input--square" placeholder="Permission name" value="{{ old('name') }}">
                </div>
                <div class="form-group m-form__group">
                    <label>
                        Chức năng
                    </label>
                    <select name="performs[]" class="form-control m-bootstrap-select m_selectpicker" multiple>
                        @foreach(\App\Admin::$permission_performs as $key => $perform)
                            <option value="{{ $key }}" selected>{{ $perform }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">
                        Thêm
                    </button>
                    <a href="{{ route('permission') }}" class="btn btn-secondary">
                        Huỷ
                    </a>
                </div>
            </div>
        </form>
    </div>
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@stop