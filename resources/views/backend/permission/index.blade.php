@extends('backend.layouts.dashboard')
@section('title', 'Danh sách quyền')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                <div class="text-right">
                    <a href="{{ route('add-permission') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la la-plus-circle"></i>
                                <span>
                                    Thêm
                                </span>
                            </span>
                    </a>
                </div>
            </div>
            <div class="">
                <table class="table table-bordered m-table">
                    <thead>
                        <tr>
                            <th>Group</th>
                            <th>Performs</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($list as  $key => $items)
                    <tr>
                        <td>{{ ucfirst($key)  }}</td>
                        <td>
                            @foreach($items as $item)
                            {{ \App\Admin::$permission_performs[explode(' ', $item->name)[0]] }}{{ $loop->last?'':', ' }}
                            @endforeach
                        </td>
                        <td>
                            @if(auth()->guard('admin')->user()->can('edit permission'))
                            <a href="{{ route('edit-permission', [$key]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-edit"></i>
                            </a>
                            @endif
                            @if(auth()->guard('admin')->user()->can('delete permission'))
                            <a href="#" data-id="{{ $key }}" data-name="{{ ucfirst($key) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete-item">
                                <i class="la la-remove"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
        $('.delete-item').click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $parent = $(this).parents('tr');
            $.confirm.show({
                'title': 'Xoá phân quyền',
                'message': 'Xác nhận xoá vĩnh viễn phân quyền '+$(this).data('name')+'?',
                'yesText': 'Đồng ý',
                'noText': 'Huỷ',
                'type': 'warning',
                'yes': function (){
                    $.get('{{ route('delete-permission') }}?id=' + $id).done(function (data) {
                        if (data['status'] === 'success') {
                            toastr.success(data['msg'], data['title']);
                            $parent.remove();
                        } else {
                            toastr.error(data['msg'], data['title']);
                        }
                    });
                }
            });

        });
    </script>
@stop