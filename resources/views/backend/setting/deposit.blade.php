@extends('backend.layouts.dashboard')
@section('title', 'Phí nạp tiền')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        @include('backend.includes.errors')
        @include('backend.includes.alert')
        <div class="m-portlet__body">
                <form action="" method="POST" role="form">

                    <div class="row">
                        @if(Session::has('status'))
                            <div class="alert alert-{{ Session::get('status')['class'] }}">{{ Session::get('status')['msg'] }}</div>
                        @endif
                        {{ csrf_field() }}
                        <div class="col-sm-12 col-md-6" id="list_card">
                            <h4>Thẻ cào <a class="btn_add_card_cfg" href="#">[Thêm mới]</a></h4>
                            @foreach($settings['deposit']['card'] as $card => $value)
                                <div class="form-group">
                                    <label for="">Thẻ {{ ucfirst($card) }}</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="deposit[card][{{ $card }}]"
                                               value="{{ $value }}">
                                        <div class="input-group-addon delete">
                                            <i class="fa fa-times-circle-o"></i>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <div class="col-sm-12 col-md-6" id="list_sms">
                            <h4>SMS <a class="btn_add_sms_cfg" href="#">[Thêm mới]</a></h4>
                            @foreach($settings['deposit']['sms'] as $sms => $value)
                                <div class="form-group">
                                    <label for="">Đầu số {{ $sms }}</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="deposit[sms][{{ $sms }}]"
                                               value="{{ $value }}">
                                        <div class="input-group-addon delete">
                                            <i class="fa fa-times-circle-o"></i>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                    </div>
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary">Lưu cài đặt</button>
                    </div>
                </form>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/base/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/custom/alertify/alertify.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/alertify/alertify.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
            $(document).ready(function () {
                $(document).on('click', '.delete', function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    alertify.logPosition("bottom right");
                    alertify.confirm("Xác nhận xóa cài đặt này?", function () {
                        $this.parents('.form-group').remove();
                        alertify.success("Xóa thành công");
                    }, function () {

                    });

                });

                $('.btn_add_card_cfg').click(function (e) {
                    e.preventDefault();
                    var html = '<div class="form-group"> <div class="input-group"> <input type="text" class="form-control" name="new_card[key][]" placeholder="Loại thẻ(viết liền không có khoảng cách)"> <input type="text" class="form-control" name="new_card[value][]" placeholder="giá trị"> <div class="input-group-addon delete"> <i class="fa fa-times-circle-o"></i> </div></div></div>';
                    $('#list_card').append(html);
                });

                $('.btn_add_sms_cfg').click(function (e) {
                    e.preventDefault();
                    var html = '<div class="form-group"> <div class="input-group"> <input type="text" class="form-control" name="new_sms[key][]" placeholder="Đầu số"> <input type="text" class="form-control" name="new_sms[value][]" placeholder="giá trị"> <div class="input-group-addon delete"> <i class="fa fa-times-circle-o"></i> </div></div></div>';
                    $('#list_sms').append(html);
                })
            });
    </script>
@stop