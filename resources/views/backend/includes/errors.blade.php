@if($errors->all())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ $error }}
        </div>
    @endforeach
@endif