<?php
    $items = [
        [
            'icon' => 'flaticon-line-graph',
            'url' => route('dashboard'),
            'title' => 'Dashboard',
            'permission' => '',
            'childs' => '',
        ],
        [
            'icon' => 'flaticon-users',
            'url' => route('user'),
            'title' => 'Quản lý thành viên',
            'permission' => '',
            'childs' => '',
        ],
//        [
//            'icon' => 'flaticon-security',
//            'url' => '',
//            'title' => 'Quản lý thành viên',
//            'permission' => '',
//            'childs' => [
//                [
//                    'url' => route('user'),
//                    'title' => 'Quản trị viên',
//                    'permission' => 'list administrator',
//                ],
//                [
//                    'url' => route('permission'),
//                    'title' => 'Danh sách quyền',
//                    'permission' => 'list permission',
//                ],
//            ]
//        ],
        [
            'icon' => 'flaticon-diagram',
            'url' => '',
            'title' => 'Lịch sử dòng tiền',
            'permission' => '',
            'childs' => [
                [
                    'url' => route('deposit'),
                    'title' => 'Lịch sử nạp tiền',
                    'permission' => 'list administrator',
                ],
                [
                    'url' => route('invoice'),
                    'title' => 'lịch sử rút tiền',
                    'permission' => 'list permission',
                ],
                [
                    'url' => route('history'),
                    'title' => 'lịch sử thay đổi số dư',
                    'permission' => 'list permission',
                ],
            ]
        ],
        [
            'icon' => 'flaticon-cogwheel-1',
            'url' => '',
            'title' => 'Cài đặt',
            'permission' => '',
            'childs' => [
                [
                    'url' => route('setting-deposit'),
                    'title' => 'Phí nạp tiền',
                    'permission' => 'list administrator',
                ],
                [
                    'url' => route('setting-discount'),
                    'title' => 'chiết khấu toup, thẻ cào',
                    'permission' => 'list permission',
                ],
            ]
        ],
        [
            'icon' => 'flaticon-security',
            'url' => '',
            'title' => 'Quản trị',
            'permission' => '',
            'childs' => [
                [
                    'url' => route('administrator'),
                    'title' => 'Quản trị viên',
                    'permission' => 'list administrator',
                ],
                [
                    'url' => route('permission'),
                    'title' => 'Danh sách quyền',
                    'permission' => 'list permission',
                ],
            ]
        ],
//        [
//            'icon' => '',
//            'url' => '',
//            'title' => '',
//            'permission' => '',
//            'childs' => [
//                [
//                    'url' => '',
//                    'title' => '',
//                    'permission' => '',
//                ],
//            ]
//        ],
    ];
    function checkRoute($list){
        foreach ($list as $item){
            if($item['url'] == url()->current()) return true;
        }
        return false;
    }
?>
<div
        id="m_ver_menu"
        class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
        data-menu-vertical="true"
        data-menu-scrollable="false" data-menu-dropdown-timeout="500"
>
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        @foreach($items as $item)
            @if(!$item['childs'])
                @if(!empty($item['permission'])&&!auth()->guard('admin')->user()->can($item['permission']))
                    @continue
                @endif
                <li class="m-menu__item {{ $item['url']==url()->current()?'m-menu__item--active':'' }}" aria-haspopup="true" >
                    <a href="{{ $item['url'] }}" class="m-menu__link ">
                        <i class="m-menu__link-icon {{ $item['icon'] }}"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    {{ $item['title'] }}
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
            @else
                <li class="m-menu__item m-menu__item--submenu {{ checkRoute($item['childs'])?'m-menu__item--open m-menu__item--expanded':'' }}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon {{ $item['icon'] }}"></i>
                        <span class="m-menu__link-text">
                             {{ $item['title'] }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item m-menu__item--parent" aria-haspopup="true" >
                                <a href="#" class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        {{ $item['title'] }}
                                    </span>
                                </a>
                            </li>
                            @foreach($item['childs'] as $child)
                                @if(!empty($child['permission'])&&!auth()->guard('admin')->user()->can($child['permission']))
                                    @continue
                                @endif
                                <li class="m-menu__item {{ $child['url']==url()->current()?'m-menu__item--active':'' }}" aria-haspopup="true" >
                                    <a href="{{ $child['url'] }}" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">
                                            {{ $child['title'] }}
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endif
        @endforeach
    </ul>
</div>