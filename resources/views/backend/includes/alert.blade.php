@if(Session::has('alert'))
    <div class="alert alert-{{ session('alert')['class'] }} alert-dismissible fade show   m-alert m-alert--air" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session('alert')['msg'] }}
    </div>
@endif