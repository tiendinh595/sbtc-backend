@extends('backend.layouts.dashboard')
@section('title', 'Lịch sử thay đổi số dư')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <form action="">

                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="number" class="form-control m-input m-input--solid"
                                               placeholder="user id." value="{{ app('request')->input('user_id') }}" name="user_id" id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <!--end: Datatable -->
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded">
                <table class="table table-bordered m-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>user</th>
                        <th>type</th>
                        <th>pre amount</th>
                        <th>after amount</th>
                        <th>amount</th>
                        <th>created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $key => $item)
                        <tr>
                            <td>{{ $item->id  }}</td>
                            <td><a href="{{ route('edit-user', [$item->user->id]) }}">{{ $item->user->name }}</a></td>
                            <td>{{ \App\History::$history_type[$item->transaction_type] }}</td>
                            <td>{{ number_format($item->pre_amount, 0, ',', '.') }}</td>
                            <td>{{ number_format($item->after_amount, 0, ',', '.') }}</td>
                            <td>{{ number_format($item->amount, 0, ',', '.') }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                    <ul class="m-datatable__pager-nav">
                        {{ $list->links() }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/base/bootstrap.min.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}"
            type="text/javascript"></script>
    <script type="application/javascript">
        $('.delete-item').click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $parent = $(this).parents('tr');
            $.confirm.show({
                'title': 'Xoá phân quyền',
                'message': 'Xác nhận xoá vĩnh viễn phân quyền ' + $(this).data('name') + '?',
                'yesText': 'Đồng ý',
                'noText': 'Huỷ',
                'type': 'warning',
                'yes': function () {
                    $.get('{{ route('delete-permission') }}?id=' + $id).done(function (data) {
                        if (data['status'] === 'success') {
                            toastr.success(data['msg'], data['title']);
                            $parent.remove();
                        } else {
                            toastr.error(data['msg'], data['title']);
                        }
                    });
                }
            });

        });
    </script>
@stop