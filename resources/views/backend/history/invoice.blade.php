@extends('backend.layouts.dashboard')
@section('title', 'Lịch sử rút tiền')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">

            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded">
                <table class="table table-bordered m-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>user</th>
                        <th>type</th>
                        <th>amount</th>
                        <th>status</th>
                        <th>created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $key => $item)
                        <tr>
                            <td>{{ $item->id  }}</td>
                            <td><a href="{{ route('user', [$item->user->id]) }}">{{ $item->user->name }}</a></td>
                            <td>{{ \App\Invoice::$invoice_type[$item->type] }}</td>
                            <td>{{ number_format($item->amount, 0, ',', '.') }}</td>
                            <td>{!! $item->status == 1 ? '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">success</span>' : '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">error</span>' !!}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                    <ul class="m-datatable__pager-nav">
                        {{ $list->links() }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/base/bootstrap.min.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}"
            type="text/javascript"></script>
    <script type="application/javascript">
        $('.delete-item').click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $parent = $(this).parents('tr');
            $.confirm.show({
                'title': 'Xoá phân quyền',
                'message': 'Xác nhận xoá vĩnh viễn phân quyền ' + $(this).data('name') + '?',
                'yesText': 'Đồng ý',
                'noText': 'Huỷ',
                'type': 'warning',
                'yes': function () {
                    $.get('{{ route('delete-permission') }}?id=' + $id).done(function (data) {
                        if (data['status'] === 'success') {
                            toastr.success(data['msg'], data['title']);
                            $parent.remove();
                        } else {
                            toastr.error(data['msg'], data['title']);
                        }
                    });
                }
            });

        });
    </script>
@stop