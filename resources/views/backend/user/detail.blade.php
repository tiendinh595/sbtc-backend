@extends('backend.layouts.dashboard')
@section('title', 'Phí nạp tiền')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Thông tin</h3></div>
                        <div class="panel-body table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>#ID</th>
                                    <td>{{ $user->id }}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Username</th>
                                    <td>{{ $user->username }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Total money</th>
                                    <td>{{ number_format($user->total_money,0, ',', '.') }} vnd</td>
                                </tr>
                                <tr>
                                    <th>Curent Money</th>
                                    <td>{{ number_format($user->curent_money,0, ',', '.') }} vnd</td>
                                </tr>
                                <tr>
                                    <th>Freeze Money</th>
                                    <td>{{ number_format($user->freeze_money,0, ',', '.') }} vnd</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{!!  $user->status == 1 ? '<span class="label label-success">active</span>' : '<span class="label label-danger">deactive</span>'  !!}</td>
                                </tr>
                                <tr>
                                    <th>Created</th>
                                    <td>{{ date('d/m/Y', strtotime($user->created_at)) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer text-center">
                            @if($user->status == 1)
                                <a href="/inside/user/{{ $user->id }}?ac=block" class="btn btn-sm btn-primary">Khóa thành
                                    viên</a>
                            @else
                                <a href="/inside/user/{{ $user->id }}?ac=active" class="btn btn-sm btn-primary">Mở thành
                                    viên</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-7">

                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Thay đổi số dư</h3></div>
                        <div class="panel-body table-responsive">
                            @if(isset($errors) && count($errors->all())>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            @endif
                            @if(Session::has('status'))
                                <div class="alert alert-{{ Session::get('status')['class'] }}">{{ Session::get('status')['msg'] }}</div>
                            @endif
                            <form action="" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="transaction_type" value="type_deposit">
                                <div class="form-group">
                                    <label for="">Số tiền</label>
                                    <input type="text" class="form-control" name="amount" id="" placeholder="Số tiền"
                                           value="{{ old('amount') }}">
                                </div>

                                <div class="form-group">
                                    <label for="">Hình thức</label>
                                    <select name="method" class="form-control">
                                        <option value="type_deposit">Cộng tiền vào</option>
                                        <option value="type_invoice">Trừ tiền</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Loại</label>
                                    <select name="type" class="form-control">
                                        @foreach($constants['type_deposit'] as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Chi tiết</label>
                                    <textarea class="form-control" name="detail"
                                              placeholder="Chi tiết giao dịch">{{ old('detail') }}</textarea>
                                </div>

                                <button type="submit" class="btn btn-primary center-block">Thay đổi</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Lịch sử thay đổi số dư</h3></div>
                        <div class="panel-body table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Transaction id</th>
                                    <th>Transaction Type</th>
                                    <th>Pre amount</th>
                                    <th>After amount</th>
                                    <th>Amount</th>
                                    <th>Note</th>
                                    <th>Created</th>
                                </tr>
                                @foreach($histories as $history)
                                    <tr>
                                        <td>{{ $history->id }}</td>
                                        <td>{{ $history->transaction_id }}</td>
                                        <td>{{ config('constant')['history_type'][$history->transaction_type] }}</td>
                                        <td>{{ number_format($history->pre_amount,0, ',', '.') }} vnd</td>
                                        <td>{{ number_format($history->after_amount,0, ',', '.') }} vnd</td>
                                        <td>{{ number_format($history->amount,0, ',', '.') }} vnd</td>
                                        <td>{{ $history->note }}</td>
                                        <td>{{ $history->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="/inside/history?user_id={{ $user->id }}" class="btn btn-sm btn-primary">Xem tất cả</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Lịch sử nạp tiền</h3></div>
                        <div class="panel-body table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Deposit type</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                </tr>
                                @foreach($deposits as $deposit)
                                    <tr data-toggle="tooltip" class="item" title="{!! $deposit->detail !!}"
                                        style="cursor: pointer">
                                        <td>{{ $deposit->id }}</td>
                                        <td>{{ config('constant.type_deposit')[$deposit->type] }}</td>
                                        <td>{{ number_format($deposit->amount,0, ',', '.') }} vnd</td>
                                        <td>{!!  $deposit->status == 1 ? '<span class="label label-success">success</span>' : '<span class="label label-danger">error</span>'  !!}</td>
                                        <td>{{ $deposit->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="/inside/deposit?user_id={{ $user->id }}" class="btn btn-sm btn-primary">Xem tất cả</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title">Lịch sử rút tiền</h3></div>
                        <div class="panel-body table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Invoice type</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                </tr>
                                @foreach($invoices as $invoice)
                                    <tr data-toggle="tooltip" class="item" title="{!! $invoice->detail !!}"
                                        style="cursor: pointer">
                                        <td>{{ $invoice->id }}</td>
                                        <td>{{ config('constant.type_invoice')[$invoice->type] }}</td>
                                        <td>{{ number_format($invoice->amount,0, ',', '.') }} vnd</td>
                                        <td>{!!  $invoice->status == 1 ? '<span class="label label-success">success</span>' : '<span class="label label-danger">error</span>'  !!}</td>
                                        <td>{{ $invoice->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="/inside/invoice?user_id={{ $user->id }}" class="btn btn-sm btn-primary">Xem tất cả</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/base/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/custom/alertify/alertify.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/alertify/alertify.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
        $(document).ready(function () {

            var type = JSON.parse('{!!  json_encode($constants)  !!}');

            $('[data-toggle="tooltip"]').tooltip({
                html: true
            });

            $('.item').click(function () {
                var msg = $(this).attr('data-original-title');
                alertify.alert(msg);
            });

            $('select[name="method"]').change(function () {
                var val = $(this).val();
                $('input[name="transaction_type"]').val(val);
                $('select[name="type"]').html('');
                for(var item in type[val]) {
                    $('select[name="type"]').append('<option value="'+item+'">'+type[val][item]+'</option>');
                }
            });

        });
    </script>
@stop