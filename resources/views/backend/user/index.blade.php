@extends('backend.layouts.dashboard')
@section('title', 'Danh sách thành viên')
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">

            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded">
                <table class="table table-bordered m-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>username</th>
                        <th>total money</th>
                        <th>current money</th>
                        <th>status</th>
                        <th width="10%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>{{ $item->id  }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->username }}</td>
                            <td>{{ number_format($item->total_money, 0, ',', '.') }}</td>
                            <td>{{ number_format($item->current_money, 0, ',', '.') }}</td>
                            <td>{!! $item->status == 1 ? '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">active</span>' : '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">banned</span>' !!}</td>
                            <td>
                                <a href="{{ route('edit-user', [$item->id]) }}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="#" data-id="{{ $item->id }}" data-name="{{ $item->username }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete-item">
                                    <i class="la la-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                    <ul class="m-datatable__pager-nav">
                        {{ $list->links() }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.css') }}">
@stop
@section('js')
    <script src="{{ url('/resources/assets/backend/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ url('/resources/assets/backend/vendors/custom/HPCF/H-confirm-alert.js') }}" type="text/javascript"></script>
    <script type="application/javascript">
        $('.delete-item').click(function (e) {
            e.preventDefault();
            var $id = $(this).data('id');
            var $parent = $(this).parents('tr');
            $.confirm.show({
                'title': 'Xoá quản trị viên',
                'message': 'Xác nhận xoá vĩnh viễn quản trị viên '+$(this).data('name')+'?',
                'yesText': 'Đồng ý',
                'noText': 'Huỷ',
                'type': 'warning',
                'yes': function (){
                    $.get('{{ route('delete-user') }}?id=' + $id).done(function (data) {
                        if (data['status'] === 'success') {
                            toastr.success(data['msg'], data['title']);
                            $parent.remove();
                        } else {
                            toastr.error(data['msg'], data['title']);
                        }
                    });
                }
            });

        });
    </script>
@stop